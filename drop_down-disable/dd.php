<?php

	require 'config.php';	//////// End of connecting to database ////////

	@$builder=$_GET['builder']; // Use this line or below line if register_global is off
	//@$subdivision=$HTTP_GET_VARS['subdivision']; // Use this line or above line if register_global is off
	/*
	If register_global is off in your server then after reloading of the page to get the value of builder from query string we have to take special care.
	*/
?>

<!doctype html public "-//w3c//dtd html 3.2//en">

<html>

	<head>
		<title>Multiple drop down list box</title>

		<script language=JavaScript>

			function reload(form){
				var val = form.builder.options[form.builder.options.selectedIndex].value;
				self.location = 'dd.php?builder=' + val ;
			}

			function disableselect(){
				<?php
					if(isset($builder) and strlen($builder) > 0){
						echo "document.f1.subdivision.disabled = false;";
					}else{
						echo "document.f1.subdivision.disabled = true;";
					}
				?>
			}

		</script>
	</head>

	<body onload=disableselect();>

	<?php

		$builderQuery = "SELECT * FROM tbl_buildercustomer";  // Getting the data from Mysql table for first list box

		if(isset($builder) and strlen($builder) > 0){  //check if builder is selected
			$subdivisionQuery = "SELECT subdivision_id,subdivision_name FROM tbl_subdivision WHERE tbl_buildercustomer_builder_id=$builder ORDER BY subdivision_name";
		}else{
			$subdivisionQuery = "SELECT subdivision_id,subdivision_name FROM tbl_subdivision ORDER BY subdivision_name ASC";
		} 

		echo "<form method=post name=f1 action='dd-check.php'>"; // form processing page
		//echo "<form method=post name=f1 action=''>"; // location  Example: action=dd-check.php
				// Builder dropdown selection - START
				echo "<select name='builder' onchange=\"reload(this.form)\"><option value=''>Select one</option>"; // changing builder reloads the form, nice!
					foreach ($dbo->query($builderQuery) as $builderItem) {
						if($builderItem['builder_id']==@$builder){
							echo "<option selected value='$builderItem[builder_id]'>$builderItem[builder_name]</option>"."<BR>";
						}else{
							echo  "<option value='$builderItem[builder_id]'>$builderItem[builder_name]</option>";}
						}
				echo "</select>";
				// Builder dropdown selection - END

				// Subdivision dropdown selection - START
				echo "<select name='subdivision'><option value=''>Select one</option>";
					foreach ($dbo->query($subdivisionQuery) as $subdivisionItem) {
						echo  "<option value='$subdivisionItem[subdivision_id]'>$subdivisionItem[subdivision_name]</option>";
					}
				echo "</select>";
				// Subdivision dropdown selection - END
				echo "Plan: <input type='text' name='planSearch' />";
			echo "<input type=submit value=Submit>"; // will send form data to action=dd-check.php page
		echo "</form>";
		
	?>

	<br><br>
		<a href=dd.php>Reset and start again</a> <!-- href'ing THIS page causes a reload -->
	<br><br>
		<center><a href='' rel="nofollow">Look a CENTERED Anchor tag!</a></center> 
	</body>

</html>
