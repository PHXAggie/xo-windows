$(document).ready(function(){
    $('#search').show();
        function filterList(selector, query, addon = 'FALSE'){
            if(addon=='FALSE'){
                    //$('div.jsError').append($('#702').text().search(query));
                    //$('div.jsError').append($('div.jsError').html(selector));
                    $(selector).each(function(){
                            if ($(this).text().search(query) < 0){
                            //if ($(this).text() == $(this).search(query)){
                                $(this).hide().removeClass('visible');
                            } else {
                                $(this).show().addClass('visible');
                            }
                    });
            } else {
                    $(selector).each(function(){
                    if ($(this).text().search(query) > 0){
                            $(this).show().addClass('visible');
                        }
                    });
            }
        }

        function filterSearch(selector, query){
            searchQuery = $.trim(query); //trim white space from search
            searchQuery = searchQuery.replace(/ /gi, '|'); //add OR for regex search

            $(selector).each(function(){
                ($(this).text().search(new RegExp(searchQuery, "i")) < 0) ? $(this).hide().removeClass('visible') : $(this).show().addClass('visible');
            });
        }

// Filter list based on Radio option chosen - START
        $("input[name='listFilter']").on('change',function(){
            var radioValue = $("input[name='listFilter']:checked").val();
            if(radioValue){
                switch (radioValue) {
                    case 'All':
                        $('tbody tr').show().addClass('visible');
                        break;
                    case 'Active':
                        filterList($('tbody tr').text($('tbody tr').data($searchBy)),'700');
                        break;
                    case 'Bidding':                      
                        filterList($('tbody tr').text($('tbody tr').data($searchBy)),'702');
                        filterList($('tbody tr').text($('tbody tr').data($searchBy)),'703','TRUE');
                        filterList($('tbody tr').text($('tbody tr').data($searchBy)),'704','TRUE'); 
                        break;
                    case 'Inactive':
                        filterList($('tbody tr').text($('tbody tr').data($searchBy)),'701');
                        filterList($('tbody tr').text($('tbody tr').data($searchBy)),'705','TRUE');
                        break;
                    default:
                }
            }
        });
// Filter list based on Radio option chosen - END

// search list probably with filter applied? NO it doesn't should set to ALL when used - START
        $('#search').keyup(function(event) {

            // set to All listing by removing current radio chosen and 'checking' All radio button
            var selectedRadioBeforeSearch = $("input[name='listFilter']:checked").val();
            $(selectedRadioBeforeSearch).removeAttr('checked');

            $("input[name='listFilter'][value='All']").prop('checked', true);
            $('#search').show();

            //if ESC is pressed or nothing is entered
            if(event.keyCode==27 || $(this).val()==''){
                //if ESC is pressed we want to clear search box
                $(this).val('');

                //we want each row to be visible because if nothing
                //is entered then all rows are matched?
                $('tbody tr').removeClass('visible').show().addClass('visible');
            } else {
                filterSearch('tbody tr', $(this).val());
            }
        });
// search list probably with filter applied? NO it doesn't should set to ALL when used - END
});