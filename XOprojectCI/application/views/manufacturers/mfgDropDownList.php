<div class="form-group row">
	<label for="mfg" class="col-2 text-right">Manufacturer:</label>
	<div class="col-5">
		<select class="form-control" id="manufacturer" name="manufacturerIdSelected">
			<option value="">SELECT MANUFACTURER</option>
			<?php
			if(!empty($list_Mfg)){
				foreach($list_Mfg as $rowItem){
					echo '<option value="'.$rowItem['manufacturer_id'].'">'.$rowItem['manufacturer_name'].'</option>';
				}
			}else{
				echo '<option value="">NO MANUFACTURERS FOUND</option>';
			}
			?>
		</select>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('#manufacturer').on('change',function(){
			var mfgID = $(this).val();
			var mfgSelected = $('[name="manufacturerIdSelected"] option:selected');
			alert(mfgID);
		});



	});
</script>