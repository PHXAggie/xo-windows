<div class="form-group row">
	<label for="seriesList" class="col-2 text-right">Series:</label>
	<div class="col-5">
		<select class="form-control" id="series" name="seriesIdSelected">
			<option value="">SELECT Series</option>
			<?php
			if(!empty($list_Series)){
				foreach($list_Series as $rowItem){
					echo '<option value="'.$rowItem['idtbl_mfg_series'].'">'.$rowItem['series'].'</option>';
				}
			}else{
				echo '<option value="">NO SERIES FOUND</option>';
			}
			?>
		</select>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('#series').on('change',function(){
			var seriesID = $(this).val();
			var seriesSelected = $('[name="seriesIdSelected"] option:selected');
			alert(seriesID);
		});

		if(seriesID){
            $.ajax({
                type:'POST',
                url:'<?php echo site_url();?>/manufacturers/getMfgSeries', //post to Controllers->Bids->getBuilderSubdivisions
                data:'idtbl_mfg_series='+seriesID, //builder_id=146 for example
                success:function(data){
                    $('#series').html('<option value="">SELECT SERIES</option>');
                    var dataObj = jQuery.parseJSON(data);                        
                    if(dataObj){
                        if(Object.keys(dataObj).length == 1){
                            $(dataObj).each(function(){
                                var option = $('<option />');
                                option.attr('value', this.idtbl_mfg_series).text(this.series);
                                option.attr('selected', 'true');
                                $('#series').append(option);
                            });
                        }else{
                            $(dataObj).each(function(){
                                var option = $('<option />');
                                option.attr('value', this.idtbl_mfg_series).text(this.series);
                                $('#series').append(option);
                            });
                        }
                    }else{
                        $('#series').html('<option value="">NO SERIES FOUND</option>');
                    }
                }
            }); 
        }else{
            $('#series').html('<option value="">Select Manufacturer first</option>');
        }


	});
</script>