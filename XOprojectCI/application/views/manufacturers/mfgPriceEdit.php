<div class="container">
    <div class="text-center">
        <div class="form-group row"></div>
        <div class="form-group row"></div>
        <h1 id="page_title1"><?php echo $title; ?></h1>
        <div class="jsError"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-2">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
				        <?php foreach($content as $contentName => $contents){
				        		if($contents['menutag'] == 'edit' ? $activeLink = ' active' : $activeLink = '');
				        		if($contents['menutag'] == 'edit' ? $selectLink = 'true' : $selectLink = 'false');
				        		if(isset($contents['auth_view']) ? $contents_auth = $contents['auth_view'] : $contents_auth = '');
				        		if(isset($contents['auth_edit']) ? $contents_edit = $contents['auth_edit'] : $contents_edit = '');

				                echo '<a class="nav-link text-left' . $activeLink . '" id="v-pills-'. $contents['menutag'] . '-tab" data-toggle="pill" href="#v-pills-'. $contents['menutag'] .'" role="tab" aria-controls="v-pill-'. $contents['menutag'] .'" aria-selected="' . $selectLink . '" ' . $contents_auth . ' ' . $contents_edit . '>'. $contents['menuLabel'] .'</a>'; // ' . $contents['auth_edit'] . '
				        }; ?>
                    </div>                  
                </div>
	            <div class="col-10 border-left border-info">
	                <div class="tab-content" id="v-pills-tabContent">

	                	<?php foreach($content as $section => $sectionItems){
	                		if($sectionItems['menutag'] == 'edit' ? $activeContent = ' active' : $activeContent = '');
                            echo '<div class="tab-pane fade show' . $activeContent . '" id="v-pills-'. $sectionItems['menutag'] .'" role="tabpanel" aria-labelledby="v-pills-'. $sectionItems['menutag'] .'-tab">';
                                echo '<div class="form-group row">';
                                    echo '<div class="col-12">';

                                        //echo $sectionItems['sectionTitle'];
                                        echo $sectionItems['sectionContent'];

                                        if(isset($sectionItems['sectionContent2'])){
                                        	echo $sectionItems['sectionContent2'];
                                        }

                                    echo '</div>';
                                echo '</div>';
                            echo '</div>';
                        }; ?>

                	</div><!-- end all tab contents -->                     
            	</div><!-- end border line -->
            </div><!-- end row for all content -->
        </div><!-- end container-fluid -->
    </div><!-- end text center -->
</div><!-- end container -->

<script type="text/javascript">
    $(document).ready(function(){



    });
</script>