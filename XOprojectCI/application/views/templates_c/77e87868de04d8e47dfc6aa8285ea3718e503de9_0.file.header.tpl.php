<?php
/* Smarty version 3.1.33, created on 2018-11-23 15:54:10
  from 'C:\wamp\www\XOprojectCI\application\views\templates\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5bf885122456e0_85848020',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '77e87868de04d8e47dfc6aa8285ea3718e503de9' => 
    array (
      0 => 'C:\\wamp\\www\\XOprojectCI\\application\\views\\templates\\header.tpl',
      1 => 1464132602,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf885122456e0_85848020 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<!--[if ie 6]><![endif]-->

<head>
	<meta charset="utf-8" />
	<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</title> 

	<meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Mono:400,400italic' rel='stylesheet' type='text/css'>

	<style type="text/css">
	
		body {background-color: #fff; color: #000; width: 800px; font-family: 'Roboto Mono', 'courier', 'courier new', monotype;}
		h1, h2 {background-color: #fff; color: #999; font-family: 'Roboto Slab', 'Times New Roman', Georgia, serif;}
		h1 {font-size: 2em;}
		h2 {font-size: 1.5em;}
		em {border: solid #000 1px; padding: 0 5px; font-style: normal;}
		label {display: inline-block; width: 10em; text-align: right;}
		input[type="submit"] {margin: 1em 0 0 10em;}
		.error {background-color: #ff0; color: #c00;}
		.message {background-color: #fff; color: #0c0;}
	
	</style>
</head> 
<body>
<?php }
}
