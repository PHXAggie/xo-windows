<?php
/* Smarty version 3.1.33, created on 2018-11-23 19:51:16
  from 'C:\wamp\www\XOprojectCI\application\views\templates\site_header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5bf8bca4cc4d99_18022035',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '38222b193292ef9e8488d2a95a1996e0807d1dc7' => 
    array (
      0 => 'C:\\wamp\\www\\XOprojectCI\\application\\views\\templates\\site_header.tpl',
      1 => 1543027849,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf8bca4cc4d99_18022035 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp\\www\\XOprojectCI\\application\\third_party\\smarty\\libs\\plugins\\function.ci_config.php','function'=>'smarty_function_ci_config',),));
?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<?php echo smarty_function_ci_config(array('name'=>"base_url"),$_smarty_tpl);?>

  <head>
  	<meta charset="utf-8" />
  	<title>XO</title>
    <?php echo '<script'; ?>
 type="text/javascript" src='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
assets/js/jquery-3.3.1.js'><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
assets/js/bootstrap.min.js'><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
assets/js/jquery-ui-1.12.1.custom/jquery-ui.js'><?php echo '</script'; ?>
>
  	<link rel='stylesheet' href='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
assets/css/bootstrap.min.css'>
    <link rel='stylesheet' href='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
assets/css/style.css'>
    <link rel='stylesheet' href='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
assets/css/font-awesome.css'>
    <link rel="icon" href='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
assets/images/favicon.ico' type="image/x-icon">
  </head>

  <body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary sticky-top">
      <a class="navbar-brand" href='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
'>XO Windows</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
'>Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
index.php/bids'>Bids</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;
echo '<?php ';?>//index.php/starts <?php echo '?>';?>'>Starts</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Maintenance</a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <h6 class="dropdown-header">Maintenance</h6>
                <a class="dropdown-item" href='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
index.php/Maintenance/builders'>Builders</a>
                <a class="dropdown-item" href='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
index.php/Maintenance/installtypes'>Install Types</a>
                <a class="dropdown-item" href='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;
echo '<?php ';?>//index.php/Maintenance/manufacturers <?php echo '?>';?>'>Manufacturers</a>
                <a class="dropdown-item" href='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
index.php/itemCRUD/index'>ItemCRUD</a>
                <a class="dropdown-item" href='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
index.php/Maintenance/subdivisions'>Subdivisions</a>
                <a class="dropdown-item" href='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;
echo '<?php ';?>//index.php/Maintenance/windowtypes <?php echo '?>';?>'>Window Types</a>
              <div class="dropdown-divider"></div>
                <a class="dropdown-item" href='<?php echo $_smarty_tpl->tpl_vars['base_url']->value;
echo '<?php ';?>//index.php/Maintenance/xousers <?php echo '?>';?>'>XO Users</a>
              </div>
          </li>
        </ul>
      </div>
    </nav><?php }
}
