<div class="form-group row">
    <label for="install" class="col-3 text-right">Installation Method:</label>
    <div class="col-5">
        <button class="btn btn-success col-7" id="typeInstall" name="typeInstall"></button>
        <select class="btn btn-primary col-7" id="install" name="install">
            <option value="0">Choose Install Method</option>
            <?php
                if(!empty($get_install_types)){
                    foreach($get_install_types as $installTypeChoice){
                        echo '<option value="'.$installTypeChoice['install_types_id'].'">'.$installTypeChoice['type'].'</option>';
                    }
                }
            ?>
        </select><span class="small">&nbsp;(dbl-click to change)</span>
    </div>
</div>
<div class="form-group row">
    <label for="manufacturer" class="col-3 text-right">Manufacturer:</label>
    <div class="col-5">
        <button class="btn btn-success col-7" id="mfgForBid" name="mfgForBid"></button>
        <select class="btn btn-primary col-7" id="manufacturer" name="manufacturer">
            <option value="0">Choose Manufacturer</option>
            <?php
            if(!empty($get_mfg)){
                foreach($get_mfg as $mfgs){
                    echo '<option value="' . $mfgs['manufacturer_id'] . '">' . $mfgs['manufacturer_name'] . '</option>';
                };
            }else{
                    echo '<option value="">NO MANUFACTURERS FOUND</option>';
            };
            ?>
        </select><span class="small">&nbsp;(dbl-click to change)</span>
    </div>
</div>
<div class="form-group row" id="typeSelection" name="typeSelection">
    <div class="col-9">
        <table class="table-bordered">
            <thead>
                <tr>
                    <th colspan="4" class="text-center"><label>Types&nbsp;<span class="small">(select from dropdown then dbl-click to change):</span></label></th>
                </tr>
                <tr>
                    <td class="text-center">Color</td>
                    <td class="text-center">Material</td>
                    <td class="text-center">Fin</td>
                    <td class="text-center">Coating</td>
                </tr>
            </thead>
                <tr>
                    <td class="text-center"><button class="btn-sm btn-success" id="colorPicked" name="colorPicked"></button><select class="btn-sm btn-warning" id="color" name="color"><option value="">Color</option></select></td>
                    <td class="text-center"><button class="btn-sm btn-success" id="materialPicked" name="materialPicked"></button><select class="btn-sm btn-warning" id="material" name="material"><option value="0">Material</option></select></td>
                    <td class="text-center"><button class="btn-sm btn-success" id="finPicked" name="finPicked"></button><select class="btn-sm btn-warning" id="fin" name="fin"><option value="0">Fin</option></select></td>
                    <td class="text-center"><button class="btn-sm btn-success" id="coatingPicked" name="coatingPicked"></button><select class="btn-sm btn-warning" id="coating" name="coating"><option value="0">Coating</option></select></td>
                </tr>
        </table>
    </div>
</div>


<div id="debug">
    <input type="text" id="tbl_bid_id" name="tbl_bid_id" value="<?php //echo $tbl_bid_id; ?>">
    <input type="text" id="tbl_buildercustomer_builder_id" name="tbl_buildercustomer_builder_id" value="<?php //echo $tbl_buildercustomer_builder_id; ?>">
    <br />
<?php /*
    <input type="text" id="builderInstallTypeIdOnLoad" name="builderInstallTypeIdOnLoad" value="<?php echo $bid_details[0]['tbl_install_types_install_types_id']; ?>">
    <input type="text" id="builderInstallTypeNameOnLoad" name="builderInstallTypeNameOnLoad" value="<?php echo $bid_details[0]['Type']; ?>">
    <input type="text" id="builderInstallTypeChange" name="builderInstallTypeChange" value="<?php echo $bid_details[0]['tbl_install_types_install_types_id']; ?>">
    <br />

    <input type="text" id="manufacturerIdOnLoad" name="manufacturerIdOnLoad" value="<?php echo $bid_details[0]['tbl_manufacturer_manufacturer_id']; ?>">
    <input type="text" id="manufacturerNameOnLoad" name="manufacturerNameOnLoad" value="<?php echo $bid_details[0]['manufacturer_name']; ?>">
    <input type="text" id="manufacturerIdChange" name="manufacturerIdChange" value="<?php echo $bid_details[0]['tbl_manufacturer_manufacturer_id']; ?>">
    <br />

    <input type="text" id="colorIdOnLoad" name="colorIdOnLoad" value="<?php echo $bid_details[0]['tbl_window_color_window_color_id']; ?>">
    <input type="text" id="colorNameOnLoad" name="colorNameOnLoad" value="<?php echo $bid_details[0]['window_color_name']; ?>">
    <input type="text" id="colorIdChange" name="colorIdChange" value="<?php echo $bid_details[0]['tbl_window_color_window_color_id']; ?>">
    <br />

    <input type="text" id="materialIdOnLoad" name="materialIdOnLoad" value="<?php echo $bid_details[0]['tbl_window_material_window_material_id']; ?>">
    <input type="text" id="materialNameOnLoad" name="materialNameOnLoad" value="<?php echo $bid_details[0]['window_material_name']; ?>">
    <input type="text" id="materialIdChange" name="materialIdChange" value="<?php echo $bid_details[0]['tbl_window_material_window_material_id']; ?>">
    <br />

    <input type="text" id="finIdOnLoad" name="finIdOnLoad" value="<?php echo $bid_details[0]['tbl_window_fin_window_fin_id']; ?>">
    <input type="text" id="finNameOnLoad" name="finNameOnLoad" value="<?php echo $bid_details[0]['window_fin_name']; ?>">
    <input type="text" id="finIdChange" name="finIdChange" value="<?php echo $bid_details[0]['tbl_window_fin_window_fin_id']; ?>">
    <br />

    <input type="text" id="coatingIdOnLoad" name="coatingIdOnLoad" value="<?php echo $bid_details[0]['tbl_window_coating_window_coating_id']; ?>">
    <input type="text" id="coatingNameOnLoad" name="coatingNameOnLoad" value="<?php echo $bid_details[0]['window_coating_name']; ?>">
    <input type="text" id="coatingIdChange" name="coatingIdChange" value="<?php echo $bid_details[0]['tbl_window_coating_window_coating_id']; ?>">
*/
?>
</div>

<script type="text/javascript">

    $(document).ready(function(){

        $('#typeInstall').hide();
        $('#mfgForBid').hide();
        $('#typeSelection').hide();
            $('#color').hide();
            $('#colorPicked').hide();
            $('#coating').hide();
            $('#coatingPicked').hide();
            $('#material').hide();
            $('#materialPicked').hide();
            $('#fin').hide();
            $('#finPicked').hide();

        $('#builder').on('change',function(){
            var builderID = $(this).val();
            if(builderID){
                $.ajax({
                    type:'POST',
                    url:'<?php echo base_url();?>index.php/maintenance/get_builders_install_types',
                    data:'builder_id=' + builderID,
                    success:function(data){
                        $('#install').html('<option value="0">Choose Install Method</option>');
                        var dataObj = jQuery.parseJSON(data);
                        if(dataObj){
                            $(dataObj).each(function(){
                                var option = $('<option />');
                                option.attr('value', this.install_type_id).text(this.install_type);
                                $('#install').append(option);
                            });
                        }
                    }
                }); // 
            }else{
                $('#install').html('<option value="">Select BUILDER first</option>');
            }
        });

        $('#install').change(function(){
            var selectedInstallID = $(this).val();
            var selectedInstall = $('[name="install"] option:selected').text().toUpperCase();
            $('#install').hide();
            $('#typeInstall').show();
            $('#typeInstall').html(selectedInstall);
            $('#builderInstallTypeChange').val(selectedInstallID);
        });

        $('#typeInstall').dblclick(function(){
            var selectedInstallID = "";
            var selectedInstall = "";
            $(this).hide();
            $('#install').show();
            $('#install').val(0);
            $('#builderInstallTypeChange').val('');
        });

        $('#manufacturer').change(function(){
            if($('[name="manufacturer"] option:selected').text() != 'Choose Manufacturer'){
                var selectedMfgID = $(this).val();
                var selectedMfg = $('[name="manufacturer"] option:selected').text();
                $('#manufacturer').hide();
                $('#mfgForBid').show();
                $('#mfgForBid').html(selectedMfg);
                $('#manufacturerIdChange').val(selectedMfgID);
                $('#typeSelection').show();
                    $('#color').show();
                    $('#coating').show();
                    $('#material').show();
                    $('#fin').show();
            }
        });

        $('#mfgForBid').dblclick(function(){
            if($('[name="manufacturerIdOnLoad"]').val() != ''){
                var selectedMfg = $('[name="manufacturerNameOnLoad"]').val();
                var selectedMfgID = $('[name="manufacturerIdOnLoad"]').val();
                //add a are you sure prompt since it was loaded from db
                //and other instructions
            }else{
                var selectedMfgID = "";
                var selectedMfg = "";
                var colorIdChange = "";
                var coatingIdChange = "";
                var materialIdChange = "";
                var finIdChange = "";
                $(this).hide();
                $('#manufacturer').show();
                $('#manufacturer').val(0);
                $('#manufacturerIdChange').val(selectedMfgID);
                $('#colorIdChange').val(colorIdChange);
                $('#colorPicked').hide();
                $('#coatingIdChange').val(coatingIdChange);
                $('#coatingPicked').hide();
                $('#materialIdChange').val(materialIdChange);
                $('#materialPicked').hide();
                $('#finIdChange').val(finIdChange);
                $('#finPicked').hide();
                $('#typeSelection').hide();
            }
            //$('#colorPicked').trigger('click');
            //$('#colorPicked').hide();
            //$('#colorPicked').val(selectedColorID);
            //$('#color').show();
            //$('#color').val(0);
            //$('#color').html('Color');
            //$('#colorIdChange').val(selectedColorID);
            //$('#coatingPicked').hide();
            //$('#coatingPicked').val(selectedCoatingID);
            //$('#coating').show();
            //$('#coating').val(0);
            //$('#coating').html('Coating');
            //$('#coatingIdChange').val(selectedCoatingID);
            //$('#manufacturer').trigger('change');
        });

    });

</script>