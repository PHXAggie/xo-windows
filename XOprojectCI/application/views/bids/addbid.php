    <div class="container">
        <h1>Add don't use Bid</h1>
        <?php echo form_open('Form_reader/check_bid', array('class' => 'jsform')); ?>
        <!--<form method="post" name="bidCheck" action="<?php //echo base_url();?>index.php/Addbidcheck/check_bid">-->
            <div class="form-group row">
                <label for="builder" class="col-form-label">Builder:&nbsp;</label>
                <div class="col-sm-4">
                    <select class="form-control" id="builder" name="builderIdSelected">
                        <option value="">SELECT BUILDER</option>
                        <?php
                        if(!empty($tbl_buildercustomer)){
                            foreach($tbl_buildercustomer as $row){ 
                                echo '<option value="'.$row['builder_id'].'">'.$row['builder_name'].'</option>';
                            }
                        }else{
                            echo '<option value="">NO BUILDERS FOUND</option>';
                        }
                        ?>
                    </select>
                </div>
                <label for="subdivision" class="col-form-label">Subdivision:&nbsp;</label>
                <div class="col-sm-4">
                    <select class="form-control" id="subdivision" name="subdivisionIdSelected"><option value="">Select BUILDER first</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="plan" class="col-form-label"> Plan:&nbsp;</label>
                <div class="col-sm-1">
                    <input type="text" class="form-control" id="bidPlan" name="bidPlan">
                </div>
                <label for="elevation" class="col-form-label"> Elevation:&nbsp;</label>
                <div class="col-sm-1">
                    <input type="text" class="form-control" id="bidElevation" name="bidElevation">
                </div>
                <div class="col-sm-1">
                    <button type="submit" class="btn btn-warning">Check for Duplicates</button>
                </div>
            </div>
        <!--</form>-->
        <?php echo form_close(); ?>
    <div class="jsError"></div>
    
        <div class="row">
            <label class="">Types <span class="small">(click to remove):</span> 
                <button class="btn btn-info" id="typeColor" style="display:none;"></button>
                <button class="btn btn-info" id="typeCoating" style="display:none;"></button>
                <button class="btn btn-info" id="typeInstall" style="display:none;"></button>
            </label>
        </div>

        <div class="form-group row">
            <select class="btn btn-info form-control col-2" id="color" name="typeColor">
                <option value="0">Color</option>
                <option value="1">Adobe</option>
                <option value="2">Bronze</option>
                <option value="3">White</option>
            </select>
            <select class="btn btn-info form-control col-2" id="coating" name="typeCoating">
                <option value="0">Coating</option>
                <option value="1">LOWe366</option>
                <option value="2">LOWe422</option>
                <option value="3">None</option>
            </select>
            <select class="btn btn-info form-control col-2" id="install" name="typeInstall">
                <option value="0">Install</option>
                <option value="1">Install</option>
                <option value="2">Rainbuster</option>
                <option value="3">Tyvek</option>
            </select>
        </div>
    </div>

<script type="text/javascript">
$(document).ready(function(){
    /* Populate data to state dropdown*/
    $('#builder').on('change',function(){
        var builderID = $(this).val();
    var selectedBldr = $('[name="builderIdSelected"] option:selected');       
        if(builderID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url();?>index.php/Bids/getBuilderSubdivisions',
                data:'builder_id='+builderID,
                success:function(data){
                    //$('#builder').attr('disabled', 'true');  //turning on breaks validation check
                    //$('#subdivision').attr('enabled');
                    $('#subdivision').html('<option value="">SELECT SUBDIVISION</option>');
                    var dataObj = jQuery.parseJSON(data);
                    //var dataObj = [{"subdivision_id":1, "subdivision_name":"GERMANN COUNTRY ESTATES"},{"subdivision_id":2, "subdivision_name":"CLEMENTE RANCH CHANDLER"}];
                    //var dataObj = '';  //triggers Not Available
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.subdivision_id).text(this.subdivision_name);
                            $('#subdivision').append(option);
                        });
                    }else{
                        $('#subdivision').html('<option value="">NO SUBDIVISIONS FOUND</option>');
                    }
                }
            }); 
        }else{
            $('#subdivision').html('<option value="">Select BUILDER first</option>');
            //$('#city').html('<option value="">Select state first</option>'); 
        }
    });

    //Get Subdivision ID selected
    $('#subdivision').on('change',function(){
        var selectedSub = $('[name="subdivisionIdSelected"] option:selected');
        //$('#subdivision').attr('disabled', 'true');
    });

    $('#color').on('change',function(){
        var selectedColorID = $(this).val();
        var selectedColor = $('[name="typeColor"] option:selected').text().toUpperCase();
        $('#color').hide();
        $('#typeColor').show();
        $('#typeColor').html(selectedColorID + ' ' + selectedColor);
    });

    $('#typeColor').on('click', function(){
        var selectedColorID = "";
        var selectedColor = "";
        $(this).hide();
        $('#color').show();
        $('#color').val(0);
    });

    $('#coating').on('change',function(){
        var selectedCoatingID = $(this).val();
        var selectedCoating = $('[name="typeCoating"] option:selected').text().toUpperCase();
        $('#coating').hide();
        $('#typeCoating').show();
        $('#typeCoating').html(selectedCoatingID + ' ' + selectedCoating);
    });

    $('#typeCoating').on('click', function(){
        var selectedCoatingID = "";
        var selectedCoating = "";
        $(this).hide();
        $('#coating').show();
        $('#coating').val(0);
    });

    $('#install').on('change',function(){
        var selectedInstallID = $(this).val();
        var selectedInstall = $('[name="typeInstall"] option:selected').text().toUpperCase();
        $('#install').hide();
        $('#typeInstall').show();
        $('#typeInstall').html(selectedInstallID + ' ' + selectedInstall);
    });

    $('#typeInstall').on('click', function(){
        var selectedInstallID = "";
        var selectedInstall = "";
        $(this).hide();
        $('#install').show();
        $('#install').val(0);
    });


                $('form.jsform').on('submit', function(form){
                    form.preventDefault();
                    $.post('<?php echo base_url();?>index.php/Form_reader/check_bid', $('form.jsform').serialize(), function(data){
                        $('div.jsError').html(data);
                    });
                });
    
});
</script>
