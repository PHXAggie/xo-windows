<div class="container">
	<div class="text-center">
		<div class="form-group row"></div>
	    <div class="form-group row"></div>
		<h1>Bids Maintenance&nbsp;&nbsp;&nbsp;<a class="btn btn-success" href="<?php echo site_url(); ?>/bids/bidsAddEdit">Add / Lookup / Edit</a></h1>
		<?php $this->load->view('search.php'); //above was /addingnewbid ?>
		<div class="jsError"></div>

		<?php if(empty($recentBidResults)){ ?>
			<h4>Recent Bids</h4>
				<?php echo 'No Bid Records found!'; ?>
		<?php } else { ?>
			<h4>Recent Bids</h4>
				<table class="table table-bordered text-xsmall table-sm table-hover">
					<thead class="thead-light">
						<tr>
							<th scope="col">Date</th>
							<th scope="col">Builder</th>
							<th scope="col">Subdivision</th>
							<th scope="col">Plan / Elev</th>
							<th scope="col">Branch</th>
							<th scope="col">City</th>
							<th scope="col">State</th>
							<th scope="col">Prepped</th>
							<th scope="col">Actions</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($recentBidResults as $item) : ?>
						<tr>
							<td><?php echo $item['Date']; ?></td>
							<td hidden><?php echo $item['tbl_status_tbl_status_id']; ?></td>
							<td><?php echo $item['Builder']; ?></td>
							<td><?php echo $item['Subdivision']; ?></td>
							<td><?php echo $item['Plan']; ?> / <?php echo $item['Elevation'] ?></td>
							<td><?php echo $item['Branch']; ?></td>
							<td><?php echo $item['City']; ?></td>
							<td><?php echo $item['State']; ?></td>
							<td><?php echo $item['Prepped']; ?></td>

							<?php //echo "<td><a class='btn-sm btn-info' href='".site_url('/bids/editbid/'.$item['tbl_bid_id'].'/'.$item['BuilderID'])."'>Edit</a></td>" //currently needs both because install types is builderid based; ?>
							<?php //echo "<td><a class='btn-sm btn-info' href='".site_url('/bids/editbid/'.$item['tbl_bid_id'])."'>Edit</a></td>"; ?>

							<?php echo "<td><a class='btn-sm btn-info' href='" . site_url('/bids/bidsAddEdit/'.$item['tbl_bid_id']) . "'>Edit</a></td>" ?>

						</tr>
					<?php endforeach; ?>
					</tbody>					
				</table>
		<?php } ?>
	</div>
</div>

<?php if($js_to_load != ''){ ?>

	<script>
		$(document).ready(function(){

		});
	</script>
	<script type="text/javascript">var $searchBy = '<?php echo $searchBy; ?>'</script>
	<script type="text/javascript" src="../assets/js/<?=$js_to_load;?>"></script>
<?php }else{ ?>
	<script>
		$(document).ready(function(){
			$('#search').show();
		});
	</script>
<?php } ?>
