<?php /*

BSPE with Bids will have ability to 'add' new Plans and Elevations
BSPE with Starts will only have lookup capability for Plans and Elevations

    Select Builder from Dropdown -> Loads Subdivions for that builder
    Select Subdivision -> Load existing Bid Plans for the SubdivisionID
    Enter the Plan (as you are entering, existing plans try to autofill) -> Load existing Elevations for BSP combination
    Enter the Elevation (as you are entering, existing elevations try to autofill)
        -> Matching BSPE combo shows Load button
        -> New shows Create button
    Choose Create -> insert into the database, then reload page with all settings including new subidivision_id for updates
    Choose Load -> get all data for editing the fields (install type, jobsite settings, options, windows, the whole enchilada)

Jobsite Specs Panel - Change title to show who editing for (Bldr / Subd / Pln / Elv)

    After BID won, move to 'allowed for STARTS'
    After BID lost, move to 'archive'?

TROUBLESHOOTING TOOLS
    //alert(JSON.stringify(data));
    //alert(Object.keys(dataObj).length);

*/ ?>
<div class="container">
    <div class="text-center">
        <div class="form-group row"></div>
        <div class="form-group row"></div>
        <h1 id="page_title1"><?php echo $title; ?></h1>
        <div class="jsError"></div>
    <?php //permissions ?>
        <?php if($this->session->userdata('level') <= '4'               ? $audit_view = '' : $audit_view = 'hidden'); ?>
        <?php if($this->session->userdata('level') <= '4'               ? $auth_edit = '' : $auth_edit = 'readonly'); ?>
        <?php if($this->session->userdata('level') <= '4'               ? $auth_view = '' : $auth_view = 'hidden'); ?>

        <?php //echo form_open(site_url('/bids/check_bid_form'), array('class' => 'jsform')); ?>
        <?php echo $form_to_open; ?>
        <!--<form method="post" name="bidCheck" action="<?php //echo base_url();?>index.php/Addbidcheck/check_bid">-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-2">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link text-left active" id="v-pills-edit-tab" data-toggle="pill" href="#v-pills-edit" role="tab" aria-controls="v-pills-edit" aria-selected="true">General</a>
            <!-- Hide these until Load or Create is determined -->
                        <a class="nav-link text-left" id="v-pills-specs-tab" data-toggle="pill" href="#v-pills-specs" role="tab" aria-controls="v-pills-specs" aria-selected="false">Jobsite Specs</a>
                        <a class="nav-link text-left" id="v-pills-options-tab" data-toggle="pill" href="#v-pills-options" role="tab" aria-controls="v-pills-options" aria-selected="false">Options</a>
                        <a class="nav-link text-left" id="v-pills-stats-tab" data-toggle="pill" href="#v-pills-stats" role="tab" aria-controls="v-pills-stats" aria-selected="false" <?php echo $auth_view; ?> <?php echo $audit_view; ?>>Statistics</a>
                        <a class="nav-link text-left" id="v-pills-audit-tab" data-toggle="pill" href="#v-pills-audit" role="tab" aria-controls="v-pills-audit" aria-selected="false" <?php echo $audit_view; ?>>Audit Log</a>
                    <!-- Generate Menu from Array -->
                            <?php foreach($left_menu as $menuItem => $name){
                                echo '<a class="nav-link text-left" id="v-pills-'. $menuItem . '-tab" data-toggle="pill" href="#v-pills-'. $menuItem .'" role="tab" aria-controls="v-pill-'. $menuItem .'" aria-selected="false">'. $name .'</a>';
                            } ?>
                    <!-- Generate Menu from Multidimensional Array -->
                    <?php //log_message('debug', 'content_test ' . print_r($content_test, TRUE)); ?>
                            <?php foreach($content_test as $contentName => $contents){
                                //log_message('debug', 'contentName ' . print_r($contentName, TRUE));
                                //log_message('debug', 'contents ' . print_r($contents, TRUE));
                                //foreach($contents as $content){
                                    echo '<a class="nav-link text-left" id="v-pills-'. $contents['left_menu'] . '-tab" data-toggle="pill" href="#v-pills-'. $contents['left_menu'] .'" role="tab" aria-controls="v-pill-'. $contents['left_menu'] .'" aria-selected="false" ' . $contents['auth_view'] . ' ' . $contents['auth_edit'] . '>'. $contents['menuLabel'] .'</a>'; // ' . $contents['auth_edit'] . '
                                //}
                            } ?>

                    </div>                  
                </div>

<!-- <div id="thisIsATest"></div> -->
<!-- General tab -->
            <div class="col-10 border-left border-info">
                <div class="tab-content" id="v-pills-tabContent">

                    <!-- Generate EACH Section from the Menu Array -->
                        <?php foreach($left_menu as $sectionItem => $sectionName){
                            echo '<div class="tab-pane fade show" id="v-pills-'. $sectionItem .'" role="tabpanel" aria-labelledby="v-pills-'. $sectionItem .'-tab">';
                                echo '<div class="form-group row">';
                                    echo '<div class="col-5">';

                                        echo $sectionName;

                                    echo '</div>';
                                echo '</div>';
                            echo '</div>';
                        } ?>

                        <?php foreach($content_test as $section => $sectionItems){
                            echo '<div class="tab-pane fade show" id="v-pills-'. $sectionItems['left_menu'] .'" role="tabpanel" aria-labelledby="v-pills-'. $sectionItems['left_menu'] .'-tab">';
                                echo '<div class="form-group row">';
                                    echo '<div class="col-12">';

                                        //echo $sectionItems['sectionTitle'];
                                        echo $sectionItems['sectionContent'];

                                    echo '</div>';
                                echo '</div>';
                            echo '</div>';
                        } ?>

                    <div class="tab-pane fade show active" id="v-pills-edit" role="tabpanel" aria-labelledby="v-pills-edit-tab">
                        <div class="form-group row">
                            <label for="builder" class="col-3 text-right">Builder:</label>
                            <div class="col-5">
                                <select class="form-control" id="builder" name="builderIdSelected">
                                    <option value="">SELECT BUILDER</option>
                                    <?php
                                    if(!empty($list_of_buildercustomers)){
                                        foreach($list_of_buildercustomers as $rowItem){ 
                                            echo '<option value="'.$rowItem['builder_id'].'">'.$rowItem['builder_name'].'</option>';
                                        }
                                    }else{
                                        echo '<option value="">NO BUILDERS FOUND</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="subdivision" class="col-3 text-right">Subdivision:</label>
                            <div class="col-5">
                                <select class="form-control" id="subdivision" name="subdivisionIdSelected"><option value="">Select BUILDER first</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="plan" class="col-3 text-right">Plan:</label>
                            <div class="col-5">
                                <input type="text" class="form-control" id="bidPlan" name="bidPlan">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="elevation" class="col-3 text-right">Elevation:</label>
                            <div class="col-5">
                                <input type="text" class="form-control" id="bidElevation" name="bidElevation">
                            </div>
                        </div>
                        <?php if($title == 'Add (Edit) Bid'){
                                echo '<div class="form-group row">';
                                //echo '<div class="col-4"><button type="submit" class="btn btn-info" id="chk_dup_btn">Check for Duplicates</button>';
                                echo '<div class="col-10"><button type="submit" class="btn btn-info" id="submitType"></button>';
                                echo '<button type="button" class="btn btn-danger" id="cancelEntry_edit">Cancel</button></div></div>';
                            }else{
                                echo '<div class="form-group row">';
                                echo '<div class="col-10"><button type="submit" class="btn btn-danger pull-right" id="cancel_btn">Cancel</button>';
                                echo '<button type="button" class="btn btn-danger" id="cancelEntry_edit">Cancel</button></div></div>';                          
                            };
                        ?>
                    </div><!-- end tab pane for edit -->
                    <div class="tab-pane fade show" id="v-pills-specs" role="tabpanel" aria-labelledby="v-pills-specs-tab">
                        <div class="col-8">Editing for BSPE</div>
                        <div class="form-group row">
                            <label for="install" class="col-8 col-form-label">Installation Method&nbsp;<span class="small">(dbl-click to change):&nbsp;</span> 
                                <button class="btn btn-success" id="typeInstall" name="typeInstall"></button>
                                <select class="btn btn-primary" id="install" name="install">
                                    <option value="0">Choose Install Method</option>
                                    <?php
                                        if(!empty($get_install_types)){
                                            foreach($get_install_types as $installTypeChoice){
                                                echo '<option value="'.$installTypeChoice['install_types_id'].'">'.$installTypeChoice['type'].'</option>';
                                            }
                                        }else{
                                                echo '<option value="">NO INSTALL METHODS SET</option>';
                                            }
                                    ?>
                                </select>
                            </label>
                        </div>
        <!-- OR   OR   OR    OR    OR   OR   OR   OR   OR -->
                        <div class="form-group row">
                            <label for="install" class="col-8">Installation Method <span class="small">(click to change):&nbsp;</span> 
                                <button class="btn-sm btn-success" id="typeInstall" style="display:none;"></button>
                            
                            <select class="btn btn-primary" id="install" name="typeInstall">
                                <option value="0">Choose Method</option>
                                <option value="1">Install</option>
                                <option value="2">Rainbuster</option>
                                <option value="3">Tyvek</option>
                            </select>
                        </div>



                        <div class="form-group row">
                            <label class="">Manufacturer&nbsp;<span class="small">(dbl-click to change):&nbsp;</span></label>
                                <button class="btn-sm btn-success" id="mfgForBid" name="mfgForBid"></button>
                                <select class="btn-sm btn-warning" id="manufacturer" name="manufacturer">
                                    <option value="0">Choose Manufacturer</option>
                                    <?php
                                        if(!empty($get_mfg)){
                                            foreach($get_mfg as $mfgs){
                                                echo '<option value="'.$mfgs['manufacturer_id'].'">'.$mfgs['manufacturer_name'].'</option>';
                                            };
                                        }else{
                                                echo '<option value="">NO MANUFACTURERS FOUND</option>';
                                        };
                                    ?>
                                </select>
                        </div>
                        <div class="form-group row justify-content-center" id="typeSelection" name="typeSelection">
                            <table class="table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="4" class="text-center"><label>Types&nbsp;<span class="small">(select from dropdown then dbl-click to change):</span></label></th>
                                    </tr>
                                    <tr>
                                        <td class="text-center">Color</td>
                                        <td class="text-center">Material</td>
                                        <td class="text-center">Fin</td>
                                        <td class="text-center">Coating</td>
                                    </tr>
                                </thead>
                                    <tr>
                                        <td class="text-center"><button class="btn-sm btn-success" id="colorPicked" name="colorPicked"></button><select class="btn-sm btn-warning" id="color" name="color"><option value="">Color</option></select></td>
                                        <td class="text-center"><button class="btn-sm btn-success" id="materialPicked" name="materialPicked"></button><select class="btn-sm btn-warning" id="material" name="material"><option value="0">Material</option></select></td>
                                        <td class="text-center"><button class="btn-sm btn-success" id="finPicked" name="finPicked"></button><select class="btn-sm btn-warning" id="fin" name="fin"><option value="0">Fin</option></select></td>
                                        <td class="text-center"><button class="btn-sm btn-success" id="coatingPicked" name="coatingPicked"></button><select class="btn-sm btn-warning" id="coating" name="coating"><option value="0">Coating</option></select></td>
                                    </tr>
                            </table>
                        </div>
                        <div class="accordian" id="addBidsAccordion">
                            <div class="card">
                                <div class="card-header alert-success" id="headingZero">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="true" aria-controls="collapseOne collapseTwo collapseThree collapseFour collapseFive collapseSix collapseSeven collapseEight collapseNine">
                                            Toggle All Sections (Closed will Open, Opened will Close) - PRODUCTION
                                        </button>
                                    </h5>
                                </div>
                            </div><!-- end All Card toggle -->
                            <div class="card">
                                <div class="card-header alert-success" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Installation Types - PRODUCTION
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="collapse multi-collapse" aria-labelledby="headingOne">
                                    <div class="card-body">
                                        <div class="form-group row justify-content-center">
                                            <label for="install" class="col-form-label">Installation Type&nbsp;<span class="small">(dbl-click to change):&nbsp;</span> 
                                                <button class="btn-sm btn-success" id="typeInstall" name="typeInstall"></button>
                                                <select class="btn-sm btn-warning" id="install" name="install">
                                                    <option value="0">Choose Type</option>
                                                    <?php
                                                    if(!empty($get_install_types)){
                                                        foreach($get_install_types as $installTypeChoice){
                                                            echo '<option value="'.$installTypeChoice['install_types_id'].'">'.$installTypeChoice['type'].'</option>';
                                                        };
                                                    }else{
                                                            echo '<option value="">NO INSTALL TYPES SET</option>';
                                                        };
                                                    ?>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="">Installation Type <span class="small">(click to change):</span> 
                                                <button class="btn-sm btn-success" id="typeInstall" style="display:none;"></button>
                                            </label>
                                            <select class="btn-sm btn-warning form-control col-4" id="install" name="typeInstall">
                                                <option value="0">Choose Type</option>
                                                <option value="1">Install</option>
                                                <option value="2">Rainbuster</option>
                                                <option value="3">Tyvek</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- end install type card -->
                            <div class="card">
                                <div class="card-header alert-success" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                            Manufacturer / Manufacturer-based Selections - PRODUCTION
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseTwo" class="collapse multi-collapse" aria-labelledby="headingTwo">
                                    <div class="card-body">

                                        <div class="form-group row justify-content-center">
                                            <label class="">Manufacturer&nbsp;<span class="small">(dbl-click to change):&nbsp;</span></label>
                                                <button class="btn-sm btn-success" id="mfgForBid" name="mfgForBid"></button>
                                                <select class="btn-sm btn-warning" id="manufacturer" name="manufacturer">
                                                    <option value="0">Choose Manufacturer</option>
                                                    <?php
                                                        if(!empty($get_mfg)){
                                                            foreach($get_mfg as $mfgs){
                                                                echo '<option value="'.$mfgs['manufacturer_id'].'">'.$mfgs['manufacturer_name'].'</option>';
                                                            };
                                                        }else{
                                                                echo '<option value="">NO MANUFACTURERS FOUND</option>';
                                                        };
                                                    ?>
                                                </select>
                                        </div>
                                        <div class="form-group row justify-content-center" id="typeSelection" name="typeSelection">
                                            <table class="table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th colspan="4" class="text-center"><label>Types&nbsp;<span class="small">(select from dropdown then dbl-click to change):</span></label></th>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center">Color</td>
                                                        <td class="text-center">Material</td>
                                                        <td class="text-center">Fin</td>
                                                        <td class="text-center">Coating</td>
                                                    </tr>
                                                </thead>
                                                    <tr>
                                                        <td class="text-center"><button class="btn-sm btn-success" id="colorPicked" name="colorPicked"></button><select class="btn-sm btn-warning" id="color" name="color"><option value="">Color</option></select></td>
                                                        <td class="text-center"><button class="btn-sm btn-success" id="materialPicked" name="materialPicked"></button><select class="btn-sm btn-warning" id="material" name="material"><option value="0">Material</option></select></td>
                                                        <td class="text-center"><button class="btn-sm btn-success" id="finPicked" name="finPicked"></button><select class="btn-sm btn-warning" id="fin" name="fin"><option value="0">Fin</option></select></td>
                                                        <td class="text-center"><button class="btn-sm btn-success" id="coatingPicked" name="coatingPicked"></button><select class="btn-sm btn-warning" id="coating" name="coating"><option value="0">Coating</option></select></td>
                                                    </tr>
                                            </table>
                                        </div>
                                        <div id="debug">
                                            <input type="text" id="tbl_bid_id" name="tbl_bid_id" value="1">
                                            <input type="text" id="tbl_buildercustomer_builder_id" name="tbl_buildercustomer_builder_id" value="146">
                                            <br />

                                            <input type="text" id="builderInstallTypeNameOnLoad" name="builderInstallTypeNameOnLoad" value="">
                                            <input type="text" id="builderInstallTypeChange" name="builderInstallTypeChange" value="">
                                            <br />

                                            <input type="text" id="manufacturerIdOnLoad" name="manufacturerIdOnLoad" value="">
                                            <input type="text" id="manufacturerNameOnLoad" name="manufacturerNameOnLoad" value="">
                                            <input type="text" id="manufacturerIdChange" name="manufacturerIdChange" value="">
                                            <br />

                                            <input type="text" id="colorIdOnLoad" name="colorIdOnLoad" value="">
                                            <input type="text" id="colorNameOnLoad" name="colorNameOnLoad" value="">
                                            <input type="text" id="colorIdChange" name="colorIdChange" value="">
                                            <br />


                                            <input type="text" id="materialIdOnLoad" name="materialIdOnLoad" value="">
                                            <input type="text" id="materialNameOnLoad" name="materialNameOnLoad" value="">
                                            <input type="text" id="materialIdChange" name="materialIdChange" value="">
                                            <br />

                                            <input type="text" id="finIdOnLoad" name="finIdOnLoad" value="">
                                            <input type="text" id="finNameOnLoad" name="finNameOnLoad" value="">
                                            <input type="text" id="finIdChange" name="finIdChange" value="">
                                            <br />

                                            <input type="text" id="coatingIdOnLoad" name="coatingIdOnLoad" value="">
                                            <input type="text" id="coatingNameOnLoad" name="coatingNameOnLoad" value="">
                                            <input type="text" id="coatingIdChange" name="coatingIdChange" value="">
                                        </div><!-- end debug -->
                                    </div><!-- end mfg card body -->
                                </div><!-- end mfg accordian -->
                            </div><!-- end mfg card -->
                        </div><!-- end all accordians -->
                    </div><!-- end tab pane for jobsite specs -->

                    <div class="tab-pane fade show" id="v-pills-options" role="tabpanel" aria-labelledby="v-pills-options-tab">
                        <!--<div id="#bspeFile">here</div>-->
                        <?php
                            $bspeData['list_of_bldercustomers'] = $list_of_buildercustomers; //pass-through of controller provided
                            $this->load->view('maintenance/BSPEselect', $bspeData);
                        ?>
                    </div><!-- end tab pane for options -->
                </div><!-- end all tab contents -->                     
            </div><!-- end border line -->
            </div><!-- end row for all content -->
            
        <?php echo form_close(); ?><!--</form>-->

    
 
        </div><!-- end container-fluid -->
    </div><!-- end text center -->
</div><!-- end container -->

<script type="text/javascript">

    $(document).ready(function(){

        //$('#thisIsATest').load('<?php echo base_url();?>application/libraries/thatHTML.php');
        //$('#thisIsATest').load('<?php echo base_url();?>/application/views/maintenance/BSPEselect.php');
        //$('#bspeFile').load('<?php echo base_url();?>index.php/maintenance/getBSPEselect');

        //$('#v-pills-specs-tab').hide();
        //$('#v-pills-options-tab').hide();
        //$('#v-pills-stats-tab').hide();
        //$('#v-pills-audit-tab').hide();

        //$('#subdivision').prop('disabled', true);
        //$('#bidPlan').prop('disabled', true);
        //$('#bidElevation').prop('disabled', true);

        $('#submitType').hide();
        $('#typeInstall').hide();
        $('#mfgForBid').hide();
        $('#typeSelection').hide();
            $('#color').hide();
            $('#colorPicked').hide();
            $('#coating').hide();
            $('#coatingPicked').hide();
            $('#material').hide();
            $('#materialPicked').hide();
            $('#fin').hide();
            $('#finPicked').hide();


        $('#install').change(function(){
            var selectedInstallID = $(this).val();
            var selectedInstall = $('[name="install"] option:selected').text().toUpperCase();
            $('#install').hide();
            $('#typeInstall').show();
            $('#typeInstall').html(selectedInstall);
            $('#builderInstallTypeChange').val(selectedInstallID);
        });

        //$('#typeInstall').on('click',function(){
        $('#typeInstall').dblclick(function(){
            var selectedInstallID = "";
            var selectedInstall = "";
            $(this).hide();
            $('#install').show();
            $('#install').val(0);
            $('#builderInstallTypeChange').val('');
        });

        $('#manufacturer').change(function(){
            if($('[name="manufacturer"] option:selected').text() != 'Choose Manufacturer'){
                var selectedMfgID = $(this).val();
                var selectedMfg = $('[name="manufacturer"] option:selected').text();
                $('#manufacturer').hide();
                $('#mfgForBid').show();
                $('#mfgForBid').html(selectedMfg);
                $('#manufacturerIdChange').val(selectedMfgID);
                $('#typeSelection').show();
                    $('#color').show();
                    $('#coating').show();
                    $('#material').show();
                    $('#fin').show();
            }
        });

        $('#mfgForBid').dblclick(function(){
            if($('[name="manufacturerIdOnLoad"]').val() != ''){
                var selectedMfg = $('[name="manufacturerNameOnLoad"]').val();
                var selectedMfgID = $('[name="manufacturerIdOnLoad"]').val();
                //add a are you sure prompt since it was loaded from db
                //and other instructions
            }else{
                var selectedMfgID = "";
                var selectedMfg = "";
                var colorIdChange = "";
                var coatingIdChange = "";
                var materialIdChange = "";
                var finIdChange = "";
                $(this).hide();
                $('#manufacturer').show();
                $('#manufacturer').val(0);
                $('#manufacturerIdChange').val(selectedMfgID);
                $('#colorIdChange').val(colorIdChange);
                $('#colorPicked').hide();
                $('#coatingIdChange').val(coatingIdChange);
                $('#coatingPicked').hide();
                $('#materialIdChange').val(materialIdChange);
                $('#materialPicked').hide();
                $('#finIdChange').val(finIdChange);
                $('#finPicked').hide();
                $('#typeSelection').hide();
            }
        });

        $('#color').change(function(){
            if($('[name="color"] option:selected').text() != 'SELECT COLOR'){
                var selectedColorID = $(this).val();
                var selectedColor = $('[name="color"] option:selected').text();
                $('#color').hide();
                $('#colorPicked').show();
                $('#colorPicked').html(selectedColor);
                $('#colorIdChange').val(selectedColorID);
            }
        });

        $('#colorPicked').dblclick(function(){
            if(selectedColorID != ''){
                var selectedColorID = '';
                var selectedColor = '';
                $(this).hide();
                $('#color').show();
                $('#color').val(0);
                $('#colorIdChange').val(selectedColorID);
            }
        });

        $('#coating').change(function(){
            var selectedCoatingID = $(this).val();
            var selectedCoating = $('[name="coating"] option:selected').text();
            $('#coating').hide();
            $('#coatingPicked').show();
            $('#coatingPicked').html(selectedCoating);
            $('#coatingIdChange').val(selectedCoatingID);
        });

        $('#coatingPicked').dblclick(function(){
            var selectedCoatingID = "";
            var selectedCoating = "";
            $(this).hide();
            $('#coating').show();
            $('#coating').val(0);
            $('#coatingIdChange').val(selectedCoatingID);
        });

        $('#material').change(function(){
            var selectedMaterialID = $(this).val();
            var selectedMaterial = $('[name="material"] option:selected').text();
            $('#material').hide();
            $('#materialPicked').show();
            $('#materialPicked').html(selectedMaterial);
            $('#materialIdChange').val(selectedMaterialID);
        });

        $('#materialPicked').dblclick(function(){
            var selectedMaterialID = "";
            var selectedMaterial = "";
            $(this).hide();
            $('#material').show();
            $('#material').val(0);
            $('#materialIdChange').val(selectedMaterialID);
        });

        $('#fin').change(function(){
            var selectedFinID = $(this).val();
            var selectedFin = $('[name="fin"] option:selected').text();
            $('#fin').hide();
            $('#finPicked').show();
            $('#finPicked').html(selectedFin);
            $('#finIdChange').val(selectedFinID);
        });

        $('#finPicked').dblclick(function(){
            var selectedFinID = "";
            var selectedFin = "";
            $(this).hide();
            $('#fin').show();
            $('#fin').val(0);
            $('#finIdChange').val(selectedFinID);
        });

        $('#manufacturer').change(function(){
            var manufacturerID = $(this).val();
            var selectedMfg = $('[name="manufacturer"] option:selected');       
            if(manufacturerID){
                $.ajax({
                    type:'POST',
                    url:'<?php echo site_url();?>/bids/get_mfg_colors',
                    data:'manufacturer_id='+manufacturerID,
                    success:function(data){
                        $('#color').html('<option value="0">SELECT COLOR</option>');
                        var dataObj = jQuery.parseJSON(data);
                        if(dataObj){
                            $(dataObj).each(function(){
                                var option = $('<option />');
                                option.attr('value', this.window_color_id).text(this.window_color_name);
                                $('#color').append(option);
                            });
                        }
                    }
                }); // end colors AJAX
                $.ajax({
                    type:'POST',
                    url:'<?php echo site_url();?>/bids/get_mfg_materials',
                    data:'manufacturer_id='+manufacturerID,
                    success:function(data){
                        $('#material').html('<option value="0">SELECT MATERIAL</option>');
                        var dataObj = jQuery.parseJSON(data);
                        if(dataObj){
                            $(dataObj).each(function(){
                                var option = $('<option />');
                                option.attr('value', this.window_material_id).text(this.window_material_name);
                                $('#material').append(option);
                            });
                        }
                    }
                }); // end materials AJAX
                $.ajax({
                    type:'POST',
                    url:'<?php echo site_url();?>/bids/get_mfg_coatings',
                    data:'manufacturer_id='+manufacturerID,
                    success:function(data){
                        $('#coating').html('<option value="0">SELECT COATING</option>');
                        var dataObj = jQuery.parseJSON(data);
                        if(dataObj){
                            $(dataObj).each(function(){
                                var option = $('<option />');
                                option.attr('value', this.window_coating_id).text(this.window_coating_name);
                                $('#coating').append(option);
                            });
                        }
                    }
                }); // end coatings AJAX
                $.ajax({
                    type:'POST',
                    url:'<?php echo site_url();?>/bids/get_mfg_fins',
                    data:'manufacturer_id='+manufacturerID,
                    success:function(data){
                        $('#fin').html('<option value="0">SELECT FIN</option>');
                        var dataObj = jQuery.parseJSON(data);
                        if(dataObj){
                            $(dataObj).each(function(){
                                var option = $('<option />');
                                option.attr('value', this.window_fin_id).text(this.window_fin_name);
                                $('#fin').append(option);
                            });
                        }
                    }
                }); // end fin AJAX
            }
        }); // end $('#manufacturer').change(function(){

        $('#color').change(function(){
            var selectedColor = $('[name="color"] option:selected');
            //$('#color').attr('disabled', 'true');
        });

            //Get Color ID selected //WHY is this in the code I copied?
        $('#coating').change(function(){
            var selectedCoating = $('[name="coating"] option:selected');
            //$('#coating').attr('disabled', 'true');
        });

        $('#material').change(function(){
            var selectedMaterial = $('[name="material"] option:selected');
            //$('#material').attr('disabled', 'true');
        });

        $('#fin').change(function(){
            var selectedFin = $('[name="fin"] option:selected');
            //$('#fin').attr('disabled', 'true');
        });

        $('#builder').on('change',function(){
        //clear PLAN and ELEVATION FIELDS
            $('#bidPlan').val('').trigger('change');
            //$('#bidPlan').prop('disabled', true);
            $('#bidElevation').val('').trigger('change');
            //$('#bidElevation').prop('disabled', true);
            //$('#subdivision').prop('disabled', true);

            var builderID = $(this).val();
            var selectedBldr = $('[name="builderIdSelected"] option:selected'); //get the builder record id of the builder name that was chosen by user
            if(builderID){
                $.ajax({
                    type:'POST',
                    url:'<?php echo base_url();?>index.php/bids/getBuilderSubdivisions', //post to Controllers->Bids->getBuilderSubdivisions
                    data:'builder_id='+builderID, //builder_id=146 for example
                    success:function(data){ //if Model->BidsModel->getBuilderSubdivisions returns some rows run this function with 'data' as the parameter
                        //$('#builder').attr('disabled', 'true');  //turning on breaks validation check
                        //$('#subdivision').prop('disabled', false);
                        $('#subdivision').html('<option value="">SELECT SUBDIVISION</option>');
                        var dataObj = jQuery.parseJSON(data);
                        //var dataObj = [{"subdivision_id":1, "subdivision_name":"GERMANN COUNTRY ESTATES"},{"subdivision_id":2, "subdivision_name":"CLEMENTE RANCH CHANDLER"}];
                        //var dataObj = '';  //triggers Not Available
                        
                        if(dataObj){
                            //if count of dataObj is just the 1 (ie one subdivision, set it as 'selected')
                            if(Object.keys(dataObj).length == 1){
                                $(dataObj).each(function(){
                                    var option = $('<option />');
                                    option.attr('value', this.subdivision_id).text(this.subdivision_name);
                                    option.attr('selected', 'true');
                                    $('#subdivision').append(option);
                                });
                            }else{
                                $(dataObj).each(function(){
                                    var option = $('<option />');
                                    option.attr('value', this.subdivision_id).text(this.subdivision_name);
                                    $('#subdivision').append(option);
                                });
                            }
                            //$('#bidPlan').prop('disabled', false);
                        }else{
                            $('#subdivision').html('<option value="">NO SUBDIVISIONS FOUND</option>');
                        }
                    }
                }); 
            }else{
                $('#subdivision').html('<option value="">Select BUILDER first</option>');
            }
        });

        //Get Subdivision ID selected for duplicate checking post
        $('#subdivision').on('change',function(){
            $('#bidPlan').val('').trigger('change');
            //$('#bidPlan').prop('disabled', true);
            $('#bidElevation').val('').trigger('change');
            //$('#bidElevation').prop('disabled', true);
            var selectedSub = $('[name="subdivisionIdSelected"] option:selected');
            //$('#subdivision').attr('disabled', 'true');

        });

        $('#bidPlan').autocomplete({
            //https://jqueryui.com/autocomplete/#remote-jsonp
            source: function(request, response){
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url()?>/bids/get_plans',
                    dataType: 'json',
                    data: {
                        searchPlan: request.term,
                        subdivision_id: $('#subdivision').val()
                    },
                        //searchPlan: request.term
                        //term: request.term
                    success: function(data) {
                        response(data);
                    }
                });
            },            
            minLength: 0,
            autoFocus: true,
            response: function (event, ui) {
                if (ui.content.length == 0){
                    ui.content.push({
                        label: 'New Plan: ' + $(this).val(),
                        value: $(this).val(),
                        id: 0
                    });
                }
            }
        }).on('autocompleteselect', function (event, ui) {
            //id = ui.item.id; //value = ui.item.value; //alert('Selected id: ' + id + ' and selected value: ' + value);
            $('#bidPlan').val(ui.item.value);
            //$('#bidElevation').prop('disabled', false);
            //var selectedPlan = ui.item.value;
            return false;
        });

        $('#bidElevation').autocomplete({
            source: function(request, response){
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url()?>/bids/get_elevations',
                    dataType: 'json',
                    data: {
                        searchElevation: request.term,
                        planSelected: $('#bidPlan').val(),
                        subdivisionSelected: $('#subdivision').val()
                        //term: request.term
                    },

                    success: function(data) {
                        response(data);
                    }
            
                });
            },            
            minLength: 0,
            autoFocus: true,
            response: function (event, ui) {
                if (ui.content.length == 0){
                    ui.content.push({
                        label: 'New Elevation: ' + $(this).val(),
                        value: $(this).val(),
                        id: 0
                    });
                }
            }
        }).on('autocompleteselect', function (event, ui) {
            id = ui.item.id; // no id for these (undefined) since they aren't individual records but part of plan record in subdivision
            value = ui.item.value;
            $('#bidElevation').val(ui.item.value);
            if(id == 0){
                $('#submitType').html('Create Bid').focus();
            }else{
                $('#submitType').html('Load Bid').focus(); //focus is for both new and load, lets split that
            }
            $('#submitType').show();
            return false;
        });

        $('form.jsform').on('submit', function(form){
            form.preventDefault();
            $.post('<?php echo base_url();?>index.php/bids/check_bid_form', $('form.jsform').serialize(), function(data){
                $('div.jsError').html(data);
            });
        });

        $('#cancelEntry_edit').on('click',function(){
            window.location.href = "<?php echo site_url(); ?>/bids/index";
        });
    });
</script>