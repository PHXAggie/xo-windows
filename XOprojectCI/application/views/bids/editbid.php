<div class="container">
    <div class="jsError"></div>
        <h1 class="text-center" id="page_title">Edit Bid</h1>
        <img src="<?php echo base_url(); ?>assets/images/XOwinLOGO1.jpg" alt="XO Windows" class="imageLogo" style="width: 0px; height: 0px;" />
        <?php //echo form_open(base_url('bids/check_bid_form'), array('class' => 'jsform')); ?>
            <div class="form-group row justify-content-center heading_row_one">
                <label for="builder" class="col-form-label print_builder_label">&nbsp;Builder:&nbsp;</label>
                    <input class="col-xl-5 print_builder_data" type="text" id="builderName" name="builderName" value="<?php echo $bid_details[0]['Builder']; ?>">
                <label for="subdivision" class="col-form-label print_subdivision_label">&nbsp;Subdivision:&nbsp;</label>
                    <input class="col-xl-5 print_subdivision_data" type="text" id="subdivisionName" name="subdivisionName" value="<?php echo $bid_details[0]['Subdivision']; ?>">
            </div>
            <div class="form-group row justify-content-center heading_row_two">
                <label for="plan" class="col-form-label print_plan_label">&nbsp;Plan:&nbsp;</label>
                    <input class="col-xl-2 print_plan_data" type="text" id="plan" name="plan" value="<?php echo $bid_details[0]['Plan']; ?>">
                <label for="elevation" class="col-form-label print_elevation_label">&nbsp;Elevation:&nbsp;</label>
                    <input class="col-xl-2 print_elevation_data" type="text" id="elevation" name="elevation" value="<?php echo $bid_details[0]['Elevation']; ?>">
            </div>
        <?php //echo form_close(); ?>

    <div class="accordian" id="bidsAccordion">

        <div class="card">
            <div class="card-header alert-success" id="headingZero">
                <h5 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="true" aria-controls="collapseOne collapseTwo collapseThree collapseFour collapseFive collapseSix collapseSeven collapseEight collapseNine">
                        Toggle All Sections (Closed will Open, Opened will Close) - PRODUCTION
                    </button>
                </h5>
            </div>
        </div>


        <div class="card">
            <div class="card-header alert-success" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Installation Types - PRODUCTION
                    </button>
                </h5>
            </div>

            <div id="collapseOne" class="collapse multi-collapse" aria-labelledby="headingOne">
                <div class="card-body">
                    <div class="form-group row justify-content-center">
                        <label for="install" class="col-form-label">Installation Type&nbsp;<span class="small">(dbl-click to change):&nbsp;</span> 
                            <button class="btn-sm btn-success" id="typeInstall" name="typeInstall"></button>
                            <select class="btn-sm btn-warning" id="install" name="install">
                                <option value="0">Choose Type</option>
                                <?php
                                if(!empty($get_install_types)){
                                    foreach($get_install_types as $installTypeChoice){
                                        echo '<option value="'.$installTypeChoice['install_types_id'].'">'.$installTypeChoice['type'].'</option>';
                                    };
                                }else{
                                        echo '<option value="">NO INSTALL TYPES SET</option>';
                                    };
                                ?>
                            </select>
                        </label>
                    </div>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header alert-success" id="headingTwo">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                        Manufacturer / Manufacturer-based Selections - PRODUCTION
                    </button>
                </h5>
            </div>

            <div id="collapseTwo" class="collapse multi-collapse" aria-labelledby="headingTwo">
                <div class="card-body">
                    <div class="form-group row justify-content-center">
                        <label class="">Manufacturer&nbsp;<span class="small">(dbl-click to change):&nbsp;</span></label>
                            <button class="btn-sm btn-success" id="mfgForBid" name="mfgForBid"></button>
                            <select class="btn-sm btn-warning" id="manufacturer" name="manufacturer">
                                <option value="0">Choose Manufacturer</option>
                                <?php
                                if(!empty($get_mfg)){
                                    foreach($get_mfg as $mfgs){
                                        echo '<option value="'.$mfgs['manufacturer_id'].'">'.$mfgs['manufacturer_name'].'</option>';
                                    };
                                }else{
                                        echo '<option value="">NO MANUFACTURERS FOUND</option>';
                                };
                                ?>
                            </select>
                    </div>
                    <div class="form-group row justify-content-center" id="typeSelection" name="typeSelection">
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="4" class="text-center"><label>Types&nbsp;<span class="small">(select from dropdown then dbl-click to change):</span></label></th>
                                </tr>
                                <tr>
                                    <td class="text-center">Color</td>
                                    <td class="text-center">Material</td>
                                    <td class="text-center">Fin</td>
                                    <td class="text-center">Coating</td>
                                </tr>
                            </thead>
                                <tr>
                                    <td class="text-center"><button class="btn-sm btn-success" id="colorPicked" name="colorPicked"></button><select class="btn-sm btn-warning" id="color" name="color"><option value="">Color</option></select></td>
                                    <td class="text-center"><button class="btn-sm btn-success" id="materialPicked" name="materialPicked"></button><select class="btn-sm btn-warning" id="material" name="material"><option value="0">Material</option></select></td>
                                    <td class="text-center"><button class="btn-sm btn-success" id="finPicked" name="finPicked"></button><select class="btn-sm btn-warning" id="fin" name="fin"><option value="0">Fin</option></select></td>
                                    <td class="text-center"><button class="btn-sm btn-success" id="coatingPicked" name="coatingPicked"></button><select class="btn-sm btn-warning" id="coating" name="coating"><option value="0">Coating</option></select></td>
                                </tr>
                        </table>
                    </div>

                    <div id="debug">
                        <input type="text" id="tbl_bid_id" name="tbl_bid_id" value="<?php echo $tbl_bid_id; ?>">
                        <input type="text" id="tbl_buildercustomer_builder_id" name="tbl_buildercustomer_builder_id" value="<?php echo $tbl_buildercustomer_builder_id; ?>">
                        <br />

                        <input type="text" id="builderInstallTypeIdOnLoad" name="builderInstallTypeIdOnLoad" value="<?php echo $bid_details[0]['tbl_install_types_install_types_id']; ?>">
                        <input type="text" id="builderInstallTypeNameOnLoad" name="builderInstallTypeNameOnLoad" value="<?php echo $bid_details[0]['Type']; ?>">
                        <input type="text" id="builderInstallTypeChange" name="builderInstallTypeChange" value="<?php echo $bid_details[0]['tbl_install_types_install_types_id']; ?>">
                        <br />

                        <input type="text" id="manufacturerIdOnLoad" name="manufacturerIdOnLoad" value="<?php echo $bid_details[0]['tbl_manufacturer_manufacturer_id']; ?>">
                        <input type="text" id="manufacturerNameOnLoad" name="manufacturerNameOnLoad" value="<?php echo $bid_details[0]['manufacturer_name']; ?>">
                        <input type="text" id="manufacturerIdChange" name="manufacturerIdChange" value="<?php echo $bid_details[0]['tbl_manufacturer_manufacturer_id']; ?>">
                        <br />

                        <input type="text" id="colorIdOnLoad" name="colorIdOnLoad" value="<?php echo $bid_details[0]['tbl_window_color_window_color_id']; ?>">
                        <input type="text" id="colorNameOnLoad" name="colorNameOnLoad" value="<?php echo $bid_details[0]['window_color_name']; ?>">
                        <input type="text" id="colorIdChange" name="colorIdChange" value="<?php echo $bid_details[0]['tbl_window_color_window_color_id']; ?>">
                        <br />

                        <input type="text" id="materialIdOnLoad" name="materialIdOnLoad" value="<?php echo $bid_details[0]['tbl_window_material_window_material_id']; ?>">
                        <input type="text" id="materialNameOnLoad" name="materialNameOnLoad" value="<?php echo $bid_details[0]['window_material_name']; ?>">
                        <input type="text" id="materialIdChange" name="materialIdChange" value="<?php echo $bid_details[0]['tbl_window_material_window_material_id']; ?>">
                        <br />

                        <input type="text" id="finIdOnLoad" name="finIdOnLoad" value="<?php echo $bid_details[0]['tbl_window_fin_window_fin_id']; ?>">
                        <input type="text" id="finNameOnLoad" name="finNameOnLoad" value="<?php echo $bid_details[0]['window_fin_name']; ?>">
                        <input type="text" id="finIdChange" name="finIdChange" value="<?php echo $bid_details[0]['tbl_window_fin_window_fin_id']; ?>">
                        <br />

                        <input type="text" id="coatingIdOnLoad" name="coatingIdOnLoad" value="<?php echo $bid_details[0]['tbl_window_coating_window_coating_id']; ?>">
                        <input type="text" id="coatingNameOnLoad" name="coatingNameOnLoad" value="<?php echo $bid_details[0]['window_coating_name']; ?>">
                        <input type="text" id="coatingIdChange" name="coatingIdChange" value="<?php echo $bid_details[0]['tbl_window_coating_window_coating_id']; ?>">
                    </div>

                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header alert-success" id="headingThree">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                        Options Grid using icons - PRODUCTION
                    </button>
                </h5>
            </div>

            <div id="collapseThree" class="collapse multi-collapse" aria-labelledby="headingThree">
                <div class="card-body">
                    <div id="gridTableContainer"> <!-- gridTableContainer -->
                        <div class="gridTableHeadings"> <!-- gridTableHeadings -->
                            <table>
                                <thead>
                                    <tr>
                                        <th valign="bottom" align="center"><strong>Details</strong></th>
                                        <th valign="bottom" align="center"><strong>Loc</strong></th>
                                        <?php // <th valign="bottom" align="center"><strong>Edit</strong></th> ?>
                                        <?php foreach($get_bid_options as $optionItem){ ?>
                                            <th class="rotate"><div><span><?php echo $optionItem['option_name']; ?></span></div></th>
                                        <?php }; ?>
                                    </tr>
                                </thead>
   
                                <tbody id="sortable">
                                    <?php
                                        echo form_open(base_url('bids/editbid_grid_update_forms2'), array('class' => 'gridform2'));
                                            foreach($get_bid_windows2 as $eachWindow){
                                                // row start
                                                // first 3 columns temp removed the row edit button--> //<td><button class='btn btn-sm'><i class='fa fa-edit rowEdit'></i></button></td>
                                                echo "<tr class='table-bordered ui-state-default' id='windowID-".$eachWindow['bid_windows_id']."'><td>".$eachWindow['window_details']."</td><td>".$eachWindow['window_location']."</td>";
                                                // rest of the columns (ie options)
                                                for($columns = 0; $columns < count($get_bid_options); $columns++){ //arrow-left arrow-right ban plus
                                                    // if the window and the option intersection is in option has windows (verifyCrossRef in MY_array_helper)
                                                    if(verifyCrossRef($option_has_windows2, array('option_id', 'tbl_bid_windows_bid_windows_id'), array($get_bid_options[$columns]['option_id'],$eachWindow['bid_windows_id']))){
                                                            // if the window is removed by the option
                                                            if(windowIsAddedRemoved($option_has_windows2, array('option_id', 'tbl_bid_windows_bid_windows_id', 'add_remove_action'), array($get_bid_options[$columns]['option_id'], $eachWindow['bid_windows_id'], 0))){
                                                                    echo "<td>".form_button('buttoned[]','<i class="fa fa-ban"></i>',array('class' => 'btn btn-sm btn-danger ban', 'value' => $eachWindow['bid_windows_id'].'-'.$get_bid_options[$columns]['option_id']))."</td>";
                                                            }
                                                            // if the window is added by the option (or is listed for handing purposes)
                                                            if(windowIsAddedRemoved($option_has_windows2, array('option_id', 'tbl_bid_windows_bid_windows_id', 'add_remove_action'), array($get_bid_options[$columns]['option_id'], $eachWindow['bid_windows_id'], 1))){

                                                                foreach($option_has_windows2 as $oHw2){
                                                                    // two or more add window entries
                                                                    if($oHw2['tbl_bid_windows_bid_windows_id'] == $eachWindow['bid_windows_id'] && $oHw2['option_id'] == $get_bid_options[$columns]['option_id'] && $oHw2['ttl_option_count'] >= 2){
                                                                        // if the option is LHAND
                                                                        if($oHw2['option_name'] == 'LHAND'){
                                                                            echo "<td>".form_button('buttoned[]','<i class="fa fa-arrow-left"></i>',array('class' => 'btn btn-sm '.$eachWindow["bid_windows_id"].' left btn-info', 'value' => $eachWindow['bid_windows_id'].'-'.$get_bid_options[$columns]['option_id']))."</td>";
                                                                        // if the option is RHAND
                                                                        }elseif($oHw2['option_name'] == 'RHAND'){
                                                                            echo "<td>".form_button('buttoned[]','<i class="fa fa-arrow-right"></i>',array('class' => 'btn btn-sm '.$eachWindow["bid_windows_id"].' right btn-info', 'value' => $eachWindow['bid_windows_id'].'-'.$get_bid_options[$columns]['option_id']))."</td>";
                                                                        // all other options
                                                                        }else{
                                                                            echo "<td>".form_button('buttoned[]','<i class="fa fa-plus"></i>',array('class' => 'btn btn-sm btn-success plus', 'value' => $eachWindow['bid_windows_id'].'-'.$get_bid_options[$columns]['option_id']))."</td>";
                                                                        };
                                                                    // only one entry in window has options
                                                                    }elseif($oHw2['tbl_bid_windows_bid_windows_id'] == $eachWindow['bid_windows_id'] && $oHw2['option_id'] == $get_bid_options[$columns]['option_id'] && $oHw2['ttl_option_count'] == 1){
                                                                        // if that one option is LHAND
                                                                        if($oHw2['option_name'] == 'LHAND'){
                                                                            echo "<td>".form_button('buttoned[]','<i class="fa fa-arrow-left"></i>',array('class' => 'btn btn-sm '.$eachWindow["bid_windows_id"].' left btn-info', 'value' => $eachWindow['bid_windows_id'].'-'.$get_bid_options[$columns]['option_id']))."</td>";
                                                                        // if that one option is RHAND
                                                                        }elseif($oHw2['option_name'] == 'RHAND'){
                                                                            echo "<td>".form_button('buttoned[]','<i class="fa fa-arrow-right"></i>',array('class' => 'btn btn-sm '.$eachWindow["bid_windows_id"].' right btn-info', 'value' => $eachWindow['bid_windows_id'].'-'.$get_bid_options[$columns]['option_id']))."</td>";
                                                                        // all other options
                                                                        }else{
                                                                            echo "<td>".form_button('buttoned[]','<i class="fa fa-plus"></i>',array('class' => 'btn btn-sm btn-success plus', 'value' => $eachWindow['bid_windows_id'].'-'.$get_bid_options[$columns]['option_id']))."</td>";                                        
                                                                        };
                                                                    };
                                                                };
                                                            };
                                                    // the window is in every option so display and edit field button
                                                    }else{
                                                        if($get_bid_options[$columns]['option_name'] == 'LHAND'){
                                                            echo "<td>".form_button('buttoned[]','<i class="fa fa-edit"></i>',array('class' => 'btn btn-sm '.$eachWindow["bid_windows_id"].' left text-light', 'value' => $eachWindow['bid_windows_id'].'-'.$get_bid_options[$columns]['option_id']))."</td>";
                                                        }elseif($get_bid_options[$columns]['option_name'] == 'RHAND'){
                                                            echo "<td>".form_button('buttoned[]','<i class="fa fa-edit"></i>',array('class' => 'btn btn-sm '.$eachWindow["bid_windows_id"].' right text-light', 'value' => $eachWindow['bid_windows_id'].'-'.$get_bid_options[$columns]['option_id']))."</td>";
                                                        }else{
                                                            echo "<td>".form_button('buttoned[]','<i class="fa fa-edit"></i>',array('class' => 'btn btn-sm text-light edit', 'value' => $eachWindow['bid_windows_id'].'-'.$get_bid_options[$columns]['option_id']))."</td>";
                                                        };
                                                    };
                                                }; // end of for $columns

                                                echo "</tr>";
                                            }; // end of foreach($get_bid_windows as $eachWindow)
                                    ?>
                                </tbody>
                            </table>
                        </div>
                                    <?php
                                        //$buttonData = array('class' => 'btn-sm btn-info', 'id' => 'gridSave'); OR for below form_button('gridSave','Save Changes', $buttonData);
                                        echo form_submit('gridSave2','Save Changes', 'class="btn-sm btn-info" id="gridSave2"');
                    echo "</div>"; //end of id="gridTableContainer"
                    echo "<div class='gridError'></div>";
                                        //$textareaData = array('id' => 'addRemoveTb'); OR for below form_textarea('addRemoveTb','',$textareaData)
                                        //echo form_hidden('hiddenTextArea',form_textarea('addRemoveTb2','','id="addRemoveTb2"'));
                    echo form_textarea('addRemoveTb3','','id="addRemoveTb3"');
                                    echo form_close();
                                    ?>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header alert-success" id="headingFour">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                        Option Takeoffs using Icon Grid - PRODUCTION
                    </button>
                </h5>
            </div>

            <div id="collapseFour" class="collapse multi-collapse" aria-labelledby="headingFour">
                <div class="card-body">

                        <style>
                        @media print {
                            /* insert your style declarations here */

                            code, pre {
                                font-size: 8pt;
                                background-color: transparent;
                            /* just in case the user configured browser to print backgrounds */
                            }

                            blockquote {
                                background-image: none;
                            /* though they may not print, I’d rather be sure */
                            } 
                            
                            #nav, .card-header, .footer {
                                display: none;
                                border: none !important;
                                margin-left: 0px;
                                padding-left: 0px;
                                padding-bottom: 0px;
                            }

                            .card, #collapseFour, #card-body {
                                border: none !important;
                                margin-top: 0px;
                                margin-bottom: 0px;
                                margin-left: 0px;
                                padding-top: 0px;
                                padding-left: 0px;                               
                            }

                            #page_title {
                                font-size: 14pt;
                                text-indent:-9999px;
                            }

                            #page_title:before {
                                text-indent:400px;
                                content:'Option Takeoffs';
                                float: left;
                                /* border: 1px solid black; */
                            }

                            .imageLogo {
                                width: 130px !important;
                                height: 95px !important;
                                /* border: 1px solid black; */
                                margin: 0px;
                                padding: 0px;
                                float: left;
                            }

                            input {
                                border: none !important;
                                box-shadow: none !important;
                                outline: none !important;
                            }
                            /*
                                margin-left: 0px;
                                margin-top: 0px;
                                padding-left: 0px;
                                padding-bottom: 0px;
                            }
                            */

                            #builderName, #subdivisionName, #plan, #elevation {
                                background: #fff;
                                color: #000;
                                font-size: 12pt; 
                            }

                            .heading_row_one {
                                float: right;
                                width: 85%;
                                margin-left: 0px;
                                padding-left: 0px; 
                               /* border: 1px solid black; */                     
                            }

                            .print_builder_label {
                                text-align: right;
                                width: 60px;
                            }

                            .print_builder_data {
                                width: 300px;                                
                            }

                            .print_subdivision_label {
                                text-align: right;
                                width: 80px;
                            }

                            .print_subdivision_data {
                                width: 350px;
                            }

                            .heading_row_two {
                                width: 85%;
                                margin-left: 160px;
                                padding-left: 0px;
                              /*  border: 1px solid black;     */              
                            }

                            .print_plan_label {
                                text-align: right;
                                width: 60px;
                            }

                            .print_plan_data {
                                width: 300px;                                
                            }

                            .print_elevation_label {
                                text-align: right;
                                width: 80px;
                            }

                            .print_elevation_data {
                                width: 350px;
                            }

                            body {
                                background: #fff;
                                color: #000;
                                font-size: 10pt;
                                line-height: 100%;
                                margin: 0px;
                                padding: 0px;
                                border: none !important;
                            }
/*
                            .row {
                                width: 70%;
                                border: none;
                                margin-top: 0px;
                                margin-left: 0px;
                                padding-left: 0px; 
                            }
*/

                            hr {
                                width: 100%;
                                margin-top: 5px;
                                margin-bottom: 5px;
                                margin-left: -37px;
                                padding: 0px;
                            }

                            .print_full_row {
                                width: 70%;
                                margin-left: 0px;
                                padding-left: 0px;
                                border: none;
                            }

                            .print_window_details {
                                width: 40%;
                                margin-left: 12px;
                                padding-left: 0px; 
                            }

                            .print_window_location {
                                width: 30%;
                                margin-left: 12px;
                                padding-left: 0px;
                            }

                        }
                        </style>

                    <div class="container">
                        <div id="takeoffResults" name="takeoffResults"></div>
                    </div>
               
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header alert-success" id="headingFive">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                        Option Simulator for New Icon driven Grid - PRODUCTION
                    </button>
                </h5>
            </div>
            <div id="collapseFive" class="collapse multi-collapse" aria-labelledby="headingFive">
                <div class="card-body">
                    <div id="optionCreation" name="optionCreation">
                        <div id="optionList" name="optionList">

                            <div class="row">
                                <div class=""></div>
                                <div class="col-sm-6 text-right">XO's Name for Option</div>
                                <div class="col-sm-2">Builder's Code</div>
                                <div class="col-sm-1">Co$t</div>
                                <div class="col-sm-1"><button class="btn-sm btn-info" id="simulate2" name="simulate2">Simulate</button></div>
                            </div>
                            <?php foreach($get_bid_options as $optionItem){ ?>
                            <div class="row hoverDiv">
                                <div class="col-sm-6 text-right"><?php echo $optionItem['option_name']; ?></div>
                                <div class="col-sm-2"><?php echo $optionItem['planOptionCode']; ?></div>
                                <div class="col-sm-1"><?php echo $optionItem['cost']; ?></div>
                                <?php
                                    $optionIdName = implode(',', array($optionItem['option_id'],$optionItem['option_name']));
                                    if($optionItem['option_name'] == 'LHAND' || $optionItem['option_name'] == 'RHAND'){
                                        //echo "<td class='text-center'>".form_radio('optionSimulationChecked[]',$optionItem['option_id'],FALSE)."</td>";
                                        echo "<div class='col-sm-1 my-auto'>".form_radio('optionSimulationChecked[]',$optionIdName,FALSE)."</div>";
                                    }else{
                                        //echo "<td class='text-center'>".form_checkbox('optionSimulationChecked[]',$optionItem['option_id'],FALSE)."</td>";
                                        echo "<div class='col-sm-1 my-auto'>".form_checkbox('optionSimulationChecked[]',$optionIdName,FALSE)."</div>";
                                    };
                                ?>
                            </div>
                            <?php }; ?>

                        </div>
                    </div>
                    <div class="container">
                        <div id="simulationResults2" name="simulationResults2"></div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header alert-danger" id="headingSix">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                        Windows in Options Grid - TEST
                    </button>
                </h5>
            </div>

            <div id="collapseSix" class="collapse multi-collapse" aria-labelledby="headingSix">
                <div class="card-body">
                    <div id="testGridContainer">
                        <div class="testGridHeadings">
                        <table>
                            <thead>
                                <tr>
                                    <th valign="bottom" align="center"><strong>Details</strong></th>
                                    <th valign="bottom" align="center"><strong>Loc</strong></th>
                                        <?php foreach($get_bid_options as $optionItem){ ?>
                                    <th class="rotate"><div><span><?php echo $optionItem['option_name']; ?></span></div></th>
                                        <?php }; ?>
                                </tr>
                            </thead>                           
                                <tbody id="sortable">
                                    <?php
                                        foreach($get_bid_windows as $eachWindow){
                                            echo "<tr class='table-bordered ui-state-default' id='windowID-".$eachWindow['bid_windows_id']."'><td>".$eachWindow['window_details']."</td><td>".$eachWindow['window_location']."</td>";
                                            for($columns = 0; $columns < count($get_bid_options); $columns++){
                                                if(verifyCrossRef($option_has_windows, array('option_id', 'tbl_bid_windows_bid_windows_id'), array($get_bid_options[$columns]['option_id'],$eachWindow['bid_windows_id']))){
                                                    $tf = TRUE;
                                                }else{
                                                    $tf = FALSE;
                                                };
                                                echo "<td>".form_checkbox('selected[]',$eachWindow['bid_windows_id'].'-'.$get_bid_options[$columns]['option_id'],$tf)."</td>";
                                            }; // end of for $columns
                                            echo "</tr>";
                                        }; // end of foreach($get_bid_windows as $eachWindow)
                                    ?>
                                </tbody>
                        </table>
                    </div>
                    <textarea id="addRemoveTb" /hidden></textarea>
                    <div class="col-md-12"><button class="btn-sm btn-info" id="gridSave" name="gridSave">Save Changes</button></div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header alert-danger" id="headingSeven">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                        Windows in Options Grid (Save using CI forms coding) - TEST
                    </button>
                </h5>
            </div>

            <div id="collapseSeven" class="collapse multi-collapse" aria-labelledby="headingSeven">
                <div class="card-body">
                    <div id="gridTableContainer">

                            <table>
                        <div class="gridTableHeadings">
                                <thead>
                                    <tr>
                                        <th valign="bottom" align="center"><strong>Details</strong></th>
                                        <th valign="bottom" align="center"><strong>Loc</strong></th>
                                        <?php foreach($get_bid_options as $optionItem){ ?>
                                            <th class="rotate"><div><span><?php echo $optionItem['option_name']; ?></span></div></th>
                                        <?php }; ?>
                                    </tr>
                                </thead>
                        </div>
                            <tbody class="scrollContent">
                                <?php
                                    echo form_open(base_url('bids/editbid_grid_update_forms'), array('class' => 'gridform'));
                                        foreach($get_bid_windows as $eachWindow){
                                            echo "<tr class='table-bordered table-sm'><td>".$eachWindow['window_details']."</td><td>".$eachWindow['window_location']."</td>";
                                                for($columns = 0; $columns < count($get_bid_options); $columns++){
                                                    if(verifyCrossRef($option_has_windows, array('option_id', 'tbl_bid_windows_bid_windows_id'), array($get_bid_options[$columns]['option_id'],$eachWindow['bid_windows_id']))){
                                                        $tf = TRUE;
                                                    }else{
                                                        $tf = FALSE;
                                                    };
                                                    echo "<td>".form_checkbox('selected[]',$eachWindow['bid_windows_id'].'-'.$get_bid_options[$columns]['option_id'],$tf)."</td>";
                                                }; // end of for $columns
                                            echo "</tr>";
                                        }; // end of foreach($get_bid_windows as $eachWindow)
                                ?>
                            </tbody>
                            </table>

                                <?php
                                            //$buttonData = array('class' => 'btn-sm btn-info', 'id' => 'gridSave'); OR for below form_button('gridSave','Save Changes', $buttonData);
                            echo form_submit('gridSave','Save Changes', 'class="btn-sm btn-info" id="gridSave"');
                    echo "</div>"; //end of id="gridTableContainer"
                    echo "<div class='gridError'></div>";
                                            //$textareaData = array('id' => 'addRemoveTb'); OR for below form_textarea('addRemoveTb','',$textareaData)
                                        //echo form_hidden('hiddenTextArea',form_textarea('addRemoveTb2','','id="addRemoveTb2"'));
                    echo form_textarea('addRemoveTb2','','id="addRemoveTb2"');
                                    echo form_close();
                                ?>
                    </div>
                </div>
            </div>
        </div>



        <div class="card">
            <div class="card-header alert-danger" id="headingEight">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="true" aria-controls="collapseEight">
                        Option Takeoffs (based on Windows in Options Grid) - Non Icon Grid - Under Construction - TEST
                    </button>
                </h5>
            </div>

            <div id="collapseEight" class="collapse multi-collapse" aria-labelledby="headingEight">
                <div class="card-body">
                    <?php
                        $baseWindowBucket = array();
                        $notBaseWindowBucket = array();
                        $notOptionWindowBucket = array();
                        $optionWindowBucket = array();
                        foreach($get_bid_options as $optionTakeoff){
                            foreach($get_bid_windows as $optionTakeoffWindow){ // take each window in the bid and if it is found under BASE column (checked); put in its own array
                                if($optionTakeoff['option_name'] == 'BASE'){ // for the option named BASE that every bid will have
                                    if(verifyCrossRef($option_has_windows, array('option_id', 'tbl_bid_windows_bid_windows_id'), array($optionTakeoff['option_id'], $optionTakeoffWindow['bid_windows_id']))){
                                        //building the BASE array to compare other options
                                        $baseWindowBucket[] = array('option_id' => $optionTakeoff['option_id'], 'option_name' => $optionTakeoff['option_name'], 'bid_windows_id' => $optionTakeoffWindow['bid_windows_id'],'window_details' => $optionTakeoffWindow['window_details'],'window_location' => $optionTakeoffWindow['window_location']);
                                    }else{ // otherwise put in a separate array
                                        $notBaseWindowBucket[] = array('option_id' => $optionTakeoff['option_id'], 'option_name' => $optionTakeoff['option_name'], 'bid_windows_id' => $optionTakeoffWindow['bid_windows_id'],'window_details' => $optionTakeoffWindow['window_details'],'window_location' => $optionTakeoffWindow['window_location']);
                                    };

                                }else{ // not the BASE option take each window in the bid and if it is found under CURRENT column (checked); put in its own array
                                    if($optionTakeoff['option_name'] != 'LHAND' && $optionTakeoff['option_name'] != 'RHAND') {
                                        if(verifyCrossRef($option_has_windows, array('option_id', 'tbl_bid_windows_bid_windows_id'), array($optionTakeoff['option_id'], $optionTakeoffWindow['bid_windows_id']))){
                                            //building THIS options list of windows to compare to BASE
                                            $optionWindowBucket[] = array('option_id' => $optionTakeoff['option_id'], 'option_name' => $optionTakeoff['option_name'], 'bid_windows_id' => $optionTakeoffWindow['bid_windows_id'],'window_details' => $optionTakeoffWindow['window_details'],'window_location' => $optionTakeoffWindow['window_location']);
                                        }else{ // otherwise put in a separate array
                                            $notOptionWindowBucket[] = array('option_id' => $optionTakeoff['option_id'], 'option_name' => $optionTakeoff['option_name'], 'bid_windows_id' => $optionTakeoffWindow['bid_windows_id'],'window_details' => $optionTakeoffWindow['window_details'],'window_location' => $optionTakeoffWindow['window_location']);
                                        };
                                    };
                                };
                            };
                        };
                        echo "<div class='container'>";
                            // Take all of the bids options, excluding BASE, LHAND, RHAND show what each option adds / removes
                            foreach($get_bid_options as $optionTakeoff){
                                if($optionTakeoff['option_name'] != 'BASE' && $optionTakeoff['option_name'] != 'LHAND' && $optionTakeoff['option_name'] != 'RHAND'){
                                    echo "<table class='table table-bordered table-sm'>";
                                    echo "<thead>";
                                    echo "<tr><td colspan='2'>Option Name: <strong>".$optionTakeoff['option_name']."</strong></td></tr></thead>";

                                    $addedWindows = array();
                                    $removedWindows = array();
                                    // Using the BASE windows that are CHECKED
                                    foreach($baseWindowBucket as $bWB){
                                        // if the current window that is checked under BASE column is UNCHECKED under the current Option, put in removed array
                                        if(verifyCrossRef($notOptionWindowBucket, array('bid_windows_id', 'option_id'), array($bWB['bid_windows_id'], $optionTakeoff['option_id']))){
                                            $removedWindows[] = array('option_name' => $bWB['option_name'], 'window_details' => $bWB['window_details'], 'window_location' => $bWB['window_location']);
                                        };
                                    }; // end $bWB
                                    // Using the BASE windows that are UNCHECKED
                                    foreach($notBaseWindowBucket as $nBW){
                                        // if the current window that is unchecked under the BASE column is CHECKED under the current Option, put in added array
                                        if(verifyCrossRef($optionWindowBucket, array('bid_windows_id', 'option_id'), array($nBW['bid_windows_id'], $optionTakeoff['option_id']))){
                                            $addedWindows[] = array('option_name' => $bWB['option_name'], 'window_details' => $nBW['window_details'], 'window_location' => $nBW['window_location']);
                                        };
                                    }; // end $nBW
                                // Referencing the Removes and Adds arrays
                                    // if Removes is populated
                                    if(!empty($removedWindows)){
                                        echo "<tr><td colspan='2'>&nbsp;&nbsp;&nbsp;<em>Removes</em></td></tr>";
                                        // go through each one and add to table
                                        foreach($removedWindows as $rWs){
                                            echo "<tr><td>&nbsp;&nbsp;&nbsp;<strong>-</strong>&nbsp;&nbsp;&nbsp;".$rWs['window_details']."</td><td>&nbsp;&nbsp;&nbsp;".$rWs['window_location']."</td></tr>";
                                        }; // end $rWs
                                        $removedWindows = array(); // reset the array for the next Option
                                    };
                                    // if Adds is populated
                                    if(!empty($addedWindows)){
                                        echo "<tr><td colspan='2'>&nbsp;&nbsp;&nbsp;<em>Adds</em></td></tr>";
                                        // go through each one and add to table
                                        foreach($addedWindows as $aWs){
                                            echo "<tr><td>&nbsp;&nbsp;&nbsp;<strong>+</strong>&nbsp;&nbsp;&nbsp;".$aWs['window_details']."</td><td>&nbsp;&nbsp;&nbsp;".$aWs['window_location']."</td></tr>";
                                        }; // end $aWs
                                        $addedWindows = array(); // reset the array for the next Option
                                    };
                                echo "</table>";
                                }; // end of if($optionTakeoff......
                            }; // end foreach($get_bid_options as $optionTakeoff)
                        echo "</div>";
                    ?>               
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header alert-danger" id="headingNine">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="true" aria-controls="collapseNine">
                        Option Simulator - TEST and PRODUCTION (loaded settings grid needs to be moved)
                    </button>
                </h5>
            </div>
            <div id="collapseNine" class="collapse multi-collapse" aria-labelledby="headingNine">
                <div class="card-body">
                    <div id="optionCreation" name="optionCreation">
                        <div id="optionList" name="optionList">

                            <div class="row">
                                <div class=""></div>
                                <div class="col-sm-6 text-right">XO's Name for Option</div>
                                <div class="col-sm-2">Builder's Code</div>
                                <div class="col-sm-1">Co$t</div>
                                <div class="col-sm-1"><button class="btn-sm btn-info" id="simulate" name="simulate">Simulate</button></div>
                            </div>
                            <?php foreach($get_bid_options as $optionItem){ ?>
                            <div class="row hoverDiv">
                                <div class="col-sm-6 text-right"><?php echo $optionItem['option_name']; ?></div>
                                <div class="col-sm-2"><?php echo $optionItem['planOptionCode']; ?></div>
                                <div class="col-sm-1"><?php echo $optionItem['cost']; ?></div>
                                <?php
                                    $optionIdName = implode(',', array($optionItem['option_id'],$optionItem['option_name']));
                                    if($optionItem['option_name'] == 'LHAND' || $optionItem['option_name'] == 'RHAND'){
                                        //echo "<td class='text-center'>".form_radio('optionSimulationChecked[]',$optionItem['option_id'],FALSE)."</td>";
                                        echo "<div class='col-sm-1 my-auto'>".form_radio('optionSimulationChecked[]',$optionIdName,FALSE)."</div>";
                                    }else{
                                        //echo "<td class='text-center'>".form_checkbox('optionSimulationChecked[]',$optionItem['option_id'],FALSE)."</td>";
                                        echo "<div class='col-sm-1 my-auto'>".form_checkbox('optionSimulationChecked[]',$optionIdName,FALSE)."</div>";
                                    };
                                ?>
                            </div>
                            <?php }; ?>

                        </div>
                    </div>

                    <div class="container">
                        <div id="simulationResults" name="simulationResults"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
    $(document).ready(function(){

        //$('#debug').hide();

//on load, record any preset items from db
//for mfg changes, load new types and red button any that the new mfg does not have available
//if above, prevent the saving of the record until the selection is resolved

        $('#builderName').attr('disabled', true);
        $('#subdivisionName').attr('disabled', true);
        $('#plan').attr('disabled', true);
        $('#elevation').attr('disabled', true);

        $('#typeInstall').hide();
        $('#mfgForBid').hide();
        $('#typeSelection').hide();
            $('#color').hide();
            $('#colorPicked').hide();
            $('#coating').hide();
            $('#coatingPicked').hide();
            $('#material').hide();
            $('#materialPicked').hide();
            $('#fin').hide();
            $('#finPicked').hide();
        $('#addRemoveTb2').hide();
        $('#addRemoveTb3').hide();
        $('#gridSave2').hide();
        $('#simulationResults').hide();


        $( '#sortable' ).sortable({
            cursor: 'move',
            update: function (event, ui) {
                var data = $(this).sortable('serialize');
                $.ajax({
                    data: data,
                    type: 'POST',
                    url: '<?php echo base_url();?>index.php/bids/bid_window_sort_order_change'
                });
            }
        });
        $( '#sortable' ).disableSelection();


/*
        $("#sortable").on("sortchange", function(event, ui){
            var sorted = $( "#sortable" ).sortable( "serialize", { key: "sortable" });
            console.log(sorted);        
        });
*/

        //$('#install').val($bid_details[0]['tbl_install_types_install_types_id']);
     /*   
        $('form.jsform').on('submit', function(form){
            form.preventDefault();
            $.post('<?php //echo base_url();?>index.php/bids/check_bid_form', $('form.jsform').serialize(), function(data){
                $('div.jsError').html(data);
            });
        });
    */
//need to set the selected install type if edit is loading existing bid??
        //$('#install').val($("[name='builderInstallTypeIdOnLoad']").val());

        //if($('#install').val($("[name='builderInstallTypeIdOnLoad']").val()) != 'Choose Type'){
        //    $('#install').hide();
        //    $('#typeInstall').show().html($("[name='builderInstallTypeNameOnLoad']").val());
            //alert('Got this far!');
        //}

                    if($("[name='builderInstallTypeIdOnLoad']").val() != ''){
                        var selectedInstall = $("[name='builderInstallTypeNameOnLoad']").val();
                        $('#install').hide();
                        $('#typeInstall').html(selectedInstall);
                        $('#typeInstall').show();
                        //$('#typeInstall').attr('disabled', true);
                    }

                    //$('#install').on('change',function(){
                    $('#install').change(function(){
                        var selectedInstallID = $(this).val();
                        var selectedInstall = $('[name="install"] option:selected').text().toUpperCase();
                        $('#install').hide();
                        $('#typeInstall').show();
                        $('#typeInstall').html(selectedInstall);
                        $('#builderInstallTypeChange').val(selectedInstallID);
                    });

                    //$('#typeInstall').on('click',function(){
                    $('#typeInstall').dblclick(function(){
                        var selectedInstallID = "";
                        var selectedInstall = "";
                        $(this).hide();
                        $('#install').show();
                        $('#install').val(0);
                        $('#builderInstallTypeChange').val('');
                    });
/*
        $('#install').on('change', function(){
            if($("[name='install']").val() != 'Choose Type'){
                $('#builderInstallTypeChange').val($("[name='install']").val());
                $('#builderInstallTypeNameOnLoad').val($("[name='typeInstall']").text());
            }
        });

*/
                    if($('[name="manufacturerIdOnLoad"]').val() != ''){
                        var selectedMfg = $('[name="manufacturerNameOnLoad"]').val();
                        var selectedMfgID = $('[name="manufacturerIdOnLoad"]').val();
                        $('#manufacturer').hide();
                        $('#mfgForBid').html(selectedMfg);
                        $('#mfgForBid').val(selectedMfgID);
                        $('#mfgForBid').show();
                        $('#typeSelection').show();
                            $('#color').show();
                            $('#coating').show();
                            $('#material').show();
                            $('#fin').show();       
                    }

                    $('#manufacturer').change(function(){
                        if($('[name="manufacturer"] option:selected').text() != 'Choose Manufacturer'){
                            var selectedMfgID = $(this).val();
                            var selectedMfg = $('[name="manufacturer"] option:selected').text();
                            $('#manufacturer').hide();
                            $('#mfgForBid').show();
                            $('#mfgForBid').html(selectedMfg);
                            $('#manufacturerIdChange').val(selectedMfgID);
                            $('#typeSelection').show();
                                $('#color').show();
                                $('#coating').show();
                                $('#material').show();
                                $('#fin').show();
                        }
                    });

                    $('#mfgForBid').dblclick(function(){
                        if($('[name="manufacturerIdOnLoad"]').val() != ''){
                            var selectedMfg = $('[name="manufacturerNameOnLoad"]').val();
                            var selectedMfgID = $('[name="manufacturerIdOnLoad"]').val();
                            //add a are you sure prompt since it was loaded from db
                            //and other instructions
                        }else{
                            var selectedMfgID = "";
                            var selectedMfg = "";
                            var colorIdChange = "";
                            var coatingIdChange = "";
                            var materialIdChange = "";
                            var finIdChange = "";
                            $(this).hide();
                            $('#manufacturer').show();
                            $('#manufacturer').val(0);
                            $('#manufacturerIdChange').val(selectedMfgID);
                            $('#colorIdChange').val(colorIdChange);
                            $('#colorPicked').hide();
                            $('#coatingIdChange').val(coatingIdChange);
                            $('#coatingPicked').hide();
                            $('#materialIdChange').val(materialIdChange);
                            $('#materialPicked').hide();
                            $('#finIdChange').val(finIdChange);
                            $('#finPicked').hide();
                            $('#typeSelection').hide();
                        }
                        //$('#colorPicked').trigger('click');
                        //$('#colorPicked').hide();
                        //$('#colorPicked').val(selectedColorID);
                        //$('#color').show();
                        //$('#color').val(0);
                        //$('#color').html('Color');
                        //$('#colorIdChange').val(selectedColorID);
                        //$('#coatingPicked').hide();
                        //$('#coatingPicked').val(selectedCoatingID);
                        //$('#coating').show();
                        //$('#coating').val(0);
                        //$('#coating').html('Coating');
                        //$('#coatingIdChange').val(selectedCoatingID);
                        //$('#manufacturer').trigger('change');
                    });

        if($('[name="colorIdOnLoad"]').val() != ''){
            var selectedColor = $('[name="colorNameOnLoad"]').val();
            var selectedColorID = $('[name="colorIdOnLoad"]').val();
            $('#color').hide();
            $('#colorPicked').html(selectedColor);
            $('#colorPicked').val(selectedColor);
            $('#colorPicked').show();
        }

        $('#color').change(function(){
            if($('[name="color"] option:selected').text() != 'SELECT COLOR'){
                var selectedColorID = $(this).val();
                var selectedColor = $('[name="color"] option:selected').text();
                $('#color').hide();
                $('#colorPicked').show();
                $('#colorPicked').html(selectedColor);
                $('#colorIdChange').val(selectedColorID);
            }
        });

        $('#colorPicked').dblclick(function(){
            if(selectedColorID != ''){
            var selectedColorID = '';
            var selectedColor = '';
            $(this).hide();
            $('#color').show();
            $('#color').val(0);
            $('#colorIdChange').val(selectedColorID);
            }
        });

        if($('[name="coatingIdOnLoad"]').val() != ''){
            var selectedCoating = $('[name="coatingNameOnLoad"]').val();
            var selectedCoatingID = $('[name="coatingIdOnLoad"]').val();
            $('#coating').hide();
            $('#coatingPicked').html(selectedCoating);
            $('#coatingPicked').val(selectedCoating);
            $('#coatingPicked').show();
        }

        $('#coating').change(function(){
            var selectedCoatingID = $(this).val();
            var selectedCoating = $('[name="coating"] option:selected').text();
            $('#coating').hide();
            $('#coatingPicked').show();
            $('#coatingPicked').html(selectedCoating);
            $('#coatingIdChange').val(selectedCoatingID);
        });

        $('#coatingPicked').dblclick(function(){
            var selectedCoatingID = "";
            var selectedCoating = "";
            $(this).hide();
            $('#coating').show();
            $('#coating').val(0);
            $('#coatingIdChange').val(selectedCoatingID);
        });

        if($('[name="materialIdOnLoad"]').val() != ''){
            var selectedMaterial = $('[name="materialNameOnLoad"]').val();
            var selectedMaterialID = $('[name="materialIdOnLoad"]').val();
            $('#material').hide();
            $('#materialPicked').html(selectedMaterial);
            $('#materialPicked').val(selectedMaterial);
            $('#materialPicked').show();
        }

        $('#material').change(function(){
            var selectedMaterialID = $(this).val();
            var selectedMaterial = $('[name="material"] option:selected').text();
            $('#material').hide();
            $('#materialPicked').show();
            $('#materialPicked').html(selectedMaterial);
            $('#materialIdChange').val(selectedMaterialID);
        });

        $('#materialPicked').dblclick(function(){
            var selectedMaterialID = "";
            var selectedMaterial = "";
            $(this).hide();
            $('#material').show();
            $('#material').val(0);
            $('#materialIdChange').val(selectedMaterialID);
        });

        if($('[name="finIdOnLoad"]').val() != ''){
            var selectedFin = $('[name="finNameOnLoad"]').val();
            var selectedFinID = $('[name="finIdOnLoad"]').val();
            $('#fin').hide();
            $('#finPicked').html(selectedFin);
            $('#finPicked').val(selectedFin);
            $('#finPicked').show();
        }

        $('#fin').change(function(){
            var selectedFinID = $(this).val();
            var selectedFin = $('[name="fin"] option:selected').text();
            $('#fin').hide();
            $('#finPicked').show();
            $('#finPicked').html(selectedFin);
            $('#finIdChange').val(selectedFinID);
        });

        $('#finPicked').dblclick(function(){
            var selectedFinID = "";
            var selectedFin = "";
            $(this).hide();
            $('#fin').show();
            $('#fin').val(0);
            $('#finIdChange').val(selectedFinID);
        });

    $('#manufacturer').change(function(){
        var manufacturerID = $(this).val();
        var selectedMfg = $('[name="manufacturer"] option:selected');       
        if(manufacturerID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url();?>index.php/bids/get_mfg_colors',
                data:'manufacturer_id='+manufacturerID,
                success:function(data){
                    $('#color').html('<option value="0">SELECT COLOR</option>');
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.window_color_id).text(this.window_color_name);
                            $('#color').append(option);
                        });
                    }
                }
            }); // end colors AJAX 
            $.ajax({
                type:'POST',
                url:'<?php echo base_url();?>index.php/bids/get_mfg_materials',
                data:'manufacturer_id='+manufacturerID,
                success:function(data){
                    $('#material').html('<option value="0">SELECT MATERIAL</option>');
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.window_material_id).text(this.window_material_name);
                            $('#material').append(option);
                        });
                    }
                }
            }); // end materials AJAX
            $.ajax({
                type:'POST',
                url:'<?php echo base_url();?>index.php/bids/get_mfg_coatings',
                data:'manufacturer_id='+manufacturerID,
                success:function(data){
                    $('#coating').html('<option value="0">SELECT COATING</option>');
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.window_coating_id).text(this.window_coating_name);
                            $('#coating').append(option);
                        });
                    }
                }
            }); // end coatings AJAX
            $.ajax({
                type:'POST',
                url:'<?php echo base_url();?>index.php/bids/get_mfg_fins',
                data:'manufacturer_id='+manufacturerID,
                success:function(data){
                    $('#fin').html('<option value="0">SELECT FIN</option>');
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.window_fin_id).text(this.window_fin_name);
                            $('#fin').append(option);
                        });
                    }
                }
            }); // end fin AJAX
        }
    }); // end $('#manufacturer').change(function(){

    //Get Color ID selected //WHY is this in the code I copied?
    $('#color').change(function(){
        var selectedColor = $('[name="color"] option:selected');
        //$('#color').attr('disabled', 'true');
    });

        //Get Color ID selected //WHY is this in the code I copied?
    $('#coating').change(function(){
        var selectedCoating = $('[name="coating"] option:selected');
        //$('#coating').attr('disabled', 'true');
    });

    $('#material').change(function(){
        var selectedMaterial = $('[name="material"] option:selected');
        //$('#material').attr('disabled', 'true');
    });

    $('#fin').change(function(){
        var selectedFin = $('[name="fin"] option:selected');
        //$('#fin').attr('disabled', 'true');
    });

    $('input[name="selected[]"]').change(function(){
        var textarea = $('#addRemoveTb2');
        var item = $(this).val();
        if(this.checked){
            //alert(item+" checked");
            if(new RegExp( (item + "-0" + ","),"g" ).test(textarea.html())){
                textarea.html(textarea.html().replace(new RegExp( (item + "-0" + ","),"g" ),""));
            }else{
                textarea.append(item + "-1" + ",");                
            };
        }else{
            //alert(item+" unchecked");
            if(new RegExp( (item + "-1" + ","),"g" ).test(textarea.html())){
                textarea.html(textarea.html().replace(new RegExp( (item + "-1" + ","),"g" ),""));
            }else{
                textarea.append(item + "-0" + ",");               
            };

            textarea.html(textarea.html().replace(new RegExp( (item + "-1" + ","),"g" ),""));
         //alert(JSON.stringify(addRemoveArray))
        };
    });


    $('[name="buttoned[]"]').click(function(){
        // [ban = 0, plus = 1, edit = 2, left = 3, right = 4]-[insert = 1, update = 2, delete = 3]
        icon = $(this).find("i");
        buton = $(this).parent().find("button");
        var textarea = $('#addRemoveTb3');
        var item = $(this).val();
        var row = $(this).closest('tr');
        var columns = row.find('td');

        var leftArrow = columns[3]; //left arrow element, when BASE is gone it will be 3
        leftIcon = $(leftArrow).find("i");
        var leftCrossRef = $(leftArrow).find('button').val();
        var leftResult = $(leftArrow).find('button').attr('class').split(' ')[4];
        var leftResultBtnClass = $(leftArrow).find('button');

        var rightArrow = columns[4]; //right arrow element, when BASE is gone it will be 4
        rightIcon = $(rightArrow).find("i");
        var rightCrossRef = $(rightArrow).find('button').val();
        var rightResult = $(rightArrow).find('button').attr('class').split(' ')[4];
        var rightResultBtnClass = $(rightArrow).find('button');


//console.log($columns); // gives td in entire row (17) of them
//console.log(leftArrow); // gives the 5th element of the row which is the left arrow
//console.log(rightArrow); // gives the 6th element of the row which is the right arrow
//console.log("Icon: " + icon);
//console.log("Buton: " + buton);
//console.log("Left Result: " + leftResult);
//console.log("Right Result: " + rightResult);
//console.log("Right Result Button Class: " + rightResultBtnClass);
//console.log($(this).attr('class').split(' ')[2]); // gets the 33 in: btn btn-sm 33 left text-light

///////////////////////////////////////// ONCE BUTTON CHANGES THIS IS THE LOGIC THAT SETS IT UP FOR DB INTERACTION //////////////////////////////////////////

        // fa-edit icon - START
        if(icon.hasClass("fa-edit")){ // COMBINES the updating of the button AND the textarea tracking for DATABASE application
            if(!buton.hasClass("left text-light") && !buton.hasClass("right text-light")){
                icon.addClass("fa-plus").removeClass("fa-edit");
                buton.addClass("btn-success").removeClass("text-light");
                    if(new RegExp( (item + "-0-2" + ","),"g" ).test(textarea.html())){
                        textarea.html(textarea.html().replace(new RegExp( (item + "-0-2" + ","),"g" ),"")); // remove ban-update
                    }else if(new RegExp( (item + "-0-1" + ","),"g" ).test(textarea.html())){
                        textarea.html(textarea.html().replace(new RegExp( (item + "-0-1" + ","),"g" ),"")); // remove ban-insert
                    }else if(new RegExp( (item + "-2-3" + ","),"g" ).test(textarea.html())){
                        textarea.html(textarea.html().replace(new RegExp( (item + "-2-3" + ","),"g" ),"")); // remove ban-delete
                    };
                    if(buton.hasClass("ban")){ // initial setting was removes
                        textarea.append(item + "-1-2" + ","); // set to adds and update db
                    }else if(buton.hasClass("edit")){ // initial setting was nothing
                        textarea.append(item + "-1-1" + ","); // set to adds and insert into db
                    };

            }else if(buton.hasClass("left text-light") && rightResult != "btn-info"){ // Garage Left edit button that was clicked AND Garage Right NOT set
                icon.addClass("fa-arrow-left").removeClass("fa-edit");
                buton.addClass("btn-info").removeClass("text-light");

                if(new RegExp( (item + "-3-3" + ","),"g" ).test(textarea.html())){
                    textarea.html(textarea.html().replace(new RegExp( (item + "-3-3" + ","),"g" ),"")); // remove LEFT - DELETE if exists
                }else{               
                    textarea.append(item + "-3-1" + ",");
                };

            }else if(buton.hasClass("right text-light") && leftResult != "btn-info"){ // Garage Right edit button that was clicked AND Garage Left NOT set
                icon.addClass("fa-arrow-right").removeClass("fa-edit");
                buton.addClass("btn-info").removeClass("text-light");
                
                // INSERT into db garage RIGHT cross reference
                if(new RegExp( (item + "-4-3" + ","),"g" ).test(textarea.html())){
                    textarea.html(textarea.html().replace(new RegExp( (item + "-4-3" + ","),"g" ),"")); // remove RIGHT - DELETE if exists
                }else{
                    textarea.append(item + "-4-1" + ",");
                };

            }else if(buton.hasClass("left text-light") && rightResult == "btn-info"){ // Garage Left edit button was clicked AND Garage Right IS set
                rightIcon.addClass("fa-edit").removeClass("fa-arrow-right");
                $(rightResultBtnClass).addClass("text-light").removeClass("btn-info");
                icon.addClass("fa-arrow-left").removeClass("fa-edit");
                buton.addClass("btn-info").removeClass("text-light");

                if(new RegExp( (rightCrossRef + "-4-1" + ","),"g" ).test(textarea.html()) && new RegExp( (item + "-3-3" + ","),"g" ).test(textarea.html())){
                    textarea.html(textarea.html().replace(new RegExp( (rightCrossRef + "-4-1" + ","),"g" ),""));
                    textarea.html(textarea.html().replace(new RegExp( (item+ "-3-3" + ","),"g" ),""));
                }else if(new RegExp( (rightCrossRef + "-4-1" + ","),"g" ).test(textarea.html())){
                    textarea.html(textarea.html().replace(new RegExp( (rightCrossRef + "-4-1" + ","),"g" ),""));
                    textarea.append(item + "-3-1" + ",");
                }else if(new RegExp( (item + "-3-3" + ","),"g" ).test(textarea.html())){
                    textarea.html(textarea.html().replace(new RegExp( (item+ "-3-3" + ","),"g" ),""));
                    textarea.append(rightCrossRef + "-4-3" + ",");
                }else{
                    textarea.append(item + "-3-1" + ",");
                    textarea.append(rightCrossRef + "-4-3" + ",");
                };

            }else if(buton.hasClass("right text-light") && leftResult == "btn-info"){ // Garage Right edit button that was clicked AND Garage Left IS set
                leftIcon.addClass("fa-edit").removeClass("fa-arrow-left");
                $(leftResultBtnClass).addClass("text-light").removeClass("btn-info");
                icon.addClass("fa-arrow-right").removeClass("fa-edit");
                buton.addClass("btn-info").removeClass("text-light");

                if(new RegExp( (leftCrossRef + "-3-1" + ","),"g" ).test(textarea.html()) && new RegExp( (item + "-4-3" + ","),"g" ).test(textarea.html())){
                    textarea.html(textarea.html().replace(new RegExp( (leftCrossRef + "-3-1" + ","),"g" ),""));
                    textarea.html(textarea.html().replace(new RegExp( (item + "-4-3" + ","),"g" ),""));
                }else if(new RegExp( (leftCrossRef + "-3-1" + ","),"g" ).test(textarea.html())){
                    textarea.html(textarea.html().replace(new RegExp( (leftCrossRef + "-3-1" + ","),"g" ),""));
                    textarea.append(item + "-4-1" + ",");
                }else if(new RegExp( (item + "-4-3" + ","),"g" ).test(textarea.html())){
                    textarea.html(textarea.html().replace(new RegExp( (item + "-4-3" + ","),"g" ),""));
                    textarea.append(leftCrossRef + "-3-3" + ",");
                }else{
                    textarea.append(item + "-4-1" + ",");
                    textarea.append(leftCrossRef + "-3-3" + ",");                    
                };
            };
        // fa-edit icon - END

        }else if(icon.hasClass("fa-plus")){
            icon.addClass("fa-ban").removeClass("fa-plus");
            buton.addClass("btn-danger").removeClass("btn-success");
                if(new RegExp( (item + "-1-2" + ","),"g" ).test(textarea.html())){
                    textarea.html(textarea.html().replace(new RegExp( (item + "-1-2" + ","),"g" ),"")); // remove plus-update
                }else if(new RegExp( (item + "-1-1" + ","),"g" ).test(textarea.html())){
                    textarea.html(textarea.html().replace(new RegExp( (item + "-1-1" + ","),"g" ),"")); // remove plus-insert
                }else if(new RegExp( (item + "-2-3" + ","),"g" ).test(textarea.html())){
                    textarea.html(textarea.html().replace(new RegExp( (item + "-2-3" + ","),"g" ),"")); // remove edit-delete
                };
                if(buton.hasClass("plus")){ // initial setting was adds
                    textarea.append(item + "-0-2" + ","); // set to removes and update db
                }else if(buton.hasClass("edit")){ // initial setting was nothing
                    textarea.append(item + "-0-1" + ","); // set to removes and insert into db
                };

        }else if(icon.hasClass("fa-ban")){
            icon.addClass("fa-edit").removeClass("fa-ban");
            buton.addClass("text-light").removeClass("btn-danger");
                if(new RegExp( (item + "-0-2" + ","),"g" ).test(textarea.html())){
                   textarea.html(textarea.html().replace(new RegExp( (item + "-0-2" + ","),"g" ),"")); // remove ban-update
                }else if(new RegExp( (item + "-0-1" + ","),"g" ).test(textarea.html())){
                   textarea.html(textarea.html().replace(new RegExp( (item + "-0-1" + ","),"g" ),"")); // remove ban-insert
                }else if(new RegExp( (item + "-1-2" + ","),"g" ).test(textarea.html())){
                    textarea.html(textarea.html().replace(new RegExp( (item + "-1-2" + ","),"g" ),"")); // remove plus-update
                }else if(new RegExp( (item + "-1-1" + ","),"g" ).test(textarea.html())){
                    textarea.html(textarea.html().replace(new RegExp( (item + "-1-1" + ","),"g" ),"")); // remove plus-insert
                };
                if(buton.hasClass("ban") || buton.hasClass("plus")){ // initial setting was removes or adds
                    textarea.append(item + "-2-3" + ","); // set to nothing and delete from db
                };

        }else if(icon.hasClass("fa-arrow-left") && rightResult != "btn-info"){
            icon.addClass("fa-edit").removeClass("fa-arrow-left");
            buton.addClass("text-light").removeClass("btn-info");
                if(new RegExp( (item + "-3-1" + ","),"g" ).test(textarea.html())){
                    textarea.html(textarea.html().replace(new RegExp( (item + "-3-1" + ","),"g" ),"")); // remove LEFT - INSERT if exists, don't want duplicate entries
                }else{
                    textarea.append(item + "-3-3" + ",");
                };

        }else if(icon.hasClass("fa-arrow-right") && leftResult != "btn-info"){
            icon.addClass("fa-edit").removeClass("fa-arrow-right");
            buton.addClass("text-light").removeClass("btn-info");
                if(new RegExp( (item + "-4-1" + ","),"g" ).test(textarea.html())){
                    textarea.html(textarea.html().replace(new RegExp( (item + "-4-1" + ","),"g" ),"")); // remove LEFT - INSERT if exists, don't want duplicate entries
                }else{
                    textarea.append(item + "-4-3" + ",");
                }; 
        };
        if($('#addRemoveTb3').val() != ''){
            $('#gridSave2').show();
        }else{
            $('#gridSave2').hide();
        };

    });

    $('form.gridform').on('submit', function(form){
        form.preventDefault();
        var addedGridOptions2 = [];
        var tbl_bid_id = $('#tbl_bid_id').val();
        var tbl_buildercustomer_builder_id = $('#tbl_buildercustomer_builder_id').val();
        $('#addRemoveTb2').each(function(){
            addedGridOptions2.push($(this).val());
        });
        //addedGridOptions2.pop();

        //alert(JSON.stringify(addedGridOptions2));
        $.post('<?php echo base_url();?>index.php/bids/editbid_grid_update_forms', {changesToGrid2: addedGridOptions2}, function(data){
            $('div.gridError').html(data);
        });
        setTimeout(function(){
            window.location.href = "<?php echo base_url(); ?>index.php/bids/editbid/" + tbl_bid_id + "/" + tbl_buildercustomer_builder_id;
        }, 5000);
    });

    $('form.gridform2').on('submit', function(form){
        form.preventDefault();
        var addedGridOptions3 = [];
        var tbl_bid_id = $('#tbl_bid_id').val();
        var tbl_buildercustomer_builder_id = $('#tbl_buildercustomer_builder_id').val();
        $('#addRemoveTb3').each(function(){
            addedGridOptions3.push($(this).val());
        });
        //addedGridOptions2.pop();

        //alert(JSON.stringify(addedGridOptions2));
        $.post('<?php echo base_url();?>index.php/bids/editbid_grid_update_forms2', {changesToGrid3: addedGridOptions3}, function(data){
            $('div.gridError').html(data);
        });
        setTimeout(function(){
            window.location.href = "<?php echo base_url(); ?>index.php/bids/editbid/" + tbl_bid_id + "/" + tbl_buildercustomer_builder_id;
        }, 5000);
    });

    $('#gridSave').click(function(){
        var addedGridOptions = [];
        var removedGridOptions = [];
        var tbl_bid_id = $('#tbl_bid_id').val();
        var tbl_buildercustomer_builder_id = $('#tbl_buildercustomer_builder_id').val();
        $('#addRemoveTb').each(function(){
            addedGridOptions.push($(this).val());
        });
        if(addedGridOptions){
            $.ajax({
                type:'POST',
                url: '<?php echo base_url();?>index.php/bids/editbid_grid_update',
                data: {changesToGrid:addedGridOptions},
                success:function(data){
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        var to_insert = dataObj[0];
                        var to_remove = dataObj[1];
                        alert(JSON.stringify(to_insert));
                        alert(JSON.stringify(to_remove));
                    }
                }
            });
        }
        //alert(addedGridOptions);
        //setTimeout(function(){
        //    window.location.href = "<?php //echo base_url(); ?>index.php/bids/editbid/" + tbl_bid_id + "/" + tbl_buildercustomer_builder_id;
        //}, 5000);
    });

    $('.hoverDiv').hover(function(){
        $(this).css("background", "#f5f5f5");
    }, function(){
        $(this).css("background", "#fff");
    });

    $('#simulate').click(function(){
        if($(this).html() == 'Simulate'){
            var simulationOptions = [];
            var tbl_bid_id= $('#tbl_bid_id').val();
            $('#optionList input:checked').each(function(){
                simulationOptions.push($(this).val());
            });
            if(simulationOptions){
                $.ajax({
                    type:'POST',
                    url:'<?php echo base_url();?>index.php/bids/editbid_bid_simulation',
                    data:{simulationOptions:simulationOptions, tbl_bid_id_ajax:tbl_bid_id},
                    success:function(data){
                        var dataObj = jQuery.parseJSON(data);
                        var added = dataObj[0];
                        var removed = dataObj[1];
                        if(dataObj){
                            //$('#simulationResults').append("<thead><tr><td colspan='3'><strong>Simulation Results</strong></td></tr>");
                            //$('#simulationResults').append("<tr><td><strong>Details</strong></td><td><strong>Loc</strong></td><td><strong>Added by Option</strong></td></tr></thead>");
                            $('#simulationResults').append("<div class='row table-bordered'><div class='col-xl-12'><strong>Simulation Results</strong></div></div>");
                            $('#simulationResults').append("<div class='row'><div class='col-xl-5'><strong>Details</strong></div><div class='col-xl-2'><strong>Location</strong></div><div class='col-xl-3'><strong>Added by Option</strong></div></div>");
                            //$(added).each(function(){
                            //    var column1 = $('<tr />');
                            //    column1.html("<td>" + this.window_details1 + "</td><td>" + this.window_location1 + "</td><td>" + this.option_name1 + "</td>");
                            //    $('#simulationResults').append(column1);
                            //});
                            $(added).each(function(){
                                var column1 = $("<div class='row' />");
                                column1.html("<div class='col-xl-5'>" + this.window_details1 + "</div><div class='col-xl-2'>" + this.window_location1 + "</div><div class='col-xl-3'>" + this.option_name1 + "</div>");
                                $('#simulationResults').append(column1);
                            });
                            //$('#simulationResults').append("<tr><td colspan='3'><strong>Windows NOT USED</strong></td></tr>");
                            //$('#simulationResults').append("<tr><td><strong>Details</strong></td><td><strong>Location</strong></td><td><strong>Removed by Option</strong></td></tr>");
                            $('#simulationResults').append("<div class='row table-bordered'><div class='col-xl-12'><strong>Windows NOT USED</strong></div></div>");
                            $('#simulationResults').append("<div class='row'><div class='col-xl-5'><strong>Details</strong></div><div class='col-xl-2'><strong>Location</strong></div><div class='col-xl-3'><strong>Removed by Option</strong></div></div>");
                            //$(removed).each(function(){
                            //    var column1 = $('<tr />');
                            //    column1.html("<td>" + this.window_details1 + "</td><td>" + this.window_location1 + "</td><td>" + this.option_name1 + "</td>");
                            //    $('#simulationResults').append(column1);
                            //});
                            $(removed).each(function(){
                                var column1 = $("<div class='row' />");
                                column1.html("<div class='col-xl-5'>" + this.window_details1 + "</div><div class='col-xl-2'>" + this.window_location1 + "</div><div class='col-xl-3'>" + this.option_name1 + "</div>");
                                $('#simulationResults').append(column1);
                            });
                        };
                    }
                });
            };
            $('#simulationResults').show();
            $('#simulate').html("Clear");
        }else{
            $('input[name="optionSimulationChecked[]"]').prop('checked', false);
            $('#simulationResults').hide();
            $('#simulationResults').empty();
            $('#simulate').html("Simulate");
        }
    });


    $('#collapseFour').ready(function(){ // click to toggle the section open could launch this logic?
        var tbl_bid_id = $('#tbl_bid_id').val();
        if(tbl_bid_id){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url();?>index.php/bids/editbid_bid_takeoffs',
                data:{tbl_bid_id_ajax:tbl_bid_id},
                success:function(data){
                    var dataObj = jQuery.parseJSON(data);
                    var takeoffs = dataObj[0];
                    if(takeoffs){
                        for(option_name in takeoffs){
                            //alert(JSON.stringify(takeoffs[option_name]));
                            $('#takeoffResults').append("<hr>");
                            $('#takeoffResults').append("<div class='row alert-primary print_full_row'><div class='col-xl-12 print_full_row'>Option Name:&nbsp;&nbsp;<strong>" + [option_name] + "</strong></div></div>");
                            if(takeoffs[option_name].removed){
                                $('#takeoffResults').append("<div class='row table-bordered alert-danger print_full_row'><div class='col-xl-12'>&nbsp;&nbsp;&nbsp;<em><strong>Removes</strong></em></div></div>");
                                $(takeoffs[option_name].removed).each(function(){
                                    var column1 = $("<div class='row alert-danger print_full_row' />");
                                    column1.html("<div class='col-xl-5 print_window_details'>&nbsp;&nbsp;<strong>-</strong>&nbsp;&nbsp;&nbsp;" + this.window_details + "</div><div class='col-xl-3 print_window_location'>" + this.window_location + "</div>");
                                    $('#takeoffResults').append(column1);
                                });
                            };
                            if(takeoffs[option_name].added){
                            $('#takeoffResults').append("<div class='row table-bordered alert-success print_full_row'><div class='col-xl-12'>&nbsp;&nbsp;&nbsp;<em><strong>Adds</strong></em></div></div>");
                                $(takeoffs[option_name].added).each(function(){
                                    var column1 = $("<div class='row alert-success print_full_row' />");
                                    column1.html("<div class='col-xl-5 print_window_details'>&nbsp;&nbsp;<strong>+</strong>&nbsp;&nbsp;&nbsp;" + this.window_details + "</div><div class='col-xl-3 print_window_location'>" + this.window_location + "</div>");
                                    $('#takeoffResults').append(column1);
                                });
                            };
                        };
                        $('#takeoffResults').append("</div>");
                    };
                }
            });
        };
    });

/*

echo "<table class='table table-bordered table-sm'>";
                                    echo "<thead>";
                                    echo "<tr><td colspan='2'>Option Name: <strong>".$optionTakeoff['option_name']."</strong></td></tr></thead>";

                                    $addedWindows = array();
                                    $removedWindows = array();
                                    // Using the BASE windows that are CHECKED
                                    foreach($baseWindowBucket as $bWB){
                                        // if the current window that is checked under BASE column is UNCHECKED under the current Option, put in removed array
                                        if(verifyCrossRef($notOptionWindowBucket, array('bid_windows_id', 'option_id'), array($bWB['bid_windows_id'], $optionTakeoff['option_id']))){
                                            $removedWindows[] = array('option_name' => $bWB['option_name'], 'window_details' => $bWB['window_details'], 'window_location' => $bWB['window_location']);
                                        };
                                    }; // end $bWB
                                    // Using the BASE windows that are UNCHECKED
                                    foreach($notBaseWindowBucket as $nBW){
                                        // if the current window that is unchecked under the BASE column is CHECKED under the current Option, put in added array
                                        if(verifyCrossRef($optionWindowBucket, array('bid_windows_id', 'option_id'), array($nBW['bid_windows_id'], $optionTakeoff['option_id']))){
                                            $addedWindows[] = array('option_name' => $bWB['option_name'], 'window_details' => $nBW['window_details'], 'window_location' => $nBW['window_location']);
                                        };
                                    }; // end $nBW
                                // Referencing the Removes and Adds arrays
                                    // if Removes is populated
                                    if(!empty($removedWindows)){
                                        echo "<tr><td colspan='2'>&nbsp;&nbsp;&nbsp;<em>Removes</em></td></tr>";
                                        // go through each one and add to table
                                        foreach($removedWindows as $rWs){
                                            echo "<tr><td>&nbsp;&nbsp;&nbsp;<strong>-</strong>&nbsp;&nbsp;&nbsp;".$rWs['window_details']."</td><td>&nbsp;&nbsp;&nbsp;".$rWs['window_location']."</td></tr>";
                                        }; // end $rWs
                                        $removedWindows = array(); // reset the array for the next Option
                                    };
                                    // if Adds is populated
                                    if(!empty($addedWindows)){
                                        echo "<tr><td colspan='2'>&nbsp;&nbsp;&nbsp;<em>Adds</em></td></tr>";
                                        // go through each one and add to table
                                        foreach($addedWindows as $aWs){
                                            echo "<tr><td>&nbsp;&nbsp;&nbsp;<strong>+</strong>&nbsp;&nbsp;&nbsp;".$aWs['window_details']."</td><td>&nbsp;&nbsp;&nbsp;".$aWs['window_location']."</td></tr>";
                                        }; // end $aWs
                                        $addedWindows = array(); // reset the array for the next Option
                                    };
                                echo "</table>";

*/

    $('#simulate2').click(function(){
        if($(this).html() == 'Simulate'){
            var simulationOptions = [];
            var tbl_bid_id= $('#tbl_bid_id').val();
            $('#optionList input:checked').each(function(){
                simulationOptions.push($(this).val());
            });
            if(simulationOptions){
                $.ajax({
                    type:'POST',
                    url:'<?php echo base_url();?>index.php/bids/editbid_bid_simulation2',
                    data:{simulationOptions:simulationOptions, tbl_bid_id_ajax:tbl_bid_id},
                    success:function(data){
                        var dataObj = jQuery.parseJSON(data);
                        var base = dataObj[0];
                        //alert(JSON.stringify(base));
                        //var added = dataObj[1];
                        var removed = dataObj[2];
                        if(dataObj){
                            //$('#simulationResults').append("<thead><tr><td colspan='3'><strong>Simulation Results</strong></td></tr>");
                            //$('#simulationResults').append("<tr><td><strong>Details</strong></td><td><strong>Loc</strong></td><td><strong>Added by Option</strong></td></tr></thead>");
                            $('#simulationResults2').append("<div class='row table-bordered'><div class='col-xl-12'><strong>Simulation Results</strong></div></div>");
                            $('#simulationResults2').append("<div class='row'><div class='col-xl-5'><strong>Details</strong></div><div class='col-xl-2'><strong>Location</strong></div><div class='col-xl-3'><strong>Added by Option</strong></div></div>");
                            //$(base).each(function(){
                            //    var column1 = $("<div class='row' />");
                            //    column1.html("<div class='col-xl-5'>" + this.window_details + "</div><div class='col-xl-2'>" + this.window_location + "</div>");
                            //    $('#simulationResults2').append(column1);
                            //});

                            //$(added).each(function(){
                            //    var column1 = $('<tr />');
                            //    column1.html("<td>" + this.window_details1 + "</td><td>" + this.window_location1 + "</td><td>" + this.option_name1 + "</td>");
                            //    $('#simulationResults').append(column1);
                            //});
                            $(base).each(function(){ //renamed from $(added) to $(base)
                                var column1 = $("<div class='row' />");
                                column1.html("<div class='col-xl-5'>" + this.window_details + "</div><div class='col-xl-2'>" + this.window_location + "</div><div class='col-xl-3'>" + this.option_name + "</div>");
                                $('#simulationResults2').append(column1);
                            });
                            //$('#simulationResults').append("<tr><td colspan='3'><strong>Windows NOT USED</strong></td></tr>");
                            //$('#simulationResults').append("<tr><td><strong>Details</strong></td><td><strong>Location</strong></td><td><strong>Removed by Option</strong></td></tr>");
                            $('#simulationResults2').append("<div class='row table-bordered'><div class='col-xl-12'><strong>Windows NOT USED</strong></div></div>");
                            $('#simulationResults2').append("<div class='row'><div class='col-xl-5'><strong>Details</strong></div><div class='col-xl-2'><strong>Location</strong></div><div class='col-xl-3'><strong>Removed by Option</strong></div></div>");
                            //$(removed).each(function(){
                            //    var column1 = $('<tr />');
                            //    column1.html("<td>" + this.window_details1 + "</td><td>" + this.window_location1 + "</td><td>" + this.option_name1 + "</td>");
                            //    $('#simulationResults').append(column1);
                            //});
                            $(removed).each(function(){
                                var column1 = $("<div class='row' />");
                                column1.html("<div class='col-xl-5'>" + this.window_details + "</div><div class='col-xl-2'>" + this.window_location + "</div><div class='col-xl-3'>" + this.option_name + "</div>");
                                $('#simulationResults2').append(column1);
                            });
                        };
                    }
                });
            };
            $('#simulationResults2').show();
            $('#simulate2').html("Clear");
        }else{
            $('input[name="optionSimulationChecked[]"]').prop('checked', false);
            $('#simulationResults2').hide();
            $('#simulationResults2').empty();
            $('#simulate2').html("Simulate");
        }
    });

//$("#testGridContainer").on('scroll', function() {
//    delta = $(this).offset().top - $(this).find('table thead').offset().top;
//    $(this).find('table thead').children().children().css({
//        "transform": "translate(0px," + (delta > 0 ? delta : 0) + "px)"
//    });
//    return false;
//});


    });
</script>