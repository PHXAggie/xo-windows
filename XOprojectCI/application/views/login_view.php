<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>XO Windows Sign In</title>

		<link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
	</head>

	<body>
		<div class="container col-12">
			<div class="text-center">
				<div class="row justify-content-center">
					<div class="col-4">
						<nav class="navbar navbar-expand-lg navbar-dark bg-primary sticky-top">
			      			<a class="navbar-brand" href="<?php echo site_url(); ?>/home">XO Windows</a>
			  			</nav>
						<form class="form-signin" action="<?php echo site_url('login/auth');?>" method="post">
							<h2 class="form-signin-heading">Please sign in</h2>
							<?php echo $this->session->flashdata('msg');?>
							<!--<label for="username" class="sr-only">Username</label>
							<input type="email" name="email" class="form-control" placeholder="Email" required autofocus>-->
							<input name="username" class="form-control" placeholder="Username" required autofocus>
							<label for="email" class="sr-only">Email</label>
							<label for="password" class="sr-only">Password</label>
							<input type="password" name="password" class="form-control" placeholder="Password" required>
							<!-- <div class="checkbox">
								<label>
									<input type="checkbox" value="remember-me"> Remember me
								</label>
							</div>/remember me -->
							<button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div><!-- /container -->
      <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.3.1.js'); ?>"></script>
      <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
	</body>
</html>