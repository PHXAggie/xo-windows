<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Welcome</title>

		<link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<div class="row">
				<nav class="navbar navbar-expand-lg navbar-dark bg-primary sticky-top">
        <a class="navbar-brand" href="#">XO Windows Login Test</a>
          <button class="btn navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div id="navbar" class="collapse navbar-collapse">
            <ul class="navbar-nav">
              <!--ACCESS MENUS FOR ADMIN-->
              <?php if($this->session->userdata('level')==='1'):?>
                <li class="nav-item active"><a class="nav-link" href="#">Dashboard</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Posts</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Pages</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Media</a></li>
              <!--ACCESS MENUS FOR STAFF-->
              <?php elseif($this->session->userdata('level')==='2'):?>
                <li class="nav-item active"><a class="nav-link" href="#">Dashboard</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Pages</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Media</a></li>
              <!--ACCESS MENUS FOR AUTHOR-->
              <?php else:?>
                <li class="nav-item active"><a href="#">Dashboard</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Posts</a></li>
              <?php endif;?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="nav-item"><a class="nav-link" href="<?php echo site_url('login/logout');?>">Sign Out: <?php echo $this->session->userdata('username');?></a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </nav>
      </div><!-- /row -->
        <div class="jumbotron">
          <h1>Welcome Back <?php echo $this->session->userdata('username');?></h1>
        </div>
 


		</div><!-- /container -->
      <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.3.1.js'); ?>"></script>
	    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>  
	</body>
</html>