<div class="container">
	<div class="text-center">
		<h1><?php echo $section_title . '' . $section_action; ?></h1>

		<?php echo $section_search_field; ?>

		<table class="table table-bordered table-sm table-hover">
			<thead class="thead-light">
				<tr>
					<th scope="col">Builder</th>
					<th scope="col">Actions</th>
				</tr>
			</thead>
			<tbody>
				{builder_list}
				<tr>
					<th scope="row">views</th>

				</tr>
				{/builders_list}
			</tbody>
		</table>
	</div>
</div>