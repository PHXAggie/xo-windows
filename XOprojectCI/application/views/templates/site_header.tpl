<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
{ci_config name="base_url"}
  <head>
  	<meta charset="utf-8" />
  	<title>XO</title>
    <script type="text/javascript" src='{$base_url}assets/js/jquery-3.3.1.js'></script>
    <script type="text/javascript" src='{$base_url}assets/js/bootstrap.min.js'></script>
    <script src='{$base_url}assets/js/jquery-ui-1.12.1.custom/jquery-ui.js'></script>
  	<link rel='stylesheet' href='{$base_url}assets/css/bootstrap.min.css'>
    <link rel='stylesheet' href='{$base_url}assets/css/style.css'>
    <link rel='stylesheet' href='{$base_url}assets/css/font-awesome.css'>
    <link rel="icon" href='{$base_url}assets/images/favicon.ico' type="image/x-icon">
  </head>

  <body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary sticky-top">
      <a class="navbar-brand" href='{$base_url}'>XO Windows</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href='{$base_url}'>Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href='{$base_url}index.php/bids'>Bids</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href='{$base_url}<?php //index.php/starts ?>'>Starts</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Maintenance</a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <h6 class="dropdown-header">Maintenance</h6>
                <a class="dropdown-item" href='{$base_url}index.php/Maintenance/builders'>Builders</a>
                <a class="dropdown-item" href='{$base_url}index.php/Maintenance/installtypes'>Install Types</a>
                <a class="dropdown-item" href='{$base_url}<?php //index.php/Maintenance/manufacturers ?>'>Manufacturers</a>
                <a class="dropdown-item" href='{$base_url}index.php/itemCRUD/index'>ItemCRUD</a>
                <a class="dropdown-item" href='{$base_url}index.php/Maintenance/subdivisions'>Subdivisions</a>
                <a class="dropdown-item" href='{$base_url}<?php //index.php/Maintenance/windowtypes ?>'>Window Types</a>
              <div class="dropdown-divider"></div>
                <a class="dropdown-item" href='{$base_url}<?php //index.php/Maintenance/xousers ?>'>XO Users</a>
              </div>
          </li>
        </ul>
      </div>
    </nav>