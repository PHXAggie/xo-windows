<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Dropdown</title>
    <script src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.js"></script>
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" />
<style type="text/css">
	.select-boxes{width: 280px;text-align: center; margin: 0 auto;}
	select {
		background-color: #F5F5F5;
		border: 1px double #FB4314;
		color: #55BB91;
		font-family: Georgia;
		font-weight: bold;
		font-size: 14px;
		height: 39px;
		padding: 7px 8px;
		width: 350px;
		outline: none;
		margin: 10px 0 10px 0;
	}
	select option{
		font-family: Georgia;
		font-size: 14px;
	}
</style>

</head>
<body>
<!-- Country dropdown -->
<label for="builder">Builder</label>
<select class="form-control" id="builder">
    <option value="">SELECT BUILDER</option>
    <?php
    if(!empty($tbl_buildercustomer)){
        foreach($tbl_buildercustomer as $row){ 
            echo '<option value="'.$row['builder_id'].'">'.$row['builder_name'].'</option>';
        }
    }else{
        echo '<option value="">NO BUILDERS FOUND</option>';
    }
    ?>
</select>

<!-- State dropdown -->
<label for="subdivision">Subdivision</label>
<select class="form-control" id="subdivision">
    <option value="">Select BUILDER first</option>
</select>

<!-- City dropdown
<select id="city">
    <option value="">Select state first</option>
</select>
 -->

<script type="text/javascript">
$(document).ready(function(){
    /* Populate data to state dropdown*/
    $('#builder').on('change',function(){
        var builderID = $(this).val();        
        if(builderID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url();?>index.php/Dropdowns/getBuilderSubdivisions',
                data:'builder_id='+builderID,
                success:function(data){
                    $('#subdivision').html('<option value="">SELECT SUBDIVISION</option>');
                    var dataObj = jQuery.parseJSON(data);
                    //var dataObj = [{"subdivision_id":1, "subdivision_name":"GERMANN COUNTRY ESTATES"},{"subdivision_id":2, "subdivision_name":"CLEMENTE RANCH CHANDLER"}];
                    //var dataObj = '';  //triggers Not Available
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.subdivision_id).text(this.subdivision_name);
                            $('#subdivision').append(option);
                        });
                    }else{
                        $('#subdivision').html('<option value="">NO SUBDIVISIONS FOUND FOR BUILDER</option>');
                    }
                }
            }); 
        }else{
            $('#subdivision').html('<option value="">Select BUILDER first</option>');
            //$('#city').html('<option value="">Select state first</option>'); 
        }
    });


    /* Populate data to city dropdown
    $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'<?php //echo base_url('dropdowns/getCities'); ?>',
                data:'state_id='+stateID,
                success:function(data){
                    $('#city').html('<option value="">Select City</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.name);           
                            $('#city').append(option);
                        });
                    }else{
                        $('#city').html('<option value="">City not available</option>');
                    }
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
    */
});
</script>
</body>
</html>