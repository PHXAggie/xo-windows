<?php echo $form_to_open; ?>
    <div class="form-group row">
        <label for="blder" class="col-2 text-right">Builder:</label>
        <div class="col-5">
            <select class="form-control" id="builder" name="builderIdSelected">
                <option value="">SELECT BUILDER</option>
                <?php
                if(!empty($list_of_bldercustomers)){
                    foreach($list_of_bldercustomers as $rowItem){ 
                        echo '<option value="'.$rowItem['builder_id'].'">'.$rowItem['builder_name'].'</option>';
                    }
                }else{
                    echo '<option value="">NO BUILDERS FOUND</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="subdivision" class="col-2 text-right">Subdivision:</label>
        <div class="col-5">
            <select class="form-control" id="subdivision" name="subdivisionIdSelected"><option value="">Select BUILDER first</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="plan" class="col-2 text-right">Plan:</label>
        <div class="col-5">
            <input type="text" class="form-control" id="bidPlan" name="bidPlan">
        </div>
    </div>
    <div class="form-group row">
        <label for="elevation" class="col-2 text-right">Elevation:</label>
        <div class="col-5">
            <input type="text" class="form-control" id="bidElevation" name="bidElevation">
        </div>
    </div>

        <?php if($title == 'Add / Edit Bid'){
                echo '<div class="form-group row">';
                echo '<div class="col-10"><button type="submit" class="btn btn-info" id="submitType"></button>';
                echo '<button type="button" class="btn btn-danger" id="cancelEntry_edit">Cancel</button></div></div>';
            }else{
                echo '<div class="form-group row">';
                echo '<div class="col-10"><button type="submit" class="btn btn-danger pull-right" id="cancel_btn">Cancel</button>';
                echo '<button type="button" class="btn btn-danger" id="cancelEntry_edit">Cancel</button></div></div>';                          
            };
        ?>
<?php echo form_close(); ?><!--</form>-->


<script type="text/javascript">
	$(document).ready(function(){

        $('#submitType').hide();

		$('#builder').on('change',function(){
	    //clear PLAN and ELEVATION FIELDS
	        $('#bidPlan').val('').trigger('change');
	        $('#bidElevation').val('').trigger('change');

	        var builderID = $(this).val();
	        var selectedBldr = $('[name="builderIdSelected"] option:selected'); //get the builder record id of the builder name that was chosen by user
	        if(builderID){
	            $.ajax({
	                type:'POST',
	                url:'<?php echo base_url();?>index.php/bids/getBuilderSubdivisions', //post to Controllers->Bids->getBuilderSubdivisions
	                data:'builder_id='+builderID, //builder_id=146 for example
	                success:function(data){
	                    $('#subdivision').html('<option value="">SELECT SUBDIVISION</option>');
	                    var dataObj = jQuery.parseJSON(data);                        
	                    if(dataObj){
	                        if(Object.keys(dataObj).length == 1){
	                            $(dataObj).each(function(){
	                                var option = $('<option />');
	                                option.attr('value', this.subdivision_id).text(this.subdivision_name);
	                                option.attr('selected', 'true');
	                                $('#subdivision').append(option);
	                            });
	                        }else{
	                            $(dataObj).each(function(){
	                                var option = $('<option />');
	                                option.attr('value', this.subdivision_id).text(this.subdivision_name);
	                                $('#subdivision').append(option);
	                            });
	                        }
	                    }else{
	                        $('#subdivision').html('<option value="">NO SUBDIVISIONS FOUND</option>');
	                    }
	                }
	            }); 
	        }else{
	            $('#subdivision').html('<option value="">Select BUILDER first</option>');
	        }
	    });
        $('#subdivision').on('change',function(){
            $('#bidPlan').val('').trigger('change');
            $('#bidElevation').val('').trigger('change');
            var selectedSub = $('[name="subdivisionIdSelected"] option:selected');
        });

        $('#bidPlan').autocomplete({
            source: function(request, response){
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url()?>/bids/get_plans',
                    dataType: 'json',
                    data: {
                        searchPlan: request.term,
                        subdivision_id: $('#subdivision').val()
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            },            
            minLength: 0,
            autoFocus: true,
            response: function (event, ui) {
                if (ui.content.length == 0){
                    ui.content.push({
                        label: 'New Plan: ' + $(this).val(),
                        value: $(this).val(),
                        id: 0
                    });
                }
            }
        }).on('autocompleteselect', function (event, ui) {
            $('#bidPlan').val(ui.item.value);
            return false;
        });

        $('#bidElevation').autocomplete({
            source: function(request, response){
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url()?>/bids/get_elevations',
                    dataType: 'json',
                    data: {
                        searchElevation: request.term,
                        planSelected: $('#bidPlan').val(),
                        subdivisionSelected: $('#subdivision').val()
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            },            
            minLength: 0,
            autoFocus: true,
            response: function (event, ui) {
                if (ui.content.length == 0){
                    ui.content.push({
                        label: 'New Elevation: ' + $(this).val(),
                        value: $(this).val(),
                        id: 0
                    });
                }
            }
        }).on('autocompleteselect', function (event, ui) {
            id = ui.item.id; // no id for these (undefined) since they aren't individual records but part of plan record in subdivision
            value = ui.item.value;
            $('#bidElevation').val(ui.item.value);
            if(id == 0){
                $('#submitType').html('Create Bid').focus();
                $('#submitType').show();
            }else{
                $('form.jsform').trigger('submit');
                ////$('#submitType').html('Load').focus(); //focus is for both new and load, lets split that
            }
            
            return false;
        });

        $('form.jsform').on('submit', function(form){
            form.preventDefault();
            $.post('<?php echo base_url();?>index.php/bids/check_bid_form', $('form.jsform').serialize(), function(data){
                $('div.jsError').html(data);
            });
        });

        $('#cancelEntry_edit').on('click',function(){
            window.location.href = "<?php echo site_url(); ?>/bids/index";
        });        

	});
</script>