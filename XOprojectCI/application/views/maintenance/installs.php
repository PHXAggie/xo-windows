<div class="container col-md-6" id="statusListDisplay" name="statusListDisplay" style="float:left;">
    <?php //echo form_open(base_url('maintenance/check_builder_add_form'), array('class' => 'jsform')); ?>
                    <div class="form-group row justify-content-center">
                        <label for="initialStatus" class="col-md-2 col-form-label">Status:</label>
                        <div class="col-md-4">
                        <select class="form-control" id="initialStatus" name="initialStatus" disabled>
                            <option value="">SET STATUS</option>
                            <?php
                            if(!empty($tbl_status)){
                                foreach($tbl_status as $statusOption){
                                    echo '<option value="'.$statusOption['tbl_status_id'].'">'.$statusOption['tbl_status_name'].'</option>';
                                }
                            }else{
                                echo '<option value="">NO STATUSES FOUND</option>';
                            }
                            ?>
                        </select>
                        <input type="text" id="builderStatusOnLoad" name="builderStatusOnLoad" value="<?php echo $tbl_buildercustomer_tbl_status_tbl_status_id; ?>" hidden>
                        <input type="text" id="builderStatusChange" name="builderStatusChange" value="<?php echo $tbl_buildercustomer_tbl_status_tbl_status_id; ?>" hidden>
                        </div>
                    </div>
    <?php //echo form_close(); ?>
</div>

<div class="container col-md-6" id="installTypesDisplay" name="installTypesDisplay" style="float:right;">

<?php //echo form_open(base_url('index.php/maintenance/builder_save'), array('class' => 'jsform')); ?>
    <label class="col-form-label">Install Type(s) (check each one this builder might use):</label>
    <!-- &nbsp;&nbsp;<button type="submit" class="btn btn-info" id="setInstallTypes" style="display:none;">Update</button>&nbsp;<button class="btn btn-danger" id="cancelInstallTypes" style="display:none;">Cancel</button> -->

        <div class="list-group" id="listOfInstallTypes" name="listOfInstallTypes">
            <?php
                if(!empty($tbl_install_types)){
                    foreach($tbl_install_types as $rowItem){
                        //echo '<input type="checkbox" value="'.$rowItem['install_types_id'].'"><input type="text" value="'.$rowItem['type'].'">';
                        if($rowItem['install_types_id'] == $rowItem['tbl_install_types_install_types_id']){
                            $tf = TRUE;
                        } else {
                            $tf = FALSE;
                        }
                        echo "<div>";
                        echo form_checkbox('completed[]', $rowItem['install_types_id'], $tf, 'disabled');
                        echo $rowItem['type'];
                        //echo $rowItem['tbl_install_types_install_types_id'];
                        //echo form_hidden('builderID',$rowItem['tbl_buildercustomer_builder_id']);
                        echo "</div>";
                    }

                }else{
                    echo '<label>NO INSTALL TYPES FOUND</label>';
                }
            ?>

        </div>

    <?php echo form_close(); ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){

        $('form.jsformTypes').on('submit', function(form){
            form.preventDefault();
            $.post('<?php echo base_url();?>index.php/maintenance/builder_save/', $('form.jsform').serialize(), function(data){
                $('div.jsError').html(data);
            });
        });

        $('#initialStatus').val($("[name='builderStatusOnLoad']").val());

        $('#initialStatus').on('change', function(){
            $('#builderStatusChange').val($("[name='initialStatus']").val());
        });

        $('input[name="completed[]"], #buildername, #initialStatus').on('change', function(){
        //$('.checkbox').change(function(){
            $('#buildername').removeClass('col-6').addClass('col-4');
            $('#setInstallTypes').show();
            $('#cancelInstallTypes').show();
            $('#saveEditBuilderForm').show();
        });

        $('#cancelInstallTypes').on('click', function(){
            location.reload(true);
        });

        $('#unlockEditBuilderForm').on('click', function(){
                $('#unlockEditBuilderForm').hide();
                $('#cancelEditBuilderForm').show();
                $('#buildername').prop('disabled', false);
                $('#initialStatus').prop('disabled', false);
                $('input[name="completed[]"]').prop('disabled', false);
                //$('#installTypesDisplay').prop('disabled', false);
        });

        $('#cancelEditBuilderForm').on('click', function(){
            location.reload(true);
        });

    });
</script>