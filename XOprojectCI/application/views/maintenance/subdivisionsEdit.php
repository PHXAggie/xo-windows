<div class="container">
	<div class="text-center">
		<!--<h1>Edit Subdivision</h1>-->
		<h2><?php echo $title . $subdivision_data[0]['subdivision_name']; ?></h2>
		<div class="jsError"></div>
		<?php if(empty($subdivision_data)){ ?>
            <?php echo 'No Subdivision Records passed in!'; ?>
        <?php } else { ?>
			<?php echo form_open(base_url('index.php/maintenance/subdivision_save'), array('class' => 'jsform')); ?>
        <?php //log_message('debug','subdivision_data ' . print_r($subdivision_data[0]['subdivision_id'],TRUE)); ?>
	<!-- Builder Textbox -->
	            <div class="form-group row justify-content-center">
                	<input type="text" id="subdivisionid" name="subdivisionid" value="<?php echo $subdivision_data[0]['subdivision_id']; ?>" hidden>
                	<input type="text" class="col-6" id="subdivisionNameInput" name="subdivisionNameInput" value="<?php echo $subdivision_data[0]['subdivision_name']; ?>" disabled>
                		<input type="text" id="subdivisionNameOnLoad" name="subdivisionNameOnLoad" value="<?php echo $subdivision_data[0]['subdivision_name']; ?>" hidden>
					<button type="button" class="btn btn-info col-2" id="unlockEditSubdivisionForm" name="unlockEditSubdivisionForm">Click to Unlock</button>
					<button type="submit" class="btn btn-primary col-2" id="saveEditSubdivisionForm" name="saveEditSubdivisionForm"  style="display:none;">Save Changes</button>
					<button type="button" class="btn btn-danger col-2" id="cancelEditSubdivisionForm" name="cancelEditSubdivisionForm" style="display:none;">Cancel</button>
	            </div>


	<!-- Status Dropdown derecha-->
				<div class="col-6" id="statusListDisplay" name="statusListDisplay">
	                <div class="form-group row">
	                    <label for="initialStatus" class="col-4 col-form-label">Subdivision Status:</label>
	                    <div class="col">
		                    <select class="form-control" id="initialStatus" name="initialStatus" disabled>
		                        <option value="">SET STATUS</option>
		                        <?php
		                        if(!empty($tbl_status)){
		                            foreach($tbl_status as $statusOption){
		                                echo '<option value="'.$statusOption['id'].'">'.$statusOption['name'].'</option>';
		                            }
		                        }else{
		                            echo '<option value="">NO STATUSES FOUND</option>';
		                        }
		                        ?>
		                    </select>
		                    <input type="text" id="subdivisionStatusOnLoad" name="subdivisionStatusOnLoad" value="<?php echo $subdivision_data[0]['tbl_status_tbl_status_id']; ?>" hidden>
		                    <input type="text" id="subdivisionStatusChange" name="subdivisionStatusChange" value="<?php echo $subdivision_data[0]['tbl_status_tbl_status_id']; ?>" hidden>
	                    </div>
	                </div>
                </div>

    <!-- Install Types Checkboxes izquierda-->
				<div class="col-8 text-left" id="installTypesDisplay" name="installTypesDisplay">
    				<label class="col-form-label">Install Type(s)<br />(check each one this builder might use):</label>
					<!-- &nbsp;&nbsp;<button type="submit" class="btn btn-info" id="setInstallTypes" style="display:none;">Update</button>&nbsp;<button class="btn btn-danger" id="cancelInstallTypes" style="display:none;">Cancel</button> -->
					<div class="list-group" id="listOfInstallTypes" name="listOfInstallTypes">
			            <?php
			                if(!empty($tbl_install_types)){
			                    foreach($tbl_install_types as $rowItem){
			                        //echo '<input type="checkbox" value="'.$rowItem['install_types_id'].'"><input type="text" value="'.$rowItem['type'].'">';
			                        if($rowItem['install_types_id'] == $rowItem['tbl_install_types_install_types_id']){
			                            $tf = TRUE;
			                        } else {
			                            $tf = FALSE;
			                        }
			                        echo "<div>";
			                        echo form_checkbox('completed[]', $rowItem['install_types_id'], $tf, 'disabled');
			                        echo $rowItem['type'];
			                        //echo $rowItem['tbl_install_types_install_types_id'];
			                        //echo form_hidden('builderID',$rowItem['tbl_buildercustomer_builder_id']);
			                        echo "</div>";
			                    }

			                }else{
			                    echo '<label>NO INSTALL TYPES FOUND</label>';
			                }
			            ?>
			        </div>
			    </div><?php //echo form_close(); ?>

			<?php echo form_close(); ?>
	    <?php } ?>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){

        $('form.jsform').on('submit', function(form){
            form.preventDefault();
            $.post('<?php echo base_url();?>index.php/maintenance/subdivision_save/', $('form.jsform').serialize(), function(data){
                $('div.jsError').html(data);
            });
            setTimeout(function(){
            	//location.reload();
            	window.location.href = "<?php echo base_url(); ?>index.php/Maintenance/subdivisions";
            }, 5000);
        });

        $('#initialStatus').val($("[name='subdivisionStatusOnLoad']").val());

        $('#initialStatus').on('change', function(){
            $('#subdivisionStatusChange').val($("[name='initialStatus']").val());
        });

        $('input[name="completed[]"], #subdivisionNameInput, #initialStatus').on('mouseup click', function(){
        //$('.checkbox').change(function(){
            $('#subdivisionNameInput').removeClass('col-6').addClass('col-4');
            $('#setInstallTypes').show();
            $('#cancelInstallTypes').show();
            $('#saveEditBuilderForm').show();
        });

        $('#cancelInstallTypes').on('click', function(){
            location.reload(true);
        });

        $('#unlockEditSubdivisionForm').on('click', function(){
                $('#unlockEditSubdivisionForm').hide();
                $('#cancelEditSubdivisionForm').show();
                $('#subdivisionNameInput').prop('disabled', false);
                $('#initialStatus').prop('disabled', false);
                $('input[name="completed[]"]').prop('disabled', false);
                //$('#installTypesDisplay').prop('disabled', false);
        });

        $('#cancelEditSubdivisionForm').on('click', function(){
            location.reload(true);
        });

    });
</script>