    <div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="errorName"><?php echo $title; ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                </div>
                <div class="modal-body text-center">
                    <form>
                        <fieldset class="form-group">
                            <p id="errorDetails"><?php echo $details; ?></p>
                        </fieldset>
                        <fieldset class="form-group">
                            <p id="errorSolution"><?php echo $solution; ?></p>
                        </fieldset>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <script>
    $(document).ready(function(){
        // Show Error Modal popup - START
        $('#errorModal').modal('show');
        $('#errorModal').on('hidden.bs.modal', function() {
            $('#InfoBanner').replaceWith("<div class='col-5 alert alert-danger'>Cancel or adjust entry</div>");
        });
        // Show Error Modal popup - END
    });
    </script>