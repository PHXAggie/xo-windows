<div class="container">
	<div class="text-center">
		<div class="form-group row"></div>
    	<div class="form-group row"></div>
		<h1><?php echo $title; ?></h1>
		<?php //no search for this page ?>
		<div class="jsError"></div>

        <?php /* if(!$builder_data[0]['tbl_buildercustomer_builder_id']){ //EDIT logic because Builder ID was passed in
            $audit_view = 'hidden';
        }else{
            $audit_view = '';
        }; */?>
        <?php if($this->session->userdata('level') <= '4'){
            $auth_edit = '';
        }else{
            $auth_edit = 'readonly';
        }; ?>
        <?php if($this->session->userdata('level') <= '4'){
            $auth_view = '';
        }else{
            $auth_view = 'hidden';
        }; ?>

		<?php echo form_open(base_url('maintenance/check_builder_add_form'), array('class' => 'jsform')); ?>
		<div class="row">
		<div class="col-2"></div>
			<div class="col-10">
				<div class="form-group row">
					<label for="builderNameInput" class="col-3 form-label text-right">Builder Name: </label>
					<input type="text" class="col-4 form-control" id="builderNameInput" name="builderNameInput" placeholder="SHEA" autofocus>
				</div>
				<div class="form-group row">
					<input type="text" class="col-4 form-control" name="xoID" value='<?php echo $this->session->userdata('xo_id'); ?>' hidden>
				</div>
				<div class="form-group row">
					<label for="initialStatus" class="col-3 form-label text-right">Initial Status:</label>
						<select class="col-4 form-control" id="initialStatus" name="initialStatus">
							<option value="">SET STATUS</option>
							<?php
							if(!empty($tbl_status)){
								foreach($tbl_status as $statusOption){
									echo '<option value="'.$statusOption['id'].'">'.$statusOption['name'].'</option>';
								}
							}else{
								echo '<option value="">NO STATUSES FOUND</option>';
							}
							?>
						</select>
				</div>
			</div>
		</div>	
			<div class="form-group row justify-content-center">
				<div class="col-md-5">
					<button type="submit" class="btn btn-info" id="enterBuilder">Add Builder</button>
					<button type="button" class="btn btn-danger" id="cancelEntry">Cancel</button>
				</div>
			</div>
	<?php echo form_close(); ?>


			</div>
		</div>

		<script type="text/javascript">
			$(document).ready(function(){
				$('form.jsform').on('submit', function(form){
					form.preventDefault();
					$.post('<?php echo base_url();?>index.php/maintenance/check_builder_add_form', $('form.jsform').serialize(), function(data){
						$('div.jsError').html(data);
					});
//					setTimeout(function(){
//            		//location.reload();
//            		window.location.href = "<?php //echo base_url(); ?>index.php/Maintenance/builders";
//            		}, 5000);
				});

				$('#cancelEntry').on('click', function(){
	            	window.location.href = "<?php echo base_url(); ?>index.php/Maintenance/builders";
	        	});
			});
		</script>
