<div class="text-center">
    <div class="container">
        <h1><?php echo $title; ?></h1>
        <?php echo form_open(base_url('maintenance/check_subdivision_add_form'), array('class' => 'jsform form-horizontal')); ?>
            <div class="form-group row justify-content-center">
                <label for="builder">Builder:</label>
                <div class="col-md-5">
                    <select class="form-control" id="builder" name="builderIdSelected">
                        <option value="">SELECT BUILDER</option>
                        <?php
                            if(!empty($tbl_buildercustomers)){
                                foreach($tbl_buildercustomers as $rowItem){ 
                                    echo '<option value="'.$rowItem['builder_id'].'">'.$rowItem['builder_name'].'</option>';
                                }
                            }else{
                                echo '<option value="">NO BUILDERS FOUND</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>

    		<div class="form-group row justify-content-center">
    			<label for="subdivisionName">Subdivision Name:</label>
    			<div class="col-md-4">
    				<input type="text" class="form-control" id="subdivisionNameInput" name="subdivisionNameInput" placeholder="CLEMENTE RANCH">
    			</div>
    		</div>
    		<div class="form-group row justify-content-center">
    			<label for="subdivisionCity" class="form-label text-left">Subdivision City:</label>
    			<div class="col-md-4">
    				<input type="text" class="form-control" id="subdivisionCityInput" name="subdivisionCityInput" placeholder="CHANDLER">
    			</div>
    		</div>
    		<div class="form-group row justify-content-center">
    			<label for="subdivisionState" class="form-label text-left">Subdivision State (2 letter abbreviation):</label>
    			<div class="col-md-2">
    				<input type="text" class="form-control" id="subdivisionStateInput" name="subdivisionStateInput" placeholder="AZ">
    			</div>
    		</div>
    		<div class="form-group row justify-content-center">
    			<label for="subdivisionZip" class="form-label">Subdivision Zip:</label>
    			<div class="col-md-4">
    				<input type="text" class="form-control" id="subdivisionZipInput" name="subdivisionZipInput" placeholder="85286">
    			</div>
    		</div>

            <div class="form-group row justify-content-center">
                <label for="branch">Branch:</label>
                <div class="col-md-5">
                    <select class="form-control" id="builder" name="branchIdSelected">
                        <option value="">SELECT BRANCH</option>
                        <?php
                        if(!empty($tbl_xo_branch)){
                            foreach($tbl_xo_branch as $rowItem){ 
                                echo '<option value="'.$rowItem['xo_branch_id'].'">'.$rowItem['location'].'</option>';
                            }
                        }else{
                            echo '<option value="">NO BUILDERS FOUND</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group row justify-content-center">
                <label for="zone" class="form-label text-left">Subdivision Zone:</label>
                <div class="col-md-4">
                    <select class="form-control" id="zone" name="zoneIdSelected">
                        <option value="">SELECT ZONE</option>
                        <?php
                        if(!empty($tbl_zones)){
                            foreach($tbl_zones as $rowItem){ 
                                echo '<option value="'.$rowItem['zone_id'].'">'.$rowItem['zone_name'].'</option>';
                            }
                        }else{
                            echo '<option value="">NO ZONES FOUND</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group row justify-content-center">
                <label for="status" class="form-label text-left">Subdivision Status:</label>
                <div class="col-md-4">
                    <select class="form-control" id="status" name="statusIdSelected">
                        <option value="">SELECT STATUS</option>
                        <?php
                        if(!empty($tbl_status)){
                            foreach($tbl_status as $rowItem){ 
                                echo '<option value="'.$rowItem['id'].'">'.$rowItem['name'].'</option>';
                            }
                        }else{
                            echo '<option value="">NO STATUSES FOUND</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group row justify-content-center">
                <div class="col-md-5">
                    <button type="submit" class="btn btn-info" id="enterSubdivision">Add Subdivision</button>
                    <button type="button" class="btn btn-danger" id="cancelEntry">Cancel</button>
                </div>
            </div>
        <?php echo form_close(); ?>
        <div class="jsError"></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){


        $('form.jsform').on('submit', function(form){
            form.preventDefault();
            $.post('<?php echo base_url();?>index.php/maintenance/check_subdivision_add_form', $('form.jsform').serialize(), function(data){
                $('div.jsError').html(data);
            });
            setTimeout(function(){
                //location.reload();
                window.location.href = "<?php echo base_url(); ?>index.php/Maintenance/subdivisions";
            }, 5000);
        });

        $('#cancelEntry').on('click', function(){
            window.location.href = "<?php echo base_url(); ?>index.php/Maintenance/subdivisions";
        });


    });
</script>