<div class="container">
	<div class="text-center">
		<div class="form-group row"></div>
    	<div class="form-group row"></div>
		<?php echo '<h2>Edit Builder: ' . $title . '</h2>'; ?>
		<div class="jsError"></div>
		<div class="row">
			<div class="col-2">
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					<a class="nav-link text-left active" id="v-pills-edit-tab" data-toggle="pill" href="#v-pills-edit" role="tab" aria-controls="v-pills-edit" aria-selected="true">General</a>
					<a class="nav-link text-left" id="v-pills-install-tab" data-toggle="pill" href="#v-pills-install" role="tab" aria-controls="v-pills-install" aria-selected="false">Install Types</a>
					<a class="nav-link text-left" id="v-pills-stats-tab" data-toggle="pill" href="#v-pills-stats" role="tab" aria-controls="v-pills-stats" aria-selected="false">Statistics</a>
					<a class="nav-link text-left" id="v-pills-audit-tab" data-toggle="pill" href="#v-pills-audit" role="tab" aria-controls="v-pills-audit" aria-selected="false">Audit Log</a>	
				</div>
			</div>

		<div class="tab-content" id="v-pills-tabContent">
			<div class="tab-pane justify-content-center fade show active" id="v-pills-edit" role="tabpanel" aria-labelledby="v-pills-edit-tab">
				<h4 class="mt-2">Edit Builder Data</h4>
				<?php if(empty($builder_id)){ ?>
		            <?php echo 'No Builder Records passed in!'; ?>
		        <?php } else { ?>
					<?php echo form_open(site_url('/maintenance/builder_save'), array('class' => 'jsform')); ?>
					<!-- Builder Textbox -->
			            <div class="form-group row justify-content-start">
		                	<input type="text" id="builderid" name="builderid" value="<?php echo $builder_id; ?>" hidden>
		                	<input type="text" class="col-6" id="builderNameInput" name="builderNameInput" value="<?php echo $builder_name; ?>" disabled>
		                	<input type="text" id="builderNameOnLoad" name="builderNameOnLoad" value="<?php echo $builder_name; ?>" hidden>
<input type="text" name="nochange" data-name="userfield@<?php echo $builder_name; ?>" value="<?php echo $builder_name; ?>" class="trigger-changed">
<input type="text" name="nochange" data-name="userfield@<?php echo $builder_id; ?>" value="<?php echo $builder_id; ?>" class="trigger-changed">
							<button type="button" class="btn btn-info col-2 unlockEditBuilderForm" id="unlockEditBuilderForm" name="unlockEditBuilderForm">Make Changes</button>
							<button type="submit" class="btn btn-primary col-2 saveEditBuilderForm" id="saveEditBuilderForm" name="saveEditBuilderForm"  style="display:none;">Save Changes</button>
							<button type="button" class="btn btn-danger col-2 cancelEditBuilderForm" id="cancelEditBuilderForm" name="cancelEditBuilderForm" style="display:none;">Cancel</button>
			            </div>
<div id="mainform"></div>

					<!-- Status Dropdown derecha-->
						<div class="form-group row justify-content-center">
										                    	<label for="initialStatus" class="col-1 col-form-label">Status:</label>
							<div class="col-4" id="statusListDisplay" name="statusListDisplay">

				                    <select class="form-control" id="initialStatus" name="initialStatus" disabled>
				                        <option value="">SET STATUS</option>
				                        <?php
				                        if(!empty($tbl_status)){
				                            foreach($tbl_status as $statusOption){
				                                echo '<option value="'.$statusOption['id'].'">'.$statusOption['name'].'</option>';
				                            }
				                        }else{
				                            echo '<option value="">NO STATUSES FOUND</option>';
				                        }
				                        ?>
				                    </select>
				                    <input type="text" id="builderStatusOnLoad" name="builderStatusOnLoad" value="<?php echo $tbl_status_tbl_status_id; ?>" hidden>
				                    <input type="text" id="builderStatusChange" name="builderStatusChange" value="<?php echo $tbl_status_tbl_status_id; ?>" hidden>
			                    
			                </div>
		                </div>
			</div>
			<?php /*<div class="tab-pane fade" id="installTypes">*/ ?>
			<div class="tab-pane fade" id="v-pills-install" role="tabpanel" aria-labelledby="v-pills-install-tab">
				<h4 class="mt-2">Edit Install Types</h4>

		    <!-- Install Types Checkboxes izquierda-->
		    	<div class="row">
					<div class="col-6 text-left" id="installTypesDisplay" name="installTypesDisplay">
						<button type="button" class="btn btn-info col-12 unlockEditBuilderForm" id="unlockEditBuilderForm" name="unlockEditBuilderForm">Make Changes</button>
						<button type="submit" class="btn btn-primary col-6 saveEditBuilderForm" id="saveEditBuilderForm" name="saveEditBuilderForm"  style="display:none;">Save Changes</button>
						<button type="button" class="btn btn-danger col-6 cancelEditBuilderForm" id="cancelEditBuilderForm" name="cancelEditBuilderForm" style="display:none; float:right;">Cancel</button>
    				<label class="col-form-label">Check each one this builder might use:</label>
					<!-- &nbsp;&nbsp;<button type="submit" class="btn btn-info" id="setInstallTypes" style="display:none;">Update</button>&nbsp;<button class="btn btn-danger" id="cancelInstallTypes" style="display:none;">Cancel</button> -->

					<div class="list-group" id="listOfInstallTypes" name="listOfInstallTypes">
			            <?php
			                if(!empty($tbl_install_types)){
			                	$arrayOfChecked = array();
			                    foreach($tbl_install_types as $rowItem){
			                        //echo '<input type="checkbox" value="'.$rowItem['install_types_id'].'"><input type="text" value="'.$rowItem['type'].'">';
			                        if($rowItem['install_types_id'] == $rowItem['tbl_install_types_install_types_id']){
			                        	array_push($arrayOfChecked, $rowItem['tbl_install_types_install_types_id']);
			                            $tf = TRUE;
			                        } else {
			                            $tf = FALSE;
			                        }
			                        echo "<div>";
			                        	echo form_checkbox('completed[]', $rowItem['install_types_id'], $tf, 'disabled');
			                        	echo $rowItem['type'];
			                        //echo $rowItem['tbl_install_types_install_types_id'];
			                        //echo form_hidden('builderID',$rowItem['tbl_buildercustomer_builder_id']);
			                        echo "</div>";
			                    }

			                }else{
			                    echo '<label>NO INSTALL TYPES FOUND</label>';
			                }
			            ?>
						<input type="text" id="installTypesArray" name="installTypesArray" value="<?php echo htmlspecialchars(json_encode($arrayOfChecked));?>" hidden>
			        </div>
			    </div>
					<?php echo form_close(); ?>
				<?php } ?>
				</div>
			</div>
			<div class="tab-pane fade" id="v-pills-stats" role="tabpanel" aria-labelledby="v-pills-stats-tab">
			<!-- Audit Log view-->
				<div class="form-group row">
					<label for="stats" class="form-label col-3 text-right">Statistics:</label>
						<div class="col-5">
						</div>
				</div>
			</div>
			<?php /*<div class="tab-pane fade" id="auditLog">*/ ?>
			<div class="tab-pane fade" id="v-pills-audit" role="tabpanel" aria-labelledby="v-pills-audit-tab">
			<!-- Audit Log view-->
				<div class="row">
					<h4 class="mt-2">Audit Log</h4>
					<?php $this->view('maintenance/builders_log'); ?>
				</div>
			</div>
		</div>
	</div>
	</div> <?php // text-center ?>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $('form.jsform').on('submit', function(form){
            form.preventDefault();
            $.post('<?php echo base_url();?>index.php/maintenance/builder_save/', $('form.jsform').serialize(), function(data){
                $('div.jsError').html(data);
            });
            //setTimeout(function(){
            	//location.reload();
           // 	window.location.href = "<?php //echo base_url(); ?>index.php/Maintenance/builders";
           // }, 5000);
        });


$(".trigger-changed").keyup(function(){
	$(this).addClass("changed");
	$(this).attr("name", $(this).data("name")).off("keyup");
	$("#mainform").append('<input type="text" name="user-updated"></input>');
});

        $('#initialStatus').val($("[name='builderStatusOnLoad']").val());

        $('#initialStatus').on('change', function(){
            $('#builderStatusChange').val($("[name='initialStatus']").val());
            //alert($('#builderStatusChange').val());
        });

        $('input[name="completed[]"], #builderNameInput, #initialStatus').on('mouseup click', function(){  //'change' was 'mouseup click'
        //$('.checkbox').change(function(){
            $('#builderNameInput').removeClass('col-6').addClass('col-4');
            $('#setInstallTypes').show();
            $('#cancelInstallTypes').show();
            $('.saveEditBuilderForm').show();
        });

        $('#cancelInstallTypes').on('click', function(){
            location.reload(true);
        });

        $('.unlockEditBuilderForm').on('click', function(){
            $('.unlockEditBuilderForm').hide();
            $('.cancelEditBuilderForm').show();
            $('#builderNameInput').prop('disabled', false);
            $('#initialStatus').prop('disabled', false);
            $('input[name="completed[]"]').prop('disabled', false);
            //$('#installTypesDisplay').prop('disabled', false);
        });

        $('.cancelEditBuilderForm').on('click', function(){
            location.reload(true);
        });

    });
</script>