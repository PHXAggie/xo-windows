<div class="container">
    <div class="text-center">
	
	    <h1>Jobsite Supers Maintenance  <a class="btn btn-primary" href="<?php echo base_url(); ?>index.php/maintenance/supersAddEdit">Add Jobsite Super</a></h1>

	    <?php $this->load->view('search.php'); ?>

		<div class="jsError"></div>

		<?php if(empty($tbl_supers)){ ?>
			<?php echo 'No Jobsite Super Records found!'; ?>
		<?php } else { ?>
			<table class="table table-bordered table-sm table-hover" id="supersResultsTable">
				<thead class="thead-light">
					<tr>
						<th scope="col">Subdivision</th>
						<th scope="col">Builder</th>
						<th scope="col">Super</th>
						<th scope="col">Phone Number</th>
						<th scope="col">XO Branch</th>
						<th scope="col">Mfg Abbrv</th>
						<th scope="col">Starts Rep</th>
						<th scope="col">XO Field Rep</th>
						<th scope="col">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($tbl_supers as $row) : ?>
						<tr>
							<th scope="row"><?php echo $row['subdivision_name']; ?></th>
							<td><?php echo $row['builder_name']; ?></td>
							<td><?php echo $row['name']; ?></td>
							<td><?php echo '('.substr($row['phone_number'], 0, 3).') '.substr($row['phone_number'], 3, 3).'-'.substr($row['phone_number'], 6); ?></td>
							<td><?php echo $row['po_abbr']; ?></td>
							<td><?php echo $row['mfg_abbr']; ?></td>
							<td><?php echo $row['xo_starts_rep']; ?></td>
							<td><?php echo $row['xo_field_rep']; ?></td>
							<?php echo "<td><a class='btn-sm btn-info' href='" . base_url('index.php/maintenance/supersAddEdit/' . $row['idtbl_supers']) . "'>Edit</a>&nbsp;<a class='supers btn-sm btn-secondary' href='#supersModal' data-toggle='modal' data-supers-id='" . $row['idtbl_supers'] . "' data-update-date='" . date('m/d/Y', strtotime($row['update_date'])) . "' data-update-time='" .  $row['update_time'] . "' data-update-by='" . $row['login'] . "' data-subdivision-name='" . $row['subdivision_name'] . "'>Info</a></td>"; ?>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php } ?>
		

	</div>
	<div class="modal fade" id="supersModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-center" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Jobsite Supers Details</h4>
				</div>
				<div class="modal-body text-center">
					<form>
						<fieldset class="form-group">
							<h4 id="subdivisionName"></h4>
						</fieldset>
						<fieldset class="form-group">
							<p id="updateDate"></p>
						</fieldset>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if($js_to_load != ''){ ?>
	<script>
	$(document).ready(function(){
		$(".supers").click(function(){
			$("#subdivisionName").html("<strong>" + $(this).data('subdivision-name') + "</strong>");
			$("#updateDate").html("<strong>Last updated:</strong> " + $(this).data('update-date') + " at " + $(this).data('update-time') + " <strong>BY</strong> " + $(this).data('update-by'));
			$('#supersModal').modal('show');
		});

	});
	</script>
	<script type="text/javascript">var $searchBy = '<?php echo $searchBy; ?>'</script>
	<script type="text/javascript" src="../../assets/js/<?=$js_to_load;?>">
<?php }else{; ?>
	<script>
		$(document).ready(function(){
		$('#search').show();
		});
<?php }; ?>
</script>