<?php
echo '<div class="col-12" id="audit_trail" name="audit_trail">';
	//<label class="col-form-label text-left">Audit Log section</label>
	//<div class="border border-primary">';
	//<?php
		if(!empty($builders_log)){
				echo '<div class="row justify-content-center"><div class="col-2"><small>Date</small></div><div class="col-2"><small>By</small></div><div class="col-8 text-left"><small>Action Taken</small></div></div>';
			foreach($builders_log as $row){
				$synopsis = "";
				//$dateShorten = date_parse($row['changed_date'])['month'] . "/" . date_parse($row['changed_date'])['day'] . "/" . date_parse($row['changed_date'])['year'] . " at " . date_parse($row['changed_date'])['hour'];
				$dateShorten = date('m/d/y h:i A', strtotime($row['changed_date']));
				// add remove INSTALL TYPES
					if($row['action'] == "added"){
						if($row['field'] == "tbl_install_types_install_types_id"){
							$synopsis = "Added '" . $row['new_value'] . "' Installation Type";
						}
					}
					if($row['action'] == "removed"){
						if($row['field'] == "tbl_install_types_install_types_id"){
							$synopsis = "Removed '" . $row['new_value'] . "' Installation Type";
						}
					}

				// add update BUILDER NAME
					if($row['action'] == "created"){
						if($row['field'] == "builder_name"){
							$synopsis = "Created '" . $row['new_value'] . "' builder";
						}
					}
					if($row['action'] == "update"){
						if($row['field'] == "builder_name"){
							$synopsis = "Changed NAME from '" . $row['old_value'] . "' to '" . $row['new_value'] . "'";
						}
					}
					if($row['action'] == "created"){
						if($row['field'] == "builder_id"){
							$synopsis = "New builder_id: '" . $row['new_value'] . "'";
						}
					}

				// add update SUBDIVISION NAME
					if($row['action'] == "created"){
						if($row['field'] == "subdivision_name"){
							$synopsis = "Created '" . $row['new_value'] . "' subdivision";
						}
					}
					if($row['action'] == "update"){
						if($row['field'] == "subdivision_name"){
							$synopsis = "Changed NAME from '" . $row['old_value'] . "' to '" . $row['new_value'] . "'";
						}
					}
					if($row['action'] == "created"){
						if($row['field'] == "subdivision_id"){
							$synopsis = "New subdivision_id: '" . $row['new_value'] . "'";
						}
					}

				// add update SUBDIVISION CITY
					if($row['action'] == "created"){
						if($row['field'] == "city"){
							$synopsis = "Initial City: '" . $row['new_value'] . "'";
						}
					}
					if($row['action'] == "update"){
						if($row['field'] == "city"){
							$synopsis = "Changed City from '" . $row['old_value'] . "' to '" . $row['new_value'] . "'";
						}
					}

				// add update SUBDIVISION STATE
					if($row['action'] == "created"){
						if($row['field'] == "state"){
							$synopsis = "Initial State: '" . $row['new_value'] . "'";
						}
					}
					if($row['action'] == "update"){
						if($row['field'] == "state"){
							$synopsis = "Changed State from '" . $row['old_value'] . "' to '" . $row['new_value'] . "'";
						}
					}

				// add update SUBDIVISION ZIP
					if($row['action'] == "created"){
						if($row['field'] == "zip"){
							$synopsis = "Initial Zip: '" . $row['new_value'] . "'";
						}
					}
					if($row['action'] == "update"){
						if($row['field'] == "zip"){
							$synopsis = "Changed Zip from '" . $row['old_value'] . "' to '" . $row['new_value'] . "'";
						}
					}

				// add update SUBDIVISION ZONE
					if($row['action'] == "created"){
						if($row['field'] == "zone"){
							$synopsis = "Initial Zone: '" . $row['new_value'] . "'";
						}
					}
					if($row['action'] == "update"){
						if($row['field'] == "zone"){
							$synopsis = "Changed Zone from '" . $row['old_value'] . "' to '" . $row['new_value'] . "'";
						}
					}

				// add update SUBDIVISION BRANCH
					if($row['action'] == "created"){
						if($row['field'] == "branch"){
							$synopsis = "Initial Branch: '" . $row['new_value'] . "'";
						}
					}
					if($row['action'] == "update"){
						if($row['field'] == "branch"){
							$synopsis = "Changed Branch from '" . $row['old_value'] . "' to '" . $row['new_value'] . "'";
						}
					}

				// add update SUBDIVISION STARTS REP
					if($row['action'] == "created"){
						if($row['field'] == "starts rep"){
							$synopsis = "Initial Starts Rep: '" . $row['new_value'] . "'";
						}
					}
					if($row['action'] == "update"){
						if($row['field'] == "starts rep"){
							$synopsis = "Changed Starts Rep from '" . $row['old_value'] . "' to '" . $row['new_value'] . "'";
						}
					}

				// add update SUBDIVISION FIELD REP
					if($row['action'] == "created"){
						if($row['field'] == "xo field rep"){
							$synopsis = "Initial XO Field Rep: '" . $row['new_value'] . "'";
						}
					}
					if($row['action'] == "update"){
						if($row['field'] == "xo field rep"){
							$synopsis = "Changed XO Field Rep from '" . $row['old_value'] . "' to '" . $row['new_value'] . "'";
						}
					}	

				// add update SUPER Name
					if($row['action'] == "created"){
						if($row['field'] == "super_name"){
							$synopsis = "Initial Super: '" . $row['new_value'] . "'";
						}
					}
					if($row['action'] == "update"){
						if($row['field'] == "super_name"){
							$synopsis = "Changed Super from '" . $row['old_value'] . "' to '" . $row['new_value'] . "'";
						}
					}

				// add update Phone Number
					if($row['action'] == "created"){
						if($row['field'] == "phone_number"){
							$synopsis = "Initial Phone number: '" . '('.substr($row['new_value'], 0, 3).') '.substr($row['new_value'], 3, 3).'-'.substr($row['new_value'], 6) . "'";
						}
					}
					if($row['action'] == "update"){
						if($row['field'] == "phone_number"){
							$synopsis = "Changed Phone number from '" . '('.substr($row['old_value'], 0, 3).') '.substr($row['old_value'], 3, 3).'-'.substr($row['old_value'], 6) . "' to '" . '('.substr($row['new_value'], 0, 3).') '.substr($row['new_value'], 3, 3).'-'.substr($row['new_value'], 6) . "'";
						}
					}		

				// add update STATUS
				// (tbl_buildercustomer:AI-(created/tbl_status_tbl_status_id);AU-(update/status_id))
				// (tbl_subdivision:AI-(created/tbl_status_tbl_status_id);AU-(update/status_id))
					if($row['action'] == "created"){
						if($row['field'] == "tbl_status_tbl_status_id"){
							$synopsis = "Initial Status: '" . $row['new_value'] . "'";
						}
					}
					if($row['action'] == "update"){
						if($row['field'] == "status_id"){
							$synopsis = "Changed Status from '" . $row['old_value'] . "' to '" . $row['new_value'] . "'";
						}
					}

				echo '<div class="row justify-content-center"><div class="col-2"><small>' . $dateShorten . '</small></div><div class="col-2"><small>' . $row['changed_by'] . '</small></div><div class="col-8 text-left"><small>' . $synopsis . '</small></div></div>';
			}
		}
	// ? ending php tag
	echo '</div>';
echo '</div>';
?>