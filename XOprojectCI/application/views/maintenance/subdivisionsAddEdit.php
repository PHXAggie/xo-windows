<div class="container">
    <div class="text-center">
        <div class="form-group row"></div>
        <div class="form-group row"></div>
        <div class="form-group row"></div>
        <h2><?php echo $title; ?></h2>
        <?php //no search for this page ?>
        <div class="jsError"></div>
    <?php //permissions ?>
        <?php if($subdivision_data[0]['tbl_buildercustomer_builder_id'] ? $audit_view = '' : $audit_view = 'hidden'); ?>
        <?php if($this->session->userdata('level') <= '4'               ? $auth_edit = '' : $auth_edit = 'readonly'); ?>
        <?php if($this->session->userdata('level') <= '4'               ? $auth_view = '' : $auth_view = 'hidden'); ?>

        <?php echo form_open(site_url('/maintenance/check_subdivision_add_form'), array('class' => 'jsform form-horizontal')); ?>
        <div class="container-fluid">
            <div class="form-group row"></div>
            <div class="row">
                <div class="col-2">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link text-left active" id="v-pills-edit-tab" data-toggle="pill" href="#v-pills-edit" role="tab" aria-controls="v-pills-edit" aria-selected="true">General</a>
                        <a class="nav-link text-left" id="v-pills-geo-tab" data-toggle="pill" href="#v-pills-geo" role="tab" aria-controls="v-pills-geo" aria-selected="false">Geographic</a>
                        <a class="nav-link text-left" id="v-pills-people-tab" data-toggle="pill" href="#v-pills-people" role="tab" aria-controls="v-pills-people" aria-selected="false">Personnel</a>
                        <a class="nav-link text-left" id="v-pills-stats-tab" data-toggle="pill" href="#v-pills-stats" role="tab" aria-controls="v-pills-stats" aria-selected="false" <?php echo $auth_view; ?> <?php echo $audit_view; ?>>Statistics</a>
                        <a class="nav-link text-left" id="v-pills-audit-tab" data-toggle="pill" href="#v-pills-audit" role="tab" aria-controls="v-pills-audit" aria-selected="false" <?php echo $audit_view; ?>>Audit Log</a>
                    </div>
                </div>
<!-- General tab -->
                <div class="col-10 border-left border-info">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-edit" role="tabpanel" aria-labelledby="v-pills-edit-tab">
                            <div class="form-group row">
                                <label for="builder" class="col-3 text-right">Builder:</label>
                                <div class="col-5">
                                    <?php if($subdivision_data[0]['tbl_buildercustomer_builder_id']){
                                            if(!empty($tbl_buildercustomers)){
                                                foreach($tbl_buildercustomers as $rowItem){
                                                    if($rowItem['builder_id'] == $subdivision_data[0]['tbl_buildercustomer_builder_id']){
                                                        echo '<input type="text" class="form-control" id="builderName" name="builderName" value="' . $rowItem['builder_name'] . '" readonly>';
                                                        echo '<input type="text" class="form-control" id="builder" name="builderIdSelected" value="' . $rowItem['builder_id'] . '" hidden>';
                                                    };
                                                };
                                            }else{
                                                echo '<option value="">NO BUILDERS FOUND</option>';
                                            };
                                    }else{ //ADD NEW MODE because there was no Builder ID passed in
                                            echo '<select class="form-control" id="builder" name="builderIdSelected">';
                                            echo '<option value="">SELECT BUILDER</option>';
                                            if(!empty($tbl_buildercustomers)){
                                                foreach($tbl_buildercustomers as $rowItem){
                                                    if($rowItem['builder_id'] == $subdivision_data[0]['tbl_buildercustomer_builder_id']){
                                                        echo '<option value="'.$rowItem['builder_id'].'" selected>'.$rowItem['builder_name'].'</option>';
                                                    }else{
                                                        echo '<option value="'.$rowItem['builder_id'].'">'.$rowItem['builder_name'].'</option>';
                                                    };
                                                };
                                            }else{
                                                echo '<option value="">NO BUILDERS FOUND</option>';
                                            };
                                            echo '</select>';
                                    }; ?>
                                </div>
                            </div>
                    		<div class="form-group row">
                   			    <label for="subdivisionName" class="form-label col-3 text-right">Subdivision Name:</label>
                                <div class="col-5">
                				    <input type="text" class="form-control" id="subdivisionName" name="subdivisionNameInput" value="<?php echo isset($subdivision_data[0]['subdivision_name']) ? set_value("subdivisionNameInput", $subdivision_data[0]['subdivision_name']) : set_value("subdivisionNameInput"); ?>" placeholder="CLEMENTE RANCH" <?php echo $auth_edit; ?>>
                    			</div>
                    		</div>
                            <div class="form-group row">
                                <label for="status" class="form-label col-3 text-right">Subdivision Status:</label>
                                <div class="col-5">
                                    <select class="form-control" id="status" name="statusIdSelected">
                                        <?php echo '<option value="">SELECT STATUS</option>';
                                            if(!empty($tbl_status)){
                                                foreach($tbl_status as $rowItem){
                                                    if($rowItem['id'] == $subdivision_data[0]['tbl_status_tbl_status_id']){
                                                        echo '<option value="'.$rowItem['id'].'" selected>'.$rowItem['name'].'</option>';
                                                    }else{   
                                                        echo '<option value="'.$rowItem['id'].'">'.$rowItem['name'].'</option>';
                                                    };
                                                };
                                            }else{
                                                echo '<option value="">NO STATUSES FOUND</option>';
                                            };
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="installType" class="form-label col-3 text-right">Installation Type:</label>
                                <div class="col-5">
                                    <select class="form-control" id="installType" name="installTypeIdSelected">
                                        <?php echo '<option value="">SELECT BUILDER FIRST</option>';
                                            if(!empty($builders_it)){
                                                foreach($builders_it as $rowItem){
                                                    if($rowItem['install_type_id'] == $subdivision_data[0]['install_type_id']){
                                                        echo '<option value="'.$rowItem['install_type_id'].'" selected>'.$rowItem['install_type'].'</option>';
                                                    }else{   
                                                        echo '<option value="'.$rowItem['install_type_id'].'">'.$rowItem['install_type'].'</option>';
                                                    };
                                                };
                                            }else{
                                                echo '<option value="">NO INSTALL TYPES FOUND</option>';
                                            };
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="windowMfg" class="form-label col-3 text-right">Window Vendor:</label>
                                <div class="col-5">
                                    <select class="form-control" id="windowMfg" name="windowMfgIdSelected">
                                        <?php echo '<option value="">SELECT WINDOW MANUFACTURER</option>';
                                            if(!empty($tbl_manufacturer)){
                                                foreach($tbl_manufacturer as $rowItem){
                                                    if($rowItem['manufacturer_id'] == $subdivision_data[0]['manufacturer_id']){
                                                        echo '<option value="'.$rowItem['manufacturer_id'].'" selected>'.$rowItem['manufacturer_name'].'</option>';
                                                    }else{   
                                                        echo '<option value="'.$rowItem['manufacturer_id'].'">'.$rowItem['manufacturer_name'].'</option>';
                                                    };
                                                };
                                            }else{
                                                echo '<option value="">NO WINDOW MANUFACTURER SET</option>';
                                            };
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php if($title == 'Add Subdivision'){
                                    echo '<div class="form-group row">';
                                    echo '<div class="col-8"><button type="submit" class="btn btn-info" id="enterSubdivision">Add Subdivision</button>';
                                    echo '<button type="button" class="btn btn-danger" id="cancelEntry_edit">Cancel</button></div></div>';
                                  }else{
                                    echo '<div class="form-group row">';
                                    echo '<div class="col-8"><button type="submit" class="btn btn-info" id="updateSubdivision">Update Subdivision</button>';
                                    echo '<button type="button" class="btn btn-danger" id="cancelEntry_edit">Cancel</button></div></div>';
                                  };
                            ?>
                        </div>
                        <div class="tab-pane fade show" id="v-pills-geo" role="tabpanel" aria-labelledby="v-pills-geo-tab">
                    		<div class="form-group row">
                                <label for="subdivisionCity" class="form-label col-3 text-right">Subdivision City:</label>
                                <div class="col-5">
                    				<input type="text" class="form-control" id="subdivisionCity" name="subdivisionCityInput" value="<?php echo isset($subdivision_data[0]['city']) ? set_value("subdivisionCityInput", $subdivision_data[0]['city']) : set_value("subdivisionCityInput"); ?>" placeholder="CHANDLER" <?php echo $auth_edit; ?>>
                    			</div>
                    		</div>
                    		<div class="form-group row">
                                <label for="subdivisionStateInput" class="form-label col-5 text-right">Subdivision State (2 letter abbreviation):</label>
                                <div class="col-3">
                    				<input type="text" class="form-control" id="subdivisionStateInput" name="subdivisionStateInput" value="<?php echo isset($subdivision_data[0]['state']) ? set_value("subdivisionStateInput", $subdivision_data[0]['state']) : set_value("subdivisionStateInput"); ?>" placeholder="AZ"  <?php echo $auth_edit; ?>>
                    			</div>
                    		</div>
                    		<div class="form-group row">
                                <label for="subdivisionZip" class="form-label col-3 text-right">Subdivision Zip:</label>
                                <div class="col-5">
                    				<input type="text" class="form-control" id="subdivisionZip" name="subdivisionZipInput" value="<?php echo isset($subdivision_data[0]['zip']) ? set_value("subdivisionZipInput", $subdivision_data[0]['zip']) : set_value("subdivisionZipInput"); ?>" placeholder="85286"  <?php echo $auth_edit; ?>>
                    			</div>
                    		</div>
                            <div class="form-group row">
                                <label for="zone" class="form-label col-3 text-right">Subdivision Zone:</label>
                                <div class="col-5">
                                    <select class="form-control" id="zone" name="zoneIdSelected">
                                        <?php echo '<option value="">SELECT ZONE</option>';
                                            if(!empty($tbl_zones)){
                                                foreach($tbl_zones as $rowItem){ 
                                                    if($rowItem['zone_id'] == $subdivision_data[0]['tbl_zone_zone_id']){
                                                        echo '<option value="'.$rowItem['zone_id'].'" selected>'.$rowItem['zone_name'].'</option>';
                                                    }else{   
                                                        echo '<option value="'.$rowItem['zone_id'].'">'.$rowItem['zone_name'].'</option>';
                                                    };
                                                };
                                            }else{
                                                echo '<option value="">NO ZONES FOUND</option>';
                                            };
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="branch" class="form-label col-3 text-right">XO Branch:</label>
                                <div class="col-5">
                                    <select class="form-control" id="branch" name="branchIdSelected">
                                        <?php    
                                        echo '<option value="">SELECT BRANCH</option>';
                                            if(!empty($tbl_xo_branch)){
                                                foreach($tbl_xo_branch as $rowItem){
                                                    if($rowItem['xo_branch_id'] == $subdivision_data[0]['tbl_xo_branch_xo_branch_id']){
                                                        echo '<option value="'.$rowItem['xo_branch_id'].'" selected>'.$rowItem['location'].'</option>';
                                                    }else{
                                                        echo '<option value="'.$rowItem['xo_branch_id'].'">'.$rowItem['location'].'</option>';
                                                    }
                                                }
                                            }else{
                                                echo '<option value="">NO BRANCH FOUND</option>';
                                            }                        
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php if($title == 'Add Subdivision'){
                                    echo '<div class="form-group row">';
                                    echo '<div class="col-8"><button type="submit" class="btn btn-info" id="enterSubdivision">Add Subdivision</button>';
                                    echo '<button type="button" class="btn btn-danger" id="cancelEntry_geo">Cancel</button></div></div>';
                                  }else{
                                    echo '<div class="form-group row">';
                                    echo '<div class="col-8"><button type="submit" class="btn btn-info" id="updateSubdivision">Update Subdivision</button>';
                                    echo '<button type="button" class="btn btn-danger" id="cancelEntry_geo">Cancel</button></div></div>';
                                  };
                            ?>
                        </div>
                        <div class="tab-pane fade show" id="v-pills-people" role="tabpanel" aria-labelledby="v-pills-people-tab">
                            <div class="form-group row">
                                <label for="startsRep" class="form-label col-3 text-right">Starts Representative:</label>
                                    <div class="col-5">
                                    <select class="form-control" id="startsRep" name="startsRepIdSelected">
                                        <?php echo '<option value="">SELECT STARTS REP</option>';
                                            if(!empty($tbl_xo_users)){
                                                foreach($tbl_xo_users as $rowItem){
                                                    if($rowItem['user_id'] == $subdivision_data[0]['tbl_users_tbl_user_id_starts']){
                                                        echo '<option value="'.$rowItem['user_id'].'" selected>'.$rowItem['user_login'].'</option>';
                                                    }else{   
                                                        echo '<option value="'.$rowItem['user_id'].'">'.$rowItem['user_login'].'</option>';
                                                    };
                                                };
                                            }else{
                                                echo '<option value="">NO REPS FOUND</option>';
                                            };
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fieldSuper" class="form-label col-3 text-right">XO Field Super:</label>
                                <div class="col-5">
                                    <select class="form-control" id="fieldSuper" name="fieldSuperIdSelected">
                                        <?php echo '<option value="">SELECT XO Field Super</option>';
                                            if(!empty($tbl_xo_users)){
                                                foreach($tbl_xo_users as $rowItem){
                                                    if($rowItem['user_id'] == $subdivision_data[0]['tbl_users_tbl_user_id_xosupers']){
                                                        echo '<option value="'.$rowItem['user_id'].'" selected>'.$rowItem['user_login'].'</option>';
                                                    }else{   
                                                        echo '<option value="'.$rowItem['user_id'].'">'.$rowItem['user_login'].'</option>';
                                                    };
                                                };
                                            }else{
                                                echo '<option value="">NO XO Field Supers FOUND</option>';
                                            };
                                        ?>
                                    </select>
                                </div>
                            </div>
                                <div class="form-group row">
                                    <label for="superName" class="col-3 text-right">Super's Name:</label>
                                    <div class="col-5">
                                        <input type="text" class="form-control" id="superName" name="superNameInput" value="<?php echo isset($subdivision_data[0]['super_name']) ? set_value("superNameInput", $subdivision_data[0]['super_name']) : set_value("superNameInput"); ?>" placeholder="Subdivision Super's Name" <?php echo $auth_edit; ?>>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="superPhone" class="col-3 text-right">Super's Phone Number:</label>
                                    <div class="col-5">
                                        <input type="text" class="form-control" id="superPhone" name="superPhoneInput" value="<?php echo isset($subdivision_data[0]['phone_number']) ? set_value("superPhoneInput", $subdivision_data[0]['phone_number']) : set_value("superPhoneInput"); ?>" placeholder="6024556616" <?php echo $auth_edit; ?>>
                                    </div>
                                </div>
                            <?php if($title == 'Add Subdivision'){
                                    echo '<div class="form-group row">';
                                    echo '<div class="col-8"><button type="submit" class="btn btn-info" id="enterSubdivision">Add Subdivision</button>';
                                    echo '<button type="button" class="btn btn-danger" id="cancelEntry_people">Cancel</button></div></div>';
                                  }else{
                                    echo '<div class="form-group row">';
                                    echo '<div class="col-8"><button type="submit" class="btn btn-info" id="updateSubdivision">Update Subdivision</button>';
                                    echo '<button type="button" class="btn btn-danger" id="cancelEntry_people">Cancel</button></div></div>';
                                  };
                            ?>
                        </div>
                        <div class="tab-pane fade show" id="v-pills-stats" role="tabpanel" aria-labelledby="v-pills-stats-tab">
                            <div class="form-group row">
                                <label for="stats" class="form-label col-3 text-right">Statistics:</label>
                                    <div class="col-5">
                                    </div>
                            </div>
                        </div>
                <!-- LOADED data for 'what changed' comparision -->          
                        <input type="text" id="subdivisionRecordID" name="subdivisionRecordID" value="<?php echo isset($subdivision_data[0]['subdivision_id']) ? set_value("subdivisionRecordID", $subdivision_data[0]['subdivision_id']) : set_value("subdivisionRecordID"); ?>" hidden>
                        <input type="text" id="builderOnLoad" name="builderOnLoad" value="<?php echo isset( $subdivision_data[0]['tbl_buildercustomer_builder_id']) ? set_value("builderOnLoad", $subdivision_data[0]['tbl_buildercustomer_builder_id']) : set_value("builderOnLoad"); ?>" hidden>
                        <input type="text" id="nameOnLoad" name="nameOnLoad" value="<?php echo isset( $subdivision_data[0]['subdivision_name']) ? set_value("nameOnLoad", $subdivision_data[0]['subdivision_name']) : set_value("nameOnLoad"); ?>" hidden>
                        <input type="text" id="statusOnLoad" name="statusOnLoad" value="<?php echo isset( $subdivision_data[0]['tbl_status_tbl_status_id']) ? set_value("statusOnLoad", $subdivision_data[0]['tbl_status_tbl_status_id']) : set_value("statusOnLoad"); ?>" hidden>
                        <input type="text" id="installTypeOnLoad" name="installTypeOnLoad" value="<?php echo isset( $subdivision_data[0]['install_type_id']) ? set_value("installTypeOnLoad", $subdivision_data[0]['install_type_id']) : set_value("installTypeOnLoad"); ?>" hidden>
                        <input type="text" id="windowMfgOnLoad" name="windowMfgOnLoad" value="<?php echo isset( $subdivision_data[0]['manufacturer_id']) ? set_value("windowMfgOnLoad", $subdivision_data[0]['manufacturer_id']) : set_value("windowMfgOnLoad"); ?>" hidden>
                        <input type="text" id="cityOnLoad" name="cityOnLoad" value="<?php echo isset( $subdivision_data[0]['city']) ? set_value("cityOnLoad", $subdivision_data[0]['city']) : set_value("cityOnLoad"); ?>" hidden>
                        <input type="text" id="stateOnLoad" name="stateOnLoad" value="<?php echo isset( $subdivision_data[0]['state']) ? set_value("stateOnLoad", $subdivision_data[0]['state']) : set_value("stateOnLoad"); ?>" hidden>
                        <input type="text" id="zipOnLoad" name="zipOnLoad" value="<?php echo isset( $subdivision_data[0]['zip']) ? set_value("zipOnLoad", $subdivision_data[0]['zip']) : set_value("zipOnLoad"); ?>" hidden>
                        <input type="text" id="branchOnLoad" name="branchOnLoad" value="<?php echo isset( $subdivision_data[0]['tbl_xo_branch_xo_branch_id']) ? set_value("branchOnLoad", $subdivision_data[0]['tbl_xo_branch_xo_branch_id']) : set_value("branchOnLoad"); ?>" hidden>
                        <input type="text" id="zoneOnLoad" name="zoneOnLoad" value="<?php echo isset( $subdivision_data[0]['tbl_zone_zone_id']) ? set_value("zoneOnLoad", $subdivision_data[0]['tbl_zone_zone_id']) : set_value("zoneOnLoad"); ?>" hidden>
                        <input type="text" id="startsRepOnLoad" name="startsRepOnLoad" value="<?php echo isset( $subdivision_data[0]['tbl_users_tbl_user_id_starts']) ? set_value("startsRepOnLoad", $subdivision_data[0]['tbl_users_tbl_user_id_starts']) : set_value("startsRepOnLoad"); ?>" hidden>
                        <input type="text" id="fieldRepOnLoad" name="fieldRepOnLoad" value="<?php echo isset( $subdivision_data[0]['tbl_users_tbl_user_id_xosupers']) ? set_value("fieldRepOnLoad", $subdivision_data[0]['tbl_users_tbl_user_id_xosupers']) : set_value("fieldRepOnLoad"); ?>" hidden>
                        <input type="text" id="superSuperIdOnLoad" name="superSuperIdOnLoad" value="<?php echo isset( $subdivision_data[0]['super_id']) ? set_value("superSuperIdOnLoad", $subdivision_data[0]['super_id']) : set_value("superSuperIdOnLoad"); ?>" hidden>
                        <input type="text" id="superSuperNameOnLoad" name="superSuperNameOnLoad" value="<?php echo isset( $subdivision_data[0]['super_name']) ? set_value("superSuperNameOnLoad", $subdivision_data[0]['super_name']) : set_value("superSuperNameOnLoad"); ?>" hidden>
                        <input type="text" id="superSuperPhoneOnLoad" name="superSuperPhoneOnLoad" value="<?php echo isset( $subdivision_data[0]['phone_number']) ? set_value("superSuperPhoneOnLoad", $subdivision_data[0]['phone_number']) : set_value("superSuperPhoneOnLoad"); ?>" hidden>

        <?php echo form_close(); ?>
<!-- Audit Log tab-->
                <?php if($subdivision_data[0]['tbl_buildercustomer_builder_id']){ //EDIT logic because Builder ID was passed in ?>
                        <div class="tab-pane fade" id="v-pills-audit" role="tabpanel" aria-labelledby="v-pills-audit-tab">
                            <div class="row">
                                <div class="col-2"></div>
                                <?php $this->view('maintenance/builders_log'); ?>
                            </div>
                        </div>
                <?php }; ?>
                    </div> <?php // END all tab content ?>
                </div> <?php // END right-side body of all pages ?>
            </div> <?php // END entire row ?>
        </div> <?php // END container-fluid ?>
    </div> <?php // END text-center ?>
</div> <?php // END entire container ?>

<script type="text/javascript">
    $(document).ready(function(){

        $('form.jsform').on('submit', function(form){
            form.preventDefault();
            //log_message('debug', 'jsformPost: ' . print_r($('form.jsform').serialize(), TRUE));
            $.post('<?php echo base_url();?>index.php/maintenance/check_subdivision_add_form', $('form.jsform').serialize(), function(data){
                $('div.jsError').html(data);
            });
            setTimeout(function(){
                //location.reload();
                window.location.href = "<?php echo site_url(); ?>/Maintenance/subdivisions";
            }, 5000);
        });

        /* Populate data to install type dropdown*/
        $('#builder').on('change',function(){
            var builderID = $(this).val();
            var selectedBldr = $('[name="builderIdSelected"] option:selected');       
            if(builderID){
                $.ajax({
                    type:'POST',
                    url:'<?php echo site_url();?>/maintenance/get_builders_install_types',
                    data:'builder_id='+builderID,
                    success:function(data){
                        $('#installType').html('<option value="">SELECT INSTALL TYPE</option>');
                        var dataObj = jQuery.parseJSON(data);
                        if(dataObj){
                            $(dataObj).each(function(){
                                var option = $('<option />');
                                option.attr('value', this.install_type_id).text(this.install_type);
                                $('#installType').append(option);
                            });
                        }else{
                            $('#installType').html('<option value="">NO INSTALL TYPES SET FOR BUILDER</option>');
                        }
                    }
                }); 
            }else{
                $('#installType').html('<option value="">SELECT BUILDER FIRST</option>');
            };
        });

        /* Cancel Button */
        $('#cancelEntry_edit, #cancelEntry_geo, #cancelEntry_people').on('click', function(){
            window.location.href = "<?php echo site_url(); ?>/Maintenance/subdivisions";
        });

    });
</script>