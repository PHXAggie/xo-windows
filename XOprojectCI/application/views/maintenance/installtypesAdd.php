
		<div class="text-center">
			<div class="container">
				<h1>Install Types Maintenance</h1>
				<?php echo form_open(base_url('maintenance/check_install_type_add_form'), array('class' => 'jsform')); ?>
					<div class="form-group row justify-content-center">
						<label for="installtypeName" class="form-label">Install Type Name:&nbsp;</label>
							<input type="text" class="form-control col-6" id="installtypeNameInput" name="installtypeNameInput">&nbsp;&nbsp;<button type="submit" class="btn btn-info col-2" id="enterInstallType">Add</button>
					</div>
				<?php echo form_close(); ?>

				<div class="jsError"></div>
			</div>
		</div>

		<script type="text/javascript">
			$(document).ready(function(){
				$('form.jsform').on('submit', function(form){
					form.preventDefault();
					$.post('<?php echo base_url();?>index.php/maintenance/check_install_type_add_form', $('form.jsform').serialize(), function(data){
						$('div.jsError').html(data);
					});
					setTimeout(function(){
            		//location.reload();
            		window.location.href = "<?php echo base_url(); ?>index.php/Maintenance/installtypes";
            		}, 5000);
				});
			});
		</script>
