<div class="container">
    <div class="text-center">
        <div class="form-group row"></div>
        <div class="form-group row"></div>
        <h1>Builders Maintenance&nbsp;&nbsp;<a class="btn btn-success" href="<?php echo site_url(); ?>/maintenance/buildersAdd">Add Builder</a></h1>
        <?php $this->load->view('search.php'); ?>
        <div class="jsError"></div>

        <?php if(empty($tbl_buildercustomer)){ ?>
            <?php echo 'No Builder Records found!'; ?>
        <?php } else { ?>
            <div class="row justify-content-center">
                <div class="col-8">
                    <table class="table table-bordered table-sm table-hover" id="builderResultsTable">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Builder</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>

                        <tbody>
            	           <?php foreach($tbl_buildercustomer as $item) : ?>
                                <tr>
                                    <th scope="row"><?php echo $item['builder_name']; ?></th>
                                    <td hidden><?php echo $item['tbl_status_tbl_status_id']; ?></td>
                                    <?php $cleanedBuilder_Name = $item['builder_name']; ?>
                                <?php //$cleanedBuilder_Name = str_replace('(', 'operen', $cleanedBuilder_Name); ?>
                                <?php //$cleanedBuilder_Name = str_replace(')', 'cperen', $cleanedBuilder_Name); ?>
                                <?php //$cleanedBuilder_Name = str_replace('/', 'slash', $cleanedBuilder_Name); ?>
                                    <?php echo 
                                    "<td>
                                        <a class='btn-sm btn-info col-4' href='".site_url('/maintenance/buildersedit/'.$item['builder_id'].'/'.$item['builder_name'].'/'.$item['tbl_status_tbl_status_id'])."'>Edit
                                        </a>&nbsp;
                                        <a class='builder btn-sm btn-secondary col-4' href='#builderEditModal' data-toggle='modal' data-builder-id='" . $item['builder_id'] . "' data-builder-name='" . $item['builder_name'] . "' data-update-date='" . date('m/d/Y', strtotime($item['update_date'])) . "' data-update-time='" . $item['update_time'] . "' data-update-by='" . $item['login'] . "' data-builder-status='" . $item['name'] . "'>Info
                                        </a>
                                    </td>";
                                    ?>
                                </tr>
            	           <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<!-- Builder Details Modal (Pop-up) - START -->
<div class="modal fade" id="builderEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Builder Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
            </div>
            <div class="modal-body text-center">
                <form>
                    <fieldset class="form-group">
                        <h4 id="builderName"></h4>
                    </fieldset>
                    <fieldset class="form-group">
                        <p id="builderStatus"></p>
                    </fieldset>                    
                    <fieldset class="form-group">
                        <p id="updateDateBy"></p>
                    </fieldset>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Builder Details Modal (Pop-up) - END -->

<!-- <script> -->
<?php if($js_to_load != ''){ ?>

    <script>
        $(document).ready(function(){
            // Show Builder Record Modal popup - START
            $(".builder").click(function(){
                $("#builderName").html("<strong>" + $(this).data('builder-name') + "</strong>");
                $("#builderStatus").html("<strong>" + $(this).data('builder-status') + "</strong>");
                $("#updateDateBy").html("<strong>Last updated:</strong> " + $(this).data('update-date') + " at " + $(this).data('update-time') + " <strong>BY</strong> " + $(this).data('update-by'));
                //$("#createDate").html("Created: " + $(this).data('create-date'));
                $('#builderEditModal').modal('show');
            });
            // Show Builder Record Modal popup - END
        });
    </script>
    <script type="text/javascript">var $searchBy = '<?php echo $searchBy; ?>'</script>
    <script type="text/javascript" src="../../assets/js/<?=$js_to_load;?>"></script>

<?php }else{ ?>
    <script>
        $(document).ready(function(){
            $('#search').show();
        });
<?php } ?>
</script> 