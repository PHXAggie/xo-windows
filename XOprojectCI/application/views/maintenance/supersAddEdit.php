<div class="container">
	<div class="text-center">
		<h2><?php echo $title; ?></h2>
		<div class="jsError"></div>
		<?php if($this->session->userdata('level') <= '4'){
			$readonly = '';
		}else{
			$readonly = 'readonly';
		}

		echo form_open(base_url('maintenance/check_super_add_form'), array('class' => 'jsform form-horizontal')); 

			if($jobsite_data[0]['subdivision_name']){
?>
				<div class="container-fluid">
					<div class="row">
						<div class="col-2">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link text-left active" id="v-pills-edit-tab" data-toggle="pill" href="#v-pills-edit" role="tab" aria-controls="v-pills-edit" aria-selected="true">General</a>
                                <a class="nav-link text-left" id="v-pills-audit-tab" data-toggle="pill" href="#v-pills-audit" role="tab" aria-controls="v-pills-audit" aria-selected="false">Audit Log</a>
                            </div>
                        </div>
                        <div class="col-10 border-left border-info">
                        	<div class="tab-pane fade show active" id="v-pills-edit" role="tabpanel" aria-labelledby="v-pills-edit-tab">
                        		<div class="form-group row">
                        			<label for="builder" class="col-3 text-right">Builder:</label>
                        			<div class="col-5">
                        				<input type="text" class="form-control" id="builderName" name="builderName" value="<?php echo $jobsite_data[0]['builder_name']; ?>" readonly>
                        			</div>
                        		</div>
                        		<div class="form-group row">
                        			<label for="subdivision" class="col-3 text-right">Subdivision:</label>
                        			<div class="col-5">
                        				<input type="text" class="form-control" id="subdivisionName" name="subdivisionName" value="<?php echo $jobsite_data[0]['subdivision_name']; ?>" readonly>
                        			</div>
                        		</div>
                        		<div class="form-group row">
                        			<label for="superName" class="col-3 text-right">Super's Name:</label>
                        			<div class="col-5">
                        				<input type="text" class="form-control" id="superName" name="superName" value="<?php echo $jobsite_data[0]['name']; ?>">
                        			</div>
                        		</div>
                        		<div class="form-group row">
                        			<label for="superPhone" class="col-3 text-right">Phone Number:</label>
                        			<div class="col-5">
                        				<input type="text" class="form-control" id="superPhone" name="superPhone" value="<?php echo $jobsite_data[0]['phone_number']; ?>">
                        			</div>
                        		</div>
                        	</div>
                        </div>
<?php 

						}else{
?>
								<div class="form-group row">
									<label for="subdivision" class="col-md-4 text-right">Subdivision:</label>
									<div class="col-md-4">
<?php
										echo '<select class="form-control" id="subdivision" name="subdivisionIdSelected">';
										echo '<option value="">SELECT SUBDIVISION</option>';
										if(!empty($tbl_subdivision)){
											foreach($tbl_subdivision as $row){
												if($row['subdivision_id'] == $jobsite_data[0]['subdivision_id']){
													echo '<option value="' . $row['subdivision_id'] . '" selected>' . $row['subdivision_name'] . '</option>';
												}else{
													echo '<option value="' . $row['subdivision_id'] . '">' . $row['subdivision_name'] . '</option>';
												};
											};
										}else{
											echo '<option value="">NO SUBDIVISIONS FOUND</option>';
										};
										echo '</select>';
						};
?>
	</div>
</div>