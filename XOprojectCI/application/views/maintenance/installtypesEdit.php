<div class="text-center">
	<div class="container">
		<h1>Edit Install Type</h1>
		<div class="jsError"></div>
		<?php if(empty($install_types_id)){ ?>
            <?php echo 'No Install Type Records passed in!'; ?>
        <?php } else { ?>
			<?php echo form_open(base_url('index.php/maintenance/install_type_save'), array('class' => 'jsform')); ?>

	<!-- Install Type Textbox -->
	            <div class="form-group row justify-content-center">
                	<input type="text" id="installtypeid" name="installtypeid" value="<?php echo $install_types_id; ?>" hidden>
                	<input type="text" class="col-6" id="installtypeNameInput" name="installtypeNameInput" value="<?php echo $type; ?>" disabled>
                		<input type="text" id="installtypeNameOnLoad" name="installtypeNameOnLoad" value="<?php echo $type; ?>" hidden>
					<button type="button" class="btn btn-info col-2" id="unlockEditInstallTypeForm" name="unlockEditInstallTypeForm">Edit Install Type</button>
					<button type="submit" class="btn btn-primary col-2" id="saveEditInstallTypeForm" name="saveEditInstallTypeForm"  style="display:none;">Save Changes</button>
					<button type="button" class="btn btn-danger col-2" id="cancelEditInstallTypeForm" name="cancelEditInstallTypeForm" style="display:none;">Cancel</button>
	            </div>
			<?php echo form_close(); ?>
	    <?php } ?>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){

        $('form.jsform').on('submit', function(form){
            form.preventDefault();
            $.post('<?php echo base_url();?>index.php/maintenance/install_type_save/', $('form.jsform').serialize(), function(data){
                $('div.jsError').html(data);
            });
            
            setTimeout(function(){
            	//location.reload();
            	window.location.href = "<?php echo base_url(); ?>index.php/Maintenance/installtypes";
            }, 5000);

        });

        $('#unlockEditInstallTypeForm').on('click', function(){
                $('#unlockEditInstallTypeForm').hide();
                $('#cancelEditInstallTypeForm').show();
                $('#installtypeNameInput').prop('disabled', false);
        });

        $('#installtypeNameInput').keyup(function(event){
        	$('#installtypeNameInput').removeClass('col-6').addClass('col-4');
            $('#saveEditInstallTypeForm').show();
            if(event.keyCode==27){
        		location.reload(true);           	
            }
        });

        $('#cancelEditInstallTypeForm').on('click', function(){
            location.reload(true);
        });

    });
</script>