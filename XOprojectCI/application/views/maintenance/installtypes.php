<div class="container">
    <div class="text-center">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <?php if(empty($tbl_install_types)){ ?>
                        <?php echo 'No Install Type Records found!'; ?>
                    <?php } else { ?>
                        <table class="table table-bordered table-sm table-hover" id="installtypesResultsTable">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Install Types</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                	           <?php foreach($tbl_install_types as $item) : ?>
                                    <tr>
                                        <th scope="row"><?php echo $item['type']; ?></th>
                                        <!--<?php //echo "<td id='" . $item['tbl_status_tbl_status_id'] . "'>"; ?></td>-->
                                        <td hidden><?php echo $item['install_types_id']; ?></td>
                                        <?php $cleanedInstall_Type = $item['type']; ?>
                                        <?php $cleanedInstall_Type = str_replace('(', 'operen', $cleanedInstall_Type); ?>
                                        <?php $cleanedInstall_Type = str_replace(')', 'cperen', $cleanedInstall_Type); ?>
                                        <?php $cleanedInstall_Type = str_replace('/', 'slash', $cleanedInstall_Type); ?>
                                        <?php $cleanedInstall_Type = str_replace('"', 'qt', $cleanedInstall_Type); ?>
                                        <?php echo "<td><a class='btn-sm btn-info' href='".base_url('index.php/maintenance/installtypeedit/'.$item['install_types_id'].'/'.$cleanedInstall_Type)."'>Edit</a>&nbsp;<a class='installtype btn-sm btn-secondary' href='#installtypeInfoModal' data-toggle='modal' data-installtype-id='" . $item['install_types_id'] . "' data-installtype-name='" . $item['type'] . "' data-update-date='" . date('m/d/Y', strtotime($item['update_date'])) . "' data-update-time='" . $item['update_time'] . "' data-update-by='" . $item['updated_by'] . "'>Info</a></td>"; ?>
                                    </tr>
                	           <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- Install Types Info Modal -->
    <div class="modal fade" id="installtypeInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Install Type Details</h4>
                </div>
                <div class="modal-body text-center">
                    <form>
                        <fieldset class="form-group">
                            <h4 id="installtypeName"></h4>
                        </fieldset>
                        <fieldset class="form-group">
                            <p id="updateDateBy"></p>
                        </fieldset>
                    </form>
                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
    // Set values at load - START

    // Set values at load - END

    // Show Builder Record Modal popup - START
        $(".installtype").click(function(){
            $("#installtypeName").html("<strong>" + $(this).data('installtype-name') + "</strong>");
            $("#updateDateBy").html("<strong>Last updated:</strong> " + $(this).data('update-date') + " at " + $(this).data('update-time') + " <strong>BY</strong> " + $(this).data('update-by'));
            $('#installtypeInfoModal').modal('show');
        });
    // Show Builder Record Modal popup - END
    });
</script> 