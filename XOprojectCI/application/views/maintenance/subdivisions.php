<div class="container">
    <div class="text-center">
        <div class="form-group row"></div>
        <div class="form-group row"></div>
        <h1>Subdivisions Maintenance&nbsp;&nbsp;<a class="btn btn-success" href="<?php echo site_url(); ?>/maintenance/subdivisionsAddEdit">Add Subdivision</a></h1>
        <?php $this->load->view('search.php'); ?>
        <div class="jsError"></div>
        
        <?php if(empty($tbl_subdivision)){ ?>
            <?php echo 'No Subdivision Records found!'; ?>
        <?php } else { ?>
            <table class="table table-bordered text-xsmall table-sm table-hover" id="subdivisionsResultsTable">
                <thead class="thead-light">
                    <tr>
                        <th scope="col" class="text-left">Subdivision</th>
                        <th scope="col" class="text-left">Builder</th>
                        <th scope="col" class="text-left">City</th>
                        <th scope="col" class="text-left">Zip</th>
                        <th scope="col" class="text-left">Jobsite Super</th>
                        <th scope="col" class="text-left">Phone Number</th>
                        <th scope="col" class="text-left">Starts Rep</th>
                        <th scope="col" class="text-left">XO Branch</th>
                        <th scope="col" class="text-left">Mfg</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
    	            <?php foreach($tbl_subdivision as $item) : ?>
                        <tr>
                            <th scope="row" class="text-left"><?php echo $item['subdivision_name']; ?></th>
                            <td hidden><?php echo $item['tbl_status_tbl_status_id']; //needed for the list filter, Active, Inactive, etc ?></td>
                            <td class="text-left"><?php echo $item['builder_name']; ?></td>
                            <td class="text-left"><?php echo $item['city']; ?></td>
                            <td class="text-left"><?php echo $item['zip']; ?></td>
                            <td class="text-left"><?php echo $item['super_name']; ?></td>
                            <td class="text-left"><?php echo '('.substr($item['phone_number'], 0, 3).') '.substr($item['phone_number'], 3, 3).'-'.substr($item['phone_number'], 6); ?></td>
                            <td class="text-left"><?php echo $item['starts_login']; ?></td>
                            <td class="text-left"><?php echo $item['loc_abbr']; ?></td>                        
                            <td class="text-left"><?php echo $item['mfg_abbr']; ?></td>
                        <!-- Actions -->
                            <?php echo "<td><a class='btn-sm btn-info' href='".site_url('/maintenance/subdivisionsAddEdit/'.$item['subdivision_id'] /*.'/'.$item['subdivision_name'].'/'.$item['tbl_status_tbl_status_id'] */)."' 
                            >Edit</a>&nbsp;<a class='subdivision btn-sm btn-secondary' href='#subdivisionEditModal' data-toggle='modal' 
                                data-subdivision-id='" . $item['subdivision_id'] . "' 
                                data-subdivision-name='" . $item['subdivision_name'] . "' 
                                data-subdivision-status='" . $item['tbl_status_name'] . "' 
                                data-subdivision-builder='" . $item['builder_name'] . "' 
                                data-subdivision-installtype='" . $item['installation_type'] . "' 
                                data-subdivision-mfg-name='" . $item['manufacturer_name'] . "'
                                data-subdivision-mfg='" . $item['mfg_abbr'] . "' 
                                data-subdivision-branch='" . $item['location'] . "' 
                                data-subdivision-branch-abbr='" . $item['loc_abbr'] . "' 
                                data-subdivision-zone='" . $item['zone_name'] . "' 
                                data-subdivision-city='" . $item['city'] . "' 
                                data-subdivision-state='" . $item['state'] . "' 
                                data-subdivision-zip='" . $item['zip'] . "' 
                                data-subdivision-super='" . $item['super_name'] . "' 
                                data-subdivision-super-phone='" . '('.substr($item['phone_number'], 0, 3).') '.substr($item['phone_number'], 3, 3).'-'.substr($item['phone_number'], 6) . "' 
                                data-subdivision-starts='" . $item['starts_login'] . "' 
                                data-subdivision-field-rep='" . $item['field_login'] . "' 
                                data-subdivision-field-phone='" . '('.substr($item['phone_number'], 0, 3).') '.substr($item['phone_number'], 3, 3).'-'.substr($item['phone_number'], 6) . "' 
                                data-update-date='" . date('m/d/Y', strtotime($item['update_date'])) . "' 
                                data-update-time='" . $item['update_time'] . "' 
                                data-update-by='" . $item['login'] . "' 
                            >Info</a></td>"; ?>
                        </tr>
    	            <?php endforeach; ?>
                </tbody>
            </table>
        <?php } ?>
    </div>
</div>

    <!-- Subdivision Details Modal -->
    <div class="modal fade" id="subdivisionEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="subdivisionName"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                </div>
                <div class="modal-body text-center">
                    <form>
                        <fieldset class="form-group">
                            <h6 id="statusBuilder"></h6>
                        </fieldset>
                        <fieldset class="form-group">
                            <h6 id="installTypeSet"></h6>
                        </fieldset>
                        <fieldset class="form-group">
                            <h6 id="windowVendor"></h6>
                        </fieldset>
                        <fieldset class="form-group">
                            <h6 id="branchZone"></h6>
                        </fieldset>
                        <fieldset class="form-group">
                            <h6 id="cityStateZip"></h6>
                        </fieldset>
                        <fieldset class="form-group">
                            <h6 id="superSuperPhone"></h6>
                        </fieldset>
                        <fieldset class="form-group">
                            <h6 id="startRep"></h6>
                        </fieldset>
                        <fieldset class="form-group">
                            <h6 id="xoFieldRep"></h6>
                        </fieldset>
                        <fieldset class="form-group">
                            <h6 id="updateDateBy"></h6>
                        </fieldset>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <script> -->
<?php if($js_to_load != ''){ ?>

    <script>
    $(document).ready(function(){
        // Show Subdivision Record Modal popup - START
        $(".subdivision").click(function(){
            $("#subdivisionName").html("<strong>" + $(this).data('subdivision-name') + "</strong>");
            $("#statusBuilder").html("<strong>Status: </strong>" + $(this).data('subdivision-status') + " | " + "<strong>Builder: </strong>" + $(this).data('subdivision-builder'));
            $("#installTypeSet").html("" + $(this).data('subdivision-installtype') + "");
            $("#windowVendor").html("<strong>Window Vendor: </strong>" + $(this).data('subdivision-mfg-name') + " (" + $(this).data('subdivision-mfg') + ")");
            $("#branchZone").html($(this).data('subdivision-branch') + " (" + $(this).data('subdivision-branch-abbr') + ") | " + $(this).data('subdivision-zone'));
            $("#cityStateZip").html($(this).data('subdivision-city') + ", " + $(this).data('subdivision-state') + " " + $(this).data('subdivision-zip'));
            $("#superSuperPhone").html("<strong>Super: </strong>" + $(this).data('subdivision-super') + " | <strong>Phone: </strong>" + $(this).data('subdivision-super-phone'));
            $("#startRep").html("<strong>XO Starts Rep: </strong>" + $(this).data('subdivision-starts'));
            $("#xoFieldRep").html("<strong>XO Field Rep: </strong>" + $(this).data('subdivision-field-rep') + " | <strong>Phone: </strong>" + $(this).data('subdivision-field-phone'));
            $("#updateDateBy").html("<strong>Last updated:</strong> " + $(this).data('update-date') + " at " + $(this).data('update-time') + " <strong>by</strong> " + $(this).data('update-by'));
            $("#subdivisionEditModal").modal('show');
        });
        // Show Subdivision Record Modal popup - END
    });
    </script>
    <script type="text/javascript">var $searchBy = '<?php echo $searchBy; ?>'</script>
    <script type="text/javascript" src="../../assets/js/<?=$js_to_load;?>"></script>

<?php }else{ ?>
    <script>
        $(document).ready(function(){
            $('#search').show();
        });
    </script>
<?php }; ?>