<!DOCTYPE html>
  <head>
  	<title>XO</title>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">
    <link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" type="image/x-icon">
  </head>

<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary sticky-top">
      <a class="navbar-brand" href="<?php echo site_url(); ?>/home">XO Windows</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url(); ?>/home">Home <span class="sr-only">(current)</span></a>
          </li>
          <?php if($this->session->userdata('level') <= '4'):?>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo site_url(); ?>/bids">Bids</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo site_url(); ?><?php ///starts ?>">Starts</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Maintenance</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <h6 class="dropdown-header">Maintenance</h6>
                  <a class="dropdown-item" href="<?php echo site_url(); ?>/Maintenance/builders">Builders</a>
                  <a class="dropdown-item" href="<?php echo site_url(); ?>/Maintenance/installtypes">Install Types</a>
                  <a class="dropdown-item" href="<?php echo site_url(); ?>/Manufacturers/landingPage">Manufacturers</a>
                  <a class="dropdown-item" href="<?php echo site_url(); ?><?php ///Maintenance/windowtypes ?>">Window Types</a>
                  <a class="dropdown-item" href="<?php echo site_url(); ?>/itemCRUD/index">ItemCRUD</a>
                  <a class="dropdown-item" href="<?php echo site_url(); ?>/Maintenance/subdivisions">Subdivisions</a>
                  <a class="dropdown-item" href="<?php echo site_url(); ?>/Maintenance/supers">Jobsite Supers</a>
                <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url(); ?><?php ///Maintenance/xousers ?>">XO Users</a>
                </div>
            </li>
          <?php endif;?>
            <li class="nav-item"><a class="nav-link" href="<?php echo site_url('login/logout');?>">Sign Out: <?php echo $this->session->userdata('username');?></a></li>          
        </ul>
      </div>
    </nav>