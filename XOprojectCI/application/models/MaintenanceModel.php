<?php
//header('Access-Control-Allow-Origin: *');
class MaintenanceModel extends CI_Model{

    function __construct() {

        $this->builderTbl = 'tbl_buildercustomer';
        $this->statusTbl = 'tbl_status';
        $this->installTypesTbl = 'tbl_install_types';
        $this->builderHasInstallTypesTbl = 'tbl_buildercustomer_has_install_types';
        $this->subdivisionTbl = 'tbl_subdivision';
        $this->branchTbl = 'tbl_xo_branch';
        $this->zoneTbl = 'tbl_zone';
    }

//Section Notes
    //log_message('debug', 'Is Unchecked set? ' . print_r(isset($installTypesUNCHECKED),TRUE));

/////// Branch, Status, Zones

/////// BRANCH related - START /////////////////////////////////////////////////////////////////////////////////    
        function getBranchList(){
            $xowindowsdb = $this->load->database('xowindows', TRUE);
            $xowindowsdb->select('*');
            $xowindowsdb->from('vw_branch_list');
            $query = $xowindowsdb->get();
            //$sql = 'CALL sp_get_branch_list()';
            //$query = $xowindowsdb->query($sql);
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            $xowindowsdb->close();
            return $result;
        }
/////// BRANCH related - END /////////////////////////////////////////////////////////////////////////////////// 

/////// STATUS related - START /////////////////////////////////////////////////////////////////////////////////
        function getStatusList(){
            $applicationdb = $this->load->database('application', TRUE);
            $applicationdb->select('*');
            $applicationdb->from('vw_status_list');
            $query = $applicationdb->get();
            //$sql = 'CALL sp_get_status_list()';
            //$query = $applicationdb->query($sql);
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            $applicationdb->close();
            return $result;
        }

/////// STATUS related - END ///////////////////////////////////////////////////////////////////////////////////

/////// ZONES related - START ////////////////////////////////////////////////////////////////////////////////// 
        function getZonesList(){
            $xowindowsdb = $this->load->database('xowindows', TRUE);
            $xowindowsdb->select('*');
            $xowindowsdb->from('vw_zone_list');
            $query = $xowindowsdb->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            $xowindowsdb->close();
            return $result;
        }
/////// ZONES related - END ////////////////////////////////////////////////////////////////////////////////////

/////// BUILDERS related - START ///////////////////////////////////////////////////////////////////////////////
    /*
        function getBuildersList($params = array()){
            $this->db->select('b.builder_id, b.builder_name, b.update_date, b.updated_by, b.create_date, b.tbl_status_tbl_status_id, status.tbl_status_name');
            $this->db->from($this->builderTbl.' as b');
            $this->db->join($this->statusTbl.' as status', 'b.tbl_status_tbl_status_id = status.tbl_status_id');

            //fetch data by conditions
            if(array_key_exists("conditions",$params)){
                foreach ($params['conditions'] as $key => $value) {
                    if(strpos($key,'.') !== false){
                        $this->db->where($key,$value);
                    }else{
                        $this->db->where('b.'.$key,$value);
                    }
                }
            }

            $this->db->order_by('b.builder_name ASC');
            //$this->db->where('b.tbl_status_tbl_status_id','700');
            $query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            //return fetched data
            return $result;
        }
    */
        
        // Get all builder names with their associated information (removed public static)
        function GetBuildersListNEW(){
            // needed because $this didn't work below
            //$ci =& get_instance();
            $buildersdb = $this->load->database('builders', TRUE);
            $buildersdb->select('*');
            $buildersdb->from('vw_builder_list');
            $query = $buildersdb->get();
            //$query = $ci->db->query($sql);
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            $buildersdb->close();
            return $result;
        }

        function GetOneBuilder($tbl_buildercustomer_builder_id){
            $buildersdb = $this->load->database('builders', TRUE);
            $buildersdb->select('*');
            $buildersdb->from('vw_builder_list');
            $buildersdb->where('builder_id', $tbl_buildercustomer_builder_id);
            $query = $buildersdb->get();
            //$sql = 'CALL sp_get_one_builder(' . $tbl_buildercustomer_builder_id . ')';
            //$query = $buildersdb->query($sql);
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            $buildersdb->close();
            return $result;
        }

        function GetBuildersLog($tbl_buildercustomer_builder_id){ //for buildercustomer
            $buildersdb = $this->load->database('builders', TRUE);
            $buildersdb->select('*');
            $buildersdb->from('vw_builders_log_changes');
            $conditions = "tbl_row_no = $tbl_buildercustomer_builder_id AND (tbl_in_builders = 'tbl_buildercustomer' OR tbl_in_builders = 'tbl_buildercustomer_has_install_types')";
            //$buildersdb->where('tbl_row_no', $tbl_buildercustomer_builder_id);
            //$buildersdb->where_in('tbl_in_builders', 'tbl_buildercustomer');
            //$buildersdb->or_where_in('tbl_in_builders', 'tbl_buildercustomer_has_install_types');
            $buildersdb->where($conditions);
            $query = $buildersdb->get();
            //$sql = 'CALL sp_get_builders_log(' . $tbl_buildercustomer_builder_id . ')';
            //$query = $buildersdb->query($sql);
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            $buildersdb->close();
            return $result;
        }


        function buildersAdd(){

        }

        function buildersEdit(){
            
        }

        function check_if_duplicate_builder(){
            $potentialBuilderName = $this->input->post('builderNameInput');
            $loadedBuilderName = $this->input->post('builderNameOnLoad');
            if (strtoupper($potentialBuilderName) != strtoupper($loadedBuilderName)){
                $buildersdb = $this->load->database('builders', TRUE);
                $buildersdb->select('builder_name');
                $buildersdb->from('tbl_buildercustomer');
                $buildersdb->where('builder_name', strtoupper($potentialBuilderName));
                $query = $buildersdb->get();
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }else{
                $result = FALSE;
            }
            return $result;
        }
/*
        function insert_new_builder($xo_id){
            $enteredBuilderName = $this->input->post('builderNameInput');
            $intialBuilderStatus = $this->input->post('initialStatus');

            $insertBuilder = array(
                'builder_name' => strtoupper($enteredBuilderName),
                'updated_by' => $xo_id,
                'tbl_status_tbl_status_id' => $intialBuilderStatus
            );
            $buildersdb = $this->load->database('builders', TRUE);
            $buildersdb->set($insertBuilder);
            $buildersdb->set('create_date', 'NOW()', FALSE);
            $buildersdb->set('update_date', 'NOW()', FALSE);
            //$addBuilderSQL = $buildersdb->get_compiled_insert('tbl_buildercustomer', $insertBuilder);
            //echo $addBuilderSQL;
            $buildersdb->insert('tbl_buildercustomer', $insertBuilder);
        }
*/
        function insert_new_builder($xo_id){
            $enteredBuilderName = $this->input->post('builderNameInput');
            $intialBuilderStatus = $this->input->post('initialStatus');

            $buildersdb = $this->load->database('builders', TRUE);
            $sql = 'CALL sp_builder_insert("' . strtoupper($enteredBuilderName) . '",' . $xo_id . ',' . $intialBuilderStatus . ')';
            $buildersdb->query($sql);
            $buildersdb->close();
        }

        /*
        function get_install_types($tbl_buildercustomer_builder_id){
            $this->db->select('
                instTypes.install_types_id,
                instTypes.type,
                bHiT.tbl_install_types_install_types_id,
                bHiT.tbl_buildercustomer_builder_id
            ');
            $this->db->from($this->installTypesTbl.' as instTypes');
            $this->db->join($this->builderHasInstallTypesTbl.' as bHiT', 'instTypes.install_types_id = bHiT.tbl_install_types_install_types_id AND bHiT.tbl_buildercustomer_builder_id='.$tbl_buildercustomer_builder_id, 'left outer');
            $query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            return $result;
        }
        */
        function get_install_types($builder_id){
            $buildersdb = $this->load->database('builders', TRUE);
            $sql = 'CALL sp_get_install_types('. $builder_id .')';
            $query = $buildersdb->query($sql);
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            $buildersdb->close();
            return $result;
        }
/*
        //function builder_has_types(){
        function builder_edit_save(){
            $selectedInstTypes = $this->input->post('completed'); //builders install types
            $selectedBuilderID = $this->input->post('builderid'); //builders record id in db
            $selectedBuilderStatus = $this->input->post('builderStatusChange'); //status changed by user to
            $loadedBuilderStatus = $this->input->post('builderStatusOnLoad'); //status when edit first clicked
            $loadedBuilderName = $this->input->post('builderNameOnLoad');//builderName when edit first clicked
            $editedBuilderName = $this->input->post('builderNameInput');//builderName user edited to

            if(strtoupper($editedBuilderName) != strtoupper($loadedBuilderName)){
                $this->db->set('builder_name', strtoupper($editedBuilderName));
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('updated_by', $this->session->userdata('xo_id'));
                $this->db->where('builder_id', $selectedBuilderID);
                $this->db->update($this->builderTbl);
                //$updateBuilderNameSQL = $this->db->get_compiled_update($this->builderTbl);
                //echo $updateBuilderNameSQL;
            }

                $this->db->where('tbl_buildercustomer_builder_id', $selectedBuilderID);
                $this->db->delete($this->builderHasInstallTypesTbl); //remote all install types then save new install types
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('updated_by', $this->session->userdata('xo_id'));
                $this->db->where('builder_id', $selectedBuilderID);
                $this->db->update($this->builderTbl);
                //$removeBuildersTypesSQL = $this->db->get_compiled_delete($this->builderHasInstallTypesTbl, $removeBuildersType);
                //echo $removeBuildersTypesSQL;
                //echo '<div class="alert alert-danger">Install Types Updated - Delete!</div>';

            if(isset($selectedInstTypes)){
                $insertBuilderITids = array();
                    foreach ($selectedInstTypes as $selectTypes)
                    {
                       $builderITids = array(
                            'tbl_buildercustomer_builder_id' => $selectedBuilderID,
                            'tbl_install_types_install_types_id' => $selectTypes
                       );
                       array_push($insertBuilderITids, $builderITids);
                    }
                    //INSERT INTO `tbl_buildercustomer_has_install_types` (`tbl_install_types_install_types_id`, `tbl_buildercustomer_builder_id`) VALUES ('206', '146'), ('211', '146'), ('207', '146'), ('218', '146'), ('212', '146');
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('updated_by', $this->session->userdata('xo_id'));
                $this->db->where('builder_id', $selectedBuilderID);
                $this->db->update($this->builderTbl);

                $this->db->insert_batch($this->builderHasInstallTypesTbl, $insertBuilderITids);
           }

            if($selectedBuilderStatus != $loadedBuilderStatus){
                $this->db->set('tbl_status_tbl_status_id', $selectedBuilderStatus);
                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('updated_by', $this->session->userdata('xo_id'));
                $this->db->where('builder_id', $selectedBuilderID);
                $this->db->update($this->builderTbl);
                //$updateStatusSQL = $this->db->get_compiled_update($this->builderTbl);
                //echo $updateStatusSQL;
            }
            //echo '<div class="alert alert-success">Builder Updated!</div>';
        }
*/

        function builder_edit_save($xo_id){
            $selectedInstTypes = $this->input->post('completed'); //builders install types
            $selectedBuilderID = $this->input->post('builderid'); //builders record id in db
            $selectedBuilderStatus = $this->input->post('builderStatusChange'); //status changed by user to
            $loadedBuilderStatus = $this->input->post('builderStatusOnLoad'); //status when edit first clicked
            $loadedBuilderName = $this->input->post('builderNameOnLoad');//builderName when edit first clicked
            $editedBuilderName = $this->input->post('builderNameInput');//builderName user edited to
            $passedITs = $this->input->post('installTypesArray');
            $procPassedITs = json_decode($passedITs);
            //log_message('debug','json DECODED INSTALL TYPES ' . print_r($procPassedITs,TRUE));
            //log_message('debug','SELECTED Builder Status ' . print_r($selectedBuilderStatus,TRUE));
            //log_message('debug','LOADED Builder Status ' . print_r($loadedBuilderStatus,TRUE));
            //log_message('debug','Are they equal? ' . print_r($selectedBuilderStatus != $loadedBuilderStatus,TRUE));

            $buildersdb = $this->load->database('builders', TRUE);

            if(strtoupper($editedBuilderName) != strtoupper($loadedBuilderName)){
                $buildersdb->set('builder_name', strtoupper($editedBuilderName));
                $buildersdb->set('update_date', date('Ymd'), FALSE);
                $buildersdb->set('update_time', date('Hi'), FALSE);                              
                $buildersdb->set('updated_by', $xo_id);
                $buildersdb->where('builder_id', $selectedBuilderID);
                $buildersdb->update('tbl_buildercustomer');
                //$updateBuilderNameSQL = $buildersdb->get_compiled_update('tbl_buildercustomer');
                //echo $updateBuilderNameSQL;

            }

            if(isset($selectedInstTypes)){
            //compare and create a list of install types to REMOVE
                $installTypesUNCHECKED = array_diff($procPassedITs, $selectedInstTypes);
            //compare and create a list of install types to ADD
                $installTypesCHECKED = array_diff($selectedInstTypes, $procPassedITs);
            //if either difference array is populated
                if(!empty($installTypesUNCHECKED) || !empty($installTypesCHECKED)){                
                    //log_message('debug', 'Is Unchecked set? ' . print_r(isset($installTypesUNCHECKED),TRUE));

                //process the REMOVALS
                    if(!empty($installTypesUNCHECKED)){
                        //get user's name since db trigger wouldn't know who the CURRENT user was that removed the entry (maybe I'll learn that later)
                        $xowindowsdb = $this->load->database('xowindows', TRUE);
                        //$userSQL = 'CALL xowindows.sp_get_one_user(' . $xo_id . ')';
                        $xowindowsdb->select('*');
                        $xowindowsdb->from('vw_users_list');
                        $xowindowsdb->where('id', $xo_id);
                        $query = $xowindowsdb->get();
                        //$query = $xowindowsdb->query($userSQL);
                        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
                        $xo_user_name = $result[0]['login'];
                        $xowindowsdb->close();

                        foreach ($installTypesUNCHECKED as $removedBuildersITids){
                        //get install type NAME for log
                            $buildersdb->reconnect();
                            $buildersdb->select('*');
                            $buildersdb->from('vw_install_types_list');
                            $buildersdb->where('install_types_id', $removedBuildersITids);
                            //$installTypeSQL = 'CALL builders.sp_get_one_install_type(' . $removedBuildersITids . ')'; //assign SP to get install type name for logging
                            //$queryIT = $buildersdb->query($installTypeSQL); //execute SP to get the name
                            $queryIT = $buildersdb->get();
                            $resultIT = ($queryIT->num_rows() > 0)?$queryIT->result_array():FALSE;
                            $itSql = "CALL builders.sp_log_insert('tbl_buildercustomer_has_install_types','removed','tbl_install_types_install_types_id'," . $selectedBuilderID . ",'" . $xo_user_name . "','REMOVED','" . $resultIT[0]['type'] . "')";
                            $buildersdb->reconnect();
                            $buildersdb->simple_query($itSql); //write to the log
                        //delete the install type for the builder from the table
                            $buildersdb->set('tbl_builder_builder_id', $selectedBuilderID);
                            $buildersdb->where('tbl_install_types_install_types_id', $removedBuildersITids);
                            $buildersdb->delete('tbl_buildercustomer_has_install_types');
                        }
                        $buildersdb->reconnect();
                        //$buildersdb->set('update_date', 'NOW()', FALSE);
                        $buildersdb->set('update_date', date('Ymd'), FALSE);
                        $buildersdb->set('update_time', date('Hi'), FALSE); 
                        $buildersdb->set('updated_by', $xo_id);
                        $buildersdb->where('builder_id', $selectedBuilderID);
                        $buildersdb->update('tbl_buildercustomer');
                        echo '<div class="alert alert-danger">Install Types Removed</div>';
                    }
                    

                //process the ADDITIONS
                    if(!empty($installTypesCHECKED)){
                        $insertBuilderITids = array();
                        $buildersdb->reconnect();
                    //for each one added, put into an array
                        foreach ($installTypesCHECKED as $addedBuildersITids){
                            $builderITids = array(
                                'tbl_install_types_install_types_id' => $addedBuildersITids,
                                'tbl_buildercustomer_builder_id' => $selectedBuilderID,
                                'updated_by' => $xo_id
                            );
                            array_push($insertBuilderITids, $builderITids);
                        }
                    //insert the array all at once
                        $buildersdb->insert_batch('tbl_buildercustomer_has_install_types', $insertBuilderITids);

                    //update the timestamp, updated by for the builder record in tbl_buildercustomer
                        $buildersdb->reconnect();
                        //$buildersdb->set('update_date', 'NOW()', FALSE);
                        $buildersdb->set('update_date', date('Ymd'), FALSE);
                        $buildersdb->set('update_time', date('Hi'), FALSE); 
                        $buildersdb->set('updated_by', $xo_id);
                        $buildersdb->where('builder_id', $selectedBuilderID);
                        $buildersdb->update('tbl_buildercustomer');
                        echo '<div class="alert alert-success">Install Types Added</div>';
                    }
                }
            }
            
            if($selectedBuilderStatus != $loadedBuilderStatus){
                $buildersdb->reconnect();
                $buildersdb->set('tbl_status_tbl_status_id', $selectedBuilderStatus);
                //$buildersdb->set('update_date', 'NOW()', FALSE);
                $buildersdb->set('update_date', date('Ymd'), FALSE);
                $buildersdb->set('update_time', date('Hi'), FALSE); 
                $buildersdb->set('updated_by', $xo_id);
                $buildersdb->where('builder_id', $selectedBuilderID);
                $buildersdb->update('tbl_buildercustomer');
                //$updateStatusSQL = $buildersdb->get_compiled_update('tbl_buildercustomer');
                //echo $updateStatusSQL;
            }
            $buildersdb->close();
            //echo '<div class="alert alert-success">Builder Updated!</div>';
        }//END builder_edit_save($xo_id)

/////// BUILDERS related - END /////////////////////////////////////////////////////////////////////////////////

/////// SUBDIVISIONS related - START ///////////////////////////////////////////////////////////////////////////
        function GetSubdivisionsListNEW(){
            $buildersdb = $this->load->database('builders', TRUE);
            $buildersdb->select('*');
            $buildersdb->from('vw_subdivision_list');
            $query = $buildersdb->get();
        	$result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        	return $result;
        }

        function getSubdivisionsList($params = array()){
            $this->db->select('
                s.subdivision_id,
                s.subdivision_name,
                s.update_date,
                s.updated_by,
                s.create_date,
                s.city,
                s.state,
                s.zip,
                branch.location,
                builder.builder_name, 
                zone.zone_name, 
                s.tbl_status_tbl_status_id, 
                status.tbl_status_name'
            );
            $this->db->from($this->subdivisionTbl.' as s');
            $this->db->join($this->builderTbl.' as builder', 's.tbl_buildercustomer_builder_id = builder.builder_id');
            $this->db->join($this->branchTbl.' as branch', 's.tbl_xo_branch_xo_branch_id = branch.xo_branch_id');
            $this->db->join($this->zoneTbl.' as zone', 's.tbl_zone_zone_id = zone.zone_id');
            $this->db->join($this->statusTbl.' as status', 's.tbl_status_tbl_status_id = status.tbl_status_id');

            //fetch data by conditions
            if(array_key_exists("conditions",$params)){
                foreach ($params['conditions'] as $key => $value) {
                    if(strpos($key,'.') !== false){
                        $this->db->where($key,$value);
                    }else{
                        $this->db->where('s.'.$key,$value);
                    }
                }
            }
            
            $query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

            //return fetched data
            return $result;
        }

        function subdivisionsAdd(){

        }

        function subdivisionsEdit(){

        }

        function get_one_subdivision($subdivision_id){
            $buildersdb = $this->load->database('builders', TRUE);
            $buildersdb->select('*');
            $buildersdb->from('vw_subdivision_list');
            $buildersdb->where('subdivision_id', $subdivision_id);
            $query = $buildersdb->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
                //log_message('debug', 'Subdivision Results ' . print_r($result,TRUE));
            $buildersdb->close();
            return $result;
        }

        function GetBuildersLogSubdivision($tbl_subdivision_subdivision_id){
            $buildersdb = $this->load->database('builders', TRUE);
            $buildersdb->select('*');
            $buildersdb->from('vw_builders_log_changes');
            $conditions = "tbl_row_no = $tbl_subdivision_subdivision_id AND (tbl_in_builders = 'tbl_subdivision' OR tbl_in_builders = 'tbl_supers')";
            $buildersdb->where($conditions);
            $query = $buildersdb->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            $buildersdb->close();
            return $result;          
        }

/*
        function check_if_duplicate_subdivision(){
            $potentialSubdivisionBuilder = $this->input->post('builderIdSelected');
            $potentialSubdivisionName = $this->input->post('subdivisionNameInput');
            $this->db->select('tbl_buildercustomer_builder_id, subdivision_name');
            $this->db->from($this->subdivisionTbl);
            $this->db->where('tbl_buildercustomer_builder_id', $potentialSubdivisionBuilder);
            $this->db->where('subdivision_name', strtoupper($potentialSubdivisionName));
            $query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            return $result;
        }
*/
        function update_or_insert_new_subdivision(){ //WORK IN PROGRESS 2190725
            if($this->input->post('subdivisionRecordID')){
                $this->maintenancemodel->update_subdivision();
            }else{

            }; //exists, then its an update
            //else it is a new record
            //read all the posted data
            //check the database
            //if record exists, update
            //if new record, insert
        }


        function check_if_duplicate_subdivision(){
            $potentialSubdivisionBuilder = $this->input->post('builderIdSelected');
            $potentialSubdivisionName = $this->input->post('subdivisionNameInput');
            $buildersdb = $this->load->database('builders', TRUE);
            $buildersdb->select('tbl_buildercustomer_builder_id, subdivision_name');
            $buildersdb->from('tbl_subdivision');
            $buildersdb->where('tbl_buildercustomer_builder_id', $potentialSubdivisionBuilder);
            $buildersdb->where('subdivision_name', strtoupper($potentialSubdivisionName));
            $query = $buildersdb->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            return $result;
        }

        function insert_new_subdivision($xo_id){
            //log_message('debug','insertPost: ' . print_r($this->input->post(),TRUE));
            $enteredSuperSuperName = $this->input->post('superNameInput');
            $enteredSuperPhoneNumber = $this->input->post('superPhoneInput');
            // add super first to get id that was created for it
            $insertSuper = array(
                'fk_subdivision_id' => NULL,
                'super_name' => strtoupper($enteredSuperSuperName),
                'phone_number' => $enteredSuperPhoneNumber,
                'updated_by' => $xo_id
            );
            $buildersdb = $this->load->database('builders', TRUE);
            $buildersdb->set($insertSuper);
            $buildersdb->set('update_date', date('Ymd'), FALSE);
            $buildersdb->set('update_time', date('Hi'), FALSE);
            $buildersdb->insert('tbl_supers', $insertSuper);
            $newSupersId = $buildersdb->insert_id(); // 'super_id' need record ID created


            $selectedBuilderID = $this->input->post('builderIdSelected');
            $enteredSubdivisionName = $this->input->post('subdivisionNameInput');
            $selectedStatusID = $this->input->post('statusIdSelected');
            $selectedInstallTypesID = $this->input->post('installTypeIdSelected');
            $selectedWindowMfgID = $this->input->post('windowMfgIdSelected');
            $enteredSubdivisionCity = $this->input->post('subdivisionCityInput');
            $enteredSubdivisionState = $this->input->post('subdivisionStateInput');
            $enteredSubdivisionZip = $this->input->post('subdivisionZipInput');
            $selectedZoneID = $this->input->post('zoneIdSelected');
            $selectedBranchID = $this->input->post('branchIdSelected');
            $selectedStartsID = $this->input->post('startsRepIdSelected');
            $selectedFieldID = $this->input->post('fieldSuperIdSelected');
            $enteredSuperName = $this->input->post('superNameInput');
            $enteredSuperPhone = $this->input->post('superPhoneInput');

            $insertSubdivision = array(
                'tbl_buildercustomer_builder_id' => $selectedBuilderID,
                'subdivision_name' => strtoupper($enteredSubdivisionName),
                'tbl_status_tbl_status_id' => $selectedStatusID,
                'tbl_install_types' => $selectedInstallTypesID,
                'vendor_tbl_manufacturer_id' => $selectedWindowMfgID,
                'city' => strtoupper($enteredSubdivisionCity),
                'state' => strtoupper($enteredSubdivisionState),
                'zip' => $enteredSubdivisionZip,
                'tbl_zone_zone_id' => $selectedZoneID,
                'tbl_xo_branch_xo_branch_id' => $selectedBranchID,
                'tbl_users_tbl_user_id_starts' => $selectedStartsID,
                'tbl_users_tbl_user_id_xosupers' => $selectedFieldID,
                'tbl_supers_super_id' => $newSupersId, //from above
                'updated_by' => $xo_id
            );

            $buildersdb->reconnect();
            $buildersdb->set($insertSubdivision);
            $buildersdb->set('update_date', date('Ymd'), FALSE);
            $buildersdb->set('update_time', date('Hi'), FALSE); 
            //$addSubdivisionSQL = $buildersdb->get_compiled_insert('tbl_subdivision', $insertSubdivision);
            //echo $addSubdivisionSQL;
            $buildersdb->insert('tbl_subdivision', $insertSubdivision);
            $newBuildersId = $buildersdb->insert_id();

            $buildersdb->reconnect();
            $buildersdb->set('fk_subdivision_id', $newBuildersId);
            $buildersdb->where('super_id', $newSupersId);
            $buildersdb->update('tbl_supers');
            // get last id here then reload the newly created super and update fk_subdivision_id to this new one...?
            $buildersdb->close();
        }

        function update_subdivision($xo_id){
            //log_message('debug','Inside updatePost: ' . print_r($this->input->post(),TRUE));
            //for each post field that doesn't match the loaded value, add to update array
            $updateSubdivision = array();
            $updateSuper = array();
            $loadedName = strtoupper($this->input->post('nameOnLoad'));
            $fieldName = strtoupper($this->input->post('subdivisionNameInput'));
                if($loadedName != $fieldName){
                    $updateSubdivision['subdivision_name'] = $fieldName;
                }
            $loadedStatus = $this->input->post('statusOnLoad');
            $fieldStatus = $this->input->post('statusIdSelected');
                if($loadedStatus != $fieldStatus){
                    $updateSubdivision['tbl_status_tbl_status_id'] = $fieldStatus;
                }
            $loadedIT = $this->input->post('installTypeOnLoad');
            $fieldInstType = $this->input->post('installTypeIdSelected');
                if($loadedIT != $fieldInstType){
                    $updateSubdivision['tbl_install_types'] = $fieldInstType;
                }
            $loadedMfg = $this->input->post('windowMfgOnLoad');
            $fieldMfg = $this->input->post('windowMfgIdSelected');
                if($loadedMfg != $fieldMfg){
                    $updateSubdivision['vendor_tbl_manufacturer_id'] = $fieldMfg;
                }
            $loadedCity = strtoupper($this->input->post('cityOnLoad'));
            $fieldCity = strtoupper($this->input->post('subdivisionCityInput'));
                if($loadedCity != $fieldCity){
                    $updateSubdivision['city'] = $fieldCity;
                }
            $loadedState = strtoupper($this->input->post('stateOnLoad'));
            $fieldState = strtoupper($this->input->post('subdivisionStateInput'));
                if($loadedState != $fieldState){
                    $updateSubdivision['state'] = $fieldState;
                }
            $loadedZip = $this->input->post('zipOnLoad');
            $fieldZip = $this->input->post('subdivisionZipInput');
                if($loadedZip != $fieldZip){
                    $updateSubdivision['zip'] = $fieldZip;
                }
            $loadedBranch = $this->input->post('branchOnLoad');
            $fieldBranch = $this->input->post('branchIdSelected');
                if($loadedBranch != $fieldBranch){
                    $updateSubdivision['tbl_xo_branch_xo_branch_id'] = $fieldBranch;
                }
            $loadedZone = $this->input->post('zoneOnLoad');
            $fieldZone = $this->input->post('zoneIdSelected');
                if($loadedZone != $fieldZone){
                    $updateSubdivision['tbl_zone_zone_id'] = $fieldZone;
                }
            $loadedStartsRep = $this->input->post('startsRepOnLoad');
            $fieldStartsRep = $this->input->post('startsRepIdSelected');
                if($loadedStartsRep != $fieldStartsRep){
                    $updateSubdivision['tbl_users_tbl_user_id_starts'] = $fieldStartsRep;
                }
            $loadedFieldRep = $this->input->post('fieldRepOnLoad');
            $fieldFieldRep = $this->input->post('fieldSuperIdSelected');
                if($loadedFieldRep != $fieldFieldRep){
                    $updateSubdivision['tbl_users_tbl_user_id_xosupers'] = $fieldFieldRep;
                }
            $loadedSuperName = $this->input->post('superSuperNameOnLoad');
            $fieldSuperName = $this->input->post('superNameInput');
                if($loadedSuperName != $fieldSuperName){
                    $updateSuper['super_name'] = $fieldSuperName;
                }
            $loadedSuperPhone = $this->input->post('superSuperPhoneOnLoad');
            $fieldSuperPhone = $this->input->post('superPhoneInput');
                if($loadedSuperPhone != $fieldSuperPhone){
                    $updateSuper['phone_number'] = $fieldSuperPhone;
                }
            //log_message('debug','Inside updatePost after nameOnLoad: ' . print_r($updateSuper,TRUE));

        // Every subdivision addition gets an entry in tbl_supers, even if blank
        // Super's Name to a different array
        // Super's Number to a different array

            $updateSubdivision['updated_by'] = $xo_id;
            $updateSuper['updated_by'] = $xo_id;


            //log_message('debug','subdivisionUpdate: ' . print_r($updateSubdivision, TRUE));
            //log_message('debug','superUpdate: ' . print_r($updateSuper, TRUE));

            if(!empty($updateSubdivision)){ //if the array of changes isn't empty load db and update the record
                $buildersdb = $this->load->database('builders', TRUE);
                $buildersdb->set($updateSubdivision);
                $buildersdb->where('subdivision_id', $this->input->post('subdivisionRecordID'));
                $buildersdb->update('tbl_subdivision');
            //$updateSubdivisionSQL = $buildersdb->get_compiled_update('tbl_subdivision', $updateSubdivision);
            //log_message('debug','updateSubdivisionSQL: ' . print_r($updateSubdivisionSQL,TRUE));
            //echo $updateSubdivisionSQL;
            }
            if(!empty($updateSuper)){
                $buildersdb->reconnect();
                $buildersdb->set($updateSuper);
                //$buildersdb->set('fk_subdivision_id', $this->input->post('subdivisionRecordID'));
                $buildersdb->where('super_id', $this->input->post('superSuperIdOnLoad'));
                $buildersdb->update('tbl_supers');
                $buildersdb->close();
                // what if it is new entry instead of update?
                // so how do i know insert vs update?
                // BECAUSE Every subdivision addition gets an entry in tbl_supers, even if blank
            }
            //log_message('debug','updateSubdivision: ' . print_r($updateSubdivision,TRUE));
        }
/////// SUBDIVISIONS related - END /////////////////////////////////////////////////////////////////////////////



/////// INSTALL TYPES related - START ////////////////////////////////////////////////////////////////////////////////// 
        function getInstallTypesList($params = array()){
            $buildersdb = $this->load->database('builders', TRUE);
            $buildersdb->select('it.install_types_id, it.type, it.update_date, it.update_time, it.updated_by');
            $buildersdb->from($this->installTypesTbl.' as it');
            
            //fetch data by conditions
            if(array_key_exists("conditions",$params)){
                foreach ($params['conditions'] as $key => $value) {
                    if(strpos($key,'.') !== false){
                        $buildersdb->where($key,$value);
                    }else{
                        $buildersdb->where('it.'.$key,$value);
                    }
                }
            }
            $buildersdb->order_by('it.type ASC');
            $query = $buildersdb->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

            //return fetched data
            return $result;
        }

        function installtypesAdd(){

        }

        function installtypesEdit(){
            
        }

        function check_if_duplicate_install_type(){
            $potentialInstallTypeName = $this->input->post('installtypeNameInput');
            $loadedInstallTypeName = $this->input->post('installtypeNameOnLoad');
                if (strtoupper($potentialInstallTypeName) != strtoupper($loadedInstallTypeName)){
                    $this->db->select('type');
                    $this->db->from($this->installTypesTbl);
                    $this->db->where('type', strtoupper($potentialInstallTypeName));
                    $query = $this->db->get();
                    $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
                }else{
                    $result = FALSE;
                }
            return $result;
        }

        function insert_new_install_type(){
            $enteredInstallTypeName = $this->input->post('installtypeNameInput');

            $insertInstallType = array(
                'type' => strtoupper($enteredInstallTypeName),
                'updated_by' => $this->session->userdata('xo_id')
            );

            $this->db->set($insertInstallType);
            //$this->db->set('create_date', 'NOW()', FALSE);
            //$this->db->set('update_date', 'NOW()', FALSE);
            $buildersdb->set('update_date', date('Ymd'), FALSE);
            $buildersdb->set('update_time', date('Hi'), FALSE); 
            //$addInstallTypeSQL = $this->db->get_compiled_insert('tbl_install_types', $insertInstallType);
            //echo $addInstallTypeSQL;
            $this->db->insert('tbl_install_types', $insertInstallType);
        }

        function install_type_edit_save(){
            if(strtoupper($this->input->post('installtypeNameInput')) != strtoupper($this->input->post('installtypeNameOnLoad'))){
                $updateInstallType = array(
                    'type' => strtoupper($this->input->post('installtypeNameInput')),
                    'updated_by' => $this->session->userdata('xo_id')
                    );
                //$this->db->set('update_date', 'NOW()', FALSE);
                $this->db->set('update_date', date('Ymd'), FALSE);
                $this->db->set('update_time', date('Hi'), FALSE); 
                $this->db->set($updateInstallType);
                $this->db->where('install_types_id', $this->input->post('installtypeid'));
                $this->db->update($this->installTypesTbl);
                //$updateInstallTypeNameSQL = $this->db->get_compiled_update($this->installTypesTbl);
                //echo $updateInstallTypeNameSQL;
            }
        }

        // get valid list of install types based on the builder
        function builders_install_types($buildersId){
            $buildersdb = $this->load->database('builders', TRUE);
            $buildersdb->select('*');
            $buildersdb->from('vw_builders_install_types');
            $buildersdb->where('builder_id', $buildersId);
            $query = $buildersdb->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            $buildersdb->close();
            return $result;
        }


/////// INSTALL TYPES related - END ////////////////////////////////////////////////////////////////////////////////////

/////// JOBSITE SUPERS related - START ////////////////////////////////////////////////////////////////////////////////////

        function getSupersList(){
            $jobsdb = $this->load->database('jobs', TRUE);
            $jobsdb->select('*');
            $jobsdb->from('vw_supers_list');
            $query = $jobsdb->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            $jobsdb->close();
            return $result;
        }

        function get_one_jobsite($jobsite_id){
            $jobsdb = $this->load->database('jobs', TRUE);
            $jobsdb->select('*');
            $jobsdb->from('vw_supers_list');
            $jobsdb->where('idtbl_supers', $jobsite_id);
            $query = $jobsdb->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            $jobsdb->close();
            return $result;
        }

/////// JOBSITE SUPERS related - END ////////////////////////////////////////////////////////////////////////////////////

/////// VENDORS related - START ////////////////////////////////////////////////////////////////////////////////////

        function getManufacturersList(){
            $vendorsdb = $this->load->database('vendors', TRUE);
            $vendorsdb->select('*');
            $vendorsdb->from('vw_manufacturer_list');
            $query = $vendorsdb->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            $vendorsdb->close();
            return $result;
        }

/////// VENDORS related - END ////////////////////////////////////////////////////////////////////////////////////

/////// XO USERS related - START ////////////////////////////////////////////////////////////////////////////////
        function getUsersList(){
            $xowindowsdb = $this->load->database('xowindows', TRUE);
            $sql = 'CALL sp_get_users_list()';
            $query = $xowindowsdb->query($sql);
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            $xowindowsdb->close();
            return $result;
        }
/////// XO USERS related - END ////////////////////////////////////////////////////////////////////////////////////

}

?>