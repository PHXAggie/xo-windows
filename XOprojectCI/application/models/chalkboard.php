<?php

        function builder_edit_save($xo_id){
            $selectedInstTypes = $this->input->post('completed'); //builders install types
            $selectedBuilderID = $this->input->post('builderid'); //builders record id in db
            $selectedBuilderStatus = $this->input->post('builderStatusChange'); //status changed by user to
            $loadedBuilderStatus = $this->input->post('builderStatusOnLoad'); //status when edit first clicked
            $loadedBuilderName = $this->input->post('builderNameOnLoad');//builderName when edit first clicked
            $editedBuilderName = $this->input->post('builderNameInput');//builderName user edited to

            $buildersdb = $this->load->database('builders', TRUE);
            if(strtoupper($editedBuilderName) != strtoupper($loadedBuilderName)){
                $buildersdb->set('builder_name', strtoupper($editedBuilderName));
                $buildersdb->set('update_date', 'NOW()', FALSE);
                $buildersdb->set('updated_by', $xo_id);
                $buildersdb->where('builder_id', $selectedBuilderID);
                $buildersdb->update('tbl_buildercustomer');
                //$updateBuilderNameSQL = $buildersdb->get_compiled_update('tbl_buildercustomer');
                //echo $updateBuilderNameSQL;
            }

                $buildersdb->where('tbl_buildercustomer_builder_id', $selectedBuilderID);
                $buildersdb->delete('tbl_buildercustomer_has_install_types'); //remote all install types then save new install types
                $buildersdb->set('update_date', 'NOW()', FALSE);
                $buildersdb->set('updated_by', $xo_id);
                $buildersdb->where('builder_id', $selectedBuilderID);
                $buildersdb->update('tbl_buildercustomer');
                //$removeBuildersTypesSQL = $buildersdb->get_compiled_delete('tbl_buildercustomer_has_install_types', $removeBuildersType);
                //echo $removeBuildersTypesSQL;
                //echo '<div class="alert alert-danger">Install Types Updated - Delete!</div>';

            if(isset($selectedInstTypes)){
                $insertBuilderITids = array();
                    foreach ($selectedInstTypes as $selectTypes)
                    {
                       $builderITids = array(
                            'tbl_buildercustomer_builder_id' => $selectedBuilderID,
                            'tbl_install_types_install_types_id' => $selectTypes
                       );
                       array_push($insertBuilderITids, $builderITids);
                    }
                    //INSERT INTO `tbl_buildercustomer_has_install_types` (`tbl_install_types_install_types_id`, `tbl_buildercustomer_builder_id`) VALUES ('206', '146'), ('211', '146'), ('207', '146'), ('218', '146'), ('212', '146');
                $buildersdb->set('update_date', 'NOW()', FALSE);
                $buildersdb->set('updated_by', $xo_id);
                $buildersdb->where('builder_id', $selectedBuilderID);
                $buildersdb->update('tbl_buildercustomer');

                $buildersdb->insert_batch('tbl_buildercustomer_has_install_types', $insertBuilderITids);
           }

            if($selectedBuilderStatus != $loadedBuilderStatus){
                $buildersdb->set('tbl_status_tbl_status_id', $selectedBuilderStatus);
                $buildersdb->set('update_date', 'NOW()', FALSE);
                $buildersdb->set('updated_by', $xo_id);
                $buildersdb->where('builder_id', $selectedBuilderID);
                $buildersdb->update('tbl_buildercustomer');
                //$updateStatusSQL = $buildersdb->get_compiled_update('tbl_buildercustomer');
                //echo $updateStatusSQL;
            }
            //echo '<div class="alert alert-success">Builder Updated!</div>';
        }