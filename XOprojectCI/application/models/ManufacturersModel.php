<?php

class ManufacturersModel extends CI_Model{

	function getMfgList(){
		$vendorsdb = $this->load->database('vendors', TRUE);
		$vendorsdb->select('*');
		$vendorsdb->from('vw_manufacturer_list');
		$query = $vendorsdb->get();
		$result = ($query->num_rows() > 0)?$query->result_array():FALSE;
		$vendorsdb->close();
		return $result;
	}
}