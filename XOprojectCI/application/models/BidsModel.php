<?php
//header('Access-Control-Allow-Origin: *');
class BidsModel extends CI_Model{

    function __construct() {
        $this->bidsTbl = 'bids.tbl_bid';
        $this->subsTbl = 'builders.tbl_subdivision';
        $this->builderTbl = 'builders.tbl_buildercustomer';
        $this->branchTbl = 'xowindows.tbl_xo_branch';
        $this->installTypesTbl = 'builders.tbl_install_types';
        $this->builderHasInstallTypesTbl = 'builders.tbl_buildercustomer_has_install_types';
        $this->mfgTbl = 'vendors.tbl_manufacturer';
        $this->windowColorTbl = 'vendors.tbl_window_color';
        $this->mfgHasWindowColor = 'vendors.tbl_mfg_has_tbl_window_color';
        $this->windowCoatingTbl = 'vendors.tbl_window_coating';
        $this->mfgHasWindowCoating = 'vendors.tbl_mfg_has_tbl_window_coating';
        $this->windowMaterialTbl = 'vendors.tbl_window_material';
        $this->mfgHasWindowMaterial = 'vendors.tbl_mfg_has_tbl_window_material';
        $this->windowFinTbl = 'vendors.tbl_window_fin';
        $this->mfgHasWindowFin = 'vendors.tbl_mfg_has_tbl_window_fin';
        $this->optionsTbl = 'bids.tbl_options';
        $this->bidWindowsTbl = 'bids.tbl_bid_windows';
        $this->optionHasWindowsTbl = 'bids.tbl_options_has_windows';
        $this->optionHasWindowsNewTbl = 'bids.tbl_options_has_windows_new';
    }
                    /*
                    function getRecentBidsListNEW(){
                        $bidsdb = $this->load->database('bids', TRUE);
                        $sql = 'CALL sp_get_recent_bids()';
                        $query = $bidsdb->query($sql);
                        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
                        $bidsdb->close();
                        return $result;
                    }
                    */

                    function getRecentBidsListNEW(){
                        $bidsdb = $this->load->database('bids', TRUE);
                        $bidsdb->select('*');
                        $bidsdb->from('vw_get_recent_bids');
                        //$statuses = array(702,703,704);
                        //$bidsdb->where_in('tbl_status_tbl_status_id', $statuses);
                        $bidsdb->limit(10);
                        $bidsdb->order_by('Date', 'DESC');
                        $query = $bidsdb->get();
                        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
                        $bidsdb->close();
                        return $result;
                    }

                    function getBuilderSubdivisionRows($params = array()){
                        //$this->db->select('s.subdivision_id, s.subdivision_name');
                        $buildersdb = $this->load->database('builders', TRUE);
                        $buildersdb->select('s.subdivision_id, s.subdivision_name');
                        $buildersdb->from('tbl_subdivision as s');
                        //$this->db->from($this->subsTbl.' as s');
                            if(array_key_exists("conditions",$params)){
                                foreach ($params['conditions'] as $key => $value) {
                                    if(strpos($key,'.') !== false){
                                        //$this->db->where($key,$value);
                                        $buildersdb->where($key,$value);
                                    }else{
                                        //$this->db->where('s.'.$key,$value);
                                        $buildersdb->where('s.' . $key, $value);
                                    }
                                }
                            } // complete query looks like: SELECT s.subdivision_id, s.subdivision_name FROM tbl_subdivision s WHERE s.tbl_buildercustomer_builder_id=146
                        $query = $buildersdb->get();
                        //$query = $this->db->get();
                        $result = ($query->num_rows() > 0)?$query->result_array():FALSE; //if there is more than zero rows, return the data, otherwise return FALSE

                        return $result; //return fetched data (or FALSE) back to Controller that called it Bids->getBuilderSubdivisions
                    }

    function get_plans($postData){
        $response = array();

        if(isset($postData['searchPlan'])){
            // Select record
            $bidsdb = $this->load->database('bids', TRUE);
            $bidsdb->select('*');
            $bidsdb->where('tbl_subdivision_subdivision_id', $postData['subdivision_id']);
            $bidsdb->where("plan like '%".$postData['searchPlan']."%' ");
            $records = $bidsdb->get('vw_get_bldrsub_plan')->result();

            foreach($records as $row){
                $response[] = array("value"=>$row->plan,"label"=>$row->plan);
            }
        }

        return $response;
    }

    function get_elevations($postData){
        $response = array();

        if(isset($postData['searchElevation'])){
            // Select record
            $bidsdb = $this->load->database('bids', TRUE);
            $bidsdb->select('*');
            $bidsdb->where('tbl_subdivision_subdivision_id', $postData['subdivisionSelected']);
            $bidsdb->where('plan', $postData['planSelected']);
            $bidsdb->where("elevation like '%".$postData['searchElevation']."%' ");
            $records = $bidsdb->get('vw_get_bldrsub_elevation')->result();

            foreach($records as $row){
                $response[] = array("value"=>$row->elevation,"label"=>$row->elevation);
            }
        }

        return $response;
    }

    function check_if_duplicate_bid(){
        $selectedBuilderID = $this->input->post('builderIdSelected');
        $selectedSubdivisionID = $this->input->post('subdivisionIdSelected');
        $selectedPlan = $this->input->post('bidPlan');
        $selectedElevation = $this->input->post('bidElevation');
	    $this->db->select('B.builder_id as builder_ID,
	    	B.builder_name as Builder,
	    	S.subdivision_id as subdivision_ID,
			S.subdivision_name as Subdivision,
            Bids.tbl_bid_id,
			Bids.plan as Plan,
            Bids.elevation as Elevation');
		$this->db->from($this->bidsTbl.' as Bids');
        $this->db->join($this->subsTbl.' as S', 'S.subdivision_id = Bids.tbl_subdivision_subdivision_id');
        $this->db->join($this->builderTbl.' as B', 'B.builder_id = S.tbl_buildercustomer_builder_id');
        //$this->db->where('Date','DESC');
        $this->db->where('B.builder_ID', $selectedBuilderID); // tbl_builderid = dropdown
        $this->db->where('S.subdivision_ID', $selectedSubdivisionID); // AND tbl_subdivisionid = dropdown
        $this->db->where('Bids.plan', $selectedPlan); // AND plan = text field
        $this->db->where('Bids.elevation', $selectedElevation); // AND elevation = text field
        $this->db->limit(1);

        $query = $this->db->get();
    //var_dump($query);
        //return $query->num_rows() == 0 ? $this->db->insert('') : FALSE;
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
    //echo $query->num_rows();
        return $result;
    }

    function insert_new_bid_combo(){ //need to add user who entered
        $selectedSubdivisionID = $this->input->post('subdivisionIdSelected');
        $selectedPlan = $this->input->post('bidPlan');
        $selectedElevation = $this->input->post('bidElevation');

        $insertData = array(
            'tbl_subdivision_subdivision_id' => $selectedSubdivisionID,
            'plan' => $selectedPlan,
            'elevation' => $selectedElevation,
            'updated_by' => strtoupper('lking')
        );

        $this->db->set($insertData);
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->set('update_date', 'NOW()', FALSE);
        //$sql = $this->db->get_compiled_insert('tbl_bid', $insertData);
        //echo $sql;
        $this->db->insert('tbl_bid', $insertData);
    }

                    function get_install_types($tbl_buildercustomer_builder_id){
                        $bidsdb = $this->load->database('bids', TRUE);
                        $sql = 'CALL sp_get_install_types_bids(' . $tbl_buildercustomer_builder_id . ')';
                        $query = $bidsdb->query($sql);
                        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
                        $bidsdb->close();
                        return $result;
                    }

                    function get_bid_details($bid_id){
                        $bidsdb = $this->load->database('bids',TRUE);
                        $bidsdb->select('*');
                        $bidsdb->from('vw_get_bid_details');
                        //$bidsdb->where($bid_id); // used when conditions are set in the controller (see bids->bidsAddEdit)
                        $bidsdb->where('tbl_bid_id', $bid_id);
                        $query = $bidsdb->get();
                        //$sql = 'CALL sp_get_bid_details()';
                        //$query = $bidsdb->query($sql);
                        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
                        $bidsdb->close();
                        return $result;
                    }

                    function get_mfgs(){
                        $vendorsdb = $this->load->database('vendors', TRUE);
                        $sql = 'CALL sp_get_manufacturers()';
                        $query = $vendorsdb->query($sql);
                        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
                        $vendorsdb->close();
                        return $result;
                    }

    function getManufacturerColorRows($params = array()){
        $this->db->select('
                wc.window_color_id, 
                wc.window_color_name,
                mHwC.tbl_window_color_window_color_id,
                mHwC.tbl_manufacturer_manufacturer_id
            ');
        $this->db->from($this->windowColorTbl.' as wc');
        $this->db->join($this->mfgHasWindowColor.' as mHwC', 'wc.window_color_id = mHwC.tbl_window_color_window_color_id');
            if(array_key_exists("conditions",$params)){
                foreach ($params['conditions'] as $key => $value) {
                    if(strpos($key,'.') !== false){
                        $this->db->where($key,$value);
                    }else{
                        $this->db->where('mHwC.'.$key,$value);
                    }
                }
            } // complete query looks like: SELECT s.subdivision_id, s.subdivision_name FROM tbl_subdivision s WHERE s.tbl_buildercustomer_builder_id=146
        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE; //if there is more than zero rows, return the data, otherwise return FALSE

        return $result; //return fetched data (or FALSE) back to Controller that called it Bids->getBuilderSubdivisions
    }

    function getManufacturerMaterialRows($params = array()){
        $this->db->select('
                wm.window_material_id, 
                wm.window_material_name,
                mHwM.tbl_window_material_window_material_id,
                mHwM.tbl_manufacturer_manufacturer_id
            ');
        $this->db->from($this->windowMaterialTbl.' as wm');
        $this->db->join($this->mfgHasWindowMaterial.' as mHwM', 'wm.window_material_id = mHwM.tbl_window_material_window_material_id');
            if(array_key_exists("conditions",$params)){
                foreach ($params['conditions'] as $key => $value) {
                    if(strpos($key,'.') !== false){
                        $this->db->where($key,$value);
                    }else{
                        $this->db->where('mHwM.'.$key,$value);
                    }
                }
            } // complete query looks like: SELECT s.subdivision_id, s.subdivision_name FROM tbl_subdivision s WHERE s.tbl_buildercustomer_builder_id=146
        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE; //if there is more than zero rows, return the data, otherwise return FALSE

        return $result; //return fetched data (or FALSE) back to Controller that called it Bids->getBuilderSubdivisions
    }

    function getManufacturerFinRows($params = array()){
        $this->db->select('
                wf.window_fin_id, 
                wf.window_fin_name,
                mHwF.tbl_window_fin_window_fin_id,
                mHwF.tbl_manufacturer_manufacturer_id
            ');
        $this->db->from($this->windowFinTbl.' as wf');
        $this->db->join($this->mfgHasWindowFin.' as mHwF', 'wf.window_fin_id = mHwF.tbl_window_fin_window_fin_id');
            if(array_key_exists("conditions",$params)){
                foreach ($params['conditions'] as $key => $value) {
                    if(strpos($key,'.') !== false){
                        $this->db->where($key,$value);
                    }else{
                        $this->db->where('mHwF.'.$key,$value);
                    }
                }
            } // complete query looks like: SELECT s.subdivision_id, s.subdivision_name FROM tbl_subdivision s WHERE s.tbl_buildercustomer_builder_id=146
        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE; //if there is more than zero rows, return the data, otherwise return FALSE

        return $result; //return fetched data (or FALSE) back to Controller that called it Bids->getBuilderSubdivisions
    }

    function getManufacturerCoatingRows($params = array()){
        $this->db->select('
                wct.window_coating_id, 
                wct.window_coating_name,
                mHwCt.tbl_window_coating_window_coating_id,
                mHwCt.tbl_manufacturer_manufacturer_id
            ');
        $this->db->from($this->windowCoatingTbl.' as wct');
        $this->db->join($this->mfgHasWindowCoating.' as mHwCt', 'wct.window_coating_id = mHwCt.tbl_window_coating_window_coating_id');
            if(array_key_exists("conditions",$params)){
                foreach ($params['conditions'] as $key => $value) {
                    if(strpos($key,'.') !== false){
                        $this->db->where($key,$value);
                    }else{
                        $this->db->where('mHwCt.'.$key,$value);
                    }
                }
            } // complete query looks like: SELECT s.subdivision_id, s.subdivision_name FROM tbl_subdivision s WHERE s.tbl_buildercustomer_builder_id=146
        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE; //if there is more than zero rows, return the data, otherwise return FALSE

        return $result; //return fetched data (or FALSE) back to Controller that called it Bids->getBuilderSubdivisions
    }

                    function get_bids_options($tbl_bid_id){
                        $bidsdb = $this->load->database('bids', TRUE);
                        $sql = 'CALL sp_get_current_bids_options(' . $tbl_bid_id . ')';
                        $query = $bidsdb->query($sql);
                        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
                        $bidsdb->close();
                        return $result;
                    }
/* changed 20190722 - START
    function get_bids_options($tbl_bid_id){
        $this->db->select('
                O.option_id,
                O.option_name,
                O.planOptionCode,
                O.cost
            ');
        $this->db->from($this->optionsTbl.' as O');
        $this->db->where('O.tbl_bid_tbl_bid_id', $tbl_bid_id);
        $this->db->order_by('O.option_id','ASC');

        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

        return $result;
    }
*/ //changed 20190722 - END
    function optionHasWindows($tbl_bid_id){
        $this->db->select('
            oHw.tbl_bid_windows_bid_windows_id,
            O.option_id,
            BW.window_details,
            BW.window_location,
            O.option_name
        ');
                //    oHw.tbl_options_option_id,BW.bid_windows_id,BW.mfg,BW.series,BW.grouping,O.planOptionCode,O.cost

        $this->db->from($this->optionHasWindowsTbl.' as oHw');
        $this->db->join($this->bidWindowsTbl.' as BW', 'BW.bid_windows_id = oHw.tbl_bid_windows_bid_windows_id');
        $this->db->join($this->optionsTbl.' as O', 'O.option_id = oHw.tbl_options_option_id');
        $this->db->where('O.tbl_bid_tbl_bid_id ='.$tbl_bid_id);

        //$this->db->where('oHw.tbl_options_option_id', 1);
        //$this->db->order_by('tbl_options_option_id', 'ASC');
        //$this->db->order_by('BW.bid_windows_id', 'ASC');
        //$this->db->where('', $tbl_bid_id)
        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

        return $result;
    }

    function optionHasWindows2($tbl_bid_id){
        $bidsdb = $this->load->database('bids', TRUE);
        $sql = 'CALL sp_option_has_windows(' . $tbl_bid_id . ')';
        $query = $bidsdb->query($sql);
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        $bidsdb->close();
        return $result;
    }
/* changed 20190722 - START
    function optionHasWindows2($tbl_bid_id){
        $bidsdb = $this->load->database('bids', TRUE);
        $bidsdb->select('
            oHw2.tbl_bid_windows_bid_windows_id,
            (
                SELECT COUNT(oHw21.tbl_bid_windows_bid_windows_id) total
                FROM tbl_options_has_windows_new oHw21 
                WHERE BW.bid_windows_id = oHw21.tbl_bid_windows_bid_windows_id
            ) ttl_option_count,
            O.option_id,
            O.option_name,
            BW.window_details,
            BW.window_location,
            oHw2.add_remove_action
        '); //SELECT tbl_bid_windows_bid_windows_id, count(tbl_bid_windows_bid_windows_id) from tbl_options_has_windows_new group BY tbl_bid_windows_bid_windows_id

        $bidsdb->from('tbl_options_has_windows_new as oHw2');
        $bidsdb->join('tbl_bid_windows as BW', 'BW.bid_windows_id = oHw2.tbl_bid_windows_bid_windows_id');
        $bidsdb->join('tbl_options as O', 'O.option_id = oHw2.tbl_options_option_id');
        $bidsdb->where('O.tbl_bid_tbl_bid_id ='.$tbl_bid_id);

        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

        return $result;
    }
*/ //changed 20190722 - END
    function insert_delete_from_optionHasWindows(){
        $gridUpdatesRaw = $this->input->post('changesToGrid2');
        $insertBucket = array();
        $deleteBucket = array();
        if(count($gridUpdatesRaw) != 0){
            foreach($gridUpdatesRaw as $gridGroup){
                $gridUpdatesGroup = explode(",", $gridGroup);
            };
        };
        array_pop($gridUpdatesGroup); // takes off the last item because it is always empty because jquery adds a comma
        if(count($gridUpdatesGroup) != 0){
            foreach($gridUpdatesGroup as $eachItem){
                if($eachItem){
                    $itemAction = explode("-", $eachItem);
                    if($itemAction[2] == 1){
                        $insertPairs = array(
                            'tbl_bid_windows_bid_windows_id' => $itemAction[0],
                            'tbl_options_option_id' => $itemAction[1]
                        );
                        $this->db->set($insertPairs);
                $this->db->insert($this->optionHasWindowsTbl);
                        //$addOptionSQL = $this->db->get_compiled_insert($this->optionHasWindowsTbl);
                        //echo $addOptionSQL;                       

                    }else{
                        $this->db->where('tbl_bid_windows_bid_windows_id',$itemAction[0]);
                        $this->db->where('tbl_options_option_id',$itemAction[1]);
                $this->db->delete($this->optionHasWindowsTbl);
                        //$removeOptionSQL = $this->db->get_compiled_delete($this->optionHasWindowsTbl);
                        //echo $removeOptionSQL;
                    };
                };
            };
        };
    }

    function insert_update_delete_optionHasWindows2(){// 1 = plus add to table with add_remove_action of 1, 2 = edit remove entry from table, 0 = ban add to table with add_remove_action of 0   [ban = 0, plus = 1, edit = 2, left = 3, right = 4]-[insert = 1, update = 2, delete = 3] // window id, option id, add_remove_action, db action
        $gridUpdatesRaw = $this->input->post('changesToGrid3');
        $insertBucket = array();
    $updateBucket = array();
        $deleteBucket = array();
        if(count($gridUpdatesRaw) != 0){
            foreach($gridUpdatesRaw as $gridGroup){
                $gridUpdatesGroup = explode(",", $gridGroup);
            };
        };
        array_pop($gridUpdatesGroup); // takes off the last item because it is always empty because jquery adds a comma
        if(count($gridUpdatesGroup) != 0){
            foreach($gridUpdatesGroup as $eachItem){
                if($eachItem){
                    $itemAction = explode("-", $eachItem);
                    if($itemAction[3] == 1){ // if last element is insert
                        if($itemAction[2] != 0){ //0, 1  3 and 4 are 1 as well
                            $variableAbove = 1; //db takes 0 for ban and 1 for plus all others are edit
                        }else{
                            $variableAbove = 0;
                        };
                        $insertPairs = array(
                            'tbl_bid_windows_bid_windows_id' => $itemAction[0],
                            'tbl_options_option_id' => $itemAction[1], //option id handles the LHAND and RHAND
                            'add_remove_action' => $variableAbove
                        );
                        $this->db->set($insertPairs);
                $this->db->insert($this->optionHasWindowsNewTbl);
                        //$addOptionSQL = $this->db->get_compiled_insert($this->optionHasWindowsNewTbl);
                        //echo $addOptionSQL;
                    }elseif($itemAction[3] == 2){
                        $this->db->where('tbl_bid_windows_bid_windows_id',$itemAction[0]);
                        $this->db->where('tbl_options_option_id',$itemAction[1]);
                        $updatePairs = array(
                            'add_remove_action' => $itemAction[2]
                        );
                        $this->db->set($updatePairs);
                $this->db->update($this->optionHasWindowsNewTbl);
                        //$updateOptionSQL = $this->db->get_compiled_update($this->optionHasWindowsNewTbl);
                        //echo $updateOptionSQL;
                    }else{
                        $this->db->where('tbl_bid_windows_bid_windows_id',$itemAction[0]);
                        $this->db->where('tbl_options_option_id',$itemAction[1]);
                $this->db->delete($this->optionHasWindowsNewTbl);
                        //$removeOptionSQL = $this->db->get_compiled_delete($this->optionHasWindowsNewTbl);
                        //echo $removeOptionSQL;
                    };
                };
            };
        };
    }

                    function update_window_order($windowID, $position){
                        $bidsdb = $this->load->database('bids', TRUE);
                        $bidsdb->where('bid_windows_id',$windowID);
                        $updateOrder = array('position' => $position);
                        $bidsdb->set($updateOrder);
                        $bidsdb->update('tbl_bid_windows');
                    }

                    function insert_into_optionHasWindows(){

                    }

                    function get_bids_windows($tbl_bid_id){
                        $bidsdb = $this->load->database('bids', TRUE);
                        $sql = 'CALL sp_get_bid_windows_option(' . $tbl_bid_id . ')';
                        $query = $bidsdb->query($sql);
                        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
                        $bidsdb->close();
                        return $result;
                    }

                    function get_bids_windows2($tbl_bid_id){
                        $bidsdb = $this->load->database('bids', TRUE);
                        $sql = 'CALL sp_get_bid_windows(' . $tbl_bid_id . ')';
                        $query = $bidsdb->query($sql);
                        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
                        $bidsdb->close();
                        return $result;
                    }

                    function bid_simulate(){
                        
                    }

}


// RETIRED CODE /////////////////////////////////////////////////////////

/* changed 20190721 - START
    function get_install_types($tbl_buildercustomer_builder_id){
        $this->db->select('
            instTypes.install_types_id,
            instTypes.type,
            bHiT.tbl_install_types_install_types_id,
            bHiT.tbl_buildercustomer_builder_id
        ');
        $this->db->from($this->installTypesTbl.' as instTypes');
        $this->db->join($this->builderHasInstallTypesTbl.' as bHiT', 'instTypes.install_types_id = bHiT.tbl_install_types_install_types_id AND bHiT.tbl_buildercustomer_builder_id='.$tbl_buildercustomer_builder_id);
        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        return $result;
    }
*/ //changed 20190721 - END

/* changed 20190721 - START
    function getRecentBidsList($params = array()){
        $bidsdb = $this->load->database('bids', TRUE);
        $this->db->select('DATE_FORMAT(Bids.update_date, "%m/%d/%Y") as Date,
                Bids.tbl_bid_id,
                B.builder_name as Builder,
                S.tbl_buildercustomer_builder_id as BuilderID,
                S.subdivision_name as Subdivision,
                Bids.plan as Plan,
                Bids.elevation as Elevation,
                U.po_abbreviation as Branch,
                S.city as City,
                S.state as State,
                Bids.updated_by as Prepped');
        $this->db->from($this->bidsTbl.' as Bids');
        $this->db->join($this->subsTbl.' as S', 'S.subdivision_id = Bids.tbl_subdivision_subdivision_id');
        $this->db->join($this->builderTbl.' as B', 'B.builder_id = S.tbl_buildercustomer_builder_id');
        $this->db->join($this->branchTbl.' as U', 'U.xo_branch_id = S.tbl_xo_branch_xo_branch_id');
        $statuses = array(702,703,704);
            $this->db->where_in('Bids.tbl_status_tbl_status_id', $statuses);
        $this->db->order_by('Date','DESC');
        $this->db->limit(10);
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                if(strpos($key,'.') !== false){
                    $this->db->where($key,$value);
                }else{
                    $this->db->where('Bids.'.$key,$value);
                }
            }
        }

        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

        //return fetched data
        return $result;
    }
*/ //changed 20190721 - END
/* changed 20190721 - START
    function getBuilderRows($params = array()){
        $this->db->select('b.builder_id, b.builder_name');
        $this->db->from($this->builderTbl.' as b');
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                if(strpos($key,'.') !== false){
                    $this->db->where($key,$value);
                }else{
                    $this->db->where('b.'.$key,$value);
                }
            }
        }
        // $this->db->where('c.status','1');
        
        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

        //return fetched data
        return $result;
    }
*/ //changed 20190721 - END shifted to maintenancemodel existing function

    /*
        public function getBuilderSubdivisions(){
        $subdivisions = array(); //container to hold all the matching subdivisions
        $builder_id = $this->input->post('builder_id'); //using this passed from users choice
            if($builder_id){
                $con['conditions'] = array('tbl_buildercustomer_builder_id'=>$builder_id); //for WHERE field equals value posted
                $subdivisions = $this->bidsmodel->getBuilderSubdivisionRows($con); //using above passed in from views->bids->addbid use Models->BidsModel->getBuilderSubdivisionRows w/ conditions
            }
        echo json_encode($subdivisions); //create as json array so view can parse it
    }
    */

/*
    public function buildersAdd(){
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        if($post['addBuilder']){
            if($post['builderName'] == ''){
                Messages::setMsg('Please Fill In All Fields', 'error');
                return;
            }
// try catch to stop duplicate entries?
            $this->query('INSERT INTO tbl_buildercustomer (builder_name,updated_by) VALUES (:buildername,:updatedBy)');
            $this->bind(':buildername', strtoupper($post['builderName']));
            $this->bind(':updatedBy', strtouppoer('lking'));
            //$this->bind(':user_id', $_SESSION['user_data']['user_id']);
            $this->execute();
            if($this->lastInsertId()){
                header('Location: '.ROOT_URL.'maintenance\builders');
            } else {
                header('Location: '.ROOT_URL.'maintenance\builders');
            }
        }
        return;
    }
*/

/* changed 20190721 - START
    function get_bid_details($params = array()){
        $this->db->select('Bids.tbl_bid_id,
                Bids.tbl_subdivision_subdivision_id,
                Bids.plan as Plan,
                Bids.elevation as Elevation,
                Bids.tbl_install_types_install_types_id,
                IT.type as Type,
                Bids.tbl_manufacturer_manufacturer_id,
                Mfg.manufacturer_name,
                Bids.tbl_window_color_window_color_id,
                WC.window_color_name,
                Bids.tbl_window_material_window_material_id,
                WM.window_material_name,
                Bids.tbl_window_fin_window_fin_id,
                WF.window_fin_name,
                Bids.tbl_window_coating_window_coating_id,
                WCt.window_coating_name,
                Bids.tbl_status_tbl_status_id,
                B.builder_name as Builder,
                S.subdivision_name as Subdivision,
                U.po_abbreviation as Branch,
                S.city as City,
                S.state as State,
                Bids.updated_by as Prepped');

        $this->db->from($this->bidsTbl.' as Bids');
        $this->db->join($this->subsTbl.' as S', 'S.subdivision_id = Bids.tbl_subdivision_subdivision_id');
        $this->db->join($this->builderTbl.' as B', 'B.builder_id = S.tbl_buildercustomer_builder_id');
        $this->db->join($this->branchTbl.' as U', 'U.xo_branch_id = S.tbl_xo_branch_xo_branch_id');
        $this->db->join($this->installTypesTbl.' as IT', 'IT.install_types_id = Bids.tbl_install_types_install_types_id', 'left outer');
        $this->db->join($this->mfgTbl.' as Mfg', 'Mfg.manufacturer_id = Bids.tbl_manufacturer_manufacturer_id', 'left outer');
        $this->db->join($this->windowColorTbl.' as WC', 'WC.window_color_id = Bids.tbl_window_color_window_color_id', 'left outer');
        $this->db->join($this->windowCoatingTbl.' as WCt', 'WCt.window_coating_id = Bids.tbl_window_coating_window_coating_id', 'left outer');
        $this->db->join($this->windowMaterialTbl.' as WM', 'WM.window_material_id = Bids.tbl_window_material_window_material_id', 'left outer');
        $this->db->join($this->windowFinTbl.' as WF', 'WF.window_fin_id = Bids.tbl_window_fin_window_fin_id', 'left outer');

        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                if(strpos($key,'.') !== false){
                    $this->db->where($key,$value);
                }else{
                    $this->db->where('Bids.'.$key,$value);
                }
            }
        }

        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

        //return fetched data
        return $result;
    }
*/ //changed 20190721 - END

/* changed 20190722 - START
    function get_mfgs(){
        $this->db->select('
            manufacturer_id,
            mfg_abbr,
            manufacturer_name
            ');
        $this->db->from($this->mfgTbl);

        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        return $result;
    }
*/ //changed 20190722 - END

/* changed 20190718 - START
    function get_install_types($tbl_buildercustomer_builder_id){
        $this->db->select('
            instTypes.install_types_id,
            instTypes.type,
            bHiT.tbl_install_types_install_types_id,
            bHiT.tbl_buildercustomer_builder_id
        ');
        $this->db->from($this->installTypesTbl.' as instTypes');
        $this->db->join($this->builderHasInstallTypesTbl.' as bHiT', 'instTypes.install_types_id = bHiT.tbl_install_types_install_types_id AND bHiT.tbl_buildercustomer_builder_id='.$tbl_buildercustomer_builder_id);
        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        return $result;
    }

    DATE_FORMAT(Bids.update_date, "%m/%d/%Y") as Date,
*/ //changed 20190718 - END

/* changed 20190721 - START
    function update_window_order($windowID, $position){
        $this->db->where('bid_windows_id',$windowID);
        $updateOrder = array('position' => $position);
        $this->db->set($updateOrder);
        $this->db->update($this->bidWindowsTbl);
    }
*/ //changed 20190721 - END

/* changed 20190721 - START
    function get_bids_windows($tbl_bid_id){
        $this->db->select('
                gBW.bid_windows_id,
                gBW.window_details,
                gBW.window_location,
                gBW.position
            ');
        $this->db->from($this->bidWindowsTbl.' as gBW');
        $this->db->join($this->optionsTbl.' as O', 'O.option_id='.$tbl_bid_id, 'LEFT');
        $this->db->order_by('position','ASC');

        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

        return $result;
    }
*/ //changed 20190721 - END

/* changed 20190721 - START
    function get_bids_windows2($tbl_bid_id){
        $this->db->select('
                gBW2.bid_windows_id,
                gBW2.window_details,
                gBW2.window_location,
                gBW2.position
            ');
        $this->db->from($this->bidWindowsTbl.' as gBW2');
        $this->db->where('tbl_bid_tbl_bid_id='.$tbl_bid_id);
        $this->db->order_by('position','ASC');

        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

        return $result;
    }
*/ //changed 20190721 - END

?>