<?php
/*

REPORTS:
    Executive summaries for XO Windows and Builder when the last lot in subdivision completes
    Each schema? / section should have list of valid status' (example jobsite->start, ordered, received, delivered, installed/complete)?

WORKFLOW:

    BIDS
        If authorized:
            Select Builder from Dropdown
            Select Subdivision from Dropdown
            Enter Plan No.
            Enter Elevation
                Check for Duplicates
            Choose
                Installation Type
                Window Manufacturer
                    Window Options available from the Manufacturer
                        Color
                        Material
                        Fin
                        Coating


            Look at plans, starting at bottom-right corner working counter-clockwise
                Enter window size
                Enter window location
                    Need to handle grouping if a window is a stack for example
            Enter Each of the Option Names for the Plan / Elevation
                Save
            Working in Option / Windows Grid select appropriate icon representing the action for window under option column
                Save

    Thoughts:
        Mechanism for copying an existing bid for an Elevation to a New Elevation


WORKFLOW END

PHP:

dot variable versus brackets (easier quotation mark usage)
// This line only works for PHP5
print "total weight = {$bottles->totalWeight( )} kg";

// This works for both PHP4 and PHP5
print "total weight = " . $bottles->totalWeight( ) . " kg";

ARRAYS:
	// Faster than array_push()  BOTH allow you to set the KEY name when adding to array variable using '=>' which I think is cool!
	$bid_simulation[] = array('window_details1' => $bid_temp[0]['window_details'],'window_location1' => $bid_temp[0]['window_location']);
	VS
	//array_push($bid_simulation, array('window_details' => $eachWindow['window_details'],'window_location' => $eachWindow['window_location']));

	// Copy an array
	array_merge($array1Name, $array2Name);  Does order matter? doesn't look like it

	// a way to override what the BidModel passes in, eventually will be an array of the options chosen by Starts to fulfill the jobsite's order of windows
	$get_bid_options = array(array('option_id' => 1,'option_name' => 'BASE'),array('option_id' => 2,'option_name' => 'LHAND'),array('option_id' => 6,'option_name' => 'SINGLE FRENCH AT MBR PATIO'),array('option_id' => 10,'option_name' => 'LARGE SGD AT PATIO'),array('option_id' => 14,'option_name' => 'RAIN GLASS ABOVE TUB'));

	// RESET / UNSET and array, just re-initialize the array
	$arrayName = array();

	// IN ARRAY seems to work without trying to specify the key
		in_array($nOWB, $baseWindowBucket);  // notice how there is no $nOWB['keyName'] or $baseWindowBucket['anotherKeyName']?


	// A multidimensional array containing arrays like this:
		$get_bid_options = array(array('option_id' => 1,'option_name' => 'BASE'),array('option_id' => 2,'option_name' => 'LHAND'),array('option_id' => 6,'option_name' => 'SINGLE FRENCH AT MBR PATIO'),array('option_id' => 10,'option_name' => 'LARGE SGD AT PATIO'),array('option_id' => 14,'option_name' => 'RAIN GLASS ABOVE TUB'));
	// AND a 'single' dimensional array containing single values like this:
        $get_bid_options = array(array('option_id' => 1),array('option_id' => 2),array('option_id' => 6),array('option_id' => 10),array('option_id' => 14));
    // WILL work with the $matchItem['option_id'] of this function ---------------------------------------------------\/
		if(verifyCrossRef($option_has_windows, array('option_id', 'tbl_bid_windows_bid_windows_id'), array($matchItem['option_id'],$eachWindow['bid_windows_id']))){
	// ADDITIONALLY, if you shorten the 'array' to this:
		$get_bid_options = array(1,2,6,10,14);
	// IT WILL work with just the $matchItem of the same function minus the 'key': --------------------------- \/
 		if(verifyCrossRef($option_has_windows, array('option_id', 'tbl_bid_windows_bid_windows_id'), array($matchItem,$eachWindow['bid_windows_id']))){


	TROUBLESHOOTING
		// Gives you the details of the array, limited to first 127? elements
	    echo "<pre>";
        var_dump(array_map('serialize', $baseWindowBucket)); // size = 27
        echo "</pre>";

        //another way to view the array variable
            $result = var_dump($gBW['bid_windows_id']);
            echo "<tr><td>".print_r($result)."</td></tr>";

        // a way to view what is getting added to an 'array' before converting to array operation only (instead of textarea array)
            $('input[name="selected[]"]').change(function(){
        var textarea = $('#addRemoveTb2');
        var item = $(this).val();
         //alert($(this).val()); //yay!
         if(this.checked){
            //alert(item+" checked");
            if(new RegExp( (item + "-0" + ","),"g" ).test(textarea.html())){
                textarea.html(textarea.html().replace(new RegExp( (item + "-0" + ","),"g" ),""));
            }else{
                textarea.append(item + "-1" + ",");                
            };
         }else{
            //alert(item+" unchecked");
            if(new RegExp( (item + "-1" + ","),"g" ).test(textarea.html())){
                textarea.html(textarea.html().replace(new RegExp( (item + "-1" + ","),"g" ),""));
            }else{
                textarea.append(item + "-0" + ",");               
            };
            //textarea.html(textarea.html().replace(new RegExp( (item + "-1" + ","),"g" ),""));
         }
    });
ARRAYS END

BOOTSTRAP

	//Remove the data-parent to stop the accordion action from collapsing previous section when opening new section
		Remove this from the element: data-parent="#bidsAccordion"

BOOTSTRAP END

JQUERY

	REDIRECT USER
	// go to a specific page after a timeout (5 sec in this example)
	        setTimeout(function(){
            	window.location.href = "<?php echo base_url(); ?>index.php/Maintenance/builders";
            }, 5000);
    // instead of?
    	location.reload();

    SET MENU NAV on CLICK (haven't gotten working yet)
    <script>
		$(document).ready(function(){
			$(".nav .nav-link").on("click", function(){
		    $(".nav").find(".active").removeClass("active");
		    $(this).addClass("active");
		});
		});
	</script>

----AJAX

	data:{variableNameAtURLpostingTo:varableNameInThisAJAXcall, variableNameAtURLpostingTo:varableNameInThisAJAXcall}

	if dataObj returned by URL is an array:
	var dataObj = jQuery.parseJSON(data);
	var name1 = dataObj[0];
	var name2 = dataObj[1];


    AJAX DEVELOPMENT MONITOR TOOLS
    //display the array
    alert(JSON.stringify(dataObj));

----AJAX END

JQUERY END

SQL START

//https://stackoverflow.com/questions/14337993/mysql-show-count-of-rows-from-other-table-in-each-row
    function optionHasWindows2($tbl_bid_id){
        $this->db->select('
            oHw2.tbl_bid_windows_bid_windows_id,
            (
                SELECT COUNT(oHw21.tbl_bid_windows_bid_windows_id) total
                FROM tbl_options_has_windows_new oHw21 
                WHERE BW.bid_windows_id = oHw21.tbl_bid_windows_bid_windows_id
            ) ttl_option_count,
            O.option_id,
            O.option_name,
            BW.window_details,
            BW.window_location,
            oHw2.add_remove_action
        '); //SELECT tbl_bid_windows_bid_windows_id, count(tbl_bid_windows_bid_windows_id) from tbl_options_has_windows_new group BY tbl_bid_windows_bid_windows_id

        $this->db->from($this->optionHasWindowsNewTbl. ' as oHw2');
        $this->db->join($this->bidWindowsTbl.' as BW', 'BW.bid_windows_id = oHw2.tbl_bid_windows_bid_windows_id');
        $this->db->join($this->optionsTbl.' as O', 'O.option_id = oHw2.tbl_options_option_id');
        $this->db->where('O.tbl_bid_tbl_bid_id ='.$tbl_bid_id);

        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

        return $result;
    }




SQL END
TROUBLESHOOTING:



UNUSED:

	FUNCTIONS with example calls

		//$bid_simulation = _group_by($bid_simulation, 'window_details');
			function _group_by($array, $key) {
			    $return = array();
			    foreach($array as $val) {
			        $return[$val[$key]][] = $val;
			    }
			    return $return;
			}


		//$bid_simulation = returndup($bid_simulation);
			function returndup($array) 
			{
			    $results = array();
			    $duplicates = array();
			    foreach ($array as $item) {
			        if (in_array($item, $results)) {
			            $duplicates[] = $item;
			        }

			        $results[] = $item;
			    }

			    return $duplicates;
			}


$get_bid_windows = array(array("bid_windows_id" => "1","window_details" => "4020 PW","window_location" => "S-BA2"),array("bid_windows_id" => "2","window_details" => "4020 PW","window_location" => "S-BA3"),array("bid_windows_id" => "3","window_details" => "2656 SH","window_location" => "S2-BR3"),array("bid_windows_id" => "4","window_details" => "2656 SH","window_location" => "S2-MBR"),array("bid_windows_id" => "5","window_details" => "2656 SH","window_location" => "S-BONUS"),array("bid_windows_id" => "6","window_details" => "3640 XO","window_location" => "S-KIT"),array("bid_windows_id" => "7","window_details" => "8056 XOX","window_location" => "R-NOOK"),array("bid_windows_id" => "8","window_details" => "8056 XOX","window_location" => "R-MBR"),array("bid_windows_id" => "9","window_details" => "10056 XOX","window_location" => "R-GRT"),array("bid_windows_id" => "10","window_details" => "2040 SH","window_location" => "S-MBR"),array("bid_windows_id" => "11","window_details" => "2040 SH","window_location" => "S-MBR"),array("bid_windows_id" => "12","window_details" => "5056 PW TEMP","window_location" => "S-MBA"),array("bid_windows_id" => "13","window_details" => "5050 XO","window_location" => "S-BR2"),array("bid_windows_id" => "14","window_details" => "2020 PW","window_location" => "S-PLAY"),array("bid_windows_id" => "15","window_details" => "2020 PW","window_location" => "S-PLAY"),array("bid_windows_id" => "16","window_details" => "2020 PW TEMP","window_location" => "S-PLAY"),array("bid_windows_id" => "17","window_details" => "2'6in CIRCLE TOP  10 00   *FACTORY STACK TOP*         2 LT 2 X 1","window_location" => "F-BR3"),array("bid_windows_id" => "18","window_details" => "2646 SH    11 11              *FACTORY STACK BTM*         4/4","window_location" => "F-BR3"),array("bid_windows_id" => "19","window_details" => "2656 SH    11 11                    4/4","window_location" => "S-BR3/F2-BR3"),array("bid_windows_id" => "20","window_details" => "2656 SH    11 11                    4/4","window_location" => "S-BR3/F2-BR3"),array("bid_windows_id" => "21","window_details" => "2656 SH    11 11                    4/4","window_location" => "S-BR3/F2-BR3"),array("bid_windows_id" => "22","window_details" => "5056 XO    13 13                    8-8=16 LT","window_location" => "S-PLAY"),array("bid_windows_id" => "23","window_details" => "2060 PW  TEMP    13 00        8 LT  2 X 4","window_location" => "F-ENT"),array("bid_windows_id" => "24","window_details" => "2056 SH    11 11                   4/4 ","window_location" => "S-BR4"),array("bid_windows_id" => "25","window_details" => "2036 PW    12 00                  6 LT  2 X 3","window_location" => "F-TOWER"),array("bid_windows_id" => "26","window_details" => "5056 PW TEMP             RAIN GLASS","window_location" => "S-MBA"),array("bid_windows_id" => "27","window_details" => "6080 PD OX","window_location" => "S-NOOK"),array("bid_windows_id" => "28","window_details" => "6080 PD XO","window_location" => "S-NOOK"),array("bid_windows_id" => "29","window_details" => "3056 SH    11 11      Verify Grids","window_location" => "S-DEN"),array("bid_windows_id" => "30","window_details" => "2020 PW 2/2","window_location" => "S2-BONUS/MBR"),array("bid_windows_id" => "31","window_details" => "2656 SH    11 11                    4/4","window_location" => "S2-BONUS/MBR"),array("bid_windows_id" => "32","window_details" => "2656 SH    11 11                    4/4","window_location" => "S2-BONUS/MBR"),array("bid_windows_id" => "33","window_details" => "4020 PW","window_location" => "S-MBA"),array("bid_windows_id" => "34","window_details" => "3056 SH GPE","window_location" => "S-DEN"),array("bid_windows_id" => "35","window_details" => "5050 XO","window_location" => "S-BR5"),array("bid_windows_id" => "36","window_details" => "12080 O2X","window_location" => "R-GRT"),array("bid_windows_id" => "37","window_details" => "12080 2XO","window_location" => "R-GRT"));


*/



//ITERATIONS GALORE! - START
/*
        $not_used_windows = array();
        $option_removed_windows = array();
        $windows_in_selected_options = array();
        $windows_affected_by_options = array();
        $windows_affected_by_selected_options = array();
        $added_windows_bucket = array();
        $deleted_windows_bucket = array();
        $sameWindowArray = array();
        $windowsCheckedArray = array();
        $notInSelectedOptions = array();


            //echo json_encode($bid_simulation_base);
            //echo json_encode($bid_simulation_affected_by_options);

            foreach($bid_simulation_affected_by_options as $bSaBo){
                foreach($options_affecting_windows as $oAw){
                    if($bSaBo[0]['window_id'] == $oAw['tbl_bid_windows_bid_windows_id']){
                        $windows_affected_by_options_actions[$bSaBo[0]['window_id']][] = array('option_id' => $oAw['option_id'], 'option_name' => $oAw['option_name'], 'window_id' => $bSaBo[0]['window_id'], 'window_details' => $bSaBo[0]['window_details'], 'window_location' => $bSaBo[0]['window_location'], 'order' => $bSaBo[0]['order'], 'add_remove_action' => $oAw['add_remove_action']);
                    };
                };
            };
*/ 

/*
            foreach($get_bid_windows as $eachWindow){
                foreach($get_bid_options as $allOptions){
                    if(verifyCrossRef($options_affecting_windows, array('option_id','tbl_bid_windows_bid_windows_id'), array($allOptions['option_id'],$eachWindow['bid_windows_id']))){
                        $windows_affected_by_options[] = array('option_id' => $allOptions['option_id'], 'option_name' => $allOptions['option_name'], 'window_id' => $eachWindow['bid_windows_id'], 'window_details' => $eachWindow['window_details'], 'window_location' => $eachWindow['window_location'], 'order' => $eachWindow['order']);
                    };
                };       
            };

            foreach($windows_affected_by_options as $wAbO){
                foreach($options_affecting_windows as $oAw){
                    if($wAbO['option_id'] == $oAw['option_id'] && $wAbO['window_id'] == $oAw['tbl_bid_windows_bid_windows_id']){
                        $windows_affected_by_options_actions[] = array('option_id' => $wAbO['option_id'], 'option_name' => $wAbO['option_name'], 'window_id' => $wAbO['window_id'], 'window_details' => $wAbO['window_details'], 'window_location' => $wAbO['window_location'], 'order' => $wAbO['order'], 'add_remove_action' => $oAw['add_remove_action']);
                    };
                };
            };

            foreach($windows_affected_by_options_actions as $wAbOaAll){
                $sameWindowArray[$wAbOaAll['window_id']] = array();
                foreach($windows_affected_by_options_actions as $wAbOaCurrent){
                    if($wAbOaCurrent['window_id'] == $wAbOaAll['window_id'] && !verifyCrossRef($sameWindowArray[$wAbOaAll['window_id']], array('window_id', 'option_id'), array($wAbOaCurrent['window_id'], $wAbOaCurrent['option_id']))){
                        $sameWindowArray[$wAbOaAll['window_id']][] = array('option_id' => $wAbOaCurrent['option_id'], 'option_name' => $wAbOaCurrent['option_name'], 'window_id' => $wAbOaCurrent['window_id'], 'window_details' => $wAbOaCurrent['window_details'], 'window_location' => $wAbOaCurrent['window_location'], 'order' => $wAbOaCurrent['order'], 'add_remove_action' => $wAbOaCurrent['add_remove_action']);
                    };
                };
            };
*/            
            //echo json_encode($sameWindowArray[5][0]['option_id']);
            //echo json_encode($sameWindowArray);

            //foreach($sameWindowArray as $sWa){
            //foreach($sWa as $sWaDetails){

/*



                if(count($wAbOa) == $option_matches || $option_matches == 0){
        foreach($wAbOa as $wAbOaDetails){
                    foreach($selected_bid_options as $simulatorSelectionsAll){
                        $simulatorSelectionsEach = explode(",", $simulatorSelectionsAll);
                        if($wAbOaDetails['option_id'] == $simulatorSelectionsEach[0]){
                            $optionFilterArray[$wAbOaDetails['window_id']][] = array('option_id' => $wAbOaDetails['option_id'], 'option_name' => $wAbOaDetails['option_name'], 'window_id' => $wAbOaDetails['window_id'], 'window_details' => $wAbOaDetails['window_details'], 'window_location' => $wAbOaDetails['window_location'], 'order' => $wAbOaDetails['order'], 'add_remove_action' => $wAbOaDetails['add_remove_action']);
                        };                  
                    };
                    if($wAbOaDetails['add_remove_action'] == 0){
                        $count_remove = $count_remove + 1;
                    };
                    if($wAbOaDetails['add_remove_action'] == 1){
                        $count_add = $count_add + 1;
                    };
                    if($count_remove == count($wAbOa)){
                        $bid_simulation_base[$wAbOaDetails['window_id']][] = array('option_name' => 'BASE', 'window_id' => $wAbOaDetails['window_id'], 'window_details' => $wAbOaDetails['window_details'], 'window_location' => $wAbOaDetails['window_location'], 'order' => $wAbOaDetails['order']);
                    };
                };
        };
            };

            foreach($optionFilterArray as $oFa){
                $oFaCount = count($oFa);
                //echo $oFaCount;
                foreach($oFa as $oFaDetails){
                    if($oFaCount == 1 && $oFaDetails['add_remove_action'] == 1 && ($oFaDetails['option_name'] == 'LHAND' || $oFaDetails['option_name'] == 'RHAND')){
                        $bid_simulation_base[$oFaDetails['window_id']][] = array('option_name' => 'BASE', 'window_id' => $oFaDetails['window_id'], 'window_details' => $oFaDetails['window_details'], 'window_location' => $oFaDetails['window_location'], 'order' => $oFaDetails['order']);
                    }elseif($oFaCount == 1 && $oFaDetails['add_remove_action'] == 0 && $oFaDetails['option_name'] != 'LHAND' && $oFaDetails['option_name'] != 'RHAND'){
                        $bid_simulation_window_removed[$oFaDetails['window_id']][] = array('option_name' => $oFaDetails['option_name'], 'window_id' => $oFaDetails['window_id'], 'window_details' => $oFaDetails['window_details'], 'window_location' => $oFaDetails['window_location'], 'order' => $oFaDetails['order']);
                    }elseif($oFaCount >= 2 && $oFaDetails['add_remove_action'] == 1 && $oFaDetails['option_name'] != 'LHAND' && $oFaDetails['option_name'] != 'RHAND'){
                        $bid_simulation_window_added[$oFaDetails['window_id']][] = array('option_name' => $oFaDetails['option_name'], 'window_id' => $oFaDetails['window_id'], 'window_details' => $oFaDetails['window_details'], 'window_location' => $oFaDetails['window_location'], 'order' => $oFaDetails['order']);
                    };
                };
            };     

                if(count($optionFilterArray) == 0){
                    $notInSelectedOptions[] = array('option_name1' => '','window_details1' => $sWa[0]['window_details'],'window_location1' => $sWa[0]['window_location'], 'order1' => $sWa[0]['order'], 'add_remove_action' => $sWa[0]['add_remove_action']);
                };


                //echo 'count: '.$array_count.' add: '.$count_add.' remove '.$count_remove.' - window: '.$wAbOa[0]['window_id'].'     ';

                if($array_count == 1 && $array_count == $count_add && $count_add == count($wAbOa) && ($wAbOa[0]['option_name'] == 'LHAND' || $wAbOa[0]['option_name'] == 'RHAND')){
                    $bid_simulation_base[$wAbOa[0]['window_id']][] = array('option_name' => 'BASE', 'window_id' => $wAbOa[0]['window_id'], 'window_details' => $wAbOa[0]['window_details'], 'window_location' => $wAbOa[0]['window_location'], 'order' => $wAbOa[0]['order']);
                    echo 'base2';           
                };

                if($array_count == 1 && $count_remove == count($wAbOa)){
                    $bid_simulation_window_removed[$wAbOa[0]['window_id']][] = array('option_name' => $wAbOa[0]['option_name'], 'window_id' => $wAbOa[0]['window_id'], 'window_details' => $wAbOa[0]['window_details'], 'window_location' => $wAbOa[0]['window_location'], 'order' => $wAbOa[0]['order']);
                    echo 'remove1';
                };

                if($array_count == 2 && $count_add == 2 && ($wAbOa[0]['option_name'] != 'LHAND' && $wAbOa[0]['option_name'] != 'RHAND')){
                    $bid_simulation_window_added[$wAbOa[0]['window_id']][] = array('option_name' => $wAbOa[0]['option_name'], 'window_id' => $wAbOa[0]['window_id'], 'window_details' => $wAbOa[0]['window_details'], 'window_location' => $wAbOa[0]['window_location'], 'order' => $wAbOa[0]['order']);                    
                    echo 'add1';
                };

                if(count($optionFilterArray) == 1 && $sWa[0]['add_remove_action'] == 0){
                    $bid_simulation_window_removed[] = array('option_name1' => $sWa[0]['option_name'], 'window_details1' => $sWa[0]['window_details'], 'window_location1' => $sWa[0]['window_location'], 'order1' => $sWa[0]['order']);
                };
                if(count($optionFilterArray) == 1 && $sWa[0]['add_remove_action'] == 1 && $sWa[0]['option_name'] != 'LHAND' && $sWa[0]['option_name'] != 'RHAND'){
                    $bid_simulation_window_added[] = array('option_name1' => $sWa[0]['option_name'], 'window_details1' => $sWa[0]['window_details'], 'window_location1' => $sWa[0]['window_location'], 'order1' => $sWa[0]['order']);
                };
                if(count($optionFilterArray) == 2 && $sWa[0]['add_remove_action'] == 1 && ($sWa[1]['option_name'] != 'LHAND' && $sWa[1]['option_name'] != 'RHAND')){
                    $bid_simulation_window_added[] = array('option_name1' => $sWa[1]['option_name'], 'window_details1' => $sWa[1]['window_details'], 'window_location1' => $sWa[1]['window_location'], 'order1' => $sWa[1]['order']);
                };
                
           
            echo json_encode($optionFilterArray);
            //array_multisort (array_column($bid_simulation_base, 'order'), SORT_ASC, $bid_simulation_base);
            //echo json_encode($notInSelectedOptions);

            foreach($sameWindowArray as $sWa){
                foreach($selected_bid_options as $simulatorSelectionsAll){
                    $simulatorSelectionsEach = explode(",", $simulatorSelectionsAll);
                    if($sWa['option_id'] == $simulatorSelectionsEach[0] && !verifyCrossRef($optionFilterArray, array('window_id', 'option_id'), array($sWa['window_id'], $sWa['option_id']))){
                        $optionFilterArray[] = array('option_id' => $sWa['option_id'], 'option_name' => $sWa['option_name'], 'window_id' => $sWa['window_id'], 'window_details' => $sWa['window_details'], 'window_location' => $sWa['window_location'], 'order' => $sWa['order'], 'add_remove_action' => $sWa['add_remove_action']);
                    };
                    if(!verifyCrossRef($windowsCheckedArray, array('window_id', 'option_id'), array($sWa['window_id'], $sWa['option_id']))){
                        $windowsCheckedArray[] = array('option_id' => $sWa['option_id'], 'option_name' => $sWa['option_name'], 'window_id' => $sWa['window_id'], 'window_details' => $sWa['window_details'], 'window_location' => $sWa['window_location'], 'order' => $sWa['order'], 'add_remove_action' => $sWa['add_remove_action']);
                    };
                };
                echo json_encode($optionFilterArray);
            };
            echo json_encode($notInSelectedOptions);
            echo json_encode($windowsCheckedArray);
*/
                // create an array of array blocks?  if the window number matches add to the array....
            /*
                if(array_count_values(array_column($windows_affected_by_options_actions, 'window_id'))[$wAbOa['window_id']] > 1){
                            

                }else{
                    foreach($selected_bid_options as $simulatorSelectionsAll){ // get ALL options selected in 'Simulator', which includes ID and Name
                    $simulatorSelectionsEach = explode(",", $simulatorSelectionsAll);
                        if(array_count_values(array_column($windows_affected_by_options_actions, 'window_id'))[$wAbOa['window_id']] == 1 && $simulatorSelectionsEach[0] == $wAbOa['option_id']){
                            $bid_simulation_base[] = array('option_name1' => '','window_details1' => $wAbOa['window_details'],'window_location1' => $wAbOa['window_location'], 'order1' => $wAbOa['order']);
                        };
                    };
                };
            };
            */
           //windowIsAddedRemoved 
/* comment out for its protection
            foreach($selected_bid_options as $simulatorSelectionsAll){ // get ALL options selected in 'Simulator', which includes ID and Name
                $simulatorSelectionsEach = explode(",", $simulatorSelectionsAll); // break selected 'Simulator' choices out into individual options ids and Names
                foreach($options_affecting_windows as $oAw){
                    if($oAw['option_id'] == $simulatorSelectionsEach[0]){
                        $windows_affected_by_selected_options[] = array('option_id' => $simulatorSelectionsEach[0], 'window_id' => $oAw['tbl_bid_windows_bid_windows_id'], 'add_remove_action' => $oAw['add_remove_action']);
                    };
                };
            };
*/ // end comment out for its protection
/*
            foreach($windows_affected_by_options_actions as $wAbOa){
                if($wAbOa['add_remove_action'] == 1){
                    $added_windows_bucket[] = array('option_id' => $wAbOa['option_id'], 'option_name' => $wAbOa['option_name'], 'window_id' => $wAbOa['window_id'], 'window_details' => $wAbOa['window_details'], 'window_location' => $wAbOa['window_location'], 'order' => $wAbOa['order'], 'add_remove_action' => $wAbOa['add_remove_action']);
                }elseif($wAbOa['add_remove_action'] == 0){
                    $deleted_windows_bucket[] = array('option_id' => $wAbOa['option_id'], 'option_name' => $wAbOa['option_name'], 'window_id' => $wAbOa['window_id'], 'window_details' => $wAbOa['window_details'], 'window_location' => $wAbOa['window_location'], 'order' => $wAbOa['order'], 'add_remove_action' => $wAbOa['add_remove_action']);                    
                };
            };

            foreach($added_windows_bucket as $aWb){
                if(array_count_values(array_column($added_windows_bucket, 'window_id'))[$aWb['window_id']] > 1){
                            

                }else{
                    foreach($selected_bid_options as $simulatorSelectionsAll){ // get ALL options selected in 'Simulator', which includes ID and Name
                    $simulatorSelectionsEach = explode(",", $simulatorSelectionsAll);
                        if(array_count_values(array_column($added_windows_bucket, 'window_id'))[$aWb['window_id']] == 1 && $simulatorSelectionsEach[0] == $aWb['option_id']){
                            $bid_simulation_base[] = array('option_name1' => '','window_details1' => $aWb['window_details'],'window_location1' => $aWb['window_location'], 'order1' => $aWb['order']);
                        };
                    };
                };
            };
*/
                /*
                foreach($selected_bid_options as $simulatorSelectionsAll){ // get ALL options selected in 'Simulator', which includes ID and Name
                    $simulatorSelectionsEach = explode(",", $simulatorSelectionsAll);
                    if($simulatorSelectionsEach[0] == $aWb['option_id']) && $aWb['option_name'] != 'LHAND' && $aWb['option_name'] != 'RHAND' && $aWb['add_remove_action'] == 1){
                        $bid_simulation_window_added[] = array('option_name1' => $aWb['option_name'], 'window_details1' => $aWb['window_details'], 'window_location1' => $aWb['window_location'], 'order1' => $aWb['order']);
                    };
                };
                */
            
/*
            foreach($windows_affected_by_options_actions as $wAbOa){
                foreach($selected_bid_options as $simulatorSelectionsAll){ // get ALL options selected in 'Simulator', which includes ID and Name
                    $simulatorSelectionsEach = explode(",", $simulatorSelectionsAll);
                    if($wAbOa['option_id'] == $simulatorSelectionsEach[0] && $wAbOa['option_name'] != 'LHAND' && $wAbOa['option_name'] != 'RHAND' && $wAbOa['add_remove_action'] == 1){
                        $bid_simulation_window_added[] = array('option_name1' => $wAbOa['option_name'], 'window_details1' => $wAbOa['window_details'], 'window_location1' => $wAbOa['window_location'], 'order1' => $wAbOa['order']);
                    };
                };
            };
*/

/*
            foreach($windows_affected_by_options as $wAbO){
                if(in_array($wAbO['window_id'], array_column($)))
                    if(verifyCrossRef($window_affected_by_options))
                        if($simulatorSelectionsEach[0] == $wAbO['option_id']){
                            $windows_affected_by_selected_options[] = array('option_id' => $simulatorSelectionsEach[0], 'window_id' => $wAbO['window_id']);         
                        };
                };// end of foreach($selected_bid_options as $simulatorSelectionsAll)
            };
/*
            foreach($windows_affected_by_selected_options as $wAbSo){
                if(($wAbSo['option_name'] == 'LHAND' && $wAbSo['add_remove_action'] == 1 && $wAbSo['option_count'] == 1) || ($wAbSo['option_name'] == 'RHAND' && $wAbSo['add_remove_action'] == 1 && $wAbSo['option_count'] == 1)){
                    // add to $bid_simulation_base with blank option name
                    $bid_simulation_base[] = array('option_name1' => '','window_details1' => $wAbSo['window_details'],'window_location1' => $wAbSo['window_location'], 'order1' => $wAbSo['order']);
                }elseif($wAbSo['add_remove_action'] == 1 && $wAbSo['option_count'] > 1 && ($wAbSo['option_name'] != 'LHAND' && $wAbSo['option_name'] != 'RHAND')){
                    $bid_simulation_window_added[] = array('option_name1' => $wAbSo['option_name'], 'window_details1' => $wAbSo['window_details'], 'window_location1' => $wAbSo['window_location'], 'order1' => $wAbSo['order']);
                }elseif($wAbSo['add_remove_action'] == 0 && $wAbSo['option_count'] > 1 && ($wAbSo['option_name'] != 'LHAND' && $wAbSo['option_name'] != 'RHAND')){
                    $bid_simulation_window_removed[] = array('option_name1' => $wAbSo['option_name'], 'window_details1' => $wAbSo['window_details'],'window_location1' => $wAbSo['window_location'], 'order1' => $wAbSo['order']);
                };
            }
*/
/*
                if(!in_array($eachWindow['bid_windows_id'], array_column($window_affected_by_options, 'window_id'))){
                    $bid_simulation_base[] = array('option_name1' => '','window_details1' => $eachWindow['window_details'],'window_location1' => $eachWindow['window_location']);                    
                };
            }; // end of foreach($get_bid_windows as $eachWindow)


            //echo json_encode($window_affected_by_options);


            // for each window affected by options, if option name is lhand or rhand and count of in array is 1 then base window
            foreach($get_bid_windows as $eachWindow){
                if(!in_array($eachWindow['bid_windows_id'], array_column($window_affected_by_options, 'window_id'))){
                        $bid_simulation_base[] = array('option_name1' => '','window_details1' => $eachWindow['window_details'],'window_location1' => $eachWindow['window_location']);                    
                };
                foreach($window_affected_by_options as $wAbO){
                    if($wAbO['option_name'] == 'LHAND' || $wAbO['option_name'] == 'RHAND'){
                        //$bid_simulation_base[] = array('option_name1' => '','window_details1' => $wAbO['window_details'],'window_location1' => $wAbO['window_location']);
                    };
                };
            };
            */
            //echo json_encode($bid_simulation_base);
/*
                if(count($window_affected_by_options) == 0){ // window is in NONE of the options selected means it is in every option so it is a BASE window
                     // add to $bid_simulation_base with blank option name
                    $bid_simulation_base[] = array('option_name1' => '','window_details1' => $eachWindow['window_details'],'window_location1' => $eachWindow['window_location']);
                };

                if(in_array('LHAND', array_column($wAbO, 'option_name')) || in_array('RHAND', array_column($wAbO, 'option_name'))){
                    // add to $bid_simulation_base with blank option name
                    //$bid_simulation_base[] = array('option_name1' => '','window_details1' => $eachWindow['window_details'],'window_location1' => $eachWindow['window_location']);
                }elseif($window_affected_by_options[0]['add_remove_action'] == 1){
                    $bid_simulation_window_added[] = array('option_name1' => $window_affected_by_options[0]['option_name'], 'window_details1' => $eachWindow['window_details'],'window_location1' => $eachWindow['window_location']);
                }elseif($window_affected_by_options[0]['add_remove_action'] == 0){
                    $bid_simulation_window_removed[] = array('option_name1' => $window_affected_by_options[0]['option_name'], 'window_details1' => $eachWindow['window_details'],'window_location1' => $eachWindow['window_location']);
                };
            };




        //echo json_encode($windows_affected_by_options);
        //echo json_encode($windows_affected_by_selected_options);
        //echo json_encode($added_windows_bucket);
        //echo json_encode($deleted_windows_bucket);

*/
//ITERATIONS GALORE! - END




            #testGridContainer {
  top: 0px;
  width: 95%; /**/
  height: 600px; /**/
  overflow: auto;
}

#testGridContainer div table > thead {
  /* display: block; */
  max-height: 310px; /**/
  width: 95%; /**/
  user-select: none;
}

#testGridContainer div table > thead > tr {
}

.testGridHeadings th.rotate {
  /* Something you can count on*/
  height: 310px; /**/
  white-space: nowrap; /**/
}

.testGridHeadings th.rotate > div {
  transform: /**/
    /* Magic Numbers left side distance, bottom to top distance, higher brings it down*/
    translate(0px, 140px) /**/
    /* 45 is really 360 - 60 was 300deg*/
    rotate(300deg); /**/
  width: 22px; /**/
}



/*
20181028 Video Notes:

<html>
<head>
    <title><?php echo "PHP Object Oriented Programming";?></title>
</head>
<body>
<?php
/*
    Object Oriented Programming allows you to model real
    world objects. Every object has attributes and things 
    it can do (Operations / Functions / Methods) You define those 
    things in a class which is a blueprint for creating
    objects.
*/
class Animal implements Singable{

    /* 
        You define attributes like this. The private means
        that only methods in the class can access this data
        public would mean that any code could directly access
        and change the values for these attributes. 
        
        Attributes that are private won't be inherited as you'll see
        but those that are public or protected are
        
        By making sure our data can only be changed by the class 
        operations we are inencapsulating or protecting it.
    */
    
    protected $name;
    protected $favorite_food;
    protected $sound;
    protected $id;
    
    // A static attribute is shared by every object. If its
    // value changes for one it changes for all
    
    public static $number_of_animals = 0;
    
    // A constant is also shared
    
    const PI = "3.14159";
    
    // You define methods just like you define functions in a class
    
    function getName(){
    
        // To refer to data stored in an object you proceed the name
        // of the attribute with $this->yourAttribute
    
        return $this->name;
    
    }
    
    // A Construtor is used to initialize objects when they are 
    // created or instantiated
    
    function __construct(){
    
        // Generate a random id between 1 and 1000000
        
        $this->id = rand(100, 1000000);
        
        echo $this->id . " has been assigned<br />";
        
        // You access static attributes with Class::$static_att
        
        Animal::$number_of_animals++;
    
    }
    
    // A Destructor is called when all references to the object have 
    // been unset. It cannot receive attributes
    
    public function __destruct(){
    
        echo $this->name . " is being destroyed :(";
    
    }
    
    // You can also use magic setters and getters which are called
    // when an attribute is set, or if its value is asked for
    
    function __get($name){
        
        echo "Asked for " . $name . "<br />";
    
        return $this->$name;
        
    }
    
    
    // If you want to check for a valid attribute you could use switch
    
    function __set($name, $value){
    
        switch($name){
        
            case "name":
                $this->name = $value;
                break;
                
            case "favorite_food":
                $this->favorite_food = $value;
                break;
                
            case "sound":
                $this->sound = $value;
                break;
                
            default : 
                echo $name . "Not Found";
        
        }
        
        echo "Set " . $name . " to " . $value . "<br />";
    
    }
    
    // 2. We will override this method in the subclass
    function run(){
        
        
        echo $this->name . " runs<br />";
        
    }
    
    // 3. To keep a method from being overridden use final
    // You can use final on a class to keep classes from
    // being overridden as well
    
    final function what_is_good(){
        
        echo "Running is Good<br />";
        
    }
    
    // 4. You can use __toString to define what prints when
    // the object is called to print
    
    function __toString(){
        
        return $this->name . " says " . $this->sound .
        " give me some ". $this->favorite_food . " my id is " .
        $this->id . " total animals = " . Animal::$number_of_animals .
        "<br /><br />";
        
    }
    
    // 5. You must define any function defined in an interface
    
    public function sing(){
        
        echo $this->name . " sings 'Grrrr grr grrr grrrrrrrrr'<br />";
        
    }
    
    // 7. static methods can be called without the need for instantiation
    
    static function add_these($num1, $num2){
        
        return ($num1 + $num2) . "<br />";
        
    }
    
}

// Inheritance occurs when you create a new class by extending another
// You will inherit all of the Attributes and Methods defined in the first
// You don't have to do anything in the class and it will still work

class Dog extends Animal implements Singable{
    
    // 2. You can override functions defined in the superclass
    function run(){
        
        
        echo $this->name . " runs like crazy<br />";
        
    }
    
    // 5. You must define any function defined in an interface
    
    public function sing(){
        
        echo $this->name . " sings 'Bow wow, woooow, woooooooooow'<br />";
        
    }
    
    
}

// 5. PHP doesn't allow muliple inheritance
// You need to use interfaces to get similar results
// Interfaces allow you to define functions that must be implemented

interface Singable{
    
    public function sing();
    
}


$animal_one = new Animal();

// These call __set

$animal_one->name = "Spot";
$animal_one->favorite_food = "Meat";
$animal_one->sound = "Ruff";

// The statements $animal_one->att_name call __get
// We call static attributes like this Class::$static_var 

echo $animal_one->name . " says " . $animal_one->sound .
    " give me some ". $animal_one->favorite_food . " my id is " .
    $animal_one->id . " total animals = " . Animal::$number_of_animals .
    "<br /><br />";
    
// If we defined a constant in the class we would get its
// value like this Class::CONTANT 
    
echo "Favorite Number " . Animal::PI . "<br />";
    
$animal_two = new Dog();

$animal_two->name = "Grover";
$animal_two->favorite_food = "Mushrooms";
$animal_two->sound = "Grrrrrrr";

// Even though we are referring to the Dog $number_of_animals it
// still increases even with subclasses

echo $animal_two->name . " says " . $animal_two->sound .
    " give me some ". $animal_two->favorite_food . " my id is " .
    $animal_two->id . " total animals = " . Dog::$number_of_animals .
    "<br /><br />";
    
// 2. Because of method overriding we get different results 
$animal_one->run();
$animal_two->run();

// 3. final methods can't be overriden
$animal_one->what_is_good();

// 4. Example using __toString()

echo $animal_two;

// 5. You call a method defined in an interface like all others

$animal_two->sing();

// 6. You can also define functions that will except classes
// extending a secific class or interface

function make_them_sing(Singable $singing_animal){
    
    $singing_animal->sing();
    
}

// 6. Polymorphism states that different classes can have different
// behaviors for the same function. The compiler is smart enough to
// just figure out which function to execute

make_them_sing($animal_one);
make_them_sing($animal_two);

echo "<br />";

function sing_animal(Animal $singing_animal){
    
    $singing_animal->sing();
    
}

sing_animal($animal_one);
sing_animal($animal_two);

// 7. Calling a static method

echo "3+5= " . Animal::add_these(3,5);

// 8. You can check the class type with instanceof

$is_it_an_animal = ($animal_two instanceof Animal) ? "True" : "False";

echo "It is " . $is_it_an_animal . ' that $animal_one is an Animal<br />';

// 9. You can clone objects and even define just what you want available 
// to clone in __clone() if it is in the class definition

$animal_clone = clone $animal_one;

// 10. You can define abstract classes and methods
// An abstract class can't be instantiated, but instead forces classes
// that implement it to override every abstract method in it

/*
    abstract class RandomClass{
        
        abstract function RandomFunction($attrib1);
        
    }
 */

 // 11. __call() if defined in a class can provide method overloading
 // but since PHP isn't strongly typed I see no reason to use it


?>
</body>
</html>

*/





?>