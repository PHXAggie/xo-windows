<?php

class MY_Exceptions extends CI_Exceptions
{
    function show_error($heading, $message, $template = 'error_general', $status_code = 500)
    {
        log_message( 'debug', 'From MY_exceptions: ' . print_r( $message, TRUE ) );
        log_message( 'debug', 'From MY_exceptions_2: ' . print_r( $message[0], TRUE ) );
        if($message[0] == "Error Number: 1451"){ // trying to remove parent when children exist
        	$status_code = 1451;
        }
        if($message[0] == "Error Number: 1062"){ // Duplicate Entry Error
        	$status_code = 1062;
        }

        throw new Exception(is_array($message) ? $message[1] : $message, $status_code );
    }
}

?>