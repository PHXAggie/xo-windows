<?php
//header('Access-Control-Allow-Origin: *');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maintenance extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        } else {
            $this->load->model('maintenancemodel');
        }
    }
    
    public function index(){
        //currently, subpages are used to go direct, there is no maintenance landing page
    }


/////// BUILDERS related - START ////////////////////////////////////////////////////////////////////////////////
    // builders view; btns(Add / Edit)
        // Add - sends no data -> buildersAdd view; on_submit -> check_builder_add_form -> insert_new_builder
        // Edit - sends builder_id, builder_name, status -> buildersedit view; on_submit -> builder_save -> builder_edit_save (single try catch)
    // builders() -> buildersAdd() -> check_builder_add_form() -> insert_new_builder()
    // builders() -> buildersedit() -> builder_save() -> insert_new_builder()
    // builders() -> buildersedit() -> builder_save() -> builder_edit_save()
    // subdivisions view; btns(Add / Edit)
        // Add - sends no data -> subdivisionAddEdit view; on_submit -> check_subdivision_add_form
        // Edit - sends subdivision_id -> subdivisionAddEdit view; on_submit -> check_subdivision_add_form
        // -> subdivisionsAddEdit() -> check_subdivision_add_form() -> check_if_duplicate_subdivision() -> update_subdivision OR insert_new_subdivision

        public function builders(){  // like an INDEX, landing from Maintenance > Builders
        	// $data['tbl_buildercustomer'] = $this->maintenancemodel->getBuildersList();
            //$data = array();
            $data['tbl_buildercustomer'] = $this->maintenancemodel->GetBuildersListNEW();
            $data['js_to_load'] = 'search.js';
            $data['searchBy'] = 'builderid';
            //$this->smarty->view('site_header.tpl');
        	$this->load->view('main');
    		$this->load->view('maintenance/builders', $data);
        	$this->load->view('footer');

            $this->output->enable_profiler(true);
        }

        public function getBSPEselect(){
            $data['tbl_buildercustomer'] = $this->maintenancemodel->GetBuildersListNEW();
            $this->load->view('maintenance/builders', $data);
            //$this->output->enable_profiler(true);
        }

        public function buildersAdd(){
            $statusData['tbl_status'] = $this->maintenancemodel->getStatusList();
            $statusData['title'] = 'Add Builder';
            $this->load->view('main');
            $this->load->view('maintenance/buildersAdd', $statusData);
            $this->load->view('footer');
            
            //$this->output->enable_profiler(true);
        }

        public function check_builder_add_form(){

            if(!$this->input->is_ajax_request()){
                exit ('no valid request found under check_builder_add_form');
            }

            $this->form_validation->set_rules('builderNameInput', 'Builder Name', 'required');
            $this->form_validation->set_rules('initialStatus', 'Status', 'required');

            if($this->form_validation->run() == TRUE){
                    echo '<div class="row justify-content-center">';
                    echo '<div id="InfoBanner" class="col-5 alert alert-info">Processing Request:</div>';
                    echo '</div>';

        try
        {
                $builderNameCheck = $this->maintenancemodel->insert_new_builder($this->session->userdata('xo_id'));
                //echo '<div id="InfoBanner" class="alert alert-sucess">New Builder added; returning to Builders.</div>';
        }
        catch ( Exception $e )
        {
            if($e->getCode() == 1062){
                $errorData['title'] = 'Entry already exists';
                $errorData['details'] = 'This Builder is already in the database.';
                $errorData['solution'] = 'Please Cancel OR adjust your entry and try again.';
                $this->load->view('maintenance/error_modal', $errorData);
                //echo '<div class="alert alert-danger">Cancel or adjust entry</div>';
            }else{
                $errorData['title'] = 'Something went wrong';
                $errorData['details'] = 'Not sure what it is';
                $errorData['solution'] = 'I guess you should Cancel for now... :(';
                $this->load->view('maintenance/error_modal', $errorData);
            }
        }


//                $builderNameCheck = $this->maintenancemodel->check_if_duplicate_builder();
//                //log_message('debug','Duplicate builder? ' . print_r($builderNameCheck,TRUE));
//                if($builderNameCheck != FALSE){
//                    echo '<div class="alert alert-danger">Builder already exists! Canceling Entry.</div>';
//                } else {

//                    $this->maintenancemodel->insert_new_builder($this->session->userdata('xo_id'));

//                }
            } else {
                echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
            }
        }

        public function buildersEdit($builder_id, $builder_name, $tbl_status_tbl_status_id){
            $editBuilderData['builder_id'] = $builder_id;

            //$builder_name = str_replace('operen', '(', $builder_name);
            //$builder_name = str_replace('cperen', ')', $builder_name);
            //$builder_name = str_replace('slash', '/', $builder_name);
            $editBuilderData['builder_name'] = $builder_name;
            $editBuilderData['title'] = $editBuilderData['builder_name'];

            $editBuilderData['tbl_status_tbl_status_id'] = $tbl_status_tbl_status_id;
            $editBuilderData['builders_log'] = $this->maintenancemodel->GetBuildersLog($builder_id);
            $editBuilderData['tbl_install_types'] = $this->maintenancemodel->get_install_types($builder_id);
            $editBuilderData['tbl_status'] = $this->maintenancemodel->getStatusList();
            //$editBuilderData['tbl_buildercustomers'] = $this->maintenancemodel->getBuildersList();
            $this->load->view('main');
            //$this->load->view('maintenance/buildersEdit', $editBuilderData);
            $this->load->view('maintenance/buildersEdit', $editBuilderData);
            //$this->load->view('maintenance/installs', $installTypesData);
            $this->load->view('footer');
        }
/*
        public function buildersInstallTypes(){
            $installTypesData['tbl_install_types'] = $this->maintenancemodel->get_install_types();
            $this->load->view('main');
            $this->load->view('maintenance/installs', $installTypesData);
            $this->load->view('footer');
        }
*/
        //public function builder_types(){
        public function builder_save(){

            if(!$this->input->is_ajax_request()){
                exit ('no valid request found under builder_save');
            }

            $FormRules = array(
                array(
                    'field' => 'builderNameInput',
                    'label' => 'Builder Name',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'initialStatus',
                    'label' => 'Status',
                    'rules' => 'required'
                )
            );

            $this->form_validation->set_rules($FormRules);
            //log_message('debug','RULES Set ');
            if($this->form_validation->run() == TRUE){
            //log_message('debug','Validation Run ');
            //$builderNameCheck = $this->maintenancemodel->check_if_duplicate_builder();


                //log_message('debug','Duplicate builder? ' . print_r($builderNameCheck,TRUE));
                /*
                if($builderNameCheck != FALSE){
                    echo '<div class="alert alert-danger">Builder name already exists! Canceling Edit!</div>';
                } else {
                    echo '<div class="alert alert-success">Updating Builder: ';
                    echo ' Returning to Builders</div>';
                }
                */
        //$this->maintenancemodel->builder_edit_save($this->session->userdata('xo_id'));
        try
        {
                    $this->maintenancemodel->builder_edit_save($this->session->userdata('xo_id'));            
        }
        catch( Exception $e )
        {
            //log_message( 'debug', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() . '>>>>> ' . $e->getCode() . ' <<<<<');
            if($e->getCode() == 1451){ //have doubts about this 500 as it was 1451 in the log (500 is web error usually)
                //echo '<div class="alert alert-danger">Install Type CANNOT be REMOVED as it is in use by some subdivisions</div>';
                $errorData['title'] = 'Unable to Remove';
                $errorData['details'] = 'An Install Type you tried to remove is in use.';
                $errorData['solution'] = 'Please Cancel OR adjust your entry and try again.';
                $this->load->view('maintenance/error_modal', $errorData);
            }
        }
                    

            } else {
                echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
            }
        }

/////// BUILDERS related - END //////////////////////////////////////////////////////////////////////////////////

/////// SUBDIVISIONS related - START ////////////////////////////////////////////////////////////////////////////
    public function subdivisions(){
        //$subdivisionsData['tbl_subdivision'] = $this->maintenancemodel->getSubdivisionsList();
        $subdivisionsData['tbl_subdivision'] = $this->maintenancemodel->GetSubdivisionsListNEW(); 
        //log_message('debug','Duplicate builder? ' . print_r($subdivisionsData,TRUE));   
        $subdivisionsData['js_to_load'] = 'search.js';
        $subdivisionsData['searchBy'] = 'subdivisionid';
        $this->load->view('main');
        $this->load->view('maintenance/subdivisions', $subdivisionsData);
        $this->load->view('footer');
    }
/*
    public function subdivisionsAdd(){
        $data['tbl_buildercustomers'] = $this->maintenancemodel->getBuildersList();
        $branch['tbl_xo_branch'] = $this->maintenancemodel->getBranchList();
        $zone['tbl_zones'] = $this->maintenancemodel->getZonesList();
        $this->load->view('main');
        $this->load->view('maintenance/subdivisionsAddBuilder', $data);
        $this->load->view('maintenance/subdivisionsAddBranch', $branch);
        $this->load->view('maintenance/subdivisionsAdd');
        $this->load->view('maintenance/subdivisionsAddZones', $zone);
        $this->load->view('footer');
    } NOT NEEDED, SEE NEXT FUNCTION FOR BETTER SOLUTION


    public function subdivisionsAdd(){
        $data['title'] = 'Add Subdivision';
        $data['tbl_buildercustomers'] = $this->maintenancemodel->GetBuildersListNEW();
        $data['tbl_xo_branch'] = $this->maintenancemodel->getBranchList();
        $data['tbl_zones'] = $this->maintenancemodel->getZonesList();
        $data['tbl_status'] = $this->maintenancemodel->getStatusList();
        $this->load->view('main');
        $this->load->view('maintenance/subdivisionsAdd', $data);
        $this->load->view('footer');
    }
*/
    public function subdivisionsAddEdit($subdivision_id = NULL){
        if(!$subdivision_id){
            $data['title'] = 'Add Subdivision';
            $data['tbl_buildercustomers'] = $this->maintenancemodel->GetBuildersListNEW();
            $data['subdivision_data'] = NULL;
            // once builder selected, install types are loaded via ajax
        }else{
            $data['subdivision_data'] = $this->maintenancemodel->get_one_subdivision($subdivision_id);
            $data['tbl_buildercustomers'] = $this->maintenancemodel->GetBuildersListNEW();
            $data['builders_log'] = $this->maintenancemodel->GetBuildersLogSubdivision($subdivision_id);
            $data['title'] = 'Edit Subdivision: ' . $data['subdivision_data'][0]['subdivision_name'];
            $data['builder_data'] = $this->maintenancemodel->GetOneBuilder($data['subdivision_data'][0]['tbl_buildercustomer_builder_id']);
            $data['builders_it'] = $this->maintenancemodel->builders_install_types($data['subdivision_data'][0]['tbl_buildercustomer_builder_id']);
        }
        $data['tbl_xo_branch'] = $this->maintenancemodel->getBranchList();
        $data['tbl_zones'] = $this->maintenancemodel->getZonesList();
        $data['tbl_status'] = $this->maintenancemodel->getStatusList();
        $data['tbl_xo_users'] = $this->maintenancemodel->getUsersList();
        $data['tbl_manufacturer'] = $this->maintenancemodel->getManufacturersList();
        
        $this->load->view('main');
        $this->load->view('maintenance/subdivisionsAddEdit', $data);
        $this->load->view('footer');
    }

    public function get_builders_install_types(){
        $builder_id = $this->input->post('builder_id');
        if($builder_id){
            $install_types = $this->maintenancemodel->builders_install_types($builder_id);
        };
        echo json_encode($install_types);
    }

    public function check_subdivision_add_form(){

        if(!$this->input->is_ajax_request()){
            exit ('no valid request found');
        }

        $FormRules = array(
            array(
                'field' => 'builderIdSelected',
                'label' => 'Builder',
                'rules' => 'required'
            ),
            array(
                'field' => 'subdivisionNameInput',
                'label' => 'Subdivision Name',
                'rules' => 'required'
            ),
            array(
                'field' => 'statusIdSelected',
                'label' => 'Status',
                'rules' => 'required'
            ),
            // default Install Type 'installTypeIdSelected'
            // default window mfg 'windowMfgIdSelected'
            array(
                'field' => 'subdivisionCityInput',
                'label' => 'Subdivision City',
                'rules' => 'required'
            ),
            array(
                'field' => 'subdivisionStateInput',
                'label' => 'Subdivision State',
                'rules' => 'required|exact_length[2]|in_list[AZ,NV,az,nv,Az,Nv,aZ,nV]'
            ),
            array(
                'field' => 'subdivisionZipInput',
                'label' => 'Subdivision Zip',
                'rules' => 'required|numeric|exact_length[5]'
            ),
            array(
                'field' => 'branchIdSelected',
                'label' => 'Branch',
                'rules' => 'required'
            ),
            array(
                'field' => 'zoneIdSelected',
                'label' => 'Subdivision Zone',
                'rules' => 'required'
            ),
            array(
                'field' => 'startsRepIdSelected',
                'label' => 'Starts Rep',
                'rules' => 'required'
            ),
            array(
                'field' => 'fieldSuperIdSelected',
                'label' => 'XO Field Rep',
                'rules' => 'required'
            ),
            //,super id
            array(
                'field' => 'superNameInput',
                'label' => 'Supers Name',
                'rules' => 'required'
            ),
            array(
                'field' => 'superPhoneInput',
                'label' => 'Supers Phone Number',
                'rules' => 'required|numeric|exact_length[10]'
            )
        );

        $this->form_validation->set_rules($FormRules);

        //log_message('debug','All data: ' . print_r($this->input->post(),TRUE));

        if($this->form_validation->run() == TRUE){
            //if($this->input->post('subdivisionRecordID')){
            //log_message('debug', 'Sub_record_id: ' . print_r($this->input->post('subdivisionRecordID'),TRUE));
            //}

        // try to insert then the 'catch' is doing an update instead?
            if(!$this->input->post('subdivisionRecordID')){
                try
                {
                    $this->maintenancemodel->insert_new_subdivision($this->session->userdata('xo_id'));
                    echo '<div class="alert alert-success">Adding Subdivision</div>';
                }
                catch ( Exception $e )
                {
                    if($e->getCode() == 1062){ // db error for record already exists, so update record
                        $errorData['title'] = 'Something went terribly wrong!';
                        $errorData['details'] = 'You should not have been able to get this far';
                        $errorData['solution'] = 'Please Cancel... :(';
                        $this->load->view('maintenance/error_modal', $errorData);
                        //try
                            //$this->maintenancemodel->update_subdivision($this->session->userdata('xo_id'));
                        //catch here too?
                    }else{
                        $errorData['title'] = 'Something went wrong';
                        $errorData['details'] = 'Not sure what it is';
                        $errorData['solution'] = 'I guess you should Cancel for now... :(';
                        $this->load->view('maintenance/error_modal', $errorData);
                    }
                }
            }
            if($this->input->post('subdivisionRecordID') > 0){
                //log_message('debug', 'In the If, Sub_record_id: ' . print_r($this->input->post('subdivisionRecordID'),TRUE));
                try
                {
                    $this->maintenancemodel->update_subdivision($this->session->userdata('xo_id'));
                    echo '<div class="alert alert-success">Updating Subdivision</div>';
                }
                catch ( Exception $e )
                {
                    if($e->getCode() == 1062){ // db error for record already exists, so update record

                        $errorData['title'] = 'Record Exists?! I know, thats why Im trying to update it';
                        $errorData['details'] = 'Not sure what it is';
                        $errorData['solution'] = 'I guess you should Cancel for now... :(';
                        $this->load->view('maintenance/error_modal', $errorData);                            

                    }else{
                        $errorData['title'] = 'Something went wrong';
                        $errorData['details'] = 'Not sure what it is';
                        $errorData['solution'] = 'I guess you should Cancel for now... :(';
                        $this->load->view('maintenance/error_modal', $errorData);
                    }
                }
            }
        /*
            $subdivisionNameCheck = $this->maintenancemodel->check_if_duplicate_subdivision();

            if($subdivisionNameCheck != FALSE){
                echo '<div class="alert alert-success">Updating Subdivision: ';
                $this->maintenancemodel->update_subdivision($this->session->userdata('xo_id'));
                echo 'Updated, returning to Subdivisions</div>';
            } else {
                echo '<div class="alert alert-success">Adding new Subdivision: ';
                $this->maintenancemodel->insert_new_subdivision();
                echo 'Added, returning to Subdivisions</div>';
            }
        */
        } else {
            echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
        };
    }

    public function subdivisionsEdit($subdivision_id){
        $editSubdivisionData['title'] = 'Edit Subdivision: ';
        $editSubdivisionData['subdivision_data'] = $this->maintenancemodel->get_one_subdivision($subdivision_id);
        //$editSubdivisionData['tbl_status_tbl_status_id'] = $tbl_status_tbl_status_id;
        $editSubdivisionData['tbl_status'] = $this->maintenancemodel->getStatusList();
        $this->load->view('main');
        $this->load->view('maintenance/subdivisionsEdit', $editSubdivisionData);
        $this->load->view('footer');
    }
/////// SUBDIVISIONS related - END /////////////////////////////////////////////////////////////////////////////

/////// INSTALL TYPES related - START //////////////////////////////////////////////////////////////////////////
        public function installTypes(){  // like an INDEX, landing from Maintenance > Builders
            $data['tbl_install_types'] = $this->maintenancemodel->getInstallTypesList();
            $this->load->view('main');
            $this->load->view('maintenance/installtypesadd');
            $this->load->view('maintenance/installtypes', $data);
            $this->load->view('footer');
        }

        public function installtypesAdd(){
            $this->load->view('main');
            $this->load->view('maintenance/installtypesadd');
            $this->load->view('footer');
        }

        public function check_install_type_add_form(){

            if(!$this->input->is_ajax_request()){
                exit ('no valid request found under check_install_type_add_form');
            }

            $FormRules = array(
                array(
                    'field' => 'installtypeNameInput',
                    'label' => 'Install Type Name',
                    'rules' => 'required'
                )
            );

            $this->form_validation->set_rules($FormRules);

            if($this->form_validation->run() == TRUE){
                $installtypeNameCheck = $this->maintenancemodel->check_if_duplicate_install_type();

                if($installtypeNameCheck != FALSE){
                    echo '<div class="alert alert-danger">Install Type already exists! Canceling Entry.</div>';
                } else {
                    echo '<div class="alert alert-success">Adding new Install Type: ';
                    $this->maintenancemodel->insert_new_install_type();
                    echo ' Added! Returning to Install Types.</div>';
                }
            } else {
                echo '<div class="alert alert-danger">' . validation_errors() . ' Canceling Entry.</div>';
            }
        }

        public function installtypeEdit($install_types_id, $type){
            $editInstallTypeData['install_types_id'] = $install_types_id;
            $editInstallTypeData['type'] = $type;
            $editInstallTypeData['type'] = str_replace('operen', '(', $editInstallTypeData['type']);
            $editInstallTypeData['type'] = str_replace('cperen', ')', $editInstallTypeData['type']);
            $editInstallTypeData['type'] = str_replace('slash', '/', $editInstallTypeData['type']);
            $editInstallTypeData['type'] = str_replace('qt', "''", $editInstallTypeData['type']);
            $this->load->view('main');
            //$this->load->view('maintenance/buildersEdit', $editBuilderData);
            $this->load->view('maintenance/installtypesEdit', $editInstallTypeData);
            //$this->load->view('maintenance/installs', $installTypesData);
            $this->load->view('footer');
        }

        public function install_type_save(){

            if(!$this->input->is_ajax_request()){
                exit ('no valid request found under install_type_save');
            }

            $FormRules = array(
                array(
                    'field' => 'installtypeNameInput',
                    'label' => 'Install Type Name',
                    'rules' => 'required'
                )
            );

            $this->form_validation->set_rules($FormRules);

            if($this->form_validation->run() == TRUE){
                $installtypeNameCheck = $this->maintenancemodel->check_if_duplicate_install_type();

                if($installtypeNameCheck != FALSE){
                    echo '<div class="alert alert-danger">Install Type name already exists! Canceling Edit!</div>';
                } else {
                    echo '<div class="alert alert-success">Updating Install Type: ';
                    $this->maintenancemodel->install_type_edit_save();
                    echo ' Updated! Returning to Install Types</div>';
                }
            } else {
                echo '<div class="alert alert-danger">' . validation_errors() . ' Canceling Edit!</div>';
            }
        }
/////// INSTALL TYPES related - END ////////////////////////////////////////////////////////////////////////////

/////// JOBSITE SUPERS related - START ////////////////////////////////////////////////////////////////////////////

    public function supers(){
        $supersData['tbl_supers'] = $this->maintenancemodel->getSupersList();    
        $supersData['js_to_load'] = 'search.js';
        $supersData['searchBy'] = 'jobsitesupersid';
        $this->load->view('main');
        $this->load->view('maintenance/supers', $supersData);
        $this->load->view('footer');
    }

    public function supersAddEdit($jobsite_id = NULL){
        if(!$jobsite_id){
            $data['title'] = 'Add Jobsite Super';
            $data['tbl_subdivision'] = $this->maintenancemodel->GetSubdivisionsListNEW();
            $data['jobsite_data'] = NULL;
        }else{
            $data['jobsite_data'] = $this->maintenancemodel->get_one_jobsite($jobsite_id);
            //log_message('debug', 'Jobsite_data ' . print_r($data['jobsite_data'],TRUE));
            $data['title'] = 'Edit Jobsite Super: ' . $data['jobsite_data'][0]['subdivision_name'];
        }
        $this->load->view('main');
        $this->load->view('maintenance/supersAddEdit', $data);
        $this->load->view('footer');
    }



/////// JOBSITE SUPERS related - END ////////////////////////////////////////////////////////////////////////////


    public function generate_pdf() {
        //load pdf library
        $this->load->library('Pdf');

        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('https://www.roytuts.com');
    $pdf->SetTitle('Sales Information for Products');
    $pdf->SetSubject('Report generated using Codeigniter and TCPDF');
    $pdf->SetKeywords('TCPDF, PDF, MySQL, Codeigniter');

    // set default header data
    //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set font
    $pdf->SetFont('times', 'BI', 12);
    
    // ---------------------------------------------------------
    
    
    //Generate HTML table data from MySQL - start
    $template = array(
        'table_open' => '<table border="1" cellpadding="2" cellspacing="1">'
    );

    $this->table->set_template($template);

    $this->table->set_heading('Product Id', 'Price', 'Sale Price', 'Sales Count', 'Sale Date');
    
    $salesinfo = $this->product_model->get_salesinfo();
        
    foreach ($salesinfo as $sf):
        $this->table->add_row($sf->id, $sf->price, $sf->sale_price, $sf->sales_count, $sf->sale_date);
    endforeach;
    
    $html = $this->table->generate();
    //Generate HTML table data from MySQL - end
    
    // add a page
    $pdf->AddPage();
    
    // output the HTML content
    $pdf->writeHTML($html, true, false, true, false, '');
    
    // reset pointer to the last page
    $pdf->lastPage();

    //Close and output PDF document
    $pdf->Output(md5(time()).'.pdf', 'D');


    // from website
    // https://www.roytuts.com/generate-pdf-report-from-mysql-database-using-codeigniter/
        
    // put in view
    /*
        echo anchor('product/generate_pdf', 'Generate PDF Report');
        echo '</p>';

        html representation of what you want to print (like a table)
    */

    }



}

?>