<?php
//header('Access-Control-Allow-Origin: *');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Bids extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        } else {
            $this->load->model('bidsmodel');
            $this->load->model('maintenancemodel'); //added 20190721
        }
    }

    public function index(){
    	//$data['recentBidResults'] = $this->bidsmodel->getRecentBidsList();
        $data['recentBidResults'] = $this->bidsmodel->getRecentBidsListNEW();
        $data['js_to_load'] = 'search.js';
        $data['searchBy'] = 'subdivision_id';
    	$this->load->view('main');
		$this->load->view('bids/index', $data);
    	$this->load->view('footer');
    }

    public function bidsAddEdit($bid_id = NULL){
        //ADD vs EDIT determination / settings
            if(!$bid_id){
                $setForm = form_open(site_url('/bids/check_bid_form'), array('class' => 'jsform')); //REVIEW may not work for ADD/EDIT combo page
                $data['title'] = 'Add / Edit Bid';
                $bspeData['form_to_open'] = $setForm;
                $bspeData['title'] = $data['title']; //needs title for determining which button, Load vs Edit to display
                $bspeData['list_of_bldercustomers'] = $this->maintenancemodel->GetBuildersListNEW();
                $setEdit = array(
                    'menutag' => 'edit',
                    'menuLabel' => 'General',
                    'sectionContent' => $this->load->view('maintenance/BSPEselect', $bspeData, TRUE)
                    );
            }else{
                //$bidId = array('tbl_bid_id'=>$bid_id); // could send as an array of conditions with these two lines 1 of 2
                //$bidDetails['bid_details'] = $this->bidsmodel->get_bid_details($bidId); // 2 of 2
                $bidDetails['bid_details'] = $this->bidsmodel->get_bid_details($bid_id);
                $setForm = '';
                $data['title'] = 'Editing: ' . $bidDetails['bid_details'][0]['Builder'] . ' - ' . $bidDetails['bid_details'][0]['Subdivision'] . '<br />plan: ' . $bidDetails['bid_details'][0]['Plan'] . ' elevation: ' . $bidDetails['bid_details'][0]['Elevation']; // want this to show BSPE info for $bid_id
                $setEdit = array(
                    'menutag' => 'edit',
                    'menuLabel' => 'General',
                    'sectionContent' => 'sectionContent with $bid_id'//$this->load->view('maintenance/BSPEselect', $bspeData, TRUE)
                    );
            }
        //Head (title, permissions, forms)
            if($this->session->userdata('level') <= '4' ? $audit_view = '' : $audit_view = 'hidden'); //ILO if($this->session: if($bid_data[0]['bid_id'] ? ....
            if($this->session->userdata('level') <= '4' ? $auth_edit = '' : $auth_edit = 'readonly');
            if($this->session->userdata('level') <= '4' ? $auth_view = '' : $auth_view = 'hidden');
            //$data['form_to_open'] = form_open(site_url('/bids/check_bid_form'), array('class' => 'jsform')); //REVIEW may not work for ADD/EDIT combo page
        //GET (data for page)
            //$bspeData['form_to_open'] = form_open(site_url('/bids/check_bid_form'), array('class' => 'jsform')); //REVIEW may not work for ADD/EDIT combo page
            //$bspeData['form_to_open'] = $setForm;
            //$bspeData['title'] = $data['title']; //needs title for determining which button, Load vs Edit to display
            //$bspeData['list_of_bldercustomers'] = $this->maintenancemodel->GetBuildersListNEW();
            //$jobsiteData['get_install_types'] = $this->bidsmodel->get_install_types(146); //replaced by AJAX
            $jobsiteData['get_mfg'] = $this->bidsmodel->get_mfgs();
        //Present (edit, specs, options, stats, audit)
            /*$data['content'] = array(
                'edit' => array(
                    'menutag' => 'edit',
                    'menuLabel' => 'General',
                    'sectionContent' => $this->load->view('maintenance/BSPEselect', $bspeData, TRUE)
                ),*/
            $data['content'] = array(
                'edit' => $setEdit/*,
                'specs' => array(
                    'menutag' => 'specs',
                    'menuLabel' => 'Jobsite Specs',
                    'auth_view' => $auth_view,
                    'auth_edit' => $auth_edit,
                    'sectionContent' => $this->load->view('bids/jobsiteSpecs', $jobsiteData, TRUE),
                    'sectionContent2' => ''
                ),
                'options' => array(
                    'menutag' => 'options',
                    'menuLabel' => 'Options',
                    //'auth_view' => '',
                    //'auth_edit' => '',
                    'sectionContent' => ''
                ),
                'stats' => array(
                    'menutag' => 'stats',
                    'menuLabel' => 'Statistics',
                    //'auth_view' => '',
                    //'auth_edit' => '',
                    'sectionContent' => ''
                ),
                'audit' => array(
                    'menutag' => 'audit',
                    'menuLabel' => 'Audit Log',
                    //'auth_view' => '',
                    //'auth_edit' => '',
                    'sectionContent' => ''
                )*/
            );
        //Load View
            $this->load->view('main');
            $this->load->view('bids/bidsAddEdit', $data);
            $this->load->view('footer');
    }

    public function addingNewBid(){
        //$data['list_of_buildercustomers'] = $this->bidsmodel->getBuilderRows();
        $data['title'] = 'Add (Edit) Bid';
if($this->session->userdata('level') <= '4' ? $audit_view = '' : $audit_view = 'hidden');
if($this->session->userdata('level') <= '4' ? $auth_edit = '' : $auth_edit = 'readonly');
if($this->session->userdata('level') <= '4' ? $auth_view = '' : $auth_view = 'hidden');
        $data['form_to_open'] = form_open(site_url('/bids/check_bid_form'), array('class' => 'jsform'));
        $data['left_menu'] = array('edit2' => 'General', 'specs2' => 'Jobsite Specs', 'options2' => 'Options');
$bspeData['list_of_bldercustomers'] = $this->maintenancemodel->GetBuildersListNEW();
        $data['content_test'] = array
            (
                'edit' => array
                    (
                        'left_menu' => 'edit3',
                        'menuLabel' => 'General3',
                        'auth_view' => '',
                        'auth_edit' => '',
                        'sectionTitle' => 'General Heading',
                        //'sectionContent' => 'Waldo'
                        'sectionContent' => $this->load->view('maintenance/BSPEselect', $bspeData, TRUE)
                    ),
                'specs' => array
                    (
                        'left_menu' => 'specs3',
                        'menuLabel' => 'Jobsite Specs3',
                        'auth_view' => $auth_view,
                        'auth_edit' => $auth_edit,
                        'sectionTitle' => 'Jobsite Specifications',
                        'sectionContent' => 'Hi'
                    ),
                'audit' => array
                (
                        'left_menu' => 'audit3',
                        'menuLabel' => 'Audit Log3',
                        'auth_view' => '',
                        'auth_edit' => '',
                        //'audit_view' => $audit_view,
                        'sectionTitle' => 'Audit Log Here',
                        'sectionContent' => 'Audit Content'
                )
            );

        //codeigniter multidimensional array of key value pairs from controller
        $data['list_of_buildercustomers'] = $this->maintenancemodel->GetBuildersListNEW();
        $data['get_mfg'] = $this->bidsmodel->get_mfgs();
    	$this->load->view('main');
        $this->load->view('bids/addingnewbid', $data);
    	$this->load->view('footer');
    }

    public function getBuilderSubdivisions(){
        $subdivisions = array(); //container to hold all the matching subdivisions
        $builder_id = $this->input->post('builder_id'); //using this passed from users choice
            if($builder_id){
                $con['conditions'] = array('tbl_buildercustomer_builder_id'=>$builder_id); //for WHERE field equals value posted
                $subdivisions = $this->bidsmodel->getBuilderSubdivisionRows($con); //using above passed in from views->bids->addbid use Models->BidsModel->getBuilderSubdivisionRows w/ conditions
            }
        echo json_encode($subdivisions); //create as json array so view can parse it
    }

    public function get_plans(){
        // POST data
        $postData = $this->input->post();
        // GET data
        $data = $this->bidsmodel->get_plans($postData);
        echo json_encode($data);
    }

    public function get_elevations(){
        // POST data
        $postData = $this->input->post();
        //$postData = $this->input->post(('#bidPlan').val());
        // GET data
        $data = $this->bidsmodel->get_elevations($postData);
        echo json_encode($data);
    }

    public function check_bid_form(){

        if (!$this->input->is_ajax_request()){
            exit ('no valid request found');
        }

        $FormRules = array(
            array(
                'field' => 'builderIdSelected', //exact name given
                'label' => 'Builder',  //Human name for error msgs
                'rules' => 'required' //separated by | if multiple
            ),
            array(
                'field' => 'subdivisionIdSelected',
                'label' => 'Subdivision',
                'rules' => 'required'
            ),
            array(
                'field' => 'bidPlan',
                'label' => 'Plan',
                'rules' => 'required|alpha_dash'
            ),
            array(
                'field' => 'bidElevation',
                'label' => 'Elevation',
                'rules' => 'required|alpha_numeric'
            )
        );

        $this->form_validation->set_rules($FormRules);
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');  //made the space between them smaller but not inline

        if ($this->form_validation->run() == TRUE){
            // user has successfully passed
	//$this->output->enable_profiler(TRUE);
        	$bidCheck = $this->bidsmodel->check_if_duplicate_bid();
    //var_dump($bidCheck['list_of_bids']);
    //var_dump($bidCheck[0]['tbl_bid_id']);
    //log_message('debug', '$bidCheck ' . print_r($bidCheck[0]['tbl_bid_id'], TRUE));
        	if($bidCheck != FALSE){
            	//echo '<div class="alert alert-danger">Bid exists already, edit?';
                //$this->bidsAddEdit($bidCheck[0]['tbl_bid_id']); //calling controller with parameter from within same controller (currently nests the data in current view)
                redirect('bids/bidsAddEdit/' . $bidCheck[0]['tbl_bid_id']); //same nesting behavior
        	} else {
        		echo '<div class="alert alert-success">Creating new bid.';
                //Update Bid table with new combination and load new view with data locked at top
                $this->bidsmodel->insert_new_bid_combo();
        	}
        } else {
            // form validation failed
            echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
        }
    }

    public function editbid($tbl_bid_id, $tbl_buildercustomer_builder_id){
        if($tbl_bid_id){
            //$con['conditions'] = array('Bids.tbl_bid_id'=>$tbl_bid_id);
            $con['conditions'] = array('tbl_bid_id'=>$tbl_bid_id);
            $data['bid_details'] = $this->bidsmodel->get_bid_details($con);
            $data['tbl_bid_id'] = $tbl_bid_id;
            $data['tbl_buildercustomer_builder_id'] = $tbl_buildercustomer_builder_id;
            $data['get_install_types'] = $this->bidsmodel->get_install_types($tbl_buildercustomer_builder_id);
            $data['get_mfg'] = $this->bidsmodel->get_mfgs();
            $data['get_bid_options'] = $this->bidsmodel->get_bids_options($tbl_bid_id);
            $data['option_has_windows'] = $this->bidsmodel->optionHasWindows($tbl_bid_id);
            $data['option_has_windows2'] = $this->bidsmodel->optionHasWindows2($tbl_bid_id);
            $data['get_bid_windows'] = $this->bidsmodel->get_bids_windows($tbl_bid_id);
            $data['get_bid_windows2'] = $this->bidsmodel->get_bids_windows2($tbl_bid_id);
        }
        $this->load->helper('MY_array_helper');
        $this->load->view('main');
        $this->load->view('bids/editbid', $data);
        $this->load->view('footer');
    }

    public function get_mfg_colors(){ // triggered by an AJAX call from view > editbid.php
        $colors = array(); //container to hold all the matching colors
        $mfg_id = $this->input->post('manufacturer_id'); //using this passed from users choice
            if($mfg_id){
                $con['conditions'] = array('tbl_manufacturer_manufacturer_id'=>$mfg_id); //for WHERE field equals value posted
                $colors = $this->bidsmodel->getManufacturerColorRows($con);
            }
        echo json_encode($colors); //create as json array so view can parse it
    }

    public function get_mfg_materials(){ // triggered by an AJAX call from view > editbid.php
        $materials = array(); //container to hold all the matching materials
        $mfg_id = $this->input->post('manufacturer_id'); //using this passed from users choice
            if($mfg_id){
                $con['conditions'] = array('tbl_manufacturer_manufacturer_id'=>$mfg_id); //for WHERE field equals value posted
                $materials = $this->bidsmodel->getManufacturerMaterialRows($con);
            }
        echo json_encode($materials); //create as json array so view can parse it
    }

    public function get_mfg_fins(){ // triggered by an AJAX call from view > editbid.php
        $fins = array(); //container to hold all the matching fins
        $mfg_id = $this->input->post('manufacturer_id'); //using this passed from users choice
            if($mfg_id){
                $con['conditions'] = array('tbl_manufacturer_manufacturer_id'=>$mfg_id); //for WHERE field equals value posted
                $fins = $this->bidsmodel->getManufacturerFinRows($con);
            }
        echo json_encode($fins); //create as json array so view can parse it
    }

    public function get_mfg_coatings(){ // triggered by an AJAX call from view > editbid.php
        $coatings = array(); //container to hold all the matching coatings
        $mfg_id = $this->input->post('manufacturer_id'); //using this passed from users choice
            if($mfg_id){
                $con['conditions'] = array('tbl_manufacturer_manufacturer_id'=>$mfg_id); //for WHERE field equals value posted
                $coatings = $this->bidsmodel->getManufacturerCoatingRows($con);
            }
        echo json_encode($coatings); //create as json array so view can parse it
    }

    public function editbid_grid_update(){
        $insertBucket = array();
        $deleteBucket = array();
        $gridUpdatesRaw = $this->input->post('changesToGrid');
        //$gridUpdatesGroup = explode(",", $gridUpdatesRaw);
        if(count($gridUpdatesRaw) != 0){
            foreach($gridUpdatesRaw as $gridGroup){
                $gridUpdatesGroup = explode(",", $gridGroup);
            };
        };
        if(count($gridUpdatesGroup) != 0){
            foreach($gridUpdatesGroup as $eachItem){
            if($eachItem){
                $itemAction = explode("-", $eachItem);
                if($itemAction[2] == 1){
                    $insertBucket[] = array($itemAction[0], $itemAction[1]);
                    //add $itemAction[0],$itemAction[1] to insert bucket
                }else{
                    $deleteBucket[] = array($itemAction[0],  $itemAction[1]);
                    //add $itemAction[0],$itemAction[1] to delete bucket
                };
            };
            };
        };
        
        echo json_encode(array($insertBucket, $deleteBucket));
    }

    public function editbid_grid_update_forms(){
        if(!$this->input->is_ajax_request()){
            exit ('no valid request found under editbid_grid_update_forms');
        };
        $FormRules = array(
            array(
                'field' => 'changesToGrid2', //exact name given
                'label' => '',  //Human name for error msgs
                'rules' => 'numeric' //separated by | if multiple
            )
        );
        $this->form_validation->set_rules($FormRules);
        if($this->form_validation->run() == TRUE){
            $insertDeleteOptionHasWindows = $this->bidsmodel->insert_delete_from_optionHasWindows();
            echo '<div class="alert-success">Changes Saved! Refreshing Page.</div>';
        } else {
            echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
        };
    }

    public function editbid_grid_update_forms2(){
        if(!$this->input->is_ajax_request()){
            exit ('no valid request found under editbid_grid_update_forms2');
        };
        $FormRules = array(
            array(
                'field' => 'changesToGrid3', //exact name given
                'label' => '',  //Human name for error msgs
                'rules' => 'numeric' //separated by | if multiple
            )
        );
        $this->form_validation->set_rules($FormRules);
        if($this->form_validation->run() == TRUE){
            $insertDeleteOptionHasWindows = $this->bidsmodel->insert_update_delete_optionHasWindows2();
            echo '<div class="alert-success">Changes Saved! Refreshing Page.</div>';
        } else {
            echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
        };
    }

    public function editbid_bid_simulation(){ // triggered by an AJAX call from view > editbid.php simulate button
        $this->load->helper('MY_array_helper');
        $bid_simulation = array();
        $not_used_windows = array();
        $option_removed_windows = array();
        $selected_bid_options = array(); // aka $get_bid_options
        $selected_bid_options = $this->input->post('simulationOptions'); // from AJAX logic
        $tbl_bid_id = $this->input->post('tbl_bid_id_ajax'); // from AJAX logic
        $get_bid_windows = $this->bidsmodel->get_bids_windows($tbl_bid_id); // 1 is the $tbl_bid_id
        $option_has_windows = $this->bidsmodel->optionHasWindows($tbl_bid_id); // 1 is the $tbl_bid_id
        $count_of_selected_options = count($selected_bid_options);
        if($count_of_selected_options != 0){
            foreach($get_bid_windows as $eachWindow){
                $windows_in_selected_options = array();
                $bid_temp = array();
                foreach($selected_bid_options as $matchItem){ //$bid_temp is cleared for each one
                    $matchedItem = explode(",", $matchItem);
                    if(verifyCrossRef($option_has_windows, array('option_id','tbl_bid_windows_bid_windows_id'), array($matchedItem[0],$eachWindow['bid_windows_id']))){
                        //$bid_temp[] = array('option_id' => $matchedItem[0], 'option_name' => $matchedItem[1],'window_details' => $eachWindow['window_details'], 'window_location' => $eachWindow['window_location']);
                        $windows_in_selected_options[] = array('option_id' => $matchedItem[0], 'option_name' => $matchedItem[1],'window_details' => $eachWindow['window_details'], 'window_location' => $eachWindow['window_location']);
                    };//else{
                    //    $not_used_windows[] = array('option_name1' => $matchedItem[1], 'window_details1' => $eachWindow['window_details'],'window_location1' => $eachWindow['window_location']);                            
                };// end of foreach($selected_bid_options as $matchItem)

                // window is in every option selected
                if(count($windows_in_selected_options) == $count_of_selected_options){
                     //add to $bid_simulation with blank option name
                    $bid_simulation[] = array('option_name1' => '','window_details1' => $eachWindow['window_details'],'window_location1' => $eachWindow['window_location']);
                };
                
                // count - 1
                if(count($windows_in_selected_options) == ($count_of_selected_options - 1)){ 
                    if(in_array('BASE', array_column($windows_in_selected_options, 'option_name'))){ // if BASE is one of the matching options then the window is a removed by this option
                        foreach($selected_bid_options as $matchItem){
                            $matchedItem = explode(",", $matchItem);
                            if(!in_array($matchedItem[1], array_column($windows_in_selected_options, 'option_name'))){
                                 //add to removed by this option list // 4 options matches
                                $option_removed_windows[] = array('option_name1' => $matchedItem[1], 'window_details1' => $eachWindow['window_details'],'window_location1' => $eachWindow['window_location']);
                            };                       
                        };
                    }else{ // if BASE is NOT in list, window is added by this option as long as it is not LHAND or RHAND
                        foreach($windows_in_selected_options as $matchItem){
                            if($matchedItem['option_name'] != 'LHAND' && $matchedItem['option_name'] != 'RHAND'){
                                $bid_simulation[] = array('option_name1' => $matchedItem['option_name'],'window_details1' => $eachWindow['window_details'],'window_location1' => $eachWindow['window_location']);
                            };
                        };
                    };
                };

                // count middle ground
                if(count($windows_in_selected_options) > 2 && count($windows_in_selected_options) < ($count_of_selected_options - 1)){
                    foreach($selected_bid_options as $matchItem){
                        $matchedItem = explode(",", $matchItem);
                        if($matchedItem['option_name'] == 'BASE'){ //add to $bid_simulation with null option name
                            if(in_array('LHAND', array_column($matchedItem, 'option_name')) || in_array('RHAND', array_column($matchedItem, 'option_name'))){ //get 2nd item in array
                                $bid_simulation[] = array('option_name1' => '','window_details1' => $eachWindow['window_details'],'window_location1' => $eachWindow['window_location']);
                            };
                        }else{ //add to added list by this option
                            foreach($selected_bid_options as $option_test){
                                if($option_test[1] != 'LHAND' && $option_test[1] != 'RHAND'){
                                    $bid_simulation[] = array('option_name1' => $option_test[1],'window_details1' => $eachWindow['window_details'],'window_location1' => $eachWindow['window_location']);
                                };
                            };                                    
                        };
                    };
                };

                // count of 2 means it is BASE and one of the handings (most likely) but should test to confirm
                if(count($windows_in_selected_options) == 2){
                    foreach($windows_in_selected_options as $base_test){
                        if($base_test['option_name'] == 'BASE'){
                            if(in_array('LHAND', array_column($windows_in_selected_options, 'option_name')) || in_array('RHAND', array_column($windows_in_selected_options, 'option_name'))){ //get 2nd item in array
                                $bid_simulation[] = array('option_name1' => '','window_details1' => $base_test['window_details'],'window_location1' => $base_test['window_location']);
                            };
                        }elseif(in_array('LHAND', array_column($windows_in_selected_options, 'option_name')) || in_array('RHAND', array_column($windows_in_selected_options, 'option_name'))){ //get 2nd item in array
                            if($base_test['option_name'] != 'LHAND' && $base_test['option_name'] != 'RHAND'){
                                $bid_simulation[] = array('option_name1' => $base_test['option_name'],'window_details1' => $base_test['window_details'],'window_location1' => $base_test['window_location']);
                            };                                 
                        };
                    };
                };

                // count of 1
                if(count($windows_in_selected_options) == 1){ //reset array, do nothing, toss, get rid of // 1 option match
                    //$windows_in_selected_options = array();
                };
            }; // end of foreach($get_bid_windows as $eachWindow)
        };
        echo json_encode(array($bid_simulation,$option_removed_windows));
    } // end of public function editbid_bid_simulation()

// what happens to counts that are both middle ground and 2 or count - 1?

    public function editbid_bid_takeoffs(){
        $tbl_bid_id = $this->input->post('tbl_bid_id_ajax');
        $get_bid_windows = $this->bidsmodel->get_bids_windows2($tbl_bid_id); // loads all windows for the given bid_id
        $get_bid_options = $this->bidsmodel->get_bids_options($tbl_bid_id); // get every option the bid has
        $options_affecting_windows = $this->bidsmodel->optionHasWindows2($tbl_bid_id); // 1 is the $tbl_bid_id
        $bid_takeoffs = array();
        //$bid_simulation_base = array();
        //$bid_simulation_window_added = array();
        //$bid_simulation_window_removed = array();
        $windows_affected_by_options_actions = array();
        $optionFilterArray = array();
        foreach($get_bid_windows as $eachWindow){
            foreach($options_affecting_windows as $oAw){
                if($eachWindow['bid_windows_id'] == $oAw['tbl_bid_windows_bid_windows_id']){
                    if($oAw['option_name'] != 'LHAND' && $oAw['option_name'] != 'RHAND'){
                        if($oAw['add_remove_action'] == 0){
                            $windows_affected_by_options_actions[$oAw['option_name']]['removed'][] = array('window_details' => $eachWindow['window_details'], 'window_location' => $eachWindow['window_location']);
                            //$windows_affected_by_options_actions[$oAw['option_id']][] = array('option_id' => $oAw['option_id'], 'option_name' => $oAw['option_name'], 'window_id' => $eachWindow['bid_windows_id'], 'window_details' => $eachWindow['window_details'], 'window_location' => $eachWindow['window_location'], 'position' => $eachWindow['position'], 'add_remove_action' => $oAw['add_remove_action']);
                        }elseif($oAw['add_remove_action'] == 1){
                            $windows_affected_by_options_actions[$oAw['option_name']]['added'][] = array('window_details' => $eachWindow['window_details'], 'window_location' => $eachWindow['window_location']);
                        };
                    };
                };
            };
        };
        //array_multisort (array_column($windows_affected_by_options_actions, 'option_id'), SORT_ASC, $windows_affected_by_options_actions);
                //echo json_encode(array_keys($windows_affected_by_options_actions));        
        echo json_encode(array($windows_affected_by_options_actions));
    }

    public function editbid_bid_simulation2(){
        $this->load->helper('MY_array_helper');
        $bid_simulation_base = array();
        $bid_simulation_window_added = array();
        $bid_simulation_window_removed = array();
        $selected_bid_options = array(); // aka $get_bid_options
        $selected_bid_options = $this->input->post('simulationOptions'); // from AJAX logic
        $tbl_bid_id = $this->input->post('tbl_bid_id_ajax'); // from AJAX logic
        $get_bid_windows = $this->bidsmodel->get_bids_windows2($tbl_bid_id); // loads all windows for the given bid_id
        $get_bid_options = $this->bidsmodel->get_bids_options($tbl_bid_id); // get every option the bid has
        $options_affecting_windows = $this->bidsmodel->optionHasWindows2($tbl_bid_id); // 1 is the $tbl_bid_id
        $count_of_selected_options = count($selected_bid_options);

        if($count_of_selected_options != 0){
            $count = 0;
            $windows_affected_by_options_actions = array();
            $optionFilterArray = array();
            foreach($get_bid_windows as $eachWindow){
                foreach($get_bid_options as $allOptions){
                    if(!verifyCrossRef($options_affecting_windows, array('option_id','tbl_bid_windows_bid_windows_id'), array($allOptions['option_id'],$eachWindow['bid_windows_id']))){
                        $count = $count + 1;
                    };
                };
                if($count == count($get_bid_options)){
                    $bid_simulation_base[] = array('option_name' => '', 'window_id' => $eachWindow['bid_windows_id'], 'window_details' => $eachWindow['window_details'], 'window_location' => $eachWindow['window_location'], 'position' => $eachWindow['position']);
                    //$bid_simulation_base[$eachWindow['bid_windows_id']][] = array('option_name' => 'BASE', 'window_id' => $eachWindow['bid_windows_id'], 'window_details' => $eachWindow['window_details'], 'window_location' => $eachWindow['window_location'], 'position' => $eachWindow['position']);
                }else{
                    foreach($options_affecting_windows as $oAw){
                        if($eachWindow['bid_windows_id'] == $oAw['tbl_bid_windows_bid_windows_id']){
                            $windows_affected_by_options_actions[$eachWindow['bid_windows_id']][] = array('option_id' => $oAw['option_id'], 'option_name' => $oAw['option_name'], 'window_id' => $eachWindow['bid_windows_id'], 'window_details' => $eachWindow['window_details'], 'window_location' => $eachWindow['window_location'], 'position' => $eachWindow['position'], 'add_remove_action' => $oAw['add_remove_action']);
                        };
                    };
                };
                $count = 0;        
            };
           
    //echo json_encode($windows_affected_by_options_actions);
            $handing = explode(",", $selected_bid_options[0])[1];
            foreach($windows_affected_by_options_actions as $wAbOa){
                $count_remove = 0;
                $count_add = 0;
                $option_matches = 0;
                foreach($wAbOa as $wAbOaDetails){
                    //$optionFilterArray[$sWaDetails['window_id']] = array();
    //echo json_encode(explode(",", $selected_bid_options[0])[1]);
                    foreach($selected_bid_options as $simulatorSelectionsAll){
                        $simulatorSelectionsEach = explode(",", $simulatorSelectionsAll);
                        if($wAbOaDetails['option_id'] == $simulatorSelectionsEach[0]){
                            $option_matches = $option_matches + 1;
                        };
                    };
                    if($wAbOaDetails['add_remove_action'] == 0){
                        $count_remove = $count_remove + 1;
                    }elseif($wAbOaDetails['add_remove_action'] == 1){
                        $count_add = $count_add + 1;
                    };
                };
                if($count_remove == count($wAbOa) && $option_matches == 0){ //should add 2,14,15,16,19,20,21,24 to BASE
                    $bid_simulation_base[] = array('option_name' => '', 'window_id' => $wAbOa[0]['window_id'], 'window_details' => $wAbOa[0]['window_details'], 'window_location' => $wAbOa[0]['window_location'], 'position' => $wAbOa[0]['position']);
                    //$bid_simulation_base[$wAbOa[0]['window_id']][] = array('option_name' => 'BASE', 'window_id' => $wAbOa[0]['window_id'], 'window_details' => $wAbOa[0]['window_details'], 'window_location' => $wAbOa[0]['window_location'], 'position' => $wAbOa[0]['position']);
                };
                if($option_matches > 0 && count($wAbOa) >= 1 && $count_remove > 0){
                    if($option_matches == 2 && $wAbOa[0]['option_name'] == $handing){
                        foreach($wAbOa as $wAbOaDetails){
                            if($wAbOaDetails['option_name'] != 'LHAND' && $wAbOaDetails['option_name'] != 'RHAND'){
                                foreach($selected_bid_options as $simulatorSelectionsAll){
                                    $simulatorSelectionsEach = explode(",", $simulatorSelectionsAll);
                                    if($wAbOaDetails['option_id'] == $simulatorSelectionsEach[0] && $wAbOaDetails['add_remove_action'] == 0){
                                        $bid_simulation_window_removed[] = array('option_name' => $wAbOaDetails['option_name'], 'window_id' => $wAbOaDetails['window_id'], 'window_details' => $wAbOaDetails['window_details'], 'window_location' => $wAbOaDetails['window_location'], 'position' => $wAbOaDetails['position']);
                                        //$bid_simulation_window_removed[$wAbOa[0]['window_id']][] = array('option_name' => $wAbOaDetails['option_name'], 'window_id' => $wAbOaDetails['window_id'], 'window_details' => $wAbOaDetails['window_details'], 'window_location' => $wAbOaDetails['window_location'], 'position' => $wAbOaDetails['position']);
                                    };
                                };
                            };
                        };
                    }elseif($option_matches >= 1 && $wAbOa[0]['option_name'] != 'LHAND' && $wAbOa[0]['option_name'] != 'RHAND'){
                        foreach($wAbOa as $wAbOaDetails){
                            if($wAbOaDetails['option_name'] != 'LHAND' && $wAbOaDetails['option_name'] != 'RHAND'){
                                foreach($selected_bid_options as $simulatorSelectionsAll){
                                    $simulatorSelectionsEach = explode(",", $simulatorSelectionsAll);
                                    if($wAbOaDetails['option_id'] == $simulatorSelectionsEach[0] && $wAbOaDetails['add_remove_action'] == 0){
                                        $bid_simulation_window_removed[] = array('option_name' => $wAbOaDetails['option_name'], 'window_id' => $wAbOaDetails['window_id'], 'window_details' => $wAbOaDetails['window_details'], 'window_location' => $wAbOaDetails['window_location'], 'position' => $wAbOaDetails['position']);
                                        //$bid_simulation_window_removed[$wAbOa[0]['window_id']][] = array('option_name' => $wAbOaDetails['option_name'], 'window_id' => $wAbOaDetails['window_id'], 'window_details' => $wAbOaDetails['window_details'], 'window_location' => $wAbOaDetails['window_location'], 'position' => $wAbOaDetails['position']);
                                    };
                                };
                            };
                        };
                    };
                };
                if($option_matches == 1 && $wAbOa[0]['add_remove_action'] == 1 && $wAbOa[0]['option_name'] == $handing){ //should add 28 to BASE, did when 28 was L or R only
                    $bid_simulation_base[] = array('option_name' => '', 'window_id' => $wAbOa[0]['window_id'], 'window_details' => $wAbOa[0]['window_details'], 'window_location' => $wAbOa[0]['window_location'], 'position' => $wAbOa[0]['position']);
                    //$bid_simulation_base[$wAbOa[0]['window_id']][] = array('option_name' => 'BASE', 'window_id' => $wAbOa[0]['window_id'], 'window_details' => $wAbOa[0]['window_details'], 'window_location' => $wAbOa[0]['window_location'], 'position' => $wAbOa[0]['position']);                    
                };
    //echo count($wAbOa);
                if($count_add == count($wAbOa) && $option_matches == count($wAbOa)){ //should add 26,36 to ADDED
                    foreach($wAbOa as $wAbOaDetails){
                        if($wAbOaDetails['option_name'] != 'LHAND' && $wAbOaDetails['option_name'] != 'RHAND'){ //add to base for sorting
                            $bid_simulation_base[] = array('option_name' => $wAbOaDetails['option_name'], 'window_id' => $wAbOaDetails['window_id'], 'window_details' => $wAbOaDetails['window_details'], 'window_location' => $wAbOaDetails['window_location'], 'position' => $wAbOaDetails['position']);
                            // add to list to see what was adjusted ACTUALLY hide since the option indication is to right of window
                            //$bid_simulation_window_added[] = array('option_name' => $wAbOaDetails['option_name'], 'window_id' => $wAbOaDetails['window_id'], 'window_details' => $wAbOaDetails['window_details'], 'window_location' => $wAbOaDetails['window_location'], 'position' => $wAbOaDetails['position']);
                            //$bid_simulation_window_added[$wAbOa[0]['window_id']][] = array('option_name' => $wAbOaDetails['option_name'], 'window_id' => $wAbOaDetails['window_id'], 'window_details' => $wAbOaDetails['window_details'], 'window_location' => $wAbOaDetails['window_location'], 'position' => $wAbOaDetails['position']);
                        };
                    };
                };

            }; // end foreach($windows_affected_by_options_actions as $wAbOa)

            array_multisort (array_column($bid_simulation_base, 'position'), SORT_ASC, $bid_simulation_base);
            echo json_encode(array($bid_simulation_base,$bid_simulation_window_added,$bid_simulation_window_removed));
                //echo json_encode($bid_simulation_base);
                //echo json_encode($windows_affected_by_options_actions);
                //echo json_encode($bid_simulation_window_added);
                //echo json_encode($bid_simulation_window_removed);
        }; // end if($count_of_selected_options != 0)
    } // end public function editbid_bid_simulation2()

    public function editOption($tbl_bid_id, $option_id){
        $windowData['windows_in_option'] = $this->bidsmodel->optionHasWindows($tbl_bid_id,$option_id);
        $this->load->view('bids/optionswindows',$windowData);
        //$this->load->view('bids/editbid', $windowData);
    }

    public function bid_window_sort_order_change(){
        //echo json_encode($_POST['windowID']);
        $i = 0;
        foreach ($_POST['windowID'] as $value) {
            // Execute statement:
            // UPDATE [Table] SET [Position] = $i WHERE [EntityId] = $value
            // UPDATE tbl_bid_windows SET position = $i WHERE windowID = $value
            $this->bidsmodel->update_window_order($value,$i);
        $i++;
        };
    }

}

?>