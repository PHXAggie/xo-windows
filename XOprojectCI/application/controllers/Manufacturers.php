<?php
//header('Access-Control-Allow-Origin: *');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manufacturers extends CI_Controller{


	function __construct() {
		parent::__construct();
		if($this->session->userdata('logged_in') !== TRUE){
			redirect('login');
		} else {
			$this->load->model('manufacturersmodel');
		}
	}

	public function landingPage(){
		$data['title'] = 'Manufacturer Maintenance';
		$this->load->view('main');
		$this->load->view('manufacturers/landingPage', $data);
		$this->load->view('footer');
	}

	public function mfgPriceEdit($mfg_id = NULL){
		//Settings
			if($mfg_id){
				$data['title'] = 'Add Pricing for Manufacturer';
			}else{
				$data['title'] = 'Edit Pricing for ';
				$allContent['list_Mfg'] = $this->manufacturersmodel->getMfgList();
				$setEdit = array(
					'menutag' => 'edit',
					'menuLabel' => 'General',
					'sectionContent' => $this->load->view('manufacturers/mfgDropDownList', $allContent, TRUE)
				);

			}
		//Head (title, permissions, forms)
		//GET (data for page)
		//Present (edit, specs, options, stats, audit)
			$data['content'] = array(
				'edit' => $setEdit
			);
		//Load View
			$this->load->view('main');
			$this->load->view('manufacturers/mfgPriceEdit', $data);
			$this->load->view('footer');
	}


	public function mfgList(){
		$listOfMfg['listOfMfg'] = $this->manufacturersmodel->getMfgList();
		$this->load->view('manufacturers/mfgDropDownList', $listOfMfg, TRUE);
	}


	public function mfgWindowPricing(){
		$this->load->view('main');
		$this->load->view('footer');
	}

}

?>