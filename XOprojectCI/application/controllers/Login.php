<?php 
class Login extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('login_model');
	}

	function index(){
		$this->load->view('login_view');
		$this->load->view('footer');
	}

	function auth(){
		$username	  = $this->input->post('username',TRUE);
		$password = md5($this->input->post('password',TRUE));
		$validate = $this->login_model->validate($username,$password);
		if($validate->num_rows() > 0){
			$data	= $validate->row_array();
			$name	= $data['login'];
			$email	= $data['email'];
			$level	= $data['level'];
			$xo_id 	= $data['id'];
			$sesdata = array(
				'username'	=> $name,
				'email'		=> $email,
				'level'		=> $level,
				'xo_id'		=> $xo_id,
				'logged_in'	=> TRUE
			);
			$this->session->set_userdata($sesdata);
			// access login for admin
			/*
			if($level === '1'){
				//redirect('page');
			
			// access login for staff
			}elseif($level === '2'){
				//redirect('page/staff');

			// access login for author
			}else{
				//redirect('page/author');
			}
			*/
			if($level <= '4'){
				redirect('home');
			}
		}else{
			echo $this->session->set_flashdata('msg','Username or Password is Incorrect');
			redirect('login');
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
}
?>