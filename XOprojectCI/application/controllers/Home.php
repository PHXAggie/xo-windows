<?php
//header('Access-Control-Allow-Origin: *');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        if($this->session->userdata('logged_in') !== TRUE){
        	redirect('login');
        }else{
        	$this->load->model('homemodel');
    	}
    }

    public function index(){

    $this->load->view('main');
    $this->load->view('home/index');
    $this->load->view('footer');

    }
}

?>