<?php
//header('Access-Control-Allow-Origin: *');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Form_reader extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('bidsmodel');
    }

    public function index(){
        
    }
    
    public function check_bid(){

        if (!$this->input->is_ajax_request()){
            exit ('no valid request found');
        }

        $FormRules = array(
            array(
                'field' => 'builderIdSelected', //exact name given
                'label' => 'Builder',  //Human name for error msgs
                'rules' => 'required' //separated by | if multiple
            ),
            array(
                'field' => 'subdivisionIdSelected',
                'label' => 'Subdivision',
                'rules' => 'required'),
            array(
                'field' => 'bidPlan',
                'label' => 'Plan',
                'rules' => 'required|alpha_dash'),
            array(
                'field' => 'bidElevation',
                'label' => 'Elevation',
                'rules' => 'required|alpha_numeric')
        );

        $this->form_validation->set_rules($FormRules);
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');  //made the space between them smaller but not inline

        if ($this->form_validation->run() == TRUE){
            // user has successfully passed
            echo '<div class="success">Your data is being checked';
        } else {
            // form validation failed
            echo '<div class="error">'.validation_errors().'</div>';
        }
    }
}

?>