<?php

//Running PHP from command prompt
//from D:\wamp64\bin\php\php7.1.9> php.exe d:\wamp64\www\ooPHP\Task.php

class Task {
	public $description;

	public $completed = false;

	public function __construct($description)
	{

		$this->description = $description;

	}

	public function complete()
	{
		$this->completed = true;

	}
}

$task = new Task('Learn OOP');
$task2 = new Task('Learning it is fun!');

$task->complete();

var_dump($task->description);
var_dump($task2->description);
var_dump($task->completed);
var_dump($task2->completed);


?>