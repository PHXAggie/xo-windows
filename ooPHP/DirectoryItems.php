<?php

class DirectoryItems {
    private $filearray = array();

    //function DirectoryItems($directory) { // with same name as class acts as constructor - DEPRICATING
    public function __construct($directory,$replacechar = "_") {
        $this->directory = $directory;
        $this->replacechar = $replacechar;
        $d = "";
        if(is_dir($directory)) {
            $d = opendir($directory) or die("Failed to open directory.");

            while (false !== ($f=readdir($d))) { //loops over the directory
                if(is_file("$directory/$f")){
                    $title = $this->createTitle($f); //$title used below in method createTitle
                    $this->filearray[$f] = $title; //put the titles in an array with $f being the index number
                }
            }
            closedir($d);
        }else{
            //error
                die("Must pass in a directory.");
        }
    }

    // public is how the 'external user' will interact with the class
    // private is the internal workings of the class that the user need not care about

    private function createTitle($title) { //private because only used in class constructor
        //strip extension
        $title = substr($title,0,strrpos($title, ".")); //from start location to where period is located
        //replace word separator
        $title = str_replace($this->replacechar," ",$title); //replace any _'s with a blank space
        return $title;
    }

    function indexOrder() {
        sort($this->filearray);
    }
    //////////////////////////////////////////////////////////
    function naturalCaseInsensitiveOrder() {
        natcasesort($this->filearray);
    }
    //////////////////////////////////////////////////////////
    function getCount() {
        return count($this->filearray);
    }

    public function getFileArray(){
        return $this->filearray;
    }

    public function checkAllSpecificType($extension) {
        $extension = strtolower($extension);
        $bln = true; //$bln stands for boolean
        $ext = "";
        foreach ($this->filearray as $key => $value){
            $ext = substr($key,(strpos($key, ".") + 1));
            $ext = strtolower($ext);
            if($extension != $ext){
                $bln = false;
                break;
            }
        }
        return $bln;
    }
    
    function checkAllImages() {
        $bln = true;
        $extension = "";
        $types = array("jpg", "jpeg", "gif", "png");
        foreach ($this->filearray as $key => $value) {
            $extension = substr($value,(strpos($value, ".") + 1));
            $extension = strtolower($extension);
            if(!in_array($extension, $types)) {
                $bln = false;
                break;
            }
        }
        return $bln;
    }

    public function imagesOnly() {
        $extension = "";
        $types = array("jpg", "jpeg", "gif", "png");
        foreach($this->filearray as $key => $value) {
            $extension = substr($key,(strpos($key, ".") + 1));
            $extension = strtolower($extension);
            if(!in_array($extension, $types)){
                unset($this->filearray[$key]);
            }
        }
    }

    public function filter($extension){
        $extension = strtolower($extension);
        foreach ($this->filearray as $key => $value) {
            $ext = substr($key,(strpos($key, ".") + 1));
            $ext = strtolower($ext);
            if($ext != $extension){
                unset ($this->filearray[$key]);
            }
        }
    }

    public function removeFilter() {
        unset($this->filearray);
        $d = "";
        $d = opendir($this->directory) or die("Couldn't open directory.");
        while(false !== ($f = readdir($d))) {
            if(is_file("$this->directory/$f")){
                $title = $this->createTitle($f);
                $this->filearray[$f] = $title;
            }
        }
        closedir($d);
    }

}

?>