<html>
<head>
	<title>Images</title>
</head>
<body>
	<?php
	/*
		require 'DirectoryItems.php';
		$di = new DirectoryItems('graphics');
		$di->checkAllImages(); // or die("Not all files are images.");
		$di->naturalCaseInsensitiveOrder();
		//get array
		foreach ($di->filearray as $key => $value) {
			echo "<div style='text-align:center;'><img src='graphics/" . $value . "' style='height:150px;width:150px;' /><br />";
			echo "</div><br />";
		}
	*/


		require 'DirectoryItems.php';
		$dc = new DirectoryItems('graphics');
		$dc->imagesOnly();
		$dc->naturalCaseInsensitiveOrder();
		$path = "";
		$filearray = $dc->getFileArray();
		echo "<div style=\"text-align:center;\">";
		echo "Click to view full-sized version.<br />";
		//specify size of thumbnail
		$size = 100;
		foreach($filearray as $key => $value){
			$path = "graphics/".$key;
			/*errors in getthumb or in class will result in broken links
			- error will not display*/
			echo "<a href=\"$path\" target=\"_blank\" ><img src=\"getthumb.php?path=$path&amp;size=$size\" "."style=\"border:1px solid black;margin-top:20px;\" "."alt= \"$value\" /><br />\n";
			//echo "<a href=\"$path\" target=\"_blank\" >";
			echo "Title: $value</a> <br />\n";
		}
		echo "</div><br />";
	?>
</body>
</html>