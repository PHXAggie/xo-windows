<?php
class MaintenanceModel extends Model{
	public function Index(){
		return;
	}



	public function Builders(){
		$this->query('SELECT * FROM tbl_buildercustomer ORDER BY builder_name ASC');
		$rows = $this->resultSet();
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		$this->execute();
		return $rows;
	}

	public function buildersAdd(){
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

		if($post['addBuilder']){
			if($post['builderName'] == ''){
				Messages::setMsg('Please Fill In All Fields', 'error');
				return;
			}
// try catch to stop duplicate entries?
			$this->query('INSERT INTO tbl_buildercustomer (builder_name,updated_by) VALUES (:buildername,:updatedBy)');
			$this->bind(':buildername', strtoupper($post['builderName']));
			$this->bind(':updatedBy', strtouppoer('lking'));
			//$this->bind(':user_id', $_SESSION['user_data']['user_id']);
			$this->execute();
			if($this->lastInsertId()){
				header('Location: '.ROOT_URL.'maintenance\builders');
			} else {
				header('Location: '.ROOT_URL.'maintenance\builders');
			}
		}
		return;
	}

	public function Subdivisions(){
		$this->query('SELECT * FROM tbl_subdivision ORDER BY subdivision_name ASC');
		$rows = $this->resultSet();
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		$this->execute();
		return $rows;
	}
}


?>