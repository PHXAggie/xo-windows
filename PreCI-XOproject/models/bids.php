<?php
class BidsModel extends Model{

	public function Index(){
		//$this->query('SELECT * FROM tbl_bid ORDER BY tbl_bid_id ASC');
		$this->query("SELECT
				date_format(Bids.create_date, '%m/%d/%Y') as 'Date',
				B.builder_name as 'Builder',
				S.subdivision_name as 'Subdivision',
				Bids.plan as 'Plan',
				U.po_abbreviation as 'Branch',
				S.city as 'City',
				S.state as 'State',
				Bids.updated_by as 'Prepped'

				FROM tbl_bid Bids

				JOIN tbl_subdivision S on
					S.subdivision_id = Bids.tbl_subdivision_subdivision_id
				    
				JOIN tbl_buildercustomer B on
					B.builder_id = S.tbl_buildercustomer_builder_id
				    
				JOIN tbl_xo_branch U on
					U.xo_branch_id = S.tbl_xo_branch_xo_branch_id

				ORDER BY Date DESC LIMIT 10");
		$rows = $this->resultSet();
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		$this->execute();
		return $rows;
	}

	public function bidsAdd(){
	    $this->query("SELECT * FROM tbl_buildercustomer");  // Getting the data from Mysql table for first list box
	    $builderQuery = $this->resultSet();
	    $this->execute();
	    return $builderQuery;
	}

	public function subdivisionList($query){
		$this->query($query);
	   	$subdivisionQuery = $this->resultSet();
	    $this->execute();
	    return $subdivisionQuery;
    }

}    

?>