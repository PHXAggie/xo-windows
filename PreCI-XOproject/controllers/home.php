<?php
class Home extends Controller{
	protected function Index(){
		$viewmodel = new HomeModel();
// print_r($viewmodel); //HomeModel Object ( [dbh:protected] => PDO Object ( ) [stmt:protected] => )
		$this->returnView($viewmodel->Index(), true);  // if false, no main.php html
// print_r($this->returnView($viewmodel->Index(), true)); //the call to Home/Index fires the entire fuction Index(), inside the returnView the $view is created.
	}
}

?>