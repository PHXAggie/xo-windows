<?php
abstract class Controller{
	protected $request;
	protected $action;

	public function __construct($action, $request){
		$this->action = $action; //Index
		$this->request = $request; //$_GET
	}

	public function executeAction(){
// print_r($this->{$this->action}());
		return $this->{$this->action}();  // return $action?
	}

	protected function returnView($viewmodel, $fullview){
		$view = 'views/' . get_class($this) . '/' . $this->action . '.php';
		if($fullview){ // $view is created so when views/main.php fires, $view is set and can populate the require($view) on the views/main.php page
			require('views/main.php');
		} else {
			require($view);
		}
	}
}
?>