<html>
  <head>
  	<title>XO</title>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>assets/js/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo ROOT_PATH; ?>assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
  	<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>assets/css/bootstrap.min.css">
  	<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>assets/css/font-awesome.css">
    <link rel="icon" href="<?php echo ROOT_PATH; ?>assets/images/favicon.ico" type="image/x-icon">
  </head>

  <body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary sticky-top">
      <a class="navbar-brand" href="<?php echo ROOT_URL; ?>">XO Windows</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo ROOT_URL; ?>">Home<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo ROOT_URL; ?>bids">Bids</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo ROOT_URL; ?>starts">Starts</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Maintenance
          </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <h6 class="dropdown-header">Maintenance</h6>
                <a class="dropdown-item" href="<?php echo ROOT_URL; ?>maintenance\builders">Builders</a>
                <a class="dropdown-item" href="<?php echo ROOT_URL; ?>maintenance\installtypes">Install Types</a>
                <a class="dropdown-item" href="<?php echo ROOT_URL; ?>maintenance\manufacturers">Manufacturers</a>
                <a class="dropdown-item" href="<?php echo ROOT_URL; ?>maintenance\subdivisions">Subdivisions</a>
                <a class="dropdown-item" href="<?php echo ROOT_URL; ?>windowtypes">Window Types</a>


                <a class="dropdown-item" href="<?php echo ROOT_URL; ?>installationtypes
                ">Installation Types</a>
                <a class="dropdown-item" href="<?php echo ROOT_URL; ?>manufacturers">Manufacturers</a>
              <div class="dropdown-divider"></div>



                <a class="dropdown-item" href="<?php echo ROOT_URL; ?>xousers">XO Users</a>
              </div>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container">

      <div class="row">
        <?php Messages::display(); ?>
      </div>


<?php echo Maintenance::numberIncrease(5); ?>


      <div class="row">
        <?php require($view); ?>
      </div>

    </div>
<?php
require('views/footer.php');
?>