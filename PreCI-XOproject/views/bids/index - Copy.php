<div class="text-center">
	<h1>Bids Maintenance&nbsp;&nbsp;&nbsp;<a class="btn btn-primary" href="<?php echo ROOT_PATH; ?>bids\bidsAddbuilder">Add New Bid</a></h1>

	<?php if(empty($viewmodel)){ ?>
		<h4>Recent Bids</h4>
			<?php echo 'No Records found!'; ?>
	<?php } else { ?>
		<h4>Recent Bids</h4>
			<div class="row">
				<div class="col">Date</div>
				<div class="col">Builder</div>
				<div class="col-3">Subdivision</div>
				<div class="col">Plan</div>
				<div class="col">Branch</div>
				<div class="col">City</div>
				<div class="col">State</div>
				<div class="col">Bidder</div>
			</div>
		<?php foreach($viewmodel as $item) : ?>
			<div class="text-left">
				<div class="container">
					<div class="row">
						<div class="col">
							<?php echo $item['Date']; ?>
						</div>
						<div class="col">
							<?php echo $item['Builder']; ?>
						</div>
						<div class="col-3">
							<?php echo $item['Subdivision']; ?>
						</div>
						<div class="col">
							<?php echo $item['Plan']; ?>
						</div>
						<div class="col">
							<?php echo $item['Branch']; ?>
						</div>
						<div class="col">
							<?php echo $item['City']; ?>
						</div>
						<div class="col">
							<?php echo $item['State']; ?>
						</div>
						<div class="col">
							<?php echo $item['Prepped']; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	<?php } ?>
</div>