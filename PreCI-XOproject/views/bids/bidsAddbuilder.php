<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Add New Bid</h3>
  </div>
  <div class="panel-body">
    <form> <!-- <?php $_SERVER['PHP_SELF']; ?> -->
    	<div class="form-group">
    		<label for="bldr">Select Builder</label>
        <select class="form-control" id="bldr" name="bldr">
          <?php
        foreach($rows as $builderTable){
        echo $builderTable['builder_id'].$builderTable['builder_name'].'</br>';
              }
          ?>
        </select>
      </div>
      <div class="form-group">
        <label for="subd">Select Subdivision</label>
        <select class="form-control" id="subd" name="subd" disabled></select>
      </div>
      <div class="form-group">
        <label for="plan">Select Plan</label>
        <select class="form-control" id="plan" name="plan" disabled></select>
      </div>

    	<input class="btn btn-primary" name="addBid" type="submit" value="Offline" />
    	<a class="btn btn-danger" href="<?php echo ROOT_PATH; ?>bids">Cancel</a>
    </form>
  </div>
</div>