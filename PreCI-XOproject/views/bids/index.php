<div class="text-center">
	<h1>Bids Maintenance&nbsp;&nbsp;&nbsp;<a class="btn btn-primary" href="<?php echo ROOT_PATH; ?>bids\bidsAdd">Add New Bid</a></h1>
	<div class="container">
		<?php if(empty($viewmodel)){ ?>
			<h4>Recent Bids</h4>
				<?php echo 'No Records found!'; ?>
		<?php } else { ?>
			<h4>Recent Bids</h4>
				<table>
					<tr>
						<th>Date</th>
						<th>Builder</th>
						<th>Subdivision</th>
						<th>Plan</th>
						<th>Branch</th>
						<th>City</th>
						<th>State</th>
						<th>Bidder</th>
					</tr>
			<?php foreach($viewmodel as $item) : ?>
					<tr>
						<td><?php echo $item['Date']; ?></td>
						<td><?php echo $item['Builder']; ?></td>
						<td><?php echo $item['Subdivision']; ?></td>
						<td><?php echo $item['Plan']; ?></td>
						<td><?php echo $item['Branch']; ?></td>
						<td><?php echo $item['City']; ?></td>
						<td><?php echo $item['State']; ?></td>
						<td><?php echo $item['Prepped']; ?></td>
					</tr>
			<?php endforeach; ?>
				</table>
		<?php } ?>
	</div>
</div>