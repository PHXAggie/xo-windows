<?php

  echo "this is views/bidsAdd";




    echo "<div class='panel panel-default'>";
        echo "<div class='panel-heading'>";
          echo "<h3 class='panel-title'>Add New Bid (models/bids.php)</h3>";
        echo "</div>";
      echo "<div class='panel-body'>";

                //viewmodel results in isset working but not strlen
                if(isset($viewmodel)){ // and strlen($builder) > 0){  //check if builder is selected
                  echo "<option>builder IS SET</option>";
                  $subdivisionQuery = "SELECT subdivision_id,subdivision_name FROM tbl_subdivision WHERE tbl_buildercustomer_builder_id='146' ORDER BY subdivision_name";
                }else{
                  echo "<option>builder NOT SET</option>";
                  $subdivisionQuery = "SELECT subdivision_id,subdivision_name FROM tbl_subdivision ORDER BY subdivision_name ASC";
                }


        echo "<form method=post name=f1 action=''>"; // form processing page
        //echo "<form method=post name=f1 action=''>"; // location  Example: action=dd-check.php
            // Builder dropdown selection - START
          echo "<div class='form-group'>";
            echo "<select class='form-control' name='builder' onchange=\"reload(this.form)\"><option value=''>Select one</option>"; // changing builder reloads the form, nice!
              foreach ($viewmodel as $builderItem) {
                  if($builderItem['builder_id']==@$builder){ //@ sign supresses error messages
                    echo "<option selected value='$builderItem[builder_id]'>$builderItem[builder_name]</option>"."<BR>";
                  }else{
                    echo  "<option value='$builderItem[builder_id]'>$builderItem[builder_name]</option>";
                  }
              }
            echo "</select>";
            // Builder dropdown selection - END

            // Subdivision dropdown selection - START
            echo "<select class='form-control' name='subdivision'><option value=''>Select one</option>";

              foreach ($anothermodel::subdivisionList($subdivisionQuery) as $subdivisionItem) {
                echo  "<option value='$subdivisionItem[subdivision_id]'>$subdivisionItem[subdivision_name]</option>";
              }
            echo "</select>";
            // Subdivision dropdown selection - END
            echo "Plan: <input type='text' name='planSearch' />";
          echo "</div>";
          echo "<input type=submit value=Submit>"; // will send form data to action=dd-check.php page
          echo "</div>";
        echo "</form>";
      echo "</div>";
    echo "</div>";

?>