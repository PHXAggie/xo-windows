<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Add Builder</h3>
  </div>
  <div class="panel-body">
    <form method="post" action=""> <!-- <?php $_SERVER['PHP_SELF']; ?> -->
    	<div class="form-group">
    		<label>Builder Name</label>
    		<input type="text" name="builderName" class="form-control" />
    	</div>

    	<input class="btn btn-primary" name="addBuilder" type="submit" value="Add Builder" />
    	<a class="btn btn-danger" href="<?php echo ROOT_PATH; ?>maintenance\builders">Cancel</a>
    </form>
  </div>
</div>