<div class="text-center">
	<h1>Builders Maintenance
		<a class="btn btn-primary" href="<?php echo ROOT_PATH; ?>maintenance\buildersAdd">Add Builder</a></h1>

	<?php foreach($viewmodel as $item) : ?>
		<div class="text-left">
			<div class="container">
				<div class="row">
					<div class="col-md-auto order-1">
						<a class="builder" href="#builderEditModal" data-toggle="modal" data-builder-id="<?php echo $item['builder_id']; ?>"data-builder-name="<?php echo $item['builder_name']; ?>"
                        data-update-date="<?php echo $item['update_date']; ?>" data-update-by="<?php echo $item['updated_by']; ?>" data-create-date="<?php echo $item['create_date']; ?>"><?php echo $item['builder_name']; ?></a>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>

    <!-- Builder Details Modal -->
    <div class="modal fade" id="builderEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Builder Details</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <fieldset class="form-group">
                            <h4 id="builderName"></h4>
                        <!--
                            <label for="builderName">Builder Name</label>
                            <input type="text" class="form-control" id="builderName">
                        -->
                        </fieldset>
                        <fieldset class="form-group">
                            <p id="updateDateBy"></p>
                        <!--
                            <label><strong>Last updated:</strong> <p id="updateDate"><?php echo $item['update_date']; ?></p> <strong>BY</strong> <p id="updateBy"><?php echo $item['updated_by']; ?></p></label>
                        -->
                        </fieldset>
                        <fieldset class="form-group">
                            <p id="createDate"></p>
                        <!--
                            <label><strong>Created: </strong><?php echo $item['create_date']; ?></label>
                        -->
                        </fieldset>
                    <!--
                        <fieldset class="form-group">
                            <p id="builderId"></p>
                        </fieldset>
                    -->
                    </form>
                </div>

                <div class="modal-footer">
                <!--
                    <button type="button" id="builderSaveChangeButton" class="btn btn-primary">Save Changes</button>
                    <input class="btn btn-primary" name="editBuilder" type="submit" value="Save Changes" />
                -->
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>