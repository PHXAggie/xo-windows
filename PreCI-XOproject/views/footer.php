	<footer class="footer">
		<div class="container">
			<p>&copy; XO Windows, LLC 2018</p>
		</div>
	</footer>

    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>assets/js/jquery-3.3.1.js"></script>

    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>assets/js/bootstrap.js"></script>

	<script>
		// Show Builder Record popup - START
		$(document).ready(function(){
			$(".builder").click(function(){
				$("#builderName").html("<strong>" + $(this).data('builder-name') + "</strong>");
				$("#updateDateBy").html("<strong>Last updated:</strong> " + $(this).data('update-date') + " <strong>BY</strong> " + $(this).data('update-by'));
				$("#createDate").html("Created: " + $(this).data('create-date'));
				//$("#builderId").html("db ID: " + $(this).data('builder-id'));
				$('#builderEditModal').modal('show');
			});
		// Show Builder Record popup - END

		function reload(form){
				var val = form.builder.options[form.builder.options.selectedIndex].value;
				self.location = '../models/bids.php?builder=' + val ;
			}

		function disableselect(){
				<?php
					if(isset($builder) and strlen($builder) > 0){
						echo "document.f1.subdivision.disabled = false;";
					}else{
						echo "document.f1.subdivision.disabled = true;";
					}
				?>
			}

		});
	</script>

  </body>
</html>