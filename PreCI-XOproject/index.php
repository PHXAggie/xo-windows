<?php
// Start Session
session_start();

// Include Config
require('config.php');

require('classes/Messages.php');
require('classes/Bootstrap.php');
require('classes/Controller.php');
require('classes/Model.php');

require('controllers/home.php');
require('controllers/maintenance.php');
require('controllers/bids.php');

require('models/HomeModel.php');
require('models/MaintenanceModel.php');
require('models/BidsModel.php');


$bootstrap = new Bootstrap($_GET); // not the css but the process of bootstrapping (a new request received from the web)
// print_r($bootstrap); // generates the following
// Bootstrap Object ( [controller:Bootstrap:private] => home [action:Bootstrap:private] => index [request:Bootstrap:private] => Array ( [controller] => home [action] => index [id] => ) )

$controller = $bootstrap->createController(); // method in Bootstrap.php
// print_r($controller); // generates the following
// Home Object ( [request:protected] => Array ( [controller] => home [action] => index [id] => ) [action:protected] => index )

if($controller){
	$controller->executeAction(); // method in Controller.php  //received new Controller(Index,$_GET) from Bootstrap.php, now send to executeAction in Controller.php
}
// the returnView method of Controller.php sets the $view variable and is part of the Index method.
// how does index make its way to main.php? so that views? can be displayed?
?>