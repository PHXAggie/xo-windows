DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Count_ArrayV1mod`(IN optionsArray longtext)
begin

	SET @optionCall = CONCAT("
		SELECT
			BW.tbl_bid_tbl_bid_id,
			BW.mfg,
			BW.series,
			BW.window_details,
			BW.window_location,
			OW.tbl_options_option_id,
			OW.tbl_bid_windows_bid_windows_id,
			OW.add_remove_action
			FROM
				tbl_bid_windows BW
			LEFT JOIN
				tbl_options_has_windows_new OW on
					OW.tbl_bid_windows_bid_windows_id = BW.bid_windows_id -- up to here gives me 45 records includes NULLs
					-- where OW.tbl_options_option_id is NULL -- in every option regardless of those chosen
			LEFT JOIN
				tbl_options O on
					O.option_id = OW.tbl_options_option_id
		WHERE
		--     BW.tbl_bid_tbl_bid_id = 1 AND OW.tbl_options_option_id in (1,2,6,10,14) AND add_remove_action <> 0) OR tbl_options_option_id is NULL
			BW.tbl_bid_tbl_bid_id = 1 AND (OW.tbl_options_option_id IN (", optionsArray ,") AND add_remove_action <> 0) OR tbl_options_option_id is NULL
		GROUP BY
		  BW.tbl_bid_tbl_bid_id, OW.tbl_options_option_id
		  -- HAVING COUNT(OW.tbl_bid_windows_bid_windows_id) IN (NULL,1,2,(CHAR_LENGTH('", optionsArray ,"') - CHAR_LENGTH(REPLACE('", optionsArray ,"', ',', '')) + 1))
        ");
	PREPARE stmt1 FROM @optionCall;
	EXECUTE stmt1;
    DEALLOCATE PREPARE stmt1;
		-- HAVING COUNT(O.option_id) IN (2,(CHAR_LENGTH(", array , ") - CHAR_LENGTH(REPLACE(", array , ", ',', '')) + 1))
end $$
	/*
	SELECT (CHAR_LENGTH(array) - CHAR_LENGTH(REPLACE(array, ',', '')) + 1) as Fields;
	*/
DELIMITER ;

DELIMITER $$