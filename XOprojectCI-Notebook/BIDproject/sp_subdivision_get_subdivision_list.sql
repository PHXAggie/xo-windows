drop procedure if exists sp_subdivision_get_subdivision_list;

delimiter //

	create procedure sp_subdivision_get_subdivision_list()
        
BEGIN
	SELECT s.subdivision_id,
    s.subdivision_name,
    s.update_date,
	s.updated_by,
	s.create_date,
	s.city,
	s.state,
	s.zip,
	branch.location,
	builder.builder_name, 
	zone.zone_name, 
	s.tbl_status_tbl_status_id, 
	state.tbl_status_name
    
    from tbl_subdivision s
    
    join tbl_buildercustomer builder 
		on s.tbl_buildercustomer_builder_id = builder.builder_id
        
	join tbl_xo_branch branch
		on s.tbl_xo_branch_xo_branch_id = branch.xo_branch_id
        
	join tbl_zone zone
		on s.tbl_zone_zone_id = zone.zone_id
        
	join tbl_status state 
		on s.tbl_status_tbl_status_id = state.tbl_status_id;
END//

delimiter ;