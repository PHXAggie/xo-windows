DROP TEMPORARY TABLE IF EXISTS temp_allWindows;
CREATE TEMPORARY TABLE temp_allWindows as (
Select
-- B.tbl_bid_id,
BW.tbl_bid_tbl_bid_id,
BW.mfg,
BW.series,
BW.window_details,
BW.window_location,
BW.bid_windows_id,
OW.tbl_options_option_id,
OW.tbl_bid_windows_bid_windows_id,
OW.add_remove_action
FROM
	tbl_bid_windows BW
LEFT JOIN
	tbl_options_has_windows_new OW on
		OW.tbl_bid_windows_bid_windows_id = BW.bid_windows_id -- up to here gives me 45 records includes NULLs
        -- where OW.tbl_options_option_id is NULL -- in every option regardless of those chosen
LEFT JOIN
	tbl_options O on
		O.option_id = OW.tbl_options_option_id
WHERE
	BW.tbl_bid_tbl_bid_id = 1
    -- and find_in_set(tbl_options_option_id, '10,30,50,70') = 0 and add_remove_action <> 1 -- not removed by any selected options
    and find_in_set(tbl_options_option_id, '10,30,50,70') is null -- all windows that are in EVERY option
    -- or (find_in_set(tbl_options_option_id, '10,30,50,70') > 1 and add_remove_action <> 0))
    
    
    -- or (find_in_set(tbl_options_option_id, '10,30,50,70') = 0 and add_remove_action <> 0)
    -- group by bid_windows_id
	-- HAVING COUNT(tbl_options_option_id) in (1,(CHAR_LENGTH('10,30,50,70') - CHAR_LENGTH(REPLACE('10,30,50,70', ',', ''))+ 1))
	/* and
    (tbl_options_option_id in (10,30,50,70) and add_remove_action <> 0) OR 
	tbl_options_option_id is null */
    );

select * from temp_allWindows

/*
having count(*) > 2
-- where (tbl_options_option_id in (10,30,50,70) and add_remove_action <> 0) OR 
-- tbl_options_option_id is null
-- HAVING COUNT(tbl_bid_tbl_bid_id) in (1)
-- HAVING COUNT(tbl_bid_windows_bid_windows_id) in (2,(CHAR_LENGTH('10,30,50,70') - CHAR_LENGTH(REPLACE('10,30,50,70', ',', ''))+ 1))
-- or add_remove_action = 1;*/