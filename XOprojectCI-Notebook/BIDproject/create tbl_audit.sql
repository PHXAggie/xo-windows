CREATE TABLE `tbl_audit` (
	`audit_id`	int	NOT NULL AUTO_INCREMENT,
    `xo_po_id`	int	NOT NULL,
    `created_date`	DATETIME	NOT NULL,
    `message`	TEXT	NOT NULL,
    `code`	INT	NOT NULL,
    PRIMARY KEY (`audit_id`),
    KEY `idx_audit_xo_po_id` (`xo_po_id`)
    );