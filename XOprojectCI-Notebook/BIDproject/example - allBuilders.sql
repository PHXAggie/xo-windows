SELECT b.builder_id,
b.builder_name,
b.update_date,
b.updated_by,
b.create_date,
b.tbl_status_tbl_status_id,
status.tbl_status_name
from tbl_buildercustomer b
join tbl_status status on b.tbl_status_tbl_status_id = status.tbl_status_id
order by b.builder_name ASC