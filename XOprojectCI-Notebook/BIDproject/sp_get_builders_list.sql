delimiter &&
create procedure sp_get_builders_list()

begin
	select b.builder_id,
    b.builder_name,
    b.update_date,
    b.updated_by,
    b.create_date,
    b.tbl_status_tbl_status_id,
    s.tbl_status_name
    
    from tbl_buildercustomer b
    
    join tbl_status s
    on b.tbl_status_tbl_status_id = s.tbl_status_id;
end &&

delimiter //