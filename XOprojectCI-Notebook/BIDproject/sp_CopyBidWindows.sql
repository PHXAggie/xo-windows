drop procedure if exists sp_Copy_Bid_Windows;

delimiter //

	create procedure sp_Copy_Bid_Windows(
		existingBidID_ INT,
        newBidID_ INT
		)
        
begin
    -- as each window is processed, lookup in tbl_options_has_windows_new and insert
    -- new line with the new window_id and option_id
    declare colINSERT_ text default '';
    declare colSELECT_ text default '';
    
    select
		concat( '(', group_concat(column_name), ',tbl_bid_tbl_bid_id)'),
        concat( group_concat(column_name), ',', newBidID_ )
        into colINSERT_, colSELECT_
        from information_schema.columns where table_schema = 'mydb'
        and table_name = 'tbl_bid_windows'
        and column_name not in ('bid_windows_id', 'tbl_bid_tbl_bid_id');
       
	set @sql_windows = concat(
    'insert into tbl_bid_windows ', colINSERT_,
    ' select ' 					  , colSELECT_,
    ' from tbl_bid_windows where tbl_bid_tbl_bid_id = ', existingBidID_, ';'
    );
    
	prepare stmt_ from @sql_windows;
    execute stmt_;
    set @sql_windows = '';

end//

delimiter ;