select
	M.manufacturer_id,
	M.manufacturer_name,
    mfgS.idtbl_mfg_series,
 	mfgS.series,
    WT.idtbl_window_type,
     WT.window_type_abbr,
--    mfgS.special_instruction,
    WD.idtbl_window_dimensions as callout,
    mfgP.base_amt,
    mfgP.e366_amt,
    mfgP.e340_amt,
    mfgP.tempered_amt,
    mfgP.grid_amt
    
from

    tbl_manufacturer M
    
join
	tbl_mfg_has_series mfgHS on mfgHS.fk_mfg_id = M.manufacturer_id
    
join
	tbl_mfg_series mfgS on mfgHS.fk_idtbl_mfg_series = mfgS.idtbl_mfg_series
    
join
	tbl_window_type WT on mfgS.fk_tbl_window_type_idtbl_window_type = WT.idtbl_window_type
    
join
	tbl_mfg_has_window_dimensions mfgHWD on mfgS.idtbl_mfg_series = mfgHWD.fk_tbl_mfg_series_idtbl_mfg_series
    
join
	tbl_window_dimensions WD on mfgHWD.fk_tbl_window_dimensions_idtbl_window_dimensions = WD.idtbl_window_dimensions
    
left join
	tbl_mfg_purchasing mfgP on mfgHWD.fk_tbl_mfg_purchasing_idtbl_mfg_purchasing = mfgP.idtbl_mfg_purchasing
    
order by
	manufacturer_name asc, series asc, callout asc
