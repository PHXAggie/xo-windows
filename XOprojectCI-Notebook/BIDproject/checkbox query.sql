-- SELECT * FROM mydb.tbl_buildercustomer;
select  
		oHw.tbl_bid_windows_bid_windows_id,
		O.option_id,
		-- BW.bid_windows_id,
		-- BW.mfg,
		-- BW.series,
		-- BW.grouping,
		BW.window_details,
		BW.window_location
		-- O.option_id,
		-- O.option_name
		-- O.planOptionCode,
		-- O.cost
from
	mydb.tbl_options_has_windows oHw
join
	mydb.tbl_bid_windows BW on
    BW.bid_windows_id = oHw.tbl_bid_windows_bid_windows_id
join
 	mydb.tbl_options O on
    O.option_id = oHw.tbl_options_option_id
where
	mydb.O.tbl_bid_tbl_bid_id = 1