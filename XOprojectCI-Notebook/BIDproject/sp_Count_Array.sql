CREATE DEFINER=`root`@`localhost` PROCEDURE `Count_Array`(array longtext)
begin
	set @sql = concat("
		SELECT
			B.tbl_bid_id,
			BW.mfg,
			BW.series,
			BW.window_details,
            BW.window_location
		FROM
			tbl_bid_windows BW
		JOIN
			tbl_options_has_windows OW on
				OW.tbl_bid_windows_bid_windows_id = BW.bid_windows_id
		JOIN
			tbl_options O on
				O.option_id = OW.tbl_options_option_id
		JOIN
			tbl_bid B on
				B.tbl_bid_id = O.tbl_bid_tbl_bid_id
		WHERE
		--     B.tbl_bid_id = 1 AND OW.tbl_options_option_id in (1,2,6,10,14)
			B.tbl_bid_id = 1 AND OW.tbl_options_option_id IN (", array , ")
		GROUP BY
			OW.tbl_bid_windows_bid_windows_id
		HAVING COUNT(O.option_id) IN (2,(CHAR_LENGTH('", array ,"') - CHAR_LENGTH(REPLACE('", array ,"', ',', '')) + 1))
	");
	prepare stmt from @sql;
	execute stmt;
		-- HAVING COUNT(O.option_id) IN (2,(CHAR_LENGTH(", array , ") - CHAR_LENGTH(REPLACE(", array , ", ',', '')) + 1))
	/*
	SELECT (CHAR_LENGTH(array) - CHAR_LENGTH(REPLACE(array, ',', '')) + 1) as Fields;
	*/
end