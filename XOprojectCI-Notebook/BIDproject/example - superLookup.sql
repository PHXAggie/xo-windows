select
    B.Name as Builder,
    Subs.Name as Subdivision
from
	tbl_jobsites JS
join
    tbl_supers Sups on
		JS.fk_supers = Sups.ID
join
    tbl_subdivisions Subs on
    	Subs.ID = Sups.fk_subdivision
join
	tbl_builders B on
		B.ID = Sups.fk_builders;
-- where Sups.ID = 280 AND JS.Lot = 3011;