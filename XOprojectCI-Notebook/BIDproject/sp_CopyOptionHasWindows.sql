drop procedure if exists sp_Copy_Option_Has_Windows;

delimiter //

	create procedure sp_Copy_Option_Has_Windows(
		existingBidWindowID_ INT,
        newBidWindowID_ INT,
        existingOptionsOID_ INT,
        newOptionsOID_ INT,
        
        
        newBidID_ INT
		)
        
begin
    
    declare colINSERT_ text default '';
    declare colSELECT_ text default '';
    
    select
		concat( '(', group_concat(column_name), ',tbl_bid_windows_bid_windows_id,
												  tbl_options_option_id)'),
        concat( group_concat(column_name), ',', newBidWindowsBWID_, ',',
												newOptionsOID_ )
        into colINSERT_, colSELECT_
        from information_schema.columns where table_schema = 'mydb'
        and table_name = 'tbl_options_has_windows_new'
        and column_name not in ('tbl_bid_windows_bid_windows_id', 'tbl_options_option_id');
       
	set @sql_windows = concat(
    'insert into tbl_options_has_windows_new ', colINSERT_,
    ' select ' 					  , colSELECT_,
    ' from tbl_bid_windows BW ',
    ' join tbl_options O on O.option_id = BW.tbl_bid_tbl_bid_id ',
    ' join tbl_bid B on B.tbl_bid_id = BW.tbl_bid_tbl_bid_id ',
    ' where BW.tbl_bid_tbl_bid_id = ', existingBidWindowID_, ' and ',
    ' O.tbl_bid_tbl_bid_id = ', existingOptionsOID_ 
    );
    
	prepare stmt_ from @sql_windows;
    execute stmt_;
    set @sql_windows = '';

end//

delimiter ;