drop procedure if exists sp_Copy_Bid_Options;

delimiter //

	create procedure sp_Copy_Bid_Options(
		existingBidID_ INT,
        newBidID_ INT
		)
        
begin
    
    declare colINSERT_ text default '';
    declare colSELECT_ text default '';
    
    select
		concat( '(', group_concat(column_name), ',tbl_bid_tbl_bid_id)'),
        concat( group_concat(column_name), ',', newBidID_ )
        into colINSERT_, colSELECT_
        from information_schema.columns where table_schema = 'mydb'
        and table_name = 'tbl_options'
        and column_name not in ('option_id', 'tbl_bid_tbl_bid_id');
       
	set @sql_options = concat(
    'insert into tbl_options ', colINSERT_,
    ' select ' 					  , colSELECT_,
    ' from tbl_options where tbl_bid_tbl_bid_id = ', existingBidID_, ';'
    );
    
	prepare stmt_ from @sql_options;
    execute stmt_;
    set @sql_options = '';
    
    Call sp_Copy_Bid_Windows(existingBidID_,newBidID_);

end//

delimiter ;