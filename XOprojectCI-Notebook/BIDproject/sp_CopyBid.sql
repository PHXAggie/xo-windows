 drop procedure if exists sp_Copy_Bid;

delimiter //

 create procedure sp_Copy_Bid(
	schema_ text,
    table_ text,
    oldelevation_ text,
    newelevation_ text,
    newUpdated_by_ text
    )
    
begin

	declare colINSERT_ text default ''; -- columns needed for INSERTing
    declare colSELECT_ text default ''; -- columns needed for SELECTing
    
-- Find all columns for INSERT and SELECT, respectively.  () contain "insert" cols.
-- Exclude the 'tbl_bid_id' and 'elevation' columns from the column list(s).
	select
		concat( '(', group_concat(column_name), ',elevation, updated_by)'),
        concat( group_concat(column_name), ',"', newelevation_, '","', newUpdated_by_ )
        into colINSERT_, colSELECT_
        from information_schema.columns where table_schema = schema_
		and table_name = table_
        and column_name not in ('tbl_bid_id', 'elevation', 'updated_by');
        
	set @sql = concat(
	'insert into '			, table_, colINSERT_,
	' select '				, colSELECT_,
	'" from '					, table_,
	' where elevation = '	, "'",  oldelevation_, "';"
	);
    
	prepare stmt_ from @sql;
    execute stmt_;
    deallocate prepare stmt_;
    
	select tbl_bid_id into @bidID_ from tbl_bid where elevation = oldelevation_ LIMIT 1;
    
    Call sp_Copy_Bid_Options(@bidID_,last_insert_id());
    
    -- Call sp_Copy_Bid_Windows(@bidID_,last_insert_id());
    
end//

delimiter ;	