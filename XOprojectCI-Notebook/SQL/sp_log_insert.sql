delimiter $$
create procedure `sp_log_insert`(
	tbl_in_builders varchar(45),
    table_action varchar(6),
    field varchar(45),
    tbl_row_no int(11),
    changed_by varchar(11),
    old_value varchar(50),
    new_value varchar(50)
    )

begin
	insert into `tbl_log_builders`(
        `tbl_in_builders`,
        `action`,
        `field`,
        `tbl_row_no`,
        `changed_by`,
        `old_value`,
        `new_value`,
		`changed_date`) 
    values
		(
        tbl_in_builders,
        table_action,
        field,
        tbl_row_no,
        changed_by,
        old_value,
        new_value,
        now());
end$$

delimiter ;