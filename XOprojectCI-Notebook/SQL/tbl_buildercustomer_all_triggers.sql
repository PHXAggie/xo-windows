-- create table builders.tbl_history_buildercustomer like builders.tbl_buildercustomer;

-- alter table builders.tbl_history_buildercustomer modify column builder_id int(11) not null,
-- drop primary key, engine = MyISAM, ADD action varchar(8) default 'insert' FIRST,
-- ADD revision INT(6) NOT NULL AUTO_INCREMENT AFTER action,
-- ADD dt_datetime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER revision,
-- ADD PRIMARY KEY (builder_id, revision);

-- drop trigger if exists builders.tbl_buildercustomer__ai;
-- drop trigger if exists builders.tbl_buildercustomer__au;
-- drop trigger if exists builders.tbl_buildercustomer__bd;

-- create trigger builders.tbl_buildercustomer__ai after insert on builders.tbl_buildercustomer for each row
-- 	insert into builders.tbl_history_buildercustomer select 'insert', NULL, NOW(), d.*
--     from builders.tbl_buildercustomer as d where d.builder_id = NEW.builder_id;
    
-- create trigger builders.tbl_buildercustomer__au after update on builders.tbl_buildercustomer for each row
-- 	insert into builders.tbl_history_buildercustomer select 'update', NULL, NOW(), d.*
--     from builders.tbl_buildercustomer as d where d.builder_id = NEW.builder_id;
    
-- create trigger builders.tbl_buildercustomer__bd before delete on builders.tbl_buildercustomer for each row
-- 	insert into builders.tbl_history_buildercustomer select 'delete', NULL, NOW(), d.*
--     from builders.tbl_buildercustomer as d where d.builder_id = OLD.builder_id;