delimiter ;

drop procedure if exists Date_Totals;

delimiter #

create procedure Date_Totals
(
in p_start_date date,
in p_end_date date
)
begin

select
    date_format(JS.Turn_In_Date, "%m/%d/%Y") as 'Turn_In_Date',
    Branch.Abbreviation as 'Location',
    B.Name as 'Builder',
    Subs.Name as 'Subdivision',
    JS.lot as 'Lot',
    date_format(JS.install_date, "%m/%d/%Y") as 'Install_Date',
    concat(substring(xoRep.Name,1,1), '',substring(xoRep.Name,locate(' ',xoRep.Name)+1,1), '') as 'Turned_In_By',
    JS.Install_Revenue as 'Revenue',
    JS.Sales_Total as 'Sales',
    JS.Confirmation as 'Confirmation',
    JS.TurnIn_Reported as 'TurnIn_Reported'
from
	tbl_jobsites JS
join
    tbl_supers Sups on
		JS.fk_supers = Sups.ID
join
    tbl_subdivisions Subs on
    	Subs.ID = Sups.fk_subdivision
join
	tbl_builders B on
		B.ID = Sups.fk_builders
join
	tbl_repsxo xoRep on
		xoRep.ID = JS.Turned_In_By
join
	tbl_branchxo Branch on
		Branch.ID = Sups.fk_branchxo
where
	JS.Turn_In_Date >= p_start_date and JS.Turn_In_Date <= p_end_date -- and isnull(TurnIn_Reported)
order by
	Location;
end#
delimiter ;

-- call Date_Totals ('2017-04-02','2017-08-02')