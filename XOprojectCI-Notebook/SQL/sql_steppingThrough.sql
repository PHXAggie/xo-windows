select *
from 
tbl_window_dimensions;

select *
from
tbl_window_dimensions_mfg_mods;

select *
from
tbl_dimensions;

select
	tbl_window_dimensions.idtbl_window_dimensions as callout,
    W.inches as 'W(in)',
    H.inches as 'H(in)'
from
tbl_window_dimensions
join
	tbl_dimensions W on tbl_window_dimensions.width_callout = W.callout
join
	tbl_dimensions H on tbl_window_dimensions.height_callout = H.callout;
    
select
	tbl_mfg_series.series,
	tbl_window_dimensions.idtbl_window_dimensions as callout,
    W.inches as 'W(in)',
    H.inches as 'H(in)'
from
	tbl_mfg_has_window_dimensions
join
	tbl_window_dimensions on tbl_mfg_has_window_dimensions.fk_tbl_window_dimensions_idtbl_window_dimensions = tbl_window_dimensions.idtbl_window_dimensions
join
	tbl_dimensions W on tbl_window_dimensions.width_callout = W.callout
join
	tbl_dimensions H on tbl_window_dimensions.height_callout = H.callout
join
	tbl_mfg_series on tbl_mfg_has_window_dimensions.fk_tbl_mfg_series_idtbl_mfg_series = tbl_mfg_series.idtbl_mfg_series
where
	tbl_mfg_has_window_dimensions.fk_tbl_mfg_series_idtbl_mfg_series = 1
order by callout asc;

select
	tbl_mfg_series.series,
	tbl_window_dimensions.idtbl_window_dimensions as callout,
    W.inches as 'W(in)',
    H.inches as 'H(in)',
    -- tbl_window_dimensions_mfg_mods.opening_w,
    -- tbl_window_dimensions_mfg_mods.opening_h,
    W.inches + tbl_window_dimensions_mfg_mods.opening_w as opening_w,
    H.inches + tbl_window_dimensions_mfg_mods.opening_h as opening_h,
    W.inches + fixed_glass_w as glass_w,
    H.inches + fixed_glass_h as glass_h,
    W.inches + screen_w as screen_w,
    H.inches + screen_h as screen_h
from
	tbl_mfg_has_window_dimensions
join
	tbl_window_dimensions on tbl_mfg_has_window_dimensions.fk_tbl_window_dimensions_idtbl_window_dimensions = tbl_window_dimensions.idtbl_window_dimensions
join
	tbl_dimensions W on tbl_window_dimensions.width_callout = W.callout
join
	tbl_dimensions H on tbl_window_dimensions.height_callout = H.callout
join
	tbl_mfg_series on tbl_mfg_has_window_dimensions.fk_tbl_mfg_series_idtbl_mfg_series = tbl_mfg_series.idtbl_mfg_series
join
	tbl_window_dimensions_mfg_mods on tbl_mfg_has_window_dimensions.fk_tbl_win_dims_mfg_mods_idtbl_win_dims_mfg_mods = tbl_window_dimensions_mfg_mods.idtbl_window_dimensions_mfg_mods
-- where
	-- tbl_mfg_has_window_dimensions.fk_tbl_mfg_series_idtbl_mfg_series = 1
order by callout asc;