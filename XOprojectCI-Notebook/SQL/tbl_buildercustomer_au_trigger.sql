CREATE DEFINER=`root`@`localhost` trigger builders.tbl_buildercustomer__au after update on builders.tbl_buildercustomer for each row
	insert into builders.tbl_history_buildercustomer select 'update', NULL, NOW(), d.*
    from builders.tbl_buildercustomer as d where d.builder_id = NEW.builder_id