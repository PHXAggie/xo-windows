CREATE DEFINER = CURRENT_USER TRIGGER `builders`.`tbl_buildercustomer_has_install_types_BEFORE_DELETE` BEFORE DELETE ON `tbl_buildercustomer_has_install_types` FOR EACH ROW
BEGIN
	DECLARE install_type_name_old varchar(45);
    DECLARE xow_user_name varchar(25);
    SET xow_user_name = (SELECT xousers.login from xowindows.tbl_users xousers WHERE xousers.id = old.updated_by);
        
	-- Builder's install type initial insert
		SET install_type_name_old = (SELECT instType.type from builders.tbl_install_types instType where instType.install_types_id = old.tbl_install_types_install_types_id);
		call sp_log_insert(
			'tbl_buildercustomer_has_install_types',
			'removed',
			'tbl_install_types_install_types_id',
			old.tbl_buildercustomer_builder_id,
			xow_user_name,
			'REMOVED',
			install_type_name_old
		);
END
