-- SELECT * FROM builders.tbl_history_buildercustomer;

create view builders_history_changes as
select t2.dt_datetime, t2.action, t1.builder_id as 'builder_id',
if(t1.tbl_status_tbl_status_id = t2.tbl_status_tbl_status_id, t1.tbl_status_tbl_status_id, concat(t1.tbl_status_tbl_status_id, " to ", t2.tbl_status_tbl_status_id)) as cust_statusCREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `builders_history_changes` AS select `t2`.`dt_datetime` AS `dt_datetime`,`t2`.`action` AS `action`,`t1`.`builder_id` AS `builder_id`,if((`t1`.`tbl_status_tbl_status_id` = `t2`.`tbl_status_tbl_status_id`),`t1`.`tbl_status_tbl_status_id`,concat(`t1`.`tbl_status_tbl_status_id`,' to ',`t2`.`tbl_status_tbl_status_id`)) AS `cust_status` from (`tbl_history_buildercustomer` `t1` join `tbl_history_buildercustomer` `t2` on((`t1`.`builder_id` = `t2`.`builder_id`))) where (((`t1`.`revision` = 1) and (`t2`.`revision` = 1)) or (`t2`.`revision` = (`t1`.`revision` + 1))) order by `t1`.`builder_id`,`t2`.`revision`;

from builders.tbl_history_buildercustomer as t1 inner join builders.tbl_history_buildercustomer as t2 on t1.builder_id = t2.builder_id
where (t1.revision = 1 and t2.revision = 1) or t2.revision = t1.revision+1
order by t1.builder_id asc, t2.revision asc;