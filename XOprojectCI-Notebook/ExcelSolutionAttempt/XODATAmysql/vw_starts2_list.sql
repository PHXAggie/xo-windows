CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `xodata`.`vw_starts2_list` AS
    SELECT 
        `js`.`Confirmation` AS `Confirmation`,
        `js`.`PO` AS `PO`,
        DATE_FORMAT(`js`.`Starts_Date`, '%m/%d/%Y') AS `Starts_Date`,
        `sups`.`ID` AS `Supers_ID`,
        `b`.`Name` AS `Builder`,
        `subs`.`Name` AS `Subdivision`,
        `js`.`Lot` AS `Lot`,
        `js`.`Plan` AS `Plan`,
        `js`.`Elevation` AS `Elevation`,
        `js`.`Model` AS `Model`,
        DATE_FORMAT(`js`.`Install_Date`, '%m/%d/%Y') AS `Install_Date`,
        DATE_FORMAT(`js`.`Turn_In_Date`, '%m/%d/%Y') AS `Turn_In_Date`,
        DATE_FORMAT(`js`.`Arrival_Date_AD`, '%m/%d/%Y') AS `AD_Date`,
        DATE_FORMAT(`js`.`Latest_Order_Date`, '%m/%d/%Y') AS `Latest_Order_Date`,
        `js`.`Address` AS `Address`,
        `subs`.`City` AS `City`,
        `subs`.`State` AS `State`,
        `js`.`Super` AS `Super`,
        CONCAT('(',
                LEFT(`js`.`Supers_Phone`, 3),
                ') ',
                SUBSTR(`js`.`Supers_Phone`, 4, 3),
                '-',
                RIGHT(`js`.`Supers_Phone`, 4)) AS `Supers_Number`,
        `sr`.`Name` AS `Starts_Rep`,
        `tib`.`ID` AS `tibID`,
        `tib`.`Name` AS `Turned_In_By`,
        `tiv`.`ID` AS `tivID`,
        `tiv`.`Name` AS `Turn_In_Verification`,
        DATE_FORMAT(`js`.`Order_Date`, '%m/%d/%Y') AS `Order_Date`,
        `js`.`Window_Count` AS `Windows`,
        `js`.`Patio_Door_Count` AS `Patio_Doors`,
        `js`.`Rolling_Wall_Count` AS `Rolling_Walls`,
        `js`.`Total_Units` AS `Tot_Unit`,
        `js`.`Capacity` AS `Cap_Count`,
        `js`.`Install_Revenue` AS `Install_Revenue`,
        `js`.`Base_Contract_Price` AS `Base_Contract_Price`,
        `js`.`Option_Pricing` AS `Option_Pricing`,
        `js`.`Sales_Total` AS `Sell_Price`,
        `js`.`Install_Material_Cost` AS `Install_Material_Cost`,
        `js`.`Install_Labor_Cost` AS `Install_Labor_Cost`,
        `js`.`House_Package_Cost` AS `House_Package_Cost`,
        `mfg`.`manAbbreviation` AS `Manufacturer`,
        `brnch`.`POAbbreviation` AS `XO_Branch`,
        `brnch`.`Location` AS `XO_Location`,
        `inst`.`Type` AS `Install_Type`
    FROM
        (((((((((`xodata`.`tbl_jobsites` `js`
        JOIN `xodata`.`tbl_supers` `sups` ON ((`js`.`fk_supers` = `sups`.`ID`)))
        JOIN `xodata`.`tbl_subdivisions` `subs` ON ((`subs`.`ID` = `sups`.`fk_subdivision`)))
        JOIN `xodata`.`tbl_builders` `b` ON ((`b`.`ID` = `sups`.`fk_builders`)))
        JOIN `xodata`.`tbl_repsxo` `sr` ON ((`sr`.`ID` = `js`.`XORep`)))
        JOIN `xodata`.`tbl_manufacturers` `mfg` ON ((`mfg`.`ID` = `js`.`Manufacturer`)))
        JOIN `xodata`.`tbl_branchxo` `brnch` ON ((`brnch`.`ID` = `js`.`XOBranch`)))
        JOIN `xodata`.`tbl_installtypes` `inst` ON ((`inst`.`ID` = `js`.`InstType`)))
        LEFT JOIN `xodata`.`tbl_repsxo` `tib` ON ((`tib`.`ID` = `js`.`Turned_In_By`)))
        LEFT JOIN `xodata`.`tbl_repsxo` `tiv` ON ((`tiv`.`ID` = `js`.`Verified_By`)))