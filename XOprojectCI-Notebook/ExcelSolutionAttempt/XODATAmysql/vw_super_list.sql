CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `xodata`.`vw_super_list` AS
    SELECT 
        `xodata`.`tbl_supers`.`ID` AS `ID`,
        `xodata`.`tbl_subdivisions`.`Name` AS `Subdivision`,
        `xodata`.`tbl_builders`.`Name` AS `Builder`,
        `xodata`.`tbl_subdivisions`.`City` AS `City`,
        `xodata`.`tbl_subdivisions`.`State` AS `ST`,
        `xodata`.`tbl_supers`.`Name` AS `Super`,
        `xodata`.`tbl_supers`.`Phone_Number` AS `SuperNo`,
        `xodata`.`tbl_repsxo`.`Name` AS `startsRep`,
        `xodata`.`tbl_installtypes`.`Type` AS `installType`,
        `xodata`.`tbl_branchxo`.`POAbbreviation` AS `Loc`,
        `xodata`.`tbl_manufacturers`.`manAbbreviation` AS `Manufacturer`
    FROM
        (`xodata`.`tbl_supers`
        LEFT JOIN (((((`xodata`.`tbl_subdivisions`
        JOIN `xodata`.`tbl_builders`)
        JOIN `xodata`.`tbl_repsxo`)
        JOIN `xodata`.`tbl_installtypes`)
        JOIN `xodata`.`tbl_branchxo`)
        JOIN `xodata`.`tbl_manufacturers`) ON (((`xodata`.`tbl_subdivisions`.`ID` = `xodata`.`tbl_supers`.`fk_subdivision`)
            AND (`xodata`.`tbl_builders`.`ID` = `xodata`.`tbl_supers`.`fk_builders`)
            AND (`xodata`.`tbl_repsxo`.`ID` = `xodata`.`tbl_supers`.`fk_repsxo`)
            AND (`xodata`.`tbl_installtypes`.`ID` = `xodata`.`tbl_supers`.`fk_installtypes`)
            AND (`xodata`.`tbl_branchxo`.`ID` = `xodata`.`tbl_supers`.`fk_branchxo`)
            AND (`xodata`.`tbl_manufacturers`.`ID` = `xodata`.`tbl_supers`.`fk_manufacturers`))))