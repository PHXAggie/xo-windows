CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `xodata`.`vw_super2_list` AS
    SELECT 
        `xodata`.`tbl_supers`.`ID` AS `supID`,
        `xodata`.`tbl_subdivisions`.`ID` AS `subID`,
        `xodata`.`tbl_subdivisions`.`Name` AS `subdivision`,
        `xodata`.`tbl_builders`.`ID` AS `bldrID`,
        `xodata`.`tbl_builders`.`Name` AS `builder`,
        `xodata`.`tbl_subdivisions`.`City` AS `City`,
        `xodata`.`tbl_subdivisions`.`State` AS `ST`,
        `xodata`.`tbl_installtypes`.`ID` AS `instTypID`,
        `xodata`.`tbl_installtypes`.`Type` AS `Type`,
        `xodata`.`tbl_supers`.`Name` AS `super`,
        `xodata`.`tbl_supers`.`Phone_Number` AS `phone`,
        `xodata`.`tbl_manufacturers`.`ID` AS `mfgID`,
        `xodata`.`tbl_manufacturers`.`manAbbreviation` AS `MfgAbr`,
        `xodata`.`tbl_manufacturers`.`Name` AS `Mfg`,
        `xodata`.`tbl_repsxo`.`ID` AS `repID`,
        `xodata`.`tbl_repsxo`.`Name` AS `Rep`,
        `xodata`.`tbl_branchxo`.`ID` AS `brnchID`,
        `xodata`.`tbl_branchxo`.`POAbbreviation` AS `brnchPO`,
        `xodata`.`tbl_branchxo`.`Location` AS `Branch`
    FROM
        ((((((`xodata`.`tbl_supers`
        JOIN `xodata`.`tbl_builders`)
        JOIN `xodata`.`tbl_subdivisions`)
        JOIN `xodata`.`tbl_installtypes`)
        JOIN `xodata`.`tbl_manufacturers`)
        JOIN `xodata`.`tbl_repsxo`)
        JOIN `xodata`.`tbl_branchxo`)
    WHERE
        ((`xodata`.`tbl_subdivisions`.`ID` = `xodata`.`tbl_supers`.`fk_subdivision`)
            AND (`xodata`.`tbl_builders`.`ID` = `xodata`.`tbl_supers`.`fk_builders`)
            AND (`xodata`.`tbl_installtypes`.`ID` = `xodata`.`tbl_supers`.`fk_installtypes`)
            AND (`xodata`.`tbl_manufacturers`.`ID` = `xodata`.`tbl_supers`.`fk_manufacturers`)
            AND (`xodata`.`tbl_repsxo`.`ID` = `xodata`.`tbl_supers`.`fk_repsxo`)
            AND (`xodata`.`tbl_branchxo`.`ID` = `xodata`.`tbl_supers`.`fk_branchxo`))
    ORDER BY `xodata`.`tbl_subdivisions`.`Name`