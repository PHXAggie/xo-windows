CREATE DEFINER=`root`@`localhost` PROCEDURE `confPOIncrement`()
BEGIN
	UPDATE tbl_settings
    SET Confirmation = Confirmation + 1,
    PO = PO + 1
    WHERE id = 100;
END