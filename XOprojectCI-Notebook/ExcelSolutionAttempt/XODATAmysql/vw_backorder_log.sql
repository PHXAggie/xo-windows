CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `xodata`.`vw_backorder_log` AS
    SELECT 
        `bo`.`ID` AS `Backorder_ID`,
        `bo`.`fk_confirmation` AS `Confirmation`,
        DATE_FORMAT(`bo`.`Date`, '%m/%d/%Y') AS `Date`,
        `js`.`PO` AS `PO_Number`,
        `b`.`Name` AS `Builder`,
        `subs`.`Name` AS `Subdivision`,
        `js`.`Lot` AS `Lot`,
        `bo`.`Qty` AS `Qty`,
        `bo`.`Item` AS `Item`,
        `bo`.`Reason` AS `Reason`,
        `bo`.`fk_issuesro` AS `RO_IssueID`,
        `ro`.`Issue` AS `RO_Issue`,
        DATE_FORMAT(`bo`.`Date_to_Research`, '%m/%d/%Y') AS `Date_to_Research`,
        DATE_FORMAT(`bo`.`Date_Returned`, '%m/%d/%Y') AS `Date_Returned`,
        `bo`.`Notes` AS `Notes`,
        `bo`.`Reviewed` AS `Reviewed`
    FROM
        (((((`xodata`.`tbl_backorder` `bo`
        JOIN `xodata`.`tbl_jobsites` `js` ON ((`bo`.`fk_confirmation` = `js`.`Confirmation`)))
        JOIN `xodata`.`tbl_supers` `sups` ON ((`js`.`fk_supers` = `sups`.`ID`)))
        JOIN `xodata`.`tbl_subdivisions` `subs` ON ((`subs`.`ID` = `sups`.`fk_subdivision`)))
        JOIN `xodata`.`tbl_builders` `b` ON ((`b`.`ID` = `sups`.`fk_builders`)))
        LEFT JOIN `xodata`.`tbl_issuesro` `ro` ON ((`bo`.`fk_issuesro` = `ro`.`ID`)))