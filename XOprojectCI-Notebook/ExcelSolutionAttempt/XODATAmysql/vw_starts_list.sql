CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `xodata`.`vw_starts_list` AS
    SELECT 
        `js`.`Confirmation` AS `Confirmation`,
        `js`.`PO` AS `PO`,
        DATE_FORMAT(`js`.`Starts_Date`, '%m/%d/%Y') AS `Starts Date`,
        `b`.`Name` AS `Builder`,
        `subs`.`Name` AS `Subdivision`,
        `js`.`Lot` AS `Lot`,
        `js`.`Plan` AS `Plan`,
        `js`.`Elevation` AS `Elevation`,
        `js`.`Model` AS `Model?`,
        DATE_FORMAT(`js`.`Install_Date`, '%m/%d/%Y') AS `Install Date`,
        DATE_FORMAT(`js`.`Arrival_Date_AD`, '%m/%d/%Y') AS `AD Date`,
        DATE_FORMAT(`js`.`Latest_Order_Date`, '%m/%d/%Y') AS `Latest Order Date`,
        `js`.`Address` AS `Address`,
        `subs`.`City` AS `City`,
        `subs`.`State` AS `ST`,
        `sups`.`Name` AS `Super`,
        CONCAT('(',
                LEFT(`sups`.`Phone_Number`, 3),
                ') ',
                SUBSTR(`sups`.`Phone_Number`, 4, 3),
                '-',
                RIGHT(`sups`.`Phone_Number`, 4)) AS `Super's Number`,
        `sr`.`Name` AS `Starts Rep`,
        `tib`.`Name` AS `Turned In By`,
        `tiv`.`Name` AS `Turn In Verification`,
        DATE_FORMAT(`js`.`Order_Date`, '%m/%d/%Y') AS `Order Date`,
        `js`.`Window_Count` AS `Win`,
        `js`.`Patio_Door_Count` AS `PD`,
        `js`.`Rolling_Wall_Count` AS `RW`,
        `js`.`Total_Units` AS `Tot Unit`,
        `js`.`Capacity` AS `Cap Count`,
        `js`.`Install_Revenue` AS `Install Revenue`,
        `js`.`Base_Contract_Price` AS `Base Contract Price`,
        `js`.`Option_Pricing` AS `Option Pricing`,
        `js`.`Sales_Total` AS `Sell Price`,
        `js`.`Install_Material_Cost` AS `Install Material Cost`,
        `js`.`Install_Labor_Cost` AS `Install Labor Cost`,
        `js`.`House_Package_Cost` AS `House Package Cost`
    FROM
        ((((((`xodata`.`tbl_jobsites` `js`
        JOIN `xodata`.`tbl_supers` `sups` ON ((`js`.`fk_supers` = `sups`.`ID`)))
        JOIN `xodata`.`tbl_subdivisions` `subs` ON ((`subs`.`ID` = `sups`.`fk_subdivision`)))
        JOIN `xodata`.`tbl_builders` `b` ON ((`b`.`ID` = `sups`.`fk_builders`)))
        JOIN `xodata`.`tbl_repsxo` `sr` ON ((`sr`.`ID` = `sups`.`fk_repsxo`)))
        JOIN `xodata`.`tbl_repsxo` `tib` ON ((`tib`.`ID` = `js`.`Turned_In_By`)))
        JOIN `xodata`.`tbl_repsxo` `tiv` ON ((`tiv`.`ID` = `js`.`Verified_By`)))