CREATE DEFINER=`root`@`localhost` PROCEDURE `Wkly_Start_Track`(
-- in p_start_date date,
-- in p_end_date date
 )
begin

select
YEARWEEK(Starts_Date, 7) as 'Week',
-- YEAR(Starts_Date),
-- week(Starts_Date,7),
-- adddate( makedate(@y,@w*7), interval 2-dayofweek(makedate(@y,7)) day ),
-- adddate( makedate(year(starts_date),week(Starts_Date, 7)*7), interval 7-dayofweek(makedate(year(Starts_Date),7)) day) as 'From',
-- adddate( makedate(year(starts_date),week(Starts_Date, 7)*7), interval 6 day) as 'To',
-- CONCAT(cast(adddate( makedate(year(starts_date),week(Starts_Date, 7)*7), interval 7-dayofweek(makedate(year(Starts_Date),7)) day) as CHAR),' to ',
-- cast(adddate( makedate(year(starts_date),week(Starts_Date, 7)*7), interval 6 day) as CHAR)) as 'Starts Received',
adddate( makedate(year(starts_date),week(Starts_Date, 7)*7), interval 7-dayofweek(makedate(year(Starts_Date),7)) day) as 'StartSAT',
CONCAT(date_format(cast(adddate( makedate(year(starts_date),week(Starts_Date, 7)*7), interval 7-dayofweek(makedate(year(Starts_Date),7)) day) as CHAR),"%m/%d/%Y"),' to ',
date_format(cast(adddate( makedate(year(starts_date),week(Starts_Date, 7)*7), interval 6 day) as CHAR),"%m/%d/%Y")) as 'Starts_Received',
if(LOCATE(" - TUC", B.Name) > 0,'TUC','PHX') as 'Branch',

JS.Builder as 'ID',
B.Name as 'Builder',
-- JS.Subdivision as 'ID',
-- S.Name as 'Subdivision',
count(*) as 'Lots'
-- count(distinct JS.Subdivision) as 'Lots'

from tbl_jobsites JS

join
	tbl_builders B on
		-- JS.Builder = B.ID
        B.ID = JS.Builder
        
join
	tbl_subdivisions S on
		-- JS.Subdivision = S.ID
        S.ID = JS.Subdivision

group by B.Name, Week
-- group by Week
order by Week asc, B.Name asc; -- S.Name asc;

end