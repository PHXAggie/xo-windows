-- create view vw_backorder_log as
select
	BO.ID as 'Backorder_ID',
    BO.fk_confirmation as 'Confirmation',
	date_format(BO.Date, "%m/%d/%Y") as 'Date',
    JS.PO as 'PO_Number',
    B.Name as 'Builder',
    Subs.Name as 'Subdivision',
    JS.Lot as 'Lot',
    Qty as 'Qty',
    Item as 'Item',
    Reason as 'Reason',
    BO.fk_issuesro as 'RO_IssueID',
    RO.Issue as 'RO_Issue',
    date_format(Date_to_Research, "%m/%d/%Y") as 'Date_to_Research',
    date_format(Date_Returned, "%m/%d/%Y") as 'Date_Returned',
    BO.Notes as 'Notes',
    BO.Reviewed as 'Reviewed'
from
	tbl_backorder BO
join
	tbl_jobsites JS on
		BO.fk_confirmation = JS.Confirmation
join
    tbl_supers Sups on
		JS.fk_supers = Sups.ID
join
    tbl_subdivisions Subs on
    	Subs.ID = Sups.fk_subdivision
join
	tbl_builders B on
		B.ID = Sups.fk_builders
left join
    tbl_issuesro RO on
		BO.fk_issuesro = RO.ID;