select
    Branch.Abbreviation as 'Location',
    B.Name as 'Builder',
    Subs.Name as 'Subdivision',
    JS.lot as 'Lot #',
    date_format(JS.install_date, "%m/%d/%Y") as 'Install Date',
    date_format(JS.starts_date, "%m/%d/%Y") as 'Starts Turned In',
    concat('$ ', format(JS.Install_Revenue, 2)) as 'Revenue',
    concat('$ ', format(JS.Sales_Total, 2)) as 'Sales'
from
	tbl_jobsites JS
join
    tbl_supers Sups on
		JS.fk_supers = Sups.ID
join
    tbl_subdivisions Subs on
    	Subs.ID = Sups.fk_subdivision
join
	tbl_builders B on
		B.ID = Sups.fk_builders
join
	tbl_branchxo Branch on
		Branch.ID = Sups.fk_branchxo
-- order by
-- Sales DESC
-- where
	-- JS.Turn_In_Date > 2017-01-01
order by Location;
-- where
-- 	turnindate