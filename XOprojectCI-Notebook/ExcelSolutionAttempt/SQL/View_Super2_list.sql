-- select * from tbl_supers;
-- create view vw_super2_list as
select
tbl_supers.ID as 'supID',
tbl_subdivisions.ID as 'subID',
	tbl_subdivisions.Name as 'subdivision',
tbl_builders.ID as 'bldrID',
	tbl_builders.Name as 'builder',
tbl_subdivisions.City as 'City',
tbl_subdivisions.State as 'ST',
tbl_installtypes.ID as 'instTypID',
	tbl_installtypes.Type as 'Type',
tbl_supers.Name as 'super',
tbl_supers.Phone_Number as 'phone',
tbl_manufacturers.ID as 'mfgID',
	tbl_manufacturers.manAbbreviation as 'MfgAbr',
		tbl_manufacturers.Name as 'Mfg',
tbl_repsxo.ID as 'repID',
	tbl_repsxo.Name as 'Rep',
tbl_branchxo.ID as 'brnchID',
	tbl_branchxo.POAbbreviation as 'brnchPO',
		tbl_branchxo.Location as 'Branch'
from
tbl_supers,
tbl_builders,
tbl_subdivisions,
tbl_installtypes,
tbl_manufacturers,
tbl_repsxo,
tbl_branchxo
where
tbl_subdivisions.ID = tbl_supers.fk_subdivision AND
tbl_builders.ID = tbl_supers.fk_builders AND
tbl_installtypes.ID = tbl_supers.fk_installtypes AND
tbl_manufacturers.ID = tbl_supers.fk_manufacturers AND
tbl_repsxo.ID = tbl_supers.fk_repsxo AND
tbl_branchxo.ID = tbl_supers.fk_branchxo
order by subdivision ASC;