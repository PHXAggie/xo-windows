-- create view vw_backorder_log as
select
	BO.ID as 'BackOrder_ID',
	JS.Confirmation as 'Confirmation',
	date_format(BO.Date, "%m/%d/%Y") as 'Date',
    JS.PO as 'PO_Number',
    B.Name as 'Builder',
    Subs.Name as 'Subdivision',
    JS.Lot as 'Lot',
    Qty as 'Qty',
    IFNULL(Item,'-') as 'Item',
    IFNULL(Reason,'-') as 'Reason',
    IFNULL(RO.ID,'-') as 'RO_IssueID',
    IFNULL(RO.Issue,'-') as 'RO_Issue',
    IFNULL(date_format(Date_to_Research, "%m/%d/%Y"),'-') as 'Date_to_Research',
    IFNULL(date_format(Date_Returned, "%m/%d/%Y"),'-') as 'Date_Returned'
from
	tbl_backorder BO
left join
	tbl_jobsites JS on
		BO.fk_confirmation = JS.Confirmation
left join
    tbl_supers Sups on
		JS.fk_supers = Sups.ID
left join
    tbl_subdivisions Subs on
    	Subs.ID = Sups.fk_subdivision
left join
	tbl_builders B on
		B.ID = Sups.fk_builders
 left join
    tbl_issuesro RO on
 		BO.fk_issuesro = RO.ID;