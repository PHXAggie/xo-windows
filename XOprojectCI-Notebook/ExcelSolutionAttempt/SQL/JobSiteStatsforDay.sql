-- SELECT * FROM xodata.tbl_jobsites;

select
	count(Window_Count) As 'Jobsites',
	sum(Window_Count) AS 'Total Windows',
	sum(Patio_Door_Count) As 'Total Patio Doors',
	sum(Rolling_Wall_Count) AS 'Total Rolling Walls'
from
	xodata.tbl_jobsites;
-- where Install_Date = DATE(Now());

--  >= '2017-01-01' and Date <= '2017-12-31';