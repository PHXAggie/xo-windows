select * from tbl_jobsites;
select
	JS.confirmation as Confirmation,
    JS.po as PO,
    date_format(JS.starts_date, "%m/%d/%Y") as 'Starts Date',
    B.Name as Builder,
    Subs.Name as Subdivision,
    JS.lot as Lot,
    JS.plan as Plan,
    JS.elevation as Elevation,
    JS.model as 'Model?',
    date_format(JS.install_date, "%m/%d/%Y") as 'Install Date',
    date_format(JS.arrival_date_ad, "%m/%d/%Y") as 'AD Date',
    date_format(JS.latest_order_date, "%m/%d/%Y") as 'Latest Order Date',
    JS.address as Address,
    Subs.City as City,
    Sups.Name as 'Super\'s Number',
    concat( "(" , left(Sups.Phone_Number,3) , ") " , mid(Sups.Phone_Number,4,3) , "-", right(Sups.Phone_Number,4)) as Number,
    SR.Name as 'Starts Rep',
    TIB.Name as 'Turned In By',
    TIV.Name as 'Turn In Verification',
    date_format(JS.Order_Date, "%m/%d/%Y") as 'Order Date',
    JS.window_count as 'Win',
    JS.patio_door_count as 'PD',
    JS.rolling_wall_count as 'RW',
    JS.Total_Units as 'Tot Unit',
    JS.Capacity as 'Cap Count',
    JS.Install_Revenue as 'Install Revenue',
    JS.Base_Contract_Price as 'Base Contract Price',
    JS.Option_Pricing as 'Option Pricing',
    JS.Sales_Total as 'Sell Price'
from
	tbl_jobsites JS
join
    tbl_supers Sups on
		JS.fk_supers = Sups.ID
join
    tbl_subdivisions Subs on
    	Subs.ID = Sups.fk_subdivision
join
	tbl_builders B on
		B.ID = Sups.fk_builders
join
    tbl_repsxo SR on
		SR.ID = Sups.fk_repsxo
join
    tbl_repsxo TIB on
		TIB.ID = JS.Turned_In_By
join
    tbl_repsxo TIV on
		TIV.ID = JS.Verified_By;