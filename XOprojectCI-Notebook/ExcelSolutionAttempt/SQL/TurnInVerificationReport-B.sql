-- select * from tbl_jobsites;
select
	date_format(JS.Turn_In_Date, "%m/%d/%Y") as 'Turn-in Date',
-- 	JS.XOBranch as 'Branch',
    BXO.Abbreviation as 'Branch',
--  JS.Builder as 'Builder',
    BU.Name as 'Builder',
--  JS.Subdivision as 'Subdivision',
    SB.Name as 'Subdivision',
    JS.Lot as 'Lot #',
    date_format(JS.Install_Date, "%m/%d/%Y") as 'Install Date',
--  JS.XORep as 'Starts Rep Initials',
    RXO.Name as 'Starts Rep',
    concat('$ ', format(JS.Install_Revenue, 2)) as 'Revenue',
    concat('$ ', format(JS.Sales_Total, 2)) as 'Sales'
        
from    
    tbl_jobsites JS
join
	tbl_branchxo BXO on
		JS.XOBranch = BXO.ID
join
	tbl_builders BU on
		JS.Builder = BU.ID
join
	tbl_subdivisions SB on
		JS.Subdivision = SB.ID
join
	tbl_repsxo RXO on
		JS.XORep = RXO.ID
-- where
-- 	JS.Turn_In_Date = '2017-04-13';