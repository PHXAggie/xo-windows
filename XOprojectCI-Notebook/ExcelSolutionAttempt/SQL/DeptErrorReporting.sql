-- select * from tbl_backorder;

-- SELECT COUNT(ID) as 'Count', DATE_FORMAT(Date, '%Y/%m') as 'Period' from tbl_backorder

-- select * from tbl_backorder where fk_issuesro = (select ID from tbl_issuesro where Issue='Sales') and
-- Date >= '2017-05-01' and Date <= '2017-05-31';

-- select date, count(Date) as 'Total' from tbl_backorder group by date;

-- Take confirmation number, look in jobsites table to find start rep for that jobsite
select
	-- BO.fk_confirmation as 'BO Confirmation',
    -- JS.Confirmation as 'Jobsite Confirmation',
    	-- BO.fk_issuesro as 'BO Dept',
        YEAR(BO.Date) as 'Year',
        MONTH(BO.Date) as 'Month',
        IRO.Issue as 'Department',
        if(BO.fk_issuesro = 112, SR.Name, '') as 'Starts Rep',
		sum(Qty) as 'Total Qty',
        count(distinct(Confirmation)) as 'Distinct Confirm Count'
from
	tbl_backorder as BO
join
	tbl_jobsites JS on
		BO.fk_confirmation = JS.Confirmation
join
    tbl_repsxo SR on
		SR.ID = JS.XORep
join
	tbl_issuesro IRO on
		IRO.ID = BO.fk_issuesro
where
	-- BO.fk_issuesro = (select ID from tbl_issuesro where Issue='Start') and
	Date >= '2017-01-01' and Date <= '2017-12-31'
    
group by
-- fk_confirmation,
	fk_issuesro,
	YEAR(BO.Date),
	MONTH(BO.Date);


--    SR.Name as 'Starts Rep',
    
-- For OS Counts report would need to know how many windows, PD and RW OVERSTOCKS were created by the error if Starts
