-- select * from tbl_jobsites;
-- create view vw_starts2_list as
select
	JS.confirmation as Confirmation,
    JS.po as PO,
    date_format(JS.starts_date, "%m/%d/%Y") as 'Starts_Date',
    Sups.ID as 'Supers_ID',
    B.Name as Builder,
    Subs.Name as Subdivision,
    JS.lot as Lot,
    JS.plan as Plan,
    JS.elevation as Elevation,
    JS.model as 'Model',
    date_format(JS.install_date, "%m/%d/%Y") as 'Install_Date',
    date_format(JS.Turn_In_Date, "%m/%d/%Y") as 'Turn_In_Date',
    date_format(JS.arrival_date_ad, "%m/%d/%Y") as 'AD_Date',
    date_format(JS.latest_order_date, "%m/%d/%Y") as 'Latest_Order_Date',
    JS.address as Address,
    Subs.City as City,
    Subs.State as 'State',
    JS.Super as 'Super',
	concat( "(" , left(JS.Supers_Phone,3) , ") " , mid(JS.Supers_Phone,4,3) , "-", right(JS.Supers_Phone,4)) as 'Supers_Number',
    SR.Name as 'Starts_Rep',
    TIB.ID as 'tibID',
    TIB.Name as 'Turned_In_By',
    TIV.ID as 'tivID',
    TIV.Name as 'Turn_In_Verification',
    date_format(JS.Order_Date, "%m/%d/%Y") as 'Order_Date',
    JS.window_count as 'Windows',
    JS.patio_door_count as 'Patio_Doors',
    JS.rolling_wall_count as 'Rolling_Walls',
    JS.Total_Units as 'Tot_Unit',
    JS.Capacity as 'Cap_Count',
    JS.Install_Revenue as 'Install_Revenue',
    JS.Base_Contract_Price as 'Base_Contract_Price',
    JS.Option_Pricing as 'Option_Pricing',
    JS.Sales_Total as 'Sell_Price',
    JS.Install_Material_Cost as 'Install_Material_Cost',
    JS.Install_Labor_Cost as 'Install_Labor_Cost',
    JS.House_Package_Cost as 'House_Package_Cost',
    Mfg.manAbbreviation as 'Manufacturer',
    Brnch.POAbbreviation as 'XO_Branch',
    Brnch.Location as 'XO_Location',
    Inst.Type as 'Install_Type'
from
	tbl_jobsites JS
join
    tbl_supers Sups on
		JS.fk_supers = Sups.ID
join
    tbl_subdivisions Subs on
    	Subs.ID = Sups.fk_subdivision
join
	tbl_builders B on
		B.ID = Sups.fk_builders
join
    tbl_repsxo SR on
		SR.ID = JS.XORep
join
 	tbl_manufacturers Mfg on
 		Mfg.ID = JS.Manufacturer
join
	tbl_branchxo Brnch on
		Brnch.ID = JS.XOBranch
join
	tbl_installtypes Inst on
		Inst.ID = JS.InstType
left join
    tbl_repsxo TIB on
		TIB.ID = JS.Turned_In_By
left join
    tbl_repsxo TIV on
		TIV.ID = JS.Verified_By;