<?php

include ("Models/model.php");
include ("Views/view.php");
include ("Controllers/controller.php");

$model = new Model();
$controller = new Controller($model);
$view = new View($controller, $model);

if (isset($_GET['action']) && !empty($_GET['action'])) {
	$controller->{$_GET['action']}();
}

echo $view->output();

?>