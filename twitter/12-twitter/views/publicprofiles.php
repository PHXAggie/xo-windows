<div class="container mainContainer">
  <div class="row">
    <div class="col-md-8"> <!-- Left side of screen -->
          
          <?php if ($_GET['userid']) { ?>
          
              <?php displayTweets($_GET['userid']); ?>
          
          <?php } else { ?> 
            
              <h2>Active Users</h2>
            
              <?php displayUsers(); ?>
          
          <?php } ?>
          
    </div>
    <div class="col-md-4">  <!-- Right side of Screen -->
            
          <?php displaySearch(); ?>
          
          <hr> <!-- Horizontal Rule -->
          
          <?php displayTweetBox(); ?>
  
    </div>
  </div>
</div>