<?php
// Not the front end css kind (a request from the web)
// Controller/action/id
class Bootstrap{
	private $controller;
	private $action;
	private $request;

	public function __construct($request){
		$this->request = $request;

		if ($this->request['controller'] == ""){ // until a link is clicked
// echo "Bootstrap Controller is blank! So controller is HOME. ";
			$this->controller = 'home';
		} else {
			$this->controller = $this->request['controller'];
// echo "Bootstrap Controller contains - " . $this->controller;
		}

		if($this->request['action'] == ""){ // until a link is clicked
// echo " Bootstrap Action is blank!  So action is INDEX. ";
			$this->action = 'index';
		} else {
			$this->action = $this->request['action'];
// echo " Bootstrap Action contains - " . $this->action;
		}
	}

	public function createController(){
		// Check Class or if controller exists
		if(class_exists($this->controller)){ // is the class defined?  There is a class Home which extends Controller (controllers/home.php)
			$parents = class_parents($this->controller); // set the parent of this class (for Home, its parent is Controller)
			// Check Extend
			if(in_array("Controller", $parents)){  // if 'Controller' is in Controller array of parents (true)
				if(method_exists($this->controller, $this->action)){ // In Controller does the method of action exist?  Does INDEX exist inside Controller class?  Since Home has an INDEX and extends Controller class, then Controller has Index as well (true)
// print_r($this->action); // index
// print_r($this->request); // Array ([controller] => [action] => [id] => )
// print_r(new $this->controller($this->action, $this->request));
	// Home Object ( [request:protected] => Array ( [controller] => [action] => [id] => ) [action:protected] => index )
					return new $this->controller($this->action, $this->request);  // whomever called createController (first iteration of root/index.php) send back a 'new Controller(Index,$_GET)'
				} else {
					// Method Does Not Exist (views folder but no matching *.php file?)
					echo '<h1>Method does not exist</h1>';
					return;
				}
			} else {
					// Base Controller Does Not Exist
					echo '<h1>Base controller not found</h1>';
					return;				
			}
		} else {
			// Controller Class Does Not Exist (no folder in views?)
			echo '<h1>Controller class does not exist</h1>';
			return;
		}
	}

}
?>