<div>
	<?php if(isset($_SESSION['is_logged_in'])) : ?>
		<a class="btn btn-success btn-share" href="<?php echo ROOT_PATH; ?>shares/add">Share Something</a>
	<?php endif; ?>

	<?php foreach($viewmodel as $item) : ?>
		<div class="well">
			<!-- <?php print_r($item); ?> -->
		<form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
			<h3><?php echo $item['title']; ?><small> by <?php echo $item['email'] ?></small></h3>
			<small><?php echo $item['create_date']; ?></small>
			<br />
			<p><?php echo $item['body']; ?>
			<a class="btn btn-default" href="<?php echo $item['link']; ?>" target="_blank">Go To Website</a>
		<?php if(isset($_SESSION['is_logged_in'])) : ?>
			<input class="btn btn-danger" name="submit" type="submit" value="Remove Share"></p>
			<input name="submit" type="hidden" value="<?php echo $item['id'] ?>"></p>
		<?php endif; ?>
		</form>
		</div>
	<?php endforeach; ?>

</div>