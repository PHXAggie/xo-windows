<?php
// Model is the tool that the Controller uses
class ShareModel extends Model{

	public function Index(){
		// Set the query
		//$this->query('SELECT * FROM shares ORDER BY create_date DESC');
		// Load results into variable
$this->query('SELECT S.id, U.email, S.title, S.body, S.link, S.create_date FROM shareboard.shares S, shareboard.users U WHERE S.user_id = U.id ORDER BY S.create_date DESC');
		$rows = $this->resultSet();
		// print_r($rows);
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
// var_dump($post['submit']);
		if($post['submit']){
			if(!isset($_SESSION['is_logged_in'])){
				Messages::setMsg('Not authorized, please Login', 'error');
			return $rows;
			}
//	print_r($post);
//	print_r($post['submit']);
			$this->query('DELETE FROM shares WHERE id = :shareID');
			// Bind value(s) to key(s)
			$this->bind(':shareID', $post['submit']);
			// Execute the query
			$this->execute();
			// Tell user 2nd param blank equals success
			header('Location: '.ROOT_URL.'shares');
			Messages::setMsg('Share successfully removed!', '');
		}
		// return the data
		return $rows;
	}

	public function add(){
		// Sanitize POST
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

		// Check that fields have been completed
		if($post['submit']){

			if($post['title'] == '' || $post['body'] == '' || $post['link'] == ''){
				Messages::setMsg('Please Fill In All Fields', 'error');
				return;
			}

		// Insert into MySQL
			// Set the query
			$this->query('INSERT INTO shares (title, body, link, user_id) VALUES (:title, :body, :link, :user_id)');
			// Bind the value(s) to key(s)
			$this->bind(':title', $post['title']);
			$this->bind(':body', $post['body']);
			$this->bind(':link', $post['link']);
			$this->bind(':user_id', $_SESSION['user_data']['users_id']);
			// Execute the query
			$this->execute();
			// Verify successful insert (redirect; nothing)
			if($this->lastInsertId()){
				// Redirect
				header('Location: '.ROOT_URL.'shares');
			}
		}
		return;
	} // end public function add()


	public function remove(){

		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		echo $post;
		if($post['submit2'] == 'submit'){
			echo "submit pressed";
			// Set the query
			// $this->query('DELETE FROM shares WHERE id = :shareID');
			// Bind value(s) to key(s)
			// $this->bind(':shareID', $post['id']);
			// Execute the query
			// $this->execute();
			// return;
		}
	} // end public function remove()
} // end class ShareModel

?>