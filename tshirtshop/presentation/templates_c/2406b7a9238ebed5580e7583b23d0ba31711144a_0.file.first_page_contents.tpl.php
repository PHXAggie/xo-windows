<?php
/* Smarty version 3.1.33, created on 2018-11-12 11:01:40
  from 'D:\wamp64\www\tshirtshop\presentation\templates\first_page_contents.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5be9c004eb7138_29092421',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2406b7a9238ebed5580e7583b23d0ba31711144a' => 
    array (
      0 => 'D:\\wamp64\\www\\tshirtshop\\presentation\\templates\\first_page_contents.tpl',
      1 => 1542044939,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:products_list.tpl' => 1,
  ),
),false)) {
function content_5be9c004eb7138_29092421 (Smarty_Internal_Template $_smarty_tpl) {
?><p class="description">
	We hope you have fun developing TShirtShop, the e-commerce store from Beginning PHP and MySQL E-Commerce: From Novice to Professional!
</p>
<p class="description">
	We have the largest collection of t-shirts with postal stamps on Earth! Browse our departments and categories to find your favorite!
</p>
<?php $_smarty_tpl->_subTemplateRender("file:products_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
