<?php
// Activate session
session_start();

// Include utility files
require_once 'include/config.php';
require_once BUSINESS_DIR . 'error_handler.php';

// Set the error handler
// Because error_handler.php is required, this STATIC method can be called
ErrorHandler::SetHandler();

// Load the application page template
require_once PRESENTATION_DIR . 'application.php';
require_once PRESENTATION_DIR . 'link.php';

// Load the database handler
require_once BUSINESS_DIR . 'database_handler.php';

// Load Busienss Tier
require_once BUSINESS_DIR . 'catalog.php';

// Load Smarty template files
$application = new Application();

// Display the page
$application->display('store_front.tpl');

// Close database connection
DatabaseHandler::Close();

// Try to load inexistent file to DEMO error Handling
//require_once 'inexistent_file.php';

?>