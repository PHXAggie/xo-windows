-- MySQL dump 10.13  Distrib 5.7.19, for Win64 (x86_64)
--
-- Host: localhost    Database: jobs
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `jobs`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `jobs` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `jobs`;

--
-- Dumping events for database 'jobs'
--

--
-- Dumping routines for database 'jobs'
--

--
-- Current Database: `bids`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `bids` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `bids`;

--
-- Table structure for table `tbl_bid`
--

DROP TABLE IF EXISTS `tbl_bid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_bid` (
  `tbl_bid_id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_subdivision_subdivision_id` int(11) NOT NULL,
  `plan` varchar(15) NOT NULL,
  `elevation` varchar(15) NOT NULL,
  `tbl_install_types_install_types_id` int(11) DEFAULT NULL,
  `tbl_manufacturer_manufacturer_id` int(11) DEFAULT NULL,
  `tbl_window_color_window_color_id` int(11) DEFAULT NULL,
  `tbl_window_material_window_material_id` int(11) DEFAULT NULL,
  `tbl_window_fin_window_fin_id` int(11) DEFAULT NULL,
  `tbl_window_coating_window_coating_id` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `tbl_status_tbl_status_id` int(11) NOT NULL,
  PRIMARY KEY (`tbl_bid_id`,`tbl_subdivision_subdivision_id`,`plan`,`elevation`),
  UNIQUE KEY `tbl_bid_composite` (`tbl_subdivision_subdivision_id`,`plan`,`elevation`),
  KEY `fk_tbl_bid_tbl_subdivision1_idx` (`tbl_subdivision_subdivision_id`),
  KEY `fk_tbl_bid_tbl_window_coating1_idx` (`tbl_window_coating_window_coating_id`),
  KEY `fk_tbl_bid_tbl_window_fin1_idx` (`tbl_window_fin_window_fin_id`),
  KEY `fk_tbl_bid_tbl_window_material1_idx` (`tbl_window_material_window_material_id`),
  KEY `fk_tbl_bid_tbl_window_color1_idx` (`tbl_window_color_window_color_id`),
  KEY `fk_tbl_bid_tbl_install_types1_idx` (`tbl_install_types_install_types_id`),
  KEY `fk_tbl_bid_tbl_manufacturer1_idx` (`tbl_manufacturer_manufacturer_id`),
  KEY `fk_tbl_bid_tbl_status1_idx` (`tbl_status_tbl_status_id`),
  KEY `tbl_bid_tbl_date_idx` (`update_date`),
  CONSTRAINT `fk_tbl_bid_tbl_install_types1` FOREIGN KEY (`tbl_install_types_install_types_id`) REFERENCES `builders`.`tbl_install_types` (`install_types_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_bid_tbl_manufacturer1` FOREIGN KEY (`tbl_manufacturer_manufacturer_id`) REFERENCES `vendors`.`tbl_manufacturer` (`manufacturer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_bid_tbl_status1` FOREIGN KEY (`tbl_status_tbl_status_id`) REFERENCES `mydb`.`tbl_status` (`tbl_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_bid_tbl_subdivision1` FOREIGN KEY (`tbl_subdivision_subdivision_id`) REFERENCES `builders`.`tbl_subdivision` (`subdivision_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_bid_tbl_window_coating1` FOREIGN KEY (`tbl_window_coating_window_coating_id`) REFERENCES `vendors`.`tbl_window_coating` (`window_coating_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_bid_tbl_window_color1` FOREIGN KEY (`tbl_window_color_window_color_id`) REFERENCES `vendors`.`tbl_window_color` (`window_color_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_bid_tbl_window_fin1` FOREIGN KEY (`tbl_window_fin_window_fin_id`) REFERENCES `vendors`.`tbl_window_fin` (`window_fin_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_bid_tbl_window_material1` FOREIGN KEY (`tbl_window_material_window_material_id`) REFERENCES `vendors`.`tbl_window_material` (`window_material_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_bid_tbl_date` FOREIGN KEY (`update_date`) REFERENCES `application`.`tbl_date` (`date_key`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_bid`
--

LOCK TABLES `tbl_bid` WRITE;
/*!40000 ALTER TABLE `tbl_bid` DISABLE KEYS */;
INSERT INTO `tbl_bid` VALUES (1,1,'6031','A',NULL,NULL,NULL,NULL,NULL,NULL,20190101,1300,'INITIAL_LOAD',702),(2,1,'6031','B',NULL,NULL,NULL,NULL,NULL,NULL,20190101,1300,'lking',702),(6,1,'6031','E',NULL,NULL,NULL,NULL,NULL,NULL,20190101,1300,'lking',702),(7,7,'TP03','A',NULL,NULL,NULL,NULL,NULL,NULL,20190101,1300,'lking',702);
/*!40000 ALTER TABLE `tbl_bid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_bid_windows`
--

DROP TABLE IF EXISTS `tbl_bid_windows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_bid_windows` (
  `tbl_bid_tbl_bid_id` int(11) NOT NULL,
  `bid_windows_id` int(11) NOT NULL AUTO_INCREMENT,
  `mfg` varchar(45) DEFAULT NULL,
  `series` varchar(45) DEFAULT NULL,
  `grouping` int(2) DEFAULT NULL,
  `window_details` varchar(75) DEFAULT NULL,
  `window_location` varchar(45) DEFAULT NULL,
  `position` int(3) DEFAULT NULL,
  PRIMARY KEY (`bid_windows_id`),
  KEY `fk_tbl_bid_windows_tbl_bid1_idx` (`tbl_bid_tbl_bid_id`),
  CONSTRAINT `fk_tbl_bid_windows_tbl_bid1` FOREIGN KEY (`tbl_bid_tbl_bid_id`) REFERENCES `tbl_bid` (`tbl_bid_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_bid_windows`
--

LOCK TABLES `tbl_bid_windows` WRITE;
/*!40000 ALTER TABLE `tbl_bid_windows` DISABLE KEYS */;
INSERT INTO `tbl_bid_windows` VALUES (1,1,'AL','770',NULL,'4020 PW','S-BA2',1),(1,2,'AL','770',NULL,'4020 PW','S-BA3',0),(1,3,'AL','571',NULL,'2656 SH','S2-BR3',2),(1,4,'AL','571',NULL,'2656 SH','S2-MBR',4),(1,5,'AL','571',NULL,'2656 SH','S-BONUS',3),(1,6,'AL','272',NULL,'3640 XO','S-KIT',5),(1,7,'AL','273',NULL,'8056 XOX','R-NOOK',7),(1,8,'AL','273',NULL,'8056 XOX','R-MBR',6),(1,9,'AL','273',NULL,'10056 XOX','R-GRT',8),(1,10,'AL','571',NULL,'2040 SH','S-MBR',9),(1,11,'AL','571',NULL,'2040 SH','S-MBR',10),(1,12,'AL','770',NULL,'5056 PW TEMP','S-MBA',11),(1,13,'AL','272',NULL,'5050 XO','S-BR2',13),(1,14,'AL','770',NULL,'2020 PW','S-PLAY',14),(1,15,'AL','770',NULL,'2020 PW','S-PLAY',15),(1,16,'AL','770',NULL,'2020 PW TEMP','S-PLAY',16),(1,17,'AL','A77B',1,'2\'6\" CIRCLE TOP  10 00   *FACTORY STACK TOP*         2 LT 2 X 1','F-BR3',17),(1,18,'AL','571',1,'2646 SH    11 11              *FACTORY STACK BTM*         4/4','F-BR3',18),(1,19,'AL','571',NULL,'2656 SH    11 11                    4/4','S-BR3/F2-BR3',19),(1,20,'AL','571',NULL,'2656 SH    11 11                    4/4','S-BR3/F2-BR3',20),(1,21,'AL','571',NULL,'2656 SH    11 11                    4/4','S-BR3/F2-BR3',21),(1,22,'AL','272',NULL,'5056 XO    13 13                    8-8=16 LT','S-PLAY',27),(1,23,'AL','770',NULL,'2060 PW  TEMP    13 00        8 LT  2 X 4','F-ENT',28),(1,24,'AL','571',NULL,'2056 SH    11 11                   4/4 ','S-BR4',29),(1,25,'AL','770',NULL,'2036 PW    12 00                  6 LT  2 X 3','F-TOWER',30),(1,26,'AL','770',NULL,'5056 PW TEMP             RAIN GLASS','S-MBA',12),(1,27,'AL','172',NULL,'6080 PD OX','S-NOOK',25),(1,28,'AL','172',NULL,'6080 PD XO','S-NOOK',26),(1,29,'AL','571',NULL,'3056 SH    11 11      Verify Grids','S-DEN',31),(1,30,'AL','770',NULL,'2020 PW 2/2','S2-BONUS/MBR',22),(1,31,'AL','571',NULL,'2656 SH    11 11                    4/4','S2-BONUS/MBR',23),(1,32,'AL','571',NULL,'2656 SH    11 11                    4/4','S2-BONUS/MBR',24),(1,33,'AL','770',NULL,'4020 PW','S-MBA',32),(1,34,'AL','571',NULL,'3056 SH GPE','S-DEN',33),(1,35,'AL','272',NULL,'5050 XO','S-BR5',34),(1,36,'AL','2300',NULL,'12080 O2X','R-GRT',35),(1,37,'AL','2300',NULL,'12080 2XO','R-GRT',36),(2,39,'AL','770',NULL,'4020 PW','S-BA2',0),(2,40,'AL','770',NULL,'4020 PW','S-BA3',1),(2,41,'AL','571',NULL,'2656 SH','S2-BR3',2),(2,42,'AL','571',NULL,'2656 SH','S2-MBR',4),(2,43,'AL','571',NULL,'2656 SH','S-BONUS',3),(2,44,'AL','272',NULL,'3640 XO','S-KIT',5),(2,45,'AL','273',NULL,'8056 XOX','R-NOOK',7),(2,46,'AL','273',NULL,'8056 XOX','R-MBR',6),(2,47,'AL','273',NULL,'10056 XOX','R-GRT',8),(2,48,'AL','571',NULL,'2040 SH','S-MBR',9),(2,49,'AL','571',NULL,'2040 SH','S-MBR',10),(2,50,'AL','770',NULL,'5056 PW TEMP','S-MBA',11),(2,51,'AL','272',NULL,'5050 XO','S-BR2',13),(2,52,'AL','770',NULL,'2020 PW','S-PLAY',14),(2,53,'AL','770',NULL,'2020 PW','S-PLAY',15),(2,54,'AL','770',NULL,'2020 PW TEMP','S-PLAY',16),(2,55,'AL','A77B',1,'2\'6\" CIRCLE TOP  10 00   *FACTORY STACK TOP*         2 LT 2 X 1','F-BR3',17),(2,56,'AL','571',1,'2646 SH    11 11              *FACTORY STACK BTM*         4/4','F-BR3',18),(2,57,'AL','571',NULL,'2656 SH    11 11                    4/4','S-BR3/F2-BR3',19),(2,58,'AL','571',NULL,'2656 SH    11 11                    4/4','S-BR3/F2-BR3',20),(2,59,'AL','571',NULL,'2656 SH    11 11                    4/4','S-BR3/F2-BR3',21),(2,60,'AL','272',NULL,'5056 XO    13 13                    8-8=16 LT','S-PLAY',27),(2,61,'AL','770',NULL,'2060 PW  TEMP    13 00        8 LT  2 X 4','F-ENT',28),(2,62,'AL','571',NULL,'2056 SH    11 11                   4/4 ','S-BR4',29),(2,63,'AL','770',NULL,'2036 PW    12 00                  6 LT  2 X 3','F-TOWER',30),(2,64,'AL','770',NULL,'5056 PW TEMP             RAIN GLASS','S-MBA',12),(2,65,'AL','172',NULL,'6080 PD OX','S-NOOK',25),(2,66,'AL','172',NULL,'6080 PD XO','S-NOOK',26),(2,67,'AL','571',NULL,'3056 SH    11 11      Verify Grids','S-DEN',31),(2,68,'AL','770',NULL,'2020 PW 2/2','S2-BONUS/MBR',22),(2,69,'AL','571',NULL,'2656 SH    11 11                    4/4','S2-BONUS/MBR',23),(2,70,'AL','571',NULL,'2656 SH    11 11                    4/4','S2-BONUS/MBR',24),(2,71,'AL','770',NULL,'4020 PW','S-MBA',32),(2,72,'AL','571',NULL,'3056 SH GPE','S-DEN',33),(2,73,'AL','272',NULL,'5050 XO','S-BR5',34),(2,74,'AL','2300',NULL,'12080 O2X','R-GRT',35),(2,75,'AL','2300',NULL,'12080 2XO','R-GRT',36),(6,141,'AL','770',NULL,'4020 PW','S-BA2',0),(6,142,'AL','770',NULL,'4020 PW','S-BA3',1),(6,143,'AL','571',NULL,'2656 SH','S2-BR3',2),(6,144,'AL','571',NULL,'2656 SH','S2-MBR',4),(6,145,'AL','571',NULL,'2656 SH','S-BONUS',3),(6,146,'AL','272',NULL,'3640 XO','S-KIT',5),(6,147,'AL','273',NULL,'8056 XOX','R-NOOK',7),(6,148,'AL','273',NULL,'8056 XOX','R-MBR',6),(6,149,'AL','273',NULL,'10056 XOX','R-GRT',8),(6,150,'AL','571',NULL,'2040 SH','S-MBR',9),(6,151,'AL','571',NULL,'2040 SH','S-MBR',10),(6,152,'AL','770',NULL,'5056 PW TEMP','S-MBA',11),(6,153,'AL','272',NULL,'5050 XO','S-BR2',13),(6,154,'AL','770',NULL,'2020 PW','S-PLAY',14),(6,155,'AL','770',NULL,'2020 PW','S-PLAY',15),(6,156,'AL','770',NULL,'2020 PW TEMP','S-PLAY',16),(6,157,'AL','A77B',1,'2\'6\" CIRCLE TOP  10 00   *FACTORY STACK TOP*         2 LT 2 X 1','F-BR3',17),(6,158,'AL','571',1,'2646 SH    11 11              *FACTORY STACK BTM*         4/4','F-BR3',18),(6,159,'AL','571',NULL,'2656 SH    11 11                    4/4','S-BR3/F2-BR3',19),(6,160,'AL','571',NULL,'2656 SH    11 11                    4/4','S-BR3/F2-BR3',20),(6,161,'AL','571',NULL,'2656 SH    11 11                    4/4','S-BR3/F2-BR3',21),(6,162,'AL','272',NULL,'5056 XO    13 13                    8-8=16 LT','S-PLAY',27),(6,163,'AL','770',NULL,'2060 PW  TEMP    13 00        8 LT  2 X 4','F-ENT',28),(6,164,'AL','571',NULL,'2056 SH    11 11                   4/4 ','S-BR4',29),(6,165,'AL','770',NULL,'2036 PW    12 00                  6 LT  2 X 3','F-TOWER',30),(6,166,'AL','770',NULL,'5056 PW TEMP             RAIN GLASS','S-MBA',12),(6,167,'AL','172',NULL,'6080 PD OX','S-NOOK',25),(6,168,'AL','172',NULL,'6080 PD XO','S-NOOK',26),(6,169,'AL','571',NULL,'3056 SH    11 11      Verify Grids','S-DEN',31),(6,170,'AL','770',NULL,'2020 PW 2/2','S2-BONUS/MBR',22),(6,171,'AL','571',NULL,'2656 SH    11 11                    4/4','S2-BONUS/MBR',23),(6,172,'AL','571',NULL,'2656 SH    11 11                    4/4','S2-BONUS/MBR',24),(6,173,'AL','770',NULL,'4020 PW','S-MBA',32),(6,174,'AL','571',NULL,'3056 SH GPE','S-DEN',33),(6,175,'AL','272',NULL,'5050 XO','S-BR5',34),(6,176,'AL','2300',NULL,'12080 O2X','R-GRT',35),(6,177,'AL','2300',NULL,'12080 2XO','R-GRT',36);
/*!40000 ALTER TABLE `tbl_bid_windows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_options`
--

DROP TABLE IF EXISTS `tbl_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_options` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_bid_tbl_bid_id` int(11) NOT NULL,
  `option_name` varchar(50) NOT NULL,
  `planOptionCode` varchar(15) DEFAULT NULL COMMENT 'Builders name for the Option',
  `cost` double DEFAULT NULL,
  PRIMARY KEY (`option_id`,`tbl_bid_tbl_bid_id`),
  UNIQUE KEY `tbl_options_composite` (`tbl_bid_tbl_bid_id`,`option_name`),
  KEY `fk_tbl_options_tbl_bid1_idx` (`tbl_bid_tbl_bid_id`),
  CONSTRAINT `fk_tbl_options_tbl_bid1` FOREIGN KEY (`tbl_bid_tbl_bid_id`) REFERENCES `tbl_bid` (`tbl_bid_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_options`
--

LOCK TABLES `tbl_options` WRITE;
/*!40000 ALTER TABLE `tbl_options` DISABLE KEYS */;
INSERT INTO `tbl_options` VALUES (5,1,'BASE','BASE',4222),(10,1,'LHAND',NULL,4222),(15,1,'RHAND',NULL,4222),(20,1,'DEN W3 CAR GARAGE ILO BR4 BA3 A ELEV',NULL,NULL),(25,1,'FRENCH DOOR ILO SGD',NULL,NULL),(30,1,'SINGLE FRENCH AT MBR PATIO','STFD00030',NULL),(35,1,'CORNER LOT A ELEV',NULL,NULL),(40,1,'4 CAR GARAGE ILO BR4 BA3 A ELEV',NULL,NULL),(45,1,'SUPERSHOWER',NULL,NULL),(50,1,'LARGE SGD AT PATIO','STSG00010',3342),(55,1,'DEN ILO 3 CAR GARAGE',NULL,NULL),(60,1,'OFFICE ILO BR4 BA3 A ELEV',NULL,NULL),(65,1,'BED 5 ILO PLAYROOM',NULL,NULL),(70,1,'RAIN GLASS ABOVE TUB','WIRG00010',310),(71,2,'BASE','BASE',4222),(72,2,'LHAND',NULL,4222),(73,2,'RHAND',NULL,4222),(74,2,'DEN W3 CAR GARAGE ILO BR4 BA3 A ELEV',NULL,NULL),(75,2,'FRENCH DOOR ILO SGD',NULL,NULL),(76,2,'SINGLE FRENCH AT MBR PATIO','STFD00030',NULL),(77,2,'CORNER LOT A ELEV',NULL,NULL),(78,2,'4 CAR GARAGE ILO BR4 BA3 A ELEV',NULL,NULL),(79,2,'SUPERSHOWER',NULL,NULL),(80,2,'LARGE SGD AT PATIO','STSG00010',3342),(81,2,'DEN ILO 3 CAR GARAGE',NULL,NULL),(82,2,'OFFICE ILO BR4 BA3 A ELEV',NULL,NULL),(83,2,'BED 5 ILO PLAYROOM',NULL,NULL),(84,2,'RAIN GLASS ABOVE TUB','WIRG00010',310),(115,6,'4 CAR GARAGE ILO BR4 BA3 A ELEV',NULL,NULL),(116,6,'BASE','BASE',4222),(117,6,'BED 5 ILO PLAYROOM',NULL,NULL),(118,6,'CORNER LOT A ELEV',NULL,NULL),(119,6,'DEN ILO 3 CAR GARAGE',NULL,NULL),(120,6,'DEN W3 CAR GARAGE ILO BR4 BA3 A ELEV',NULL,NULL),(121,6,'FRENCH DOOR ILO SGD',NULL,NULL),(122,6,'LARGE SGD AT PATIO','STSG00010',3342),(123,6,'LHAND',NULL,4222),(124,6,'OFFICE ILO BR4 BA3 A ELEV',NULL,NULL),(125,6,'RAIN GLASS ABOVE TUB','WIRG00010',310),(126,6,'RHAND',NULL,4222),(127,6,'SINGLE FRENCH AT MBR PATIO','STFD00030',NULL),(128,6,'SUPERSHOWER',NULL,NULL);
/*!40000 ALTER TABLE `tbl_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_options_has_windows`
--

DROP TABLE IF EXISTS `tbl_options_has_windows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_options_has_windows` (
  `tbl_bid_windows_bid_windows_id` int(11) NOT NULL,
  `tbl_options_option_id` int(11) NOT NULL,
  PRIMARY KEY (`tbl_bid_windows_bid_windows_id`,`tbl_options_option_id`),
  KEY `fk_tbl_bid_windows_has_tbl_options_tbl_options1_idx` (`tbl_options_option_id`),
  KEY `fk_tbl_bid_windows_has_tbl_options_tbl_bid_windows1_idx` (`tbl_bid_windows_bid_windows_id`),
  CONSTRAINT `fk_tbl_bid_windows_has_tbl_options_tbl_bid_windows1` FOREIGN KEY (`tbl_bid_windows_bid_windows_id`) REFERENCES `tbl_bid_windows` (`bid_windows_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_bid_windows_has_tbl_options_tbl_options1` FOREIGN KEY (`tbl_options_option_id`) REFERENCES `tbl_options` (`option_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_options_has_windows`
--

LOCK TABLES `tbl_options_has_windows` WRITE;
/*!40000 ALTER TABLE `tbl_options_has_windows` DISABLE KEYS */;
INSERT INTO `tbl_options_has_windows` VALUES (1,5),(2,5),(3,5),(4,5),(5,5),(6,5),(7,5),(8,5),(9,5),(10,5),(11,5),(12,5),(13,5),(14,5),(15,5),(16,5),(17,5),(18,5),(19,5),(20,5),(21,5),(22,5),(23,5),(24,5),(25,5),(27,5),(28,5),(1,10),(2,10),(3,10),(4,10),(5,10),(6,10),(7,10),(8,10),(9,10),(10,10),(11,10),(12,10),(13,10),(14,10),(15,10),(16,10),(17,10),(18,10),(19,10),(20,10),(21,10),(22,10),(23,10),(24,10),(25,10),(26,10),(28,10),(29,10),(30,10),(31,10),(32,10),(33,10),(34,10),(35,10),(36,10),(1,15),(2,15),(3,15),(4,15),(5,15),(6,15),(7,15),(8,15),(9,15),(10,15),(11,15),(12,15),(13,15),(14,15),(15,15),(16,15),(17,15),(18,15),(19,15),(20,15),(21,15),(22,15),(23,15),(24,15),(25,15),(26,15),(27,15),(29,15),(30,15),(31,15),(32,15),(33,15),(34,15),(35,15),(37,15),(1,20),(3,20),(4,20),(5,20),(6,20),(7,20),(8,20),(9,20),(10,20),(11,20),(12,20),(13,20),(14,20),(15,20),(16,20),(17,20),(18,20),(21,20),(22,20),(23,20),(25,20),(29,20),(1,25),(2,25),(3,25),(4,25),(5,25),(6,25),(7,25),(8,25),(9,25),(10,25),(11,25),(12,25),(13,25),(14,25),(15,25),(16,25),(17,25),(18,25),(19,25),(20,25),(21,25),(22,25),(23,25),(24,25),(25,25),(1,30),(2,30),(3,30),(4,30),(6,30),(7,30),(8,30),(9,30),(10,30),(11,30),(12,30),(13,30),(14,30),(15,30),(16,30),(17,30),(18,30),(19,30),(20,30),(21,30),(22,30),(23,30),(24,30),(25,30),(1,35),(3,35),(4,35),(6,35),(7,35),(8,35),(9,35),(10,35),(11,35),(12,35),(13,35),(14,35),(15,35),(16,35),(17,35),(18,35),(19,35),(20,35),(21,35),(22,35),(23,35),(24,35),(25,35),(30,35),(31,35),(32,35),(1,40),(3,40),(4,40),(5,40),(6,40),(7,40),(8,40),(9,40),(10,40),(11,40),(12,40),(13,40),(14,40),(15,40),(16,40),(17,40),(18,40),(19,40),(22,40),(23,40),(25,40),(1,45),(2,45),(3,45),(4,45),(5,45),(6,45),(7,45),(8,45),(9,45),(10,45),(11,45),(13,45),(14,45),(15,45),(16,45),(17,45),(18,45),(19,45),(20,45),(21,45),(22,45),(23,45),(24,45),(25,45),(33,45),(1,50),(2,50),(3,50),(4,50),(5,50),(6,50),(7,50),(8,50),(10,50),(11,50),(12,50),(13,50),(14,50),(15,50),(16,50),(17,50),(18,50),(19,50),(20,50),(21,50),(22,50),(23,50),(24,50),(25,50),(36,50),(37,50),(1,55),(2,55),(3,55),(4,55),(5,55),(6,55),(7,55),(8,55),(9,55),(10,55),(11,55),(12,55),(13,55),(14,55),(15,55),(16,55),(17,55),(18,55),(19,55),(20,55),(21,55),(22,55),(23,55),(24,55),(25,55),(34,55),(1,60),(2,60),(3,60),(4,60),(5,60),(6,60),(7,60),(8,60),(9,60),(10,60),(11,60),(12,60),(13,60),(14,60),(15,60),(16,60),(17,60),(18,60),(19,60),(20,60),(21,60),(22,60),(23,60),(25,60),(1,65),(2,65),(3,65),(4,65),(5,65),(6,65),(7,65),(8,65),(9,65),(10,65),(11,65),(12,65),(13,65),(17,65),(18,65),(19,65),(20,65),(21,65),(22,65),(23,65),(24,65),(25,65),(35,65),(1,70),(2,70),(3,70),(4,70),(5,70),(6,70),(7,70),(8,70),(9,70),(10,70),(11,70),(13,70),(14,70),(15,70),(16,70),(17,70),(18,70),(19,70),(20,70),(21,70),(22,70),(23,70),(24,70),(25,70),(26,70);
/*!40000 ALTER TABLE `tbl_options_has_windows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_options_has_windows_new`
--

DROP TABLE IF EXISTS `tbl_options_has_windows_new`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_options_has_windows_new` (
  `tbl_bid_windows_bid_windows_id` int(11) NOT NULL,
  `tbl_options_option_id` int(11) NOT NULL,
  `add_remove_action` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`tbl_bid_windows_bid_windows_id`,`tbl_options_option_id`),
  KEY `fk_tbl_bid_windows_has_tbl_options_tbl_options1_idx` (`tbl_options_option_id`),
  KEY `fk_tbl_bid_windows_has_tbl_options_tbl_bid_windows1_idx` (`tbl_bid_windows_bid_windows_id`),
  CONSTRAINT `fk_tbl_bid_windows_has_tbl_options_tbl_bid_windows10` FOREIGN KEY (`tbl_bid_windows_bid_windows_id`) REFERENCES `tbl_bid_windows` (`bid_windows_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_bid_windows_has_tbl_options_tbl_options10` FOREIGN KEY (`tbl_options_option_id`) REFERENCES `tbl_options` (`option_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_options_has_windows_new`
--

LOCK TABLES `tbl_options_has_windows_new` WRITE;
/*!40000 ALTER TABLE `tbl_options_has_windows_new` DISABLE KEYS */;
INSERT INTO `tbl_options_has_windows_new` VALUES (2,20,0),(2,35,0),(2,40,0),(5,30,0),(5,35,0),(9,50,0),(12,45,0),(12,70,0),(14,65,0),(15,65,0),(16,65,0),(19,20,0),(20,20,0),(20,40,0),(21,40,0),(24,20,0),(24,40,0),(24,60,0),(26,70,1),(27,15,1),(27,25,0),(28,10,1),(28,25,0),(29,20,1),(30,35,1),(31,35,1),(32,35,1),(33,45,1),(34,55,1),(35,65,1),(36,10,1),(36,50,1),(37,15,1),(37,50,1),(40,74,0),(40,77,0),(40,78,0),(43,76,0),(43,77,0),(47,80,0),(50,79,0),(50,84,0),(52,83,0),(53,83,0),(54,83,0),(57,74,0),(58,74,0),(58,78,0),(59,78,0),(62,74,0),(62,78,0),(62,82,0),(64,84,1),(65,73,1),(65,75,0),(66,72,1),(66,75,0),(67,74,1),(68,77,1),(69,77,1),(70,77,1),(71,79,1),(72,81,1),(73,83,1),(74,72,1),(74,80,1),(75,73,1),(75,80,1);
/*!40000 ALTER TABLE `tbl_options_has_windows_new` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `vw_get_bid_details`
--

DROP TABLE IF EXISTS `vw_get_bid_details`;
/*!50001 DROP VIEW IF EXISTS `vw_get_bid_details`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_get_bid_details` AS SELECT 
 1 AS `tbl_bid_id`,
 1 AS `tbl_subdivision_subdivision_id`,
 1 AS `Plan`,
 1 AS `Elevation`,
 1 AS `tbl_install_types_install_types_id`,
 1 AS `Type`,
 1 AS `tbl_manufacturer_manufacturer_id`,
 1 AS `manufacturer_name`,
 1 AS `tbl_window_color_window_color_id`,
 1 AS `window_color_name`,
 1 AS `tbl_window_material_window_material_id`,
 1 AS `window_material_name`,
 1 AS `tbl_window_fin_window_fin_id`,
 1 AS `window_fin_name`,
 1 AS `tbl_window_coating_window_coating_id`,
 1 AS `window_coating_name`,
 1 AS `tbl_status_tbl_status_id`,
 1 AS `builder_id`,
 1 AS `Builder`,
 1 AS `Subdivision`,
 1 AS `Branch`,
 1 AS `City`,
 1 AS `State`,
 1 AS `Prepped`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_get_bldrsub_elevation`
--

DROP TABLE IF EXISTS `vw_get_bldrsub_elevation`;
/*!50001 DROP VIEW IF EXISTS `vw_get_bldrsub_elevation`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_get_bldrsub_elevation` AS SELECT 
 1 AS `plan`,
 1 AS `elevation`,
 1 AS `tbl_subdivision_subdivision_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_get_bldrsub_plan`
--

DROP TABLE IF EXISTS `vw_get_bldrsub_plan`;
/*!50001 DROP VIEW IF EXISTS `vw_get_bldrsub_plan`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_get_bldrsub_plan` AS SELECT 
 1 AS `plan`,
 1 AS `tbl_subdivision_subdivision_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_get_recent_bids`
--

DROP TABLE IF EXISTS `vw_get_recent_bids`;
/*!50001 DROP VIEW IF EXISTS `vw_get_recent_bids`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_get_recent_bids` AS SELECT 
 1 AS `Date`,
 1 AS `tbl_bid_id`,
 1 AS `Builder`,
 1 AS `BuilderID`,
 1 AS `Subdivision`,
 1 AS `Plan`,
 1 AS `Elevation`,
 1 AS `Branch`,
 1 AS `City`,
 1 AS `State`,
 1 AS `Prepped`,
 1 AS `tbl_status_tbl_status_id`,
 1 AS `subdivision_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'bids'
--

--
-- Dumping routines for database 'bids'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_bid_details` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_bid_details`()
begin

	Select
    Bids.tbl_bid_id,
	Bids.tbl_subdivision_subdivision_id,
    Bids.plan as Plan,
    Bids.elevation as Elevation,
    Bids.tbl_install_types_install_types_id,
    IT.type as Type,
    Bids.tbl_manufacturer_manufacturer_id,
    Mfg.manufacturer_name,
    Bids.tbl_window_color_window_color_id,
    WC.window_color_name,
    Bids.tbl_window_material_window_material_id,
    WM.window_material_name,
    Bids.tbl_window_fin_window_fin_id,
    WF.window_fin_name,
    Bids.tbl_window_coating_window_coating_id,
    WCt.window_coating_name,
    Bids.tbl_status_tbl_status_id,
    B.builder_name as Builder,
    S.subdivision_name as Subdivision,
    U.po_abbreviation as Branch,
    S.city as City,
    S.state as State,
    Bids.updated_by as Prepped
    
    from bids.tbl_bid as Bids
    join builders.tbl_subdivision as S on S.subdivision_id = Bids.tbl_subdivision_subdivision_id
    join builders.tbl_buildercustomer as B on B.builder_id = S.tbl_buildercustomer_builder_id
    join xowindows.tbl_branch as U on U.id = S.tbl_xo_branch_xo_branch_id
    left outer join builders.tbl_install_types as IT on IT.install_types_id = Bids.tbl_install_types_install_types_id
    left outer join vendors.tbl_manufacturer as Mfg on Mfg.manufacturer_id = Bids.tbl_manufacturer_manufacturer_id
    left outer join vendors.tbl_window_color as WC on WC.window_color_id = Bids.tbl_window_color_window_color_id
    left outer join vendors.tbl_window_coating as WCt on WCt.window_coating_id = Bids.tbl_window_coating_window_coating_id
    left outer join vendors.tbl_window_material as WM on WM.window_material_id = Bids.tbl_window_material_window_material_id
    left outer join vendors.tbl_window_fin as WF on WF.window_fin_id = Bids.tbl_window_fin_window_fin_id;
    
    
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_bid_windows` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_bid_windows`(IN bid_id INT)
begin
	select
		bid_windows_id,
        window_details,
        window_location,
        position
        
	from bids.tbl_bid_windows
    where tbl_bid_tbl_bid_id = bid_id
    order by position ASC;
    
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_bid_windows_option` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_bid_windows_option`(IN bid_id INT)
begin
	select
		bid_windows_id,
        window_details,
        window_location,
        position
        
	from bids.tbl_bid_windows
    left join bids.tbl_options as O on O.option_id = bid_id
    order by position ASC;
    
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_current_bids_options` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_current_bids_options`(IN bid_id INT(11))
BEGIN
	SELECT
		O.option_id,
        O.option_name,
        O.planOptionCode,
        O.cost
        
	from bids.tbl_options as O
    where O.tbl_bid_tbl_bid_id = bid_id
    order by O.option_id ASC;    

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_install_types_bids` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_install_types_bids`(IN builderId INT(3))
BEGIN
	SELECT instTypes.install_types_id,
    instTypes.type,
    bHiT.tbl_install_types_install_types_id,
    bHiT.tbl_buildercustomer_builder_id

	from builders.tbl_install_types instTypes
    join builders.tbl_buildercustomer_has_install_types bHiT on instTypes.install_types_id = bHiT.tbl_install_types_install_types_id AND
		bHiT.tbl_buildercustomer_builder_id = builderId;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_recent_bids` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_recent_bids`()
begin

	Select
    DATE_FORMAT(bids.update_date, "%m/%d/%Y") as Date,
    Bids.tbl_bid_id,
    B.builder_name as Builder,
	S.tbl_buildercustomer_builder_id as BuilderID,
    S.subdivision_name as Subdivision,
    Bids.plan as Plan,
    Bids.elevation as Elevation,
    U.po_abbreviation as Branch,
    S.city as City,
    S.state as State,
    Bids.updated_by as Prepped
    
    from bids.tbl_bid as Bids
    join builders.tbl_subdivision as S on S.subdivision_id = Bids.tbl_subdivision_subdivision_id
    join builders.tbl_buildercustomer B on B.builder_id = S.tbl_buildercustomer_builder_id
    join xowindows.tbl_branch U on U.id = S.tbl_xo_branch_xo_branch_id
    where bids.tbl_status_tbl_status_id in (702,703,704)
    order by Date DESC
    limit 10;

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_option_has_windows` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_option_has_windows`(IN bid_id INT(11))
BEGIN
	SELECT
		oHw2.tbl_bid_windows_bid_windows_id,
        (
			SELECT COUNT(oHw21.tbl_bid_windows_bid_windows_id) total
            FROM tbl_options_has_windows_new oHw21
            WHERE BW.bid_windows_id = oHw21.tbl_bid_windows_bid_windows_id
        ) ttl_option_count,
        O.option_id,
        O.option_name,
        BW.window_details,
        BW.window_location,
        oHw2.add_remove_action
        
	from bids.tbl_options_has_windows_new as oHw2
    join bids.tbl_bid_windows as BW on BW.bid_windows_id = oHw2.tbl_bid_windows_bid_windows_id
    join bids.tbl_options as O on O.option_id = oHw2.tbl_options_option_id
    where O.tbl_bid_tbl_bid_id = bid_id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `application`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `application` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `application`;

--
-- Table structure for table `tbl_date`
--

DROP TABLE IF EXISTS `tbl_date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_date` (
  `date_key` int(11) NOT NULL,
  `full_date` date DEFAULT NULL,
  `date_name` char(11) NOT NULL,
  `date_name_us` char(11) NOT NULL,
  `date_name_eu` char(11) NOT NULL,
  `day_of_week` tinyint(4) NOT NULL,
  `day_name_of_week` char(10) NOT NULL,
  `day_of_month` tinyint(4) NOT NULL,
  `day_of_year` smallint(6) NOT NULL,
  `weekday_weekend` char(10) NOT NULL,
  `week_of_year` tinyint(4) NOT NULL,
  `month_name` char(10) NOT NULL,
  `month_of_year` tinyint(4) NOT NULL,
  `is_last_day_of_month` char(1) NOT NULL,
  `calendar_quarter` tinyint(4) NOT NULL,
  `calendar_year` smallint(6) NOT NULL,
  `calendar_year_month` char(10) NOT NULL,
  `calendar_year_qtr` char(10) NOT NULL,
  `fiscal_month_of_year` tinyint(4) NOT NULL,
  `fiscal_quarter` tinyint(4) NOT NULL,
  `fiscal_year` int(11) NOT NULL,
  `fiscal_year_month` char(10) NOT NULL,
  `fiscal_year_qtr` char(10) NOT NULL,
  PRIMARY KEY (`date_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_date`
--

LOCK TABLES `tbl_date` WRITE;
/*!40000 ALTER TABLE `tbl_date` DISABLE KEYS */;
INSERT INTO `tbl_date` VALUES (20190101,'2019-01-01','2019/01/01','01/01/2019','01/01/2019',3,'Tuesday',1,1,'Weekday',1,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190102,'2019-01-02','2019/01/02','01/02/2019','02/01/2019',4,'Wednesday',2,2,'Weekday',1,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190103,'2019-01-03','2019/01/03','01/03/2019','03/01/2019',5,'Thursday',3,3,'Weekday',1,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190104,'2019-01-04','2019/01/04','01/04/2019','04/01/2019',6,'Friday',4,4,'Weekday',1,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190105,'2019-01-05','2019/01/05','01/05/2019','05/01/2019',7,'Saturday',5,5,'Weekend',1,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190106,'2019-01-06','2019/01/06','01/06/2019','06/01/2019',1,'Sunday',6,6,'Weekend',1,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190107,'2019-01-07','2019/01/07','01/07/2019','07/01/2019',2,'Monday',7,7,'Weekday',2,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190108,'2019-01-08','2019/01/08','01/08/2019','08/01/2019',3,'Tuesday',8,8,'Weekday',2,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190109,'2019-01-09','2019/01/09','01/09/2019','09/01/2019',4,'Wednesday',9,9,'Weekday',2,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190110,'2019-01-10','2019/01/10','01/10/2019','10/01/2019',5,'Thursday',10,10,'Weekday',2,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190111,'2019-01-11','2019/01/11','01/11/2019','11/01/2019',6,'Friday',11,11,'Weekday',2,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190112,'2019-01-12','2019/01/12','01/12/2019','12/01/2019',7,'Saturday',12,12,'Weekend',2,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190113,'2019-01-13','2019/01/13','01/13/2019','13/01/2019',1,'Sunday',13,13,'Weekend',2,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190114,'2019-01-14','2019/01/14','01/14/2019','14/01/2019',2,'Monday',14,14,'Weekday',3,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190115,'2019-01-15','2019/01/15','01/15/2019','15/01/2019',3,'Tuesday',15,15,'Weekday',3,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190116,'2019-01-16','2019/01/16','01/16/2019','16/01/2019',4,'Wednesday',16,16,'Weekday',3,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190117,'2019-01-17','2019/01/17','01/17/2019','17/01/2019',5,'Thursday',17,17,'Weekday',3,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190118,'2019-01-18','2019/01/18','01/18/2019','18/01/2019',6,'Friday',18,18,'Weekday',3,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190119,'2019-01-19','2019/01/19','01/19/2019','19/01/2019',7,'Saturday',19,19,'Weekend',3,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190120,'2019-01-20','2019/01/20','01/20/2019','20/01/2019',1,'Sunday',20,20,'Weekend',3,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190121,'2019-01-21','2019/01/21','01/21/2019','21/01/2019',2,'Monday',21,21,'Weekday',4,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190122,'2019-01-22','2019/01/22','01/22/2019','22/01/2019',3,'Tuesday',22,22,'Weekday',4,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190123,'2019-01-23','2019/01/23','01/23/2019','23/01/2019',4,'Wednesday',23,23,'Weekday',4,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190124,'2019-01-24','2019/01/24','01/24/2019','24/01/2019',5,'Thursday',24,24,'Weekday',4,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190125,'2019-01-25','2019/01/25','01/25/2019','25/01/2019',6,'Friday',25,25,'Weekday',4,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190126,'2019-01-26','2019/01/26','01/26/2019','26/01/2019',7,'Saturday',26,26,'Weekend',4,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190127,'2019-01-27','2019/01/27','01/27/2019','27/01/2019',1,'Sunday',27,27,'Weekend',4,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190128,'2019-01-28','2019/01/28','01/28/2019','28/01/2019',2,'Monday',28,28,'Weekday',5,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190129,'2019-01-29','2019/01/29','01/29/2019','29/01/2019',3,'Tuesday',29,29,'Weekday',5,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190130,'2019-01-30','2019/01/30','01/30/2019','30/01/2019',4,'Wednesday',30,30,'Weekday',5,'January',1,'N',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190131,'2019-01-31','2019/01/31','01/31/2019','31/01/2019',5,'Thursday',31,31,'Weekday',5,'January',1,'Y',1,2019,'2019-01','2019Q1',2,1,2019,'2019-02','2019Q1'),(20190201,'2019-02-01','2019/02/01','02/01/2019','01/02/2019',6,'Friday',1,32,'Weekday',5,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190202,'2019-02-02','2019/02/02','02/02/2019','02/02/2019',7,'Saturday',2,33,'Weekend',5,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190203,'2019-02-03','2019/02/03','02/03/2019','03/02/2019',1,'Sunday',3,34,'Weekend',5,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190204,'2019-02-04','2019/02/04','02/04/2019','04/02/2019',2,'Monday',4,35,'Weekday',6,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190205,'2019-02-05','2019/02/05','02/05/2019','05/02/2019',3,'Tuesday',5,36,'Weekday',6,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190206,'2019-02-06','2019/02/06','02/06/2019','06/02/2019',4,'Wednesday',6,37,'Weekday',6,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190207,'2019-02-07','2019/02/07','02/07/2019','07/02/2019',5,'Thursday',7,38,'Weekday',6,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190208,'2019-02-08','2019/02/08','02/08/2019','08/02/2019',6,'Friday',8,39,'Weekday',6,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190209,'2019-02-09','2019/02/09','02/09/2019','09/02/2019',7,'Saturday',9,40,'Weekend',6,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190210,'2019-02-10','2019/02/10','02/10/2019','10/02/2019',1,'Sunday',10,41,'Weekend',6,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190211,'2019-02-11','2019/02/11','02/11/2019','11/02/2019',2,'Monday',11,42,'Weekday',7,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190212,'2019-02-12','2019/02/12','02/12/2019','12/02/2019',3,'Tuesday',12,43,'Weekday',7,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190213,'2019-02-13','2019/02/13','02/13/2019','13/02/2019',4,'Wednesday',13,44,'Weekday',7,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190214,'2019-02-14','2019/02/14','02/14/2019','14/02/2019',5,'Thursday',14,45,'Weekday',7,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190215,'2019-02-15','2019/02/15','02/15/2019','15/02/2019',6,'Friday',15,46,'Weekday',7,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190216,'2019-02-16','2019/02/16','02/16/2019','16/02/2019',7,'Saturday',16,47,'Weekend',7,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190217,'2019-02-17','2019/02/17','02/17/2019','17/02/2019',1,'Sunday',17,48,'Weekend',7,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190218,'2019-02-18','2019/02/18','02/18/2019','18/02/2019',2,'Monday',18,49,'Weekday',8,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190219,'2019-02-19','2019/02/19','02/19/2019','19/02/2019',3,'Tuesday',19,50,'Weekday',8,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190220,'2019-02-20','2019/02/20','02/20/2019','20/02/2019',4,'Wednesday',20,51,'Weekday',8,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190221,'2019-02-21','2019/02/21','02/21/2019','21/02/2019',5,'Thursday',21,52,'Weekday',8,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190222,'2019-02-22','2019/02/22','02/22/2019','22/02/2019',6,'Friday',22,53,'Weekday',8,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190223,'2019-02-23','2019/02/23','02/23/2019','23/02/2019',7,'Saturday',23,54,'Weekend',8,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190224,'2019-02-24','2019/02/24','02/24/2019','24/02/2019',1,'Sunday',24,55,'Weekend',8,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190225,'2019-02-25','2019/02/25','02/25/2019','25/02/2019',2,'Monday',25,56,'Weekday',9,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190226,'2019-02-26','2019/02/26','02/26/2019','26/02/2019',3,'Tuesday',26,57,'Weekday',9,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190227,'2019-02-27','2019/02/27','02/27/2019','27/02/2019',4,'Wednesday',27,58,'Weekday',9,'February',2,'N',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190228,'2019-02-28','2019/02/28','02/28/2019','28/02/2019',5,'Thursday',28,59,'Weekday',9,'February',2,'Y',1,2019,'2019-02','2019Q1',3,1,2019,'2019-03','2019Q1'),(20190301,'2019-03-01','2019/03/01','03/01/2019','01/03/2019',6,'Friday',1,60,'Weekday',9,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190302,'2019-03-02','2019/03/02','03/02/2019','02/03/2019',7,'Saturday',2,61,'Weekend',9,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190303,'2019-03-03','2019/03/03','03/03/2019','03/03/2019',1,'Sunday',3,62,'Weekend',9,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190304,'2019-03-04','2019/03/04','03/04/2019','04/03/2019',2,'Monday',4,63,'Weekday',10,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190305,'2019-03-05','2019/03/05','03/05/2019','05/03/2019',3,'Tuesday',5,64,'Weekday',10,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190306,'2019-03-06','2019/03/06','03/06/2019','06/03/2019',4,'Wednesday',6,65,'Weekday',10,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190307,'2019-03-07','2019/03/07','03/07/2019','07/03/2019',5,'Thursday',7,66,'Weekday',10,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190308,'2019-03-08','2019/03/08','03/08/2019','08/03/2019',6,'Friday',8,67,'Weekday',10,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190309,'2019-03-09','2019/03/09','03/09/2019','09/03/2019',7,'Saturday',9,68,'Weekend',10,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190310,'2019-03-10','2019/03/10','03/10/2019','10/03/2019',1,'Sunday',10,69,'Weekend',10,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190311,'2019-03-11','2019/03/11','03/11/2019','11/03/2019',2,'Monday',11,70,'Weekday',11,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190312,'2019-03-12','2019/03/12','03/12/2019','12/03/2019',3,'Tuesday',12,71,'Weekday',11,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190313,'2019-03-13','2019/03/13','03/13/2019','13/03/2019',4,'Wednesday',13,72,'Weekday',11,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190314,'2019-03-14','2019/03/14','03/14/2019','14/03/2019',5,'Thursday',14,73,'Weekday',11,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190315,'2019-03-15','2019/03/15','03/15/2019','15/03/2019',6,'Friday',15,74,'Weekday',11,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190316,'2019-03-16','2019/03/16','03/16/2019','16/03/2019',7,'Saturday',16,75,'Weekend',11,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190317,'2019-03-17','2019/03/17','03/17/2019','17/03/2019',1,'Sunday',17,76,'Weekend',11,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190318,'2019-03-18','2019/03/18','03/18/2019','18/03/2019',2,'Monday',18,77,'Weekday',12,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190319,'2019-03-19','2019/03/19','03/19/2019','19/03/2019',3,'Tuesday',19,78,'Weekday',12,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190320,'2019-03-20','2019/03/20','03/20/2019','20/03/2019',4,'Wednesday',20,79,'Weekday',12,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190321,'2019-03-21','2019/03/21','03/21/2019','21/03/2019',5,'Thursday',21,80,'Weekday',12,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190322,'2019-03-22','2019/03/22','03/22/2019','22/03/2019',6,'Friday',22,81,'Weekday',12,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190323,'2019-03-23','2019/03/23','03/23/2019','23/03/2019',7,'Saturday',23,82,'Weekend',12,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190324,'2019-03-24','2019/03/24','03/24/2019','24/03/2019',1,'Sunday',24,83,'Weekend',12,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190325,'2019-03-25','2019/03/25','03/25/2019','25/03/2019',2,'Monday',25,84,'Weekday',13,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190326,'2019-03-26','2019/03/26','03/26/2019','26/03/2019',3,'Tuesday',26,85,'Weekday',13,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190327,'2019-03-27','2019/03/27','03/27/2019','27/03/2019',4,'Wednesday',27,86,'Weekday',13,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190328,'2019-03-28','2019/03/28','03/28/2019','28/03/2019',5,'Thursday',28,87,'Weekday',13,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190329,'2019-03-29','2019/03/29','03/29/2019','29/03/2019',6,'Friday',29,88,'Weekday',13,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190330,'2019-03-30','2019/03/30','03/30/2019','30/03/2019',7,'Saturday',30,89,'Weekend',13,'March',3,'N',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190331,'2019-03-31','2019/03/31','03/31/2019','31/03/2019',1,'Sunday',31,90,'Weekend',13,'March',3,'Y',1,2019,'2019-03','2019Q1',4,2,2019,'2019-04','2019Q2'),(20190401,'2019-04-01','2019/04/01','04/01/2019','01/04/2019',2,'Monday',1,91,'Weekday',14,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190402,'2019-04-02','2019/04/02','04/02/2019','02/04/2019',3,'Tuesday',2,92,'Weekday',14,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190403,'2019-04-03','2019/04/03','04/03/2019','03/04/2019',4,'Wednesday',3,93,'Weekday',14,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190404,'2019-04-04','2019/04/04','04/04/2019','04/04/2019',5,'Thursday',4,94,'Weekday',14,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190405,'2019-04-05','2019/04/05','04/05/2019','05/04/2019',6,'Friday',5,95,'Weekday',14,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190406,'2019-04-06','2019/04/06','04/06/2019','06/04/2019',7,'Saturday',6,96,'Weekend',14,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190407,'2019-04-07','2019/04/07','04/07/2019','07/04/2019',1,'Sunday',7,97,'Weekend',14,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190408,'2019-04-08','2019/04/08','04/08/2019','08/04/2019',2,'Monday',8,98,'Weekday',15,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190409,'2019-04-09','2019/04/09','04/09/2019','09/04/2019',3,'Tuesday',9,99,'Weekday',15,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190410,'2019-04-10','2019/04/10','04/10/2019','10/04/2019',4,'Wednesday',10,100,'Weekday',15,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190411,'2019-04-11','2019/04/11','04/11/2019','11/04/2019',5,'Thursday',11,101,'Weekday',15,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190412,'2019-04-12','2019/04/12','04/12/2019','12/04/2019',6,'Friday',12,102,'Weekday',15,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190413,'2019-04-13','2019/04/13','04/13/2019','13/04/2019',7,'Saturday',13,103,'Weekend',15,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190414,'2019-04-14','2019/04/14','04/14/2019','14/04/2019',1,'Sunday',14,104,'Weekend',15,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190415,'2019-04-15','2019/04/15','04/15/2019','15/04/2019',2,'Monday',15,105,'Weekday',16,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190416,'2019-04-16','2019/04/16','04/16/2019','16/04/2019',3,'Tuesday',16,106,'Weekday',16,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190417,'2019-04-17','2019/04/17','04/17/2019','17/04/2019',4,'Wednesday',17,107,'Weekday',16,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190418,'2019-04-18','2019/04/18','04/18/2019','18/04/2019',5,'Thursday',18,108,'Weekday',16,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190419,'2019-04-19','2019/04/19','04/19/2019','19/04/2019',6,'Friday',19,109,'Weekday',16,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190420,'2019-04-20','2019/04/20','04/20/2019','20/04/2019',7,'Saturday',20,110,'Weekend',16,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190421,'2019-04-21','2019/04/21','04/21/2019','21/04/2019',1,'Sunday',21,111,'Weekend',16,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190422,'2019-04-22','2019/04/22','04/22/2019','22/04/2019',2,'Monday',22,112,'Weekday',17,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190423,'2019-04-23','2019/04/23','04/23/2019','23/04/2019',3,'Tuesday',23,113,'Weekday',17,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190424,'2019-04-24','2019/04/24','04/24/2019','24/04/2019',4,'Wednesday',24,114,'Weekday',17,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190425,'2019-04-25','2019/04/25','04/25/2019','25/04/2019',5,'Thursday',25,115,'Weekday',17,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190426,'2019-04-26','2019/04/26','04/26/2019','26/04/2019',6,'Friday',26,116,'Weekday',17,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190427,'2019-04-27','2019/04/27','04/27/2019','27/04/2019',7,'Saturday',27,117,'Weekend',17,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190428,'2019-04-28','2019/04/28','04/28/2019','28/04/2019',1,'Sunday',28,118,'Weekend',17,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190429,'2019-04-29','2019/04/29','04/29/2019','29/04/2019',2,'Monday',29,119,'Weekday',18,'April',4,'N',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190430,'2019-04-30','2019/04/30','04/30/2019','30/04/2019',3,'Tuesday',30,120,'Weekday',18,'April',4,'Y',2,2019,'2019-04','2019Q2',5,2,2019,'2019-05','2019Q2'),(20190501,'2019-05-01','2019/05/01','05/01/2019','01/05/2019',4,'Wednesday',1,121,'Weekday',18,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190502,'2019-05-02','2019/05/02','05/02/2019','02/05/2019',5,'Thursday',2,122,'Weekday',18,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190503,'2019-05-03','2019/05/03','05/03/2019','03/05/2019',6,'Friday',3,123,'Weekday',18,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190504,'2019-05-04','2019/05/04','05/04/2019','04/05/2019',7,'Saturday',4,124,'Weekend',18,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190505,'2019-05-05','2019/05/05','05/05/2019','05/05/2019',1,'Sunday',5,125,'Weekend',18,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190506,'2019-05-06','2019/05/06','05/06/2019','06/05/2019',2,'Monday',6,126,'Weekday',19,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190507,'2019-05-07','2019/05/07','05/07/2019','07/05/2019',3,'Tuesday',7,127,'Weekday',19,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190508,'2019-05-08','2019/05/08','05/08/2019','08/05/2019',4,'Wednesday',8,128,'Weekday',19,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190509,'2019-05-09','2019/05/09','05/09/2019','09/05/2019',5,'Thursday',9,129,'Weekday',19,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190510,'2019-05-10','2019/05/10','05/10/2019','10/05/2019',6,'Friday',10,130,'Weekday',19,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190511,'2019-05-11','2019/05/11','05/11/2019','11/05/2019',7,'Saturday',11,131,'Weekend',19,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190512,'2019-05-12','2019/05/12','05/12/2019','12/05/2019',1,'Sunday',12,132,'Weekend',19,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190513,'2019-05-13','2019/05/13','05/13/2019','13/05/2019',2,'Monday',13,133,'Weekday',20,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190514,'2019-05-14','2019/05/14','05/14/2019','14/05/2019',3,'Tuesday',14,134,'Weekday',20,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190515,'2019-05-15','2019/05/15','05/15/2019','15/05/2019',4,'Wednesday',15,135,'Weekday',20,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190516,'2019-05-16','2019/05/16','05/16/2019','16/05/2019',5,'Thursday',16,136,'Weekday',20,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190517,'2019-05-17','2019/05/17','05/17/2019','17/05/2019',6,'Friday',17,137,'Weekday',20,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190518,'2019-05-18','2019/05/18','05/18/2019','18/05/2019',7,'Saturday',18,138,'Weekend',20,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190519,'2019-05-19','2019/05/19','05/19/2019','19/05/2019',1,'Sunday',19,139,'Weekend',20,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190520,'2019-05-20','2019/05/20','05/20/2019','20/05/2019',2,'Monday',20,140,'Weekday',21,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190521,'2019-05-21','2019/05/21','05/21/2019','21/05/2019',3,'Tuesday',21,141,'Weekday',21,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190522,'2019-05-22','2019/05/22','05/22/2019','22/05/2019',4,'Wednesday',22,142,'Weekday',21,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190523,'2019-05-23','2019/05/23','05/23/2019','23/05/2019',5,'Thursday',23,143,'Weekday',21,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190524,'2019-05-24','2019/05/24','05/24/2019','24/05/2019',6,'Friday',24,144,'Weekday',21,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190525,'2019-05-25','2019/05/25','05/25/2019','25/05/2019',7,'Saturday',25,145,'Weekend',21,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190526,'2019-05-26','2019/05/26','05/26/2019','26/05/2019',1,'Sunday',26,146,'Weekend',21,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190527,'2019-05-27','2019/05/27','05/27/2019','27/05/2019',2,'Monday',27,147,'Weekday',22,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190528,'2019-05-28','2019/05/28','05/28/2019','28/05/2019',3,'Tuesday',28,148,'Weekday',22,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190529,'2019-05-29','2019/05/29','05/29/2019','29/05/2019',4,'Wednesday',29,149,'Weekday',22,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190530,'2019-05-30','2019/05/30','05/30/2019','30/05/2019',5,'Thursday',30,150,'Weekday',22,'May',5,'N',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190531,'2019-05-31','2019/05/31','05/31/2019','31/05/2019',6,'Friday',31,151,'Weekday',22,'May',5,'Y',2,2019,'2019-05','2019Q2',6,2,2019,'2019-06','2019Q2'),(20190601,'2019-06-01','2019/06/01','06/01/2019','01/06/2019',7,'Saturday',1,152,'Weekend',22,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190602,'2019-06-02','2019/06/02','06/02/2019','02/06/2019',1,'Sunday',2,153,'Weekend',22,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190603,'2019-06-03','2019/06/03','06/03/2019','03/06/2019',2,'Monday',3,154,'Weekday',23,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190604,'2019-06-04','2019/06/04','06/04/2019','04/06/2019',3,'Tuesday',4,155,'Weekday',23,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190605,'2019-06-05','2019/06/05','06/05/2019','05/06/2019',4,'Wednesday',5,156,'Weekday',23,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190606,'2019-06-06','2019/06/06','06/06/2019','06/06/2019',5,'Thursday',6,157,'Weekday',23,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190607,'2019-06-07','2019/06/07','06/07/2019','07/06/2019',6,'Friday',7,158,'Weekday',23,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190608,'2019-06-08','2019/06/08','06/08/2019','08/06/2019',7,'Saturday',8,159,'Weekend',23,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190609,'2019-06-09','2019/06/09','06/09/2019','09/06/2019',1,'Sunday',9,160,'Weekend',23,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190610,'2019-06-10','2019/06/10','06/10/2019','10/06/2019',2,'Monday',10,161,'Weekday',24,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190611,'2019-06-11','2019/06/11','06/11/2019','11/06/2019',3,'Tuesday',11,162,'Weekday',24,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190612,'2019-06-12','2019/06/12','06/12/2019','12/06/2019',4,'Wednesday',12,163,'Weekday',24,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190613,'2019-06-13','2019/06/13','06/13/2019','13/06/2019',5,'Thursday',13,164,'Weekday',24,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190614,'2019-06-14','2019/06/14','06/14/2019','14/06/2019',6,'Friday',14,165,'Weekday',24,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190615,'2019-06-15','2019/06/15','06/15/2019','15/06/2019',7,'Saturday',15,166,'Weekend',24,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190616,'2019-06-16','2019/06/16','06/16/2019','16/06/2019',1,'Sunday',16,167,'Weekend',24,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190617,'2019-06-17','2019/06/17','06/17/2019','17/06/2019',2,'Monday',17,168,'Weekday',25,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190618,'2019-06-18','2019/06/18','06/18/2019','18/06/2019',3,'Tuesday',18,169,'Weekday',25,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190619,'2019-06-19','2019/06/19','06/19/2019','19/06/2019',4,'Wednesday',19,170,'Weekday',25,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190620,'2019-06-20','2019/06/20','06/20/2019','20/06/2019',5,'Thursday',20,171,'Weekday',25,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190621,'2019-06-21','2019/06/21','06/21/2019','21/06/2019',6,'Friday',21,172,'Weekday',25,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190622,'2019-06-22','2019/06/22','06/22/2019','22/06/2019',7,'Saturday',22,173,'Weekend',25,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190623,'2019-06-23','2019/06/23','06/23/2019','23/06/2019',1,'Sunday',23,174,'Weekend',25,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190624,'2019-06-24','2019/06/24','06/24/2019','24/06/2019',2,'Monday',24,175,'Weekday',26,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190625,'2019-06-25','2019/06/25','06/25/2019','25/06/2019',3,'Tuesday',25,176,'Weekday',26,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190626,'2019-06-26','2019/06/26','06/26/2019','26/06/2019',4,'Wednesday',26,177,'Weekday',26,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190627,'2019-06-27','2019/06/27','06/27/2019','27/06/2019',5,'Thursday',27,178,'Weekday',26,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190628,'2019-06-28','2019/06/28','06/28/2019','28/06/2019',6,'Friday',28,179,'Weekday',26,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190629,'2019-06-29','2019/06/29','06/29/2019','29/06/2019',7,'Saturday',29,180,'Weekend',26,'June',6,'N',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190630,'2019-06-30','2019/06/30','06/30/2019','30/06/2019',1,'Sunday',30,181,'Weekend',26,'June',6,'Y',2,2019,'2019-06','2019Q2',7,3,2019,'2019-07','2019Q3'),(20190701,'2019-07-01','2019/07/01','07/01/2019','01/07/2019',2,'Monday',1,182,'Weekday',27,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190702,'2019-07-02','2019/07/02','07/02/2019','02/07/2019',3,'Tuesday',2,183,'Weekday',27,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190703,'2019-07-03','2019/07/03','07/03/2019','03/07/2019',4,'Wednesday',3,184,'Weekday',27,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190704,'2019-07-04','2019/07/04','07/04/2019','04/07/2019',5,'Thursday',4,185,'Weekday',27,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190705,'2019-07-05','2019/07/05','07/05/2019','05/07/2019',6,'Friday',5,186,'Weekday',27,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190706,'2019-07-06','2019/07/06','07/06/2019','06/07/2019',7,'Saturday',6,187,'Weekend',27,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190707,'2019-07-07','2019/07/07','07/07/2019','07/07/2019',1,'Sunday',7,188,'Weekend',27,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190708,'2019-07-08','2019/07/08','07/08/2019','08/07/2019',2,'Monday',8,189,'Weekday',28,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190709,'2019-07-09','2019/07/09','07/09/2019','09/07/2019',3,'Tuesday',9,190,'Weekday',28,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190710,'2019-07-10','2019/07/10','07/10/2019','10/07/2019',4,'Wednesday',10,191,'Weekday',28,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190711,'2019-07-11','2019/07/11','07/11/2019','11/07/2019',5,'Thursday',11,192,'Weekday',28,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190712,'2019-07-12','2019/07/12','07/12/2019','12/07/2019',6,'Friday',12,193,'Weekday',28,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190713,'2019-07-13','2019/07/13','07/13/2019','13/07/2019',7,'Saturday',13,194,'Weekend',28,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190714,'2019-07-14','2019/07/14','07/14/2019','14/07/2019',1,'Sunday',14,195,'Weekend',28,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190715,'2019-07-15','2019/07/15','07/15/2019','15/07/2019',2,'Monday',15,196,'Weekday',29,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190716,'2019-07-16','2019/07/16','07/16/2019','16/07/2019',3,'Tuesday',16,197,'Weekday',29,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190717,'2019-07-17','2019/07/17','07/17/2019','17/07/2019',4,'Wednesday',17,198,'Weekday',29,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190718,'2019-07-18','2019/07/18','07/18/2019','18/07/2019',5,'Thursday',18,199,'Weekday',29,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190719,'2019-07-19','2019/07/19','07/19/2019','19/07/2019',6,'Friday',19,200,'Weekday',29,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190720,'2019-07-20','2019/07/20','07/20/2019','20/07/2019',7,'Saturday',20,201,'Weekend',29,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190721,'2019-07-21','2019/07/21','07/21/2019','21/07/2019',1,'Sunday',21,202,'Weekend',29,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190722,'2019-07-22','2019/07/22','07/22/2019','22/07/2019',2,'Monday',22,203,'Weekday',30,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190723,'2019-07-23','2019/07/23','07/23/2019','23/07/2019',3,'Tuesday',23,204,'Weekday',30,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190724,'2019-07-24','2019/07/24','07/24/2019','24/07/2019',4,'Wednesday',24,205,'Weekday',30,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190725,'2019-07-25','2019/07/25','07/25/2019','25/07/2019',5,'Thursday',25,206,'Weekday',30,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190726,'2019-07-26','2019/07/26','07/26/2019','26/07/2019',6,'Friday',26,207,'Weekday',30,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190727,'2019-07-27','2019/07/27','07/27/2019','27/07/2019',7,'Saturday',27,208,'Weekend',30,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190728,'2019-07-28','2019/07/28','07/28/2019','28/07/2019',1,'Sunday',28,209,'Weekend',30,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190729,'2019-07-29','2019/07/29','07/29/2019','29/07/2019',2,'Monday',29,210,'Weekday',31,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190730,'2019-07-30','2019/07/30','07/30/2019','30/07/2019',3,'Tuesday',30,211,'Weekday',31,'July',7,'N',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190731,'2019-07-31','2019/07/31','07/31/2019','31/07/2019',4,'Wednesday',31,212,'Weekday',31,'July',7,'Y',3,2019,'2019-07','2019Q3',8,3,2019,'2019-08','2019Q3'),(20190801,'2019-08-01','2019/08/01','08/01/2019','01/08/2019',5,'Thursday',1,213,'Weekday',31,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190802,'2019-08-02','2019/08/02','08/02/2019','02/08/2019',6,'Friday',2,214,'Weekday',31,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190803,'2019-08-03','2019/08/03','08/03/2019','03/08/2019',7,'Saturday',3,215,'Weekend',31,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190804,'2019-08-04','2019/08/04','08/04/2019','04/08/2019',1,'Sunday',4,216,'Weekend',31,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190805,'2019-08-05','2019/08/05','08/05/2019','05/08/2019',2,'Monday',5,217,'Weekday',32,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190806,'2019-08-06','2019/08/06','08/06/2019','06/08/2019',3,'Tuesday',6,218,'Weekday',32,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190807,'2019-08-07','2019/08/07','08/07/2019','07/08/2019',4,'Wednesday',7,219,'Weekday',32,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190808,'2019-08-08','2019/08/08','08/08/2019','08/08/2019',5,'Thursday',8,220,'Weekday',32,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190809,'2019-08-09','2019/08/09','08/09/2019','09/08/2019',6,'Friday',9,221,'Weekday',32,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190810,'2019-08-10','2019/08/10','08/10/2019','10/08/2019',7,'Saturday',10,222,'Weekend',32,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190811,'2019-08-11','2019/08/11','08/11/2019','11/08/2019',1,'Sunday',11,223,'Weekend',32,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190812,'2019-08-12','2019/08/12','08/12/2019','12/08/2019',2,'Monday',12,224,'Weekday',33,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190813,'2019-08-13','2019/08/13','08/13/2019','13/08/2019',3,'Tuesday',13,225,'Weekday',33,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190814,'2019-08-14','2019/08/14','08/14/2019','14/08/2019',4,'Wednesday',14,226,'Weekday',33,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190815,'2019-08-15','2019/08/15','08/15/2019','15/08/2019',5,'Thursday',15,227,'Weekday',33,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190816,'2019-08-16','2019/08/16','08/16/2019','16/08/2019',6,'Friday',16,228,'Weekday',33,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190817,'2019-08-17','2019/08/17','08/17/2019','17/08/2019',7,'Saturday',17,229,'Weekend',33,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190818,'2019-08-18','2019/08/18','08/18/2019','18/08/2019',1,'Sunday',18,230,'Weekend',33,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190819,'2019-08-19','2019/08/19','08/19/2019','19/08/2019',2,'Monday',19,231,'Weekday',34,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190820,'2019-08-20','2019/08/20','08/20/2019','20/08/2019',3,'Tuesday',20,232,'Weekday',34,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190821,'2019-08-21','2019/08/21','08/21/2019','21/08/2019',4,'Wednesday',21,233,'Weekday',34,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190822,'2019-08-22','2019/08/22','08/22/2019','22/08/2019',5,'Thursday',22,234,'Weekday',34,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190823,'2019-08-23','2019/08/23','08/23/2019','23/08/2019',6,'Friday',23,235,'Weekday',34,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190824,'2019-08-24','2019/08/24','08/24/2019','24/08/2019',7,'Saturday',24,236,'Weekend',34,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190825,'2019-08-25','2019/08/25','08/25/2019','25/08/2019',1,'Sunday',25,237,'Weekend',34,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190826,'2019-08-26','2019/08/26','08/26/2019','26/08/2019',2,'Monday',26,238,'Weekday',35,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190827,'2019-08-27','2019/08/27','08/27/2019','27/08/2019',3,'Tuesday',27,239,'Weekday',35,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190828,'2019-08-28','2019/08/28','08/28/2019','28/08/2019',4,'Wednesday',28,240,'Weekday',35,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190829,'2019-08-29','2019/08/29','08/29/2019','29/08/2019',5,'Thursday',29,241,'Weekday',35,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190830,'2019-08-30','2019/08/30','08/30/2019','30/08/2019',6,'Friday',30,242,'Weekday',35,'August',8,'N',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190831,'2019-08-31','2019/08/31','08/31/2019','31/08/2019',7,'Saturday',31,243,'Weekend',35,'August',8,'Y',3,2019,'2019-08','2019Q3',9,3,2019,'2019-09','2019Q3'),(20190901,'2019-09-01','2019/09/01','09/01/2019','01/09/2019',1,'Sunday',1,244,'Weekend',35,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190902,'2019-09-02','2019/09/02','09/02/2019','02/09/2019',2,'Monday',2,245,'Weekday',36,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190903,'2019-09-03','2019/09/03','09/03/2019','03/09/2019',3,'Tuesday',3,246,'Weekday',36,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190904,'2019-09-04','2019/09/04','09/04/2019','04/09/2019',4,'Wednesday',4,247,'Weekday',36,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190905,'2019-09-05','2019/09/05','09/05/2019','05/09/2019',5,'Thursday',5,248,'Weekday',36,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190906,'2019-09-06','2019/09/06','09/06/2019','06/09/2019',6,'Friday',6,249,'Weekday',36,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190907,'2019-09-07','2019/09/07','09/07/2019','07/09/2019',7,'Saturday',7,250,'Weekend',36,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190908,'2019-09-08','2019/09/08','09/08/2019','08/09/2019',1,'Sunday',8,251,'Weekend',36,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190909,'2019-09-09','2019/09/09','09/09/2019','09/09/2019',2,'Monday',9,252,'Weekday',37,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190910,'2019-09-10','2019/09/10','09/10/2019','10/09/2019',3,'Tuesday',10,253,'Weekday',37,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190911,'2019-09-11','2019/09/11','09/11/2019','11/09/2019',4,'Wednesday',11,254,'Weekday',37,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190912,'2019-09-12','2019/09/12','09/12/2019','12/09/2019',5,'Thursday',12,255,'Weekday',37,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190913,'2019-09-13','2019/09/13','09/13/2019','13/09/2019',6,'Friday',13,256,'Weekday',37,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190914,'2019-09-14','2019/09/14','09/14/2019','14/09/2019',7,'Saturday',14,257,'Weekend',37,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190915,'2019-09-15','2019/09/15','09/15/2019','15/09/2019',1,'Sunday',15,258,'Weekend',37,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190916,'2019-09-16','2019/09/16','09/16/2019','16/09/2019',2,'Monday',16,259,'Weekday',38,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190917,'2019-09-17','2019/09/17','09/17/2019','17/09/2019',3,'Tuesday',17,260,'Weekday',38,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190918,'2019-09-18','2019/09/18','09/18/2019','18/09/2019',4,'Wednesday',18,261,'Weekday',38,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190919,'2019-09-19','2019/09/19','09/19/2019','19/09/2019',5,'Thursday',19,262,'Weekday',38,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190920,'2019-09-20','2019/09/20','09/20/2019','20/09/2019',6,'Friday',20,263,'Weekday',38,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190921,'2019-09-21','2019/09/21','09/21/2019','21/09/2019',7,'Saturday',21,264,'Weekend',38,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190922,'2019-09-22','2019/09/22','09/22/2019','22/09/2019',1,'Sunday',22,265,'Weekend',38,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190923,'2019-09-23','2019/09/23','09/23/2019','23/09/2019',2,'Monday',23,266,'Weekday',39,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190924,'2019-09-24','2019/09/24','09/24/2019','24/09/2019',3,'Tuesday',24,267,'Weekday',39,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190925,'2019-09-25','2019/09/25','09/25/2019','25/09/2019',4,'Wednesday',25,268,'Weekday',39,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190926,'2019-09-26','2019/09/26','09/26/2019','26/09/2019',5,'Thursday',26,269,'Weekday',39,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190927,'2019-09-27','2019/09/27','09/27/2019','27/09/2019',6,'Friday',27,270,'Weekday',39,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190928,'2019-09-28','2019/09/28','09/28/2019','28/09/2019',7,'Saturday',28,271,'Weekend',39,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190929,'2019-09-29','2019/09/29','09/29/2019','29/09/2019',1,'Sunday',29,272,'Weekend',39,'September',9,'N',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20190930,'2019-09-30','2019/09/30','09/30/2019','30/09/2019',2,'Monday',30,273,'Weekday',40,'September',9,'Y',3,2019,'2019-09','2019Q3',10,4,2019,'2019-10','2019Q4'),(20191001,'2019-10-01','2019/10/01','10/01/2019','01/10/2019',3,'Tuesday',1,274,'Weekday',40,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191002,'2019-10-02','2019/10/02','10/02/2019','02/10/2019',4,'Wednesday',2,275,'Weekday',40,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191003,'2019-10-03','2019/10/03','10/03/2019','03/10/2019',5,'Thursday',3,276,'Weekday',40,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191004,'2019-10-04','2019/10/04','10/04/2019','04/10/2019',6,'Friday',4,277,'Weekday',40,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191005,'2019-10-05','2019/10/05','10/05/2019','05/10/2019',7,'Saturday',5,278,'Weekend',40,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191006,'2019-10-06','2019/10/06','10/06/2019','06/10/2019',1,'Sunday',6,279,'Weekend',40,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191007,'2019-10-07','2019/10/07','10/07/2019','07/10/2019',2,'Monday',7,280,'Weekday',41,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191008,'2019-10-08','2019/10/08','10/08/2019','08/10/2019',3,'Tuesday',8,281,'Weekday',41,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191009,'2019-10-09','2019/10/09','10/09/2019','09/10/2019',4,'Wednesday',9,282,'Weekday',41,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191010,'2019-10-10','2019/10/10','10/10/2019','10/10/2019',5,'Thursday',10,283,'Weekday',41,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191011,'2019-10-11','2019/10/11','10/11/2019','11/10/2019',6,'Friday',11,284,'Weekday',41,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191012,'2019-10-12','2019/10/12','10/12/2019','12/10/2019',7,'Saturday',12,285,'Weekend',41,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191013,'2019-10-13','2019/10/13','10/13/2019','13/10/2019',1,'Sunday',13,286,'Weekend',41,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191014,'2019-10-14','2019/10/14','10/14/2019','14/10/2019',2,'Monday',14,287,'Weekday',42,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191015,'2019-10-15','2019/10/15','10/15/2019','15/10/2019',3,'Tuesday',15,288,'Weekday',42,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191016,'2019-10-16','2019/10/16','10/16/2019','16/10/2019',4,'Wednesday',16,289,'Weekday',42,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191017,'2019-10-17','2019/10/17','10/17/2019','17/10/2019',5,'Thursday',17,290,'Weekday',42,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191018,'2019-10-18','2019/10/18','10/18/2019','18/10/2019',6,'Friday',18,291,'Weekday',42,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191019,'2019-10-19','2019/10/19','10/19/2019','19/10/2019',7,'Saturday',19,292,'Weekend',42,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191020,'2019-10-20','2019/10/20','10/20/2019','20/10/2019',1,'Sunday',20,293,'Weekend',42,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191021,'2019-10-21','2019/10/21','10/21/2019','21/10/2019',2,'Monday',21,294,'Weekday',43,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191022,'2019-10-22','2019/10/22','10/22/2019','22/10/2019',3,'Tuesday',22,295,'Weekday',43,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191023,'2019-10-23','2019/10/23','10/23/2019','23/10/2019',4,'Wednesday',23,296,'Weekday',43,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191024,'2019-10-24','2019/10/24','10/24/2019','24/10/2019',5,'Thursday',24,297,'Weekday',43,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191025,'2019-10-25','2019/10/25','10/25/2019','25/10/2019',6,'Friday',25,298,'Weekday',43,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191026,'2019-10-26','2019/10/26','10/26/2019','26/10/2019',7,'Saturday',26,299,'Weekend',43,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191027,'2019-10-27','2019/10/27','10/27/2019','27/10/2019',1,'Sunday',27,300,'Weekend',43,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191028,'2019-10-28','2019/10/28','10/28/2019','28/10/2019',2,'Monday',28,301,'Weekday',44,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191029,'2019-10-29','2019/10/29','10/29/2019','29/10/2019',3,'Tuesday',29,302,'Weekday',44,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191030,'2019-10-30','2019/10/30','10/30/2019','30/10/2019',4,'Wednesday',30,303,'Weekday',44,'October',10,'N',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191031,'2019-10-31','2019/10/31','10/31/2019','31/10/2019',5,'Thursday',31,304,'Weekday',44,'October',10,'Y',4,2019,'2019-10','2019Q4',11,4,2019,'2019-11','2019Q4'),(20191101,'2019-11-01','2019/11/01','11/01/2019','01/11/2019',6,'Friday',1,305,'Weekday',44,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191102,'2019-11-02','2019/11/02','11/02/2019','02/11/2019',7,'Saturday',2,306,'Weekend',44,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191103,'2019-11-03','2019/11/03','11/03/2019','03/11/2019',1,'Sunday',3,307,'Weekend',44,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191104,'2019-11-04','2019/11/04','11/04/2019','04/11/2019',2,'Monday',4,308,'Weekday',45,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191105,'2019-11-05','2019/11/05','11/05/2019','05/11/2019',3,'Tuesday',5,309,'Weekday',45,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191106,'2019-11-06','2019/11/06','11/06/2019','06/11/2019',4,'Wednesday',6,310,'Weekday',45,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191107,'2019-11-07','2019/11/07','11/07/2019','07/11/2019',5,'Thursday',7,311,'Weekday',45,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191108,'2019-11-08','2019/11/08','11/08/2019','08/11/2019',6,'Friday',8,312,'Weekday',45,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191109,'2019-11-09','2019/11/09','11/09/2019','09/11/2019',7,'Saturday',9,313,'Weekend',45,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191110,'2019-11-10','2019/11/10','11/10/2019','10/11/2019',1,'Sunday',10,314,'Weekend',45,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191111,'2019-11-11','2019/11/11','11/11/2019','11/11/2019',2,'Monday',11,315,'Weekday',46,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191112,'2019-11-12','2019/11/12','11/12/2019','12/11/2019',3,'Tuesday',12,316,'Weekday',46,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191113,'2019-11-13','2019/11/13','11/13/2019','13/11/2019',4,'Wednesday',13,317,'Weekday',46,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191114,'2019-11-14','2019/11/14','11/14/2019','14/11/2019',5,'Thursday',14,318,'Weekday',46,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191115,'2019-11-15','2019/11/15','11/15/2019','15/11/2019',6,'Friday',15,319,'Weekday',46,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191116,'2019-11-16','2019/11/16','11/16/2019','16/11/2019',7,'Saturday',16,320,'Weekend',46,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191117,'2019-11-17','2019/11/17','11/17/2019','17/11/2019',1,'Sunday',17,321,'Weekend',46,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191118,'2019-11-18','2019/11/18','11/18/2019','18/11/2019',2,'Monday',18,322,'Weekday',47,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191119,'2019-11-19','2019/11/19','11/19/2019','19/11/2019',3,'Tuesday',19,323,'Weekday',47,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191120,'2019-11-20','2019/11/20','11/20/2019','20/11/2019',4,'Wednesday',20,324,'Weekday',47,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191121,'2019-11-21','2019/11/21','11/21/2019','21/11/2019',5,'Thursday',21,325,'Weekday',47,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191122,'2019-11-22','2019/11/22','11/22/2019','22/11/2019',6,'Friday',22,326,'Weekday',47,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191123,'2019-11-23','2019/11/23','11/23/2019','23/11/2019',7,'Saturday',23,327,'Weekend',47,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191124,'2019-11-24','2019/11/24','11/24/2019','24/11/2019',1,'Sunday',24,328,'Weekend',47,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191125,'2019-11-25','2019/11/25','11/25/2019','25/11/2019',2,'Monday',25,329,'Weekday',48,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191126,'2019-11-26','2019/11/26','11/26/2019','26/11/2019',3,'Tuesday',26,330,'Weekday',48,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191127,'2019-11-27','2019/11/27','11/27/2019','27/11/2019',4,'Wednesday',27,331,'Weekday',48,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191128,'2019-11-28','2019/11/28','11/28/2019','28/11/2019',5,'Thursday',28,332,'Weekday',48,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191129,'2019-11-29','2019/11/29','11/29/2019','29/11/2019',6,'Friday',29,333,'Weekday',48,'November',11,'N',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191130,'2019-11-30','2019/11/30','11/30/2019','30/11/2019',7,'Saturday',30,334,'Weekend',48,'November',11,'Y',4,2019,'2019-11','2019Q4',12,4,2019,'2019-12','2019Q4'),(20191201,'2019-12-01','2019/12/01','12/01/2019','01/12/2019',1,'Sunday',1,335,'Weekend',48,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191202,'2019-12-02','2019/12/02','12/02/2019','02/12/2019',2,'Monday',2,336,'Weekday',49,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191203,'2019-12-03','2019/12/03','12/03/2019','03/12/2019',3,'Tuesday',3,337,'Weekday',49,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191204,'2019-12-04','2019/12/04','12/04/2019','04/12/2019',4,'Wednesday',4,338,'Weekday',49,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191205,'2019-12-05','2019/12/05','12/05/2019','05/12/2019',5,'Thursday',5,339,'Weekday',49,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191206,'2019-12-06','2019/12/06','12/06/2019','06/12/2019',6,'Friday',6,340,'Weekday',49,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191207,'2019-12-07','2019/12/07','12/07/2019','07/12/2019',7,'Saturday',7,341,'Weekend',49,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191208,'2019-12-08','2019/12/08','12/08/2019','08/12/2019',1,'Sunday',8,342,'Weekend',49,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191209,'2019-12-09','2019/12/09','12/09/2019','09/12/2019',2,'Monday',9,343,'Weekday',50,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191210,'2019-12-10','2019/12/10','12/10/2019','10/12/2019',3,'Tuesday',10,344,'Weekday',50,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191211,'2019-12-11','2019/12/11','12/11/2019','11/12/2019',4,'Wednesday',11,345,'Weekday',50,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191212,'2019-12-12','2019/12/12','12/12/2019','12/12/2019',5,'Thursday',12,346,'Weekday',50,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191213,'2019-12-13','2019/12/13','12/13/2019','13/12/2019',6,'Friday',13,347,'Weekday',50,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191214,'2019-12-14','2019/12/14','12/14/2019','14/12/2019',7,'Saturday',14,348,'Weekend',50,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191215,'2019-12-15','2019/12/15','12/15/2019','15/12/2019',1,'Sunday',15,349,'Weekend',50,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191216,'2019-12-16','2019/12/16','12/16/2019','16/12/2019',2,'Monday',16,350,'Weekday',51,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191217,'2019-12-17','2019/12/17','12/17/2019','17/12/2019',3,'Tuesday',17,351,'Weekday',51,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191218,'2019-12-18','2019/12/18','12/18/2019','18/12/2019',4,'Wednesday',18,352,'Weekday',51,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191219,'2019-12-19','2019/12/19','12/19/2019','19/12/2019',5,'Thursday',19,353,'Weekday',51,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191220,'2019-12-20','2019/12/20','12/20/2019','20/12/2019',6,'Friday',20,354,'Weekday',51,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191221,'2019-12-21','2019/12/21','12/21/2019','21/12/2019',7,'Saturday',21,355,'Weekend',51,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191222,'2019-12-22','2019/12/22','12/22/2019','22/12/2019',1,'Sunday',22,356,'Weekend',51,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191223,'2019-12-23','2019/12/23','12/23/2019','23/12/2019',2,'Monday',23,357,'Weekday',52,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191224,'2019-12-24','2019/12/24','12/24/2019','24/12/2019',3,'Tuesday',24,358,'Weekday',52,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191225,'2019-12-25','2019/12/25','12/25/2019','25/12/2019',4,'Wednesday',25,359,'Weekday',52,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191226,'2019-12-26','2019/12/26','12/26/2019','26/12/2019',5,'Thursday',26,360,'Weekday',52,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191227,'2019-12-27','2019/12/27','12/27/2019','27/12/2019',6,'Friday',27,361,'Weekday',52,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191228,'2019-12-28','2019/12/28','12/28/2019','28/12/2019',7,'Saturday',28,362,'Weekend',52,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191229,'2019-12-29','2019/12/29','12/29/2019','29/12/2019',1,'Sunday',29,363,'Weekend',52,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191230,'2019-12-30','2019/12/30','12/30/2019','30/12/2019',2,'Monday',30,364,'Weekday',1,'December',12,'N',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20191231,'2019-12-31','2019/12/31','12/31/2019','31/12/2019',3,'Tuesday',31,365,'Weekday',1,'December',12,'Y',4,2019,'2019-12','2019Q4',1,1,2020,'2020-01','2020Q1'),(20200101,'2020-01-01','2020/01/01','01/01/2020','01/01/2020',4,'Wednesday',1,1,'Weekday',1,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200102,'2020-01-02','2020/01/02','01/02/2020','02/01/2020',5,'Thursday',2,2,'Weekday',1,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200103,'2020-01-03','2020/01/03','01/03/2020','03/01/2020',6,'Friday',3,3,'Weekday',1,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200104,'2020-01-04','2020/01/04','01/04/2020','04/01/2020',7,'Saturday',4,4,'Weekend',1,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200105,'2020-01-05','2020/01/05','01/05/2020','05/01/2020',1,'Sunday',5,5,'Weekend',1,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200106,'2020-01-06','2020/01/06','01/06/2020','06/01/2020',2,'Monday',6,6,'Weekday',2,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200107,'2020-01-07','2020/01/07','01/07/2020','07/01/2020',3,'Tuesday',7,7,'Weekday',2,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200108,'2020-01-08','2020/01/08','01/08/2020','08/01/2020',4,'Wednesday',8,8,'Weekday',2,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200109,'2020-01-09','2020/01/09','01/09/2020','09/01/2020',5,'Thursday',9,9,'Weekday',2,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200110,'2020-01-10','2020/01/10','01/10/2020','10/01/2020',6,'Friday',10,10,'Weekday',2,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200111,'2020-01-11','2020/01/11','01/11/2020','11/01/2020',7,'Saturday',11,11,'Weekend',2,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200112,'2020-01-12','2020/01/12','01/12/2020','12/01/2020',1,'Sunday',12,12,'Weekend',2,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200113,'2020-01-13','2020/01/13','01/13/2020','13/01/2020',2,'Monday',13,13,'Weekday',3,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200114,'2020-01-14','2020/01/14','01/14/2020','14/01/2020',3,'Tuesday',14,14,'Weekday',3,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200115,'2020-01-15','2020/01/15','01/15/2020','15/01/2020',4,'Wednesday',15,15,'Weekday',3,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200116,'2020-01-16','2020/01/16','01/16/2020','16/01/2020',5,'Thursday',16,16,'Weekday',3,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200117,'2020-01-17','2020/01/17','01/17/2020','17/01/2020',6,'Friday',17,17,'Weekday',3,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200118,'2020-01-18','2020/01/18','01/18/2020','18/01/2020',7,'Saturday',18,18,'Weekend',3,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200119,'2020-01-19','2020/01/19','01/19/2020','19/01/2020',1,'Sunday',19,19,'Weekend',3,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200120,'2020-01-20','2020/01/20','01/20/2020','20/01/2020',2,'Monday',20,20,'Weekday',4,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200121,'2020-01-21','2020/01/21','01/21/2020','21/01/2020',3,'Tuesday',21,21,'Weekday',4,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200122,'2020-01-22','2020/01/22','01/22/2020','22/01/2020',4,'Wednesday',22,22,'Weekday',4,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200123,'2020-01-23','2020/01/23','01/23/2020','23/01/2020',5,'Thursday',23,23,'Weekday',4,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200124,'2020-01-24','2020/01/24','01/24/2020','24/01/2020',6,'Friday',24,24,'Weekday',4,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200125,'2020-01-25','2020/01/25','01/25/2020','25/01/2020',7,'Saturday',25,25,'Weekend',4,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200126,'2020-01-26','2020/01/26','01/26/2020','26/01/2020',1,'Sunday',26,26,'Weekend',4,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200127,'2020-01-27','2020/01/27','01/27/2020','27/01/2020',2,'Monday',27,27,'Weekday',5,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200128,'2020-01-28','2020/01/28','01/28/2020','28/01/2020',3,'Tuesday',28,28,'Weekday',5,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200129,'2020-01-29','2020/01/29','01/29/2020','29/01/2020',4,'Wednesday',29,29,'Weekday',5,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200130,'2020-01-30','2020/01/30','01/30/2020','30/01/2020',5,'Thursday',30,30,'Weekday',5,'January',1,'N',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200131,'2020-01-31','2020/01/31','01/31/2020','31/01/2020',6,'Friday',31,31,'Weekday',5,'January',1,'Y',1,2020,'2020-01','2020Q1',2,1,2020,'2020-02','2020Q1'),(20200201,'2020-02-01','2020/02/01','02/01/2020','01/02/2020',7,'Saturday',1,32,'Weekend',5,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200202,'2020-02-02','2020/02/02','02/02/2020','02/02/2020',1,'Sunday',2,33,'Weekend',5,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200203,'2020-02-03','2020/02/03','02/03/2020','03/02/2020',2,'Monday',3,34,'Weekday',6,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200204,'2020-02-04','2020/02/04','02/04/2020','04/02/2020',3,'Tuesday',4,35,'Weekday',6,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200205,'2020-02-05','2020/02/05','02/05/2020','05/02/2020',4,'Wednesday',5,36,'Weekday',6,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200206,'2020-02-06','2020/02/06','02/06/2020','06/02/2020',5,'Thursday',6,37,'Weekday',6,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200207,'2020-02-07','2020/02/07','02/07/2020','07/02/2020',6,'Friday',7,38,'Weekday',6,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200208,'2020-02-08','2020/02/08','02/08/2020','08/02/2020',7,'Saturday',8,39,'Weekend',6,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200209,'2020-02-09','2020/02/09','02/09/2020','09/02/2020',1,'Sunday',9,40,'Weekend',6,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200210,'2020-02-10','2020/02/10','02/10/2020','10/02/2020',2,'Monday',10,41,'Weekday',7,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200211,'2020-02-11','2020/02/11','02/11/2020','11/02/2020',3,'Tuesday',11,42,'Weekday',7,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200212,'2020-02-12','2020/02/12','02/12/2020','12/02/2020',4,'Wednesday',12,43,'Weekday',7,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200213,'2020-02-13','2020/02/13','02/13/2020','13/02/2020',5,'Thursday',13,44,'Weekday',7,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200214,'2020-02-14','2020/02/14','02/14/2020','14/02/2020',6,'Friday',14,45,'Weekday',7,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200215,'2020-02-15','2020/02/15','02/15/2020','15/02/2020',7,'Saturday',15,46,'Weekend',7,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200216,'2020-02-16','2020/02/16','02/16/2020','16/02/2020',1,'Sunday',16,47,'Weekend',7,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200217,'2020-02-17','2020/02/17','02/17/2020','17/02/2020',2,'Monday',17,48,'Weekday',8,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200218,'2020-02-18','2020/02/18','02/18/2020','18/02/2020',3,'Tuesday',18,49,'Weekday',8,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200219,'2020-02-19','2020/02/19','02/19/2020','19/02/2020',4,'Wednesday',19,50,'Weekday',8,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200220,'2020-02-20','2020/02/20','02/20/2020','20/02/2020',5,'Thursday',20,51,'Weekday',8,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200221,'2020-02-21','2020/02/21','02/21/2020','21/02/2020',6,'Friday',21,52,'Weekday',8,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200222,'2020-02-22','2020/02/22','02/22/2020','22/02/2020',7,'Saturday',22,53,'Weekend',8,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200223,'2020-02-23','2020/02/23','02/23/2020','23/02/2020',1,'Sunday',23,54,'Weekend',8,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200224,'2020-02-24','2020/02/24','02/24/2020','24/02/2020',2,'Monday',24,55,'Weekday',9,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200225,'2020-02-25','2020/02/25','02/25/2020','25/02/2020',3,'Tuesday',25,56,'Weekday',9,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200226,'2020-02-26','2020/02/26','02/26/2020','26/02/2020',4,'Wednesday',26,57,'Weekday',9,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200227,'2020-02-27','2020/02/27','02/27/2020','27/02/2020',5,'Thursday',27,58,'Weekday',9,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200228,'2020-02-28','2020/02/28','02/28/2020','28/02/2020',6,'Friday',28,59,'Weekday',9,'February',2,'N',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200229,'2020-02-29','2020/02/29','02/29/2020','29/02/2020',7,'Saturday',29,60,'Weekend',9,'February',2,'Y',1,2020,'2020-02','2020Q1',3,1,2020,'2020-03','2020Q1'),(20200301,'2020-03-01','2020/03/01','03/01/2020','01/03/2020',1,'Sunday',1,61,'Weekend',9,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200302,'2020-03-02','2020/03/02','03/02/2020','02/03/2020',2,'Monday',2,62,'Weekday',10,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200303,'2020-03-03','2020/03/03','03/03/2020','03/03/2020',3,'Tuesday',3,63,'Weekday',10,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200304,'2020-03-04','2020/03/04','03/04/2020','04/03/2020',4,'Wednesday',4,64,'Weekday',10,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200305,'2020-03-05','2020/03/05','03/05/2020','05/03/2020',5,'Thursday',5,65,'Weekday',10,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200306,'2020-03-06','2020/03/06','03/06/2020','06/03/2020',6,'Friday',6,66,'Weekday',10,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200307,'2020-03-07','2020/03/07','03/07/2020','07/03/2020',7,'Saturday',7,67,'Weekend',10,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200308,'2020-03-08','2020/03/08','03/08/2020','08/03/2020',1,'Sunday',8,68,'Weekend',10,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200309,'2020-03-09','2020/03/09','03/09/2020','09/03/2020',2,'Monday',9,69,'Weekday',11,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200310,'2020-03-10','2020/03/10','03/10/2020','10/03/2020',3,'Tuesday',10,70,'Weekday',11,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200311,'2020-03-11','2020/03/11','03/11/2020','11/03/2020',4,'Wednesday',11,71,'Weekday',11,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200312,'2020-03-12','2020/03/12','03/12/2020','12/03/2020',5,'Thursday',12,72,'Weekday',11,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200313,'2020-03-13','2020/03/13','03/13/2020','13/03/2020',6,'Friday',13,73,'Weekday',11,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200314,'2020-03-14','2020/03/14','03/14/2020','14/03/2020',7,'Saturday',14,74,'Weekend',11,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200315,'2020-03-15','2020/03/15','03/15/2020','15/03/2020',1,'Sunday',15,75,'Weekend',11,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200316,'2020-03-16','2020/03/16','03/16/2020','16/03/2020',2,'Monday',16,76,'Weekday',12,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200317,'2020-03-17','2020/03/17','03/17/2020','17/03/2020',3,'Tuesday',17,77,'Weekday',12,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200318,'2020-03-18','2020/03/18','03/18/2020','18/03/2020',4,'Wednesday',18,78,'Weekday',12,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200319,'2020-03-19','2020/03/19','03/19/2020','19/03/2020',5,'Thursday',19,79,'Weekday',12,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200320,'2020-03-20','2020/03/20','03/20/2020','20/03/2020',6,'Friday',20,80,'Weekday',12,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200321,'2020-03-21','2020/03/21','03/21/2020','21/03/2020',7,'Saturday',21,81,'Weekend',12,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200322,'2020-03-22','2020/03/22','03/22/2020','22/03/2020',1,'Sunday',22,82,'Weekend',12,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200323,'2020-03-23','2020/03/23','03/23/2020','23/03/2020',2,'Monday',23,83,'Weekday',13,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200324,'2020-03-24','2020/03/24','03/24/2020','24/03/2020',3,'Tuesday',24,84,'Weekday',13,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200325,'2020-03-25','2020/03/25','03/25/2020','25/03/2020',4,'Wednesday',25,85,'Weekday',13,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200326,'2020-03-26','2020/03/26','03/26/2020','26/03/2020',5,'Thursday',26,86,'Weekday',13,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200327,'2020-03-27','2020/03/27','03/27/2020','27/03/2020',6,'Friday',27,87,'Weekday',13,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200328,'2020-03-28','2020/03/28','03/28/2020','28/03/2020',7,'Saturday',28,88,'Weekend',13,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200329,'2020-03-29','2020/03/29','03/29/2020','29/03/2020',1,'Sunday',29,89,'Weekend',13,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200330,'2020-03-30','2020/03/30','03/30/2020','30/03/2020',2,'Monday',30,90,'Weekday',14,'March',3,'N',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200331,'2020-03-31','2020/03/31','03/31/2020','31/03/2020',3,'Tuesday',31,91,'Weekday',14,'March',3,'Y',1,2020,'2020-03','2020Q1',4,2,2020,'2020-04','2020Q2'),(20200401,'2020-04-01','2020/04/01','04/01/2020','01/04/2020',4,'Wednesday',1,92,'Weekday',14,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200402,'2020-04-02','2020/04/02','04/02/2020','02/04/2020',5,'Thursday',2,93,'Weekday',14,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200403,'2020-04-03','2020/04/03','04/03/2020','03/04/2020',6,'Friday',3,94,'Weekday',14,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200404,'2020-04-04','2020/04/04','04/04/2020','04/04/2020',7,'Saturday',4,95,'Weekend',14,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200405,'2020-04-05','2020/04/05','04/05/2020','05/04/2020',1,'Sunday',5,96,'Weekend',14,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200406,'2020-04-06','2020/04/06','04/06/2020','06/04/2020',2,'Monday',6,97,'Weekday',15,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200407,'2020-04-07','2020/04/07','04/07/2020','07/04/2020',3,'Tuesday',7,98,'Weekday',15,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200408,'2020-04-08','2020/04/08','04/08/2020','08/04/2020',4,'Wednesday',8,99,'Weekday',15,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200409,'2020-04-09','2020/04/09','04/09/2020','09/04/2020',5,'Thursday',9,100,'Weekday',15,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200410,'2020-04-10','2020/04/10','04/10/2020','10/04/2020',6,'Friday',10,101,'Weekday',15,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200411,'2020-04-11','2020/04/11','04/11/2020','11/04/2020',7,'Saturday',11,102,'Weekend',15,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200412,'2020-04-12','2020/04/12','04/12/2020','12/04/2020',1,'Sunday',12,103,'Weekend',15,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200413,'2020-04-13','2020/04/13','04/13/2020','13/04/2020',2,'Monday',13,104,'Weekday',16,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200414,'2020-04-14','2020/04/14','04/14/2020','14/04/2020',3,'Tuesday',14,105,'Weekday',16,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200415,'2020-04-15','2020/04/15','04/15/2020','15/04/2020',4,'Wednesday',15,106,'Weekday',16,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200416,'2020-04-16','2020/04/16','04/16/2020','16/04/2020',5,'Thursday',16,107,'Weekday',16,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200417,'2020-04-17','2020/04/17','04/17/2020','17/04/2020',6,'Friday',17,108,'Weekday',16,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200418,'2020-04-18','2020/04/18','04/18/2020','18/04/2020',7,'Saturday',18,109,'Weekend',16,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200419,'2020-04-19','2020/04/19','04/19/2020','19/04/2020',1,'Sunday',19,110,'Weekend',16,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200420,'2020-04-20','2020/04/20','04/20/2020','20/04/2020',2,'Monday',20,111,'Weekday',17,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200421,'2020-04-21','2020/04/21','04/21/2020','21/04/2020',3,'Tuesday',21,112,'Weekday',17,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200422,'2020-04-22','2020/04/22','04/22/2020','22/04/2020',4,'Wednesday',22,113,'Weekday',17,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200423,'2020-04-23','2020/04/23','04/23/2020','23/04/2020',5,'Thursday',23,114,'Weekday',17,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200424,'2020-04-24','2020/04/24','04/24/2020','24/04/2020',6,'Friday',24,115,'Weekday',17,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200425,'2020-04-25','2020/04/25','04/25/2020','25/04/2020',7,'Saturday',25,116,'Weekend',17,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200426,'2020-04-26','2020/04/26','04/26/2020','26/04/2020',1,'Sunday',26,117,'Weekend',17,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200427,'2020-04-27','2020/04/27','04/27/2020','27/04/2020',2,'Monday',27,118,'Weekday',18,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200428,'2020-04-28','2020/04/28','04/28/2020','28/04/2020',3,'Tuesday',28,119,'Weekday',18,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200429,'2020-04-29','2020/04/29','04/29/2020','29/04/2020',4,'Wednesday',29,120,'Weekday',18,'April',4,'N',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200430,'2020-04-30','2020/04/30','04/30/2020','30/04/2020',5,'Thursday',30,121,'Weekday',18,'April',4,'Y',2,2020,'2020-04','2020Q2',5,2,2020,'2020-05','2020Q2'),(20200501,'2020-05-01','2020/05/01','05/01/2020','01/05/2020',6,'Friday',1,122,'Weekday',18,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200502,'2020-05-02','2020/05/02','05/02/2020','02/05/2020',7,'Saturday',2,123,'Weekend',18,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200503,'2020-05-03','2020/05/03','05/03/2020','03/05/2020',1,'Sunday',3,124,'Weekend',18,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200504,'2020-05-04','2020/05/04','05/04/2020','04/05/2020',2,'Monday',4,125,'Weekday',19,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200505,'2020-05-05','2020/05/05','05/05/2020','05/05/2020',3,'Tuesday',5,126,'Weekday',19,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200506,'2020-05-06','2020/05/06','05/06/2020','06/05/2020',4,'Wednesday',6,127,'Weekday',19,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200507,'2020-05-07','2020/05/07','05/07/2020','07/05/2020',5,'Thursday',7,128,'Weekday',19,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200508,'2020-05-08','2020/05/08','05/08/2020','08/05/2020',6,'Friday',8,129,'Weekday',19,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200509,'2020-05-09','2020/05/09','05/09/2020','09/05/2020',7,'Saturday',9,130,'Weekend',19,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200510,'2020-05-10','2020/05/10','05/10/2020','10/05/2020',1,'Sunday',10,131,'Weekend',19,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200511,'2020-05-11','2020/05/11','05/11/2020','11/05/2020',2,'Monday',11,132,'Weekday',20,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200512,'2020-05-12','2020/05/12','05/12/2020','12/05/2020',3,'Tuesday',12,133,'Weekday',20,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200513,'2020-05-13','2020/05/13','05/13/2020','13/05/2020',4,'Wednesday',13,134,'Weekday',20,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200514,'2020-05-14','2020/05/14','05/14/2020','14/05/2020',5,'Thursday',14,135,'Weekday',20,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200515,'2020-05-15','2020/05/15','05/15/2020','15/05/2020',6,'Friday',15,136,'Weekday',20,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200516,'2020-05-16','2020/05/16','05/16/2020','16/05/2020',7,'Saturday',16,137,'Weekend',20,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200517,'2020-05-17','2020/05/17','05/17/2020','17/05/2020',1,'Sunday',17,138,'Weekend',20,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200518,'2020-05-18','2020/05/18','05/18/2020','18/05/2020',2,'Monday',18,139,'Weekday',21,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200519,'2020-05-19','2020/05/19','05/19/2020','19/05/2020',3,'Tuesday',19,140,'Weekday',21,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200520,'2020-05-20','2020/05/20','05/20/2020','20/05/2020',4,'Wednesday',20,141,'Weekday',21,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200521,'2020-05-21','2020/05/21','05/21/2020','21/05/2020',5,'Thursday',21,142,'Weekday',21,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200522,'2020-05-22','2020/05/22','05/22/2020','22/05/2020',6,'Friday',22,143,'Weekday',21,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200523,'2020-05-23','2020/05/23','05/23/2020','23/05/2020',7,'Saturday',23,144,'Weekend',21,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200524,'2020-05-24','2020/05/24','05/24/2020','24/05/2020',1,'Sunday',24,145,'Weekend',21,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200525,'2020-05-25','2020/05/25','05/25/2020','25/05/2020',2,'Monday',25,146,'Weekday',22,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200526,'2020-05-26','2020/05/26','05/26/2020','26/05/2020',3,'Tuesday',26,147,'Weekday',22,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200527,'2020-05-27','2020/05/27','05/27/2020','27/05/2020',4,'Wednesday',27,148,'Weekday',22,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200528,'2020-05-28','2020/05/28','05/28/2020','28/05/2020',5,'Thursday',28,149,'Weekday',22,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200529,'2020-05-29','2020/05/29','05/29/2020','29/05/2020',6,'Friday',29,150,'Weekday',22,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200530,'2020-05-30','2020/05/30','05/30/2020','30/05/2020',7,'Saturday',30,151,'Weekend',22,'May',5,'N',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200531,'2020-05-31','2020/05/31','05/31/2020','31/05/2020',1,'Sunday',31,152,'Weekend',22,'May',5,'Y',2,2020,'2020-05','2020Q2',6,2,2020,'2020-06','2020Q2'),(20200601,'2020-06-01','2020/06/01','06/01/2020','01/06/2020',2,'Monday',1,153,'Weekday',23,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200602,'2020-06-02','2020/06/02','06/02/2020','02/06/2020',3,'Tuesday',2,154,'Weekday',23,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200603,'2020-06-03','2020/06/03','06/03/2020','03/06/2020',4,'Wednesday',3,155,'Weekday',23,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200604,'2020-06-04','2020/06/04','06/04/2020','04/06/2020',5,'Thursday',4,156,'Weekday',23,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200605,'2020-06-05','2020/06/05','06/05/2020','05/06/2020',6,'Friday',5,157,'Weekday',23,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200606,'2020-06-06','2020/06/06','06/06/2020','06/06/2020',7,'Saturday',6,158,'Weekend',23,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200607,'2020-06-07','2020/06/07','06/07/2020','07/06/2020',1,'Sunday',7,159,'Weekend',23,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200608,'2020-06-08','2020/06/08','06/08/2020','08/06/2020',2,'Monday',8,160,'Weekday',24,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200609,'2020-06-09','2020/06/09','06/09/2020','09/06/2020',3,'Tuesday',9,161,'Weekday',24,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200610,'2020-06-10','2020/06/10','06/10/2020','10/06/2020',4,'Wednesday',10,162,'Weekday',24,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200611,'2020-06-11','2020/06/11','06/11/2020','11/06/2020',5,'Thursday',11,163,'Weekday',24,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200612,'2020-06-12','2020/06/12','06/12/2020','12/06/2020',6,'Friday',12,164,'Weekday',24,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200613,'2020-06-13','2020/06/13','06/13/2020','13/06/2020',7,'Saturday',13,165,'Weekend',24,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200614,'2020-06-14','2020/06/14','06/14/2020','14/06/2020',1,'Sunday',14,166,'Weekend',24,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200615,'2020-06-15','2020/06/15','06/15/2020','15/06/2020',2,'Monday',15,167,'Weekday',25,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200616,'2020-06-16','2020/06/16','06/16/2020','16/06/2020',3,'Tuesday',16,168,'Weekday',25,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200617,'2020-06-17','2020/06/17','06/17/2020','17/06/2020',4,'Wednesday',17,169,'Weekday',25,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200618,'2020-06-18','2020/06/18','06/18/2020','18/06/2020',5,'Thursday',18,170,'Weekday',25,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200619,'2020-06-19','2020/06/19','06/19/2020','19/06/2020',6,'Friday',19,171,'Weekday',25,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200620,'2020-06-20','2020/06/20','06/20/2020','20/06/2020',7,'Saturday',20,172,'Weekend',25,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200621,'2020-06-21','2020/06/21','06/21/2020','21/06/2020',1,'Sunday',21,173,'Weekend',25,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200622,'2020-06-22','2020/06/22','06/22/2020','22/06/2020',2,'Monday',22,174,'Weekday',26,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200623,'2020-06-23','2020/06/23','06/23/2020','23/06/2020',3,'Tuesday',23,175,'Weekday',26,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200624,'2020-06-24','2020/06/24','06/24/2020','24/06/2020',4,'Wednesday',24,176,'Weekday',26,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200625,'2020-06-25','2020/06/25','06/25/2020','25/06/2020',5,'Thursday',25,177,'Weekday',26,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200626,'2020-06-26','2020/06/26','06/26/2020','26/06/2020',6,'Friday',26,178,'Weekday',26,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200627,'2020-06-27','2020/06/27','06/27/2020','27/06/2020',7,'Saturday',27,179,'Weekend',26,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200628,'2020-06-28','2020/06/28','06/28/2020','28/06/2020',1,'Sunday',28,180,'Weekend',26,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200629,'2020-06-29','2020/06/29','06/29/2020','29/06/2020',2,'Monday',29,181,'Weekday',27,'June',6,'N',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200630,'2020-06-30','2020/06/30','06/30/2020','30/06/2020',3,'Tuesday',30,182,'Weekday',27,'June',6,'Y',2,2020,'2020-06','2020Q2',7,3,2020,'2020-07','2020Q3'),(20200701,'2020-07-01','2020/07/01','07/01/2020','01/07/2020',4,'Wednesday',1,183,'Weekday',27,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200702,'2020-07-02','2020/07/02','07/02/2020','02/07/2020',5,'Thursday',2,184,'Weekday',27,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200703,'2020-07-03','2020/07/03','07/03/2020','03/07/2020',6,'Friday',3,185,'Weekday',27,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200704,'2020-07-04','2020/07/04','07/04/2020','04/07/2020',7,'Saturday',4,186,'Weekend',27,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200705,'2020-07-05','2020/07/05','07/05/2020','05/07/2020',1,'Sunday',5,187,'Weekend',27,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200706,'2020-07-06','2020/07/06','07/06/2020','06/07/2020',2,'Monday',6,188,'Weekday',28,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200707,'2020-07-07','2020/07/07','07/07/2020','07/07/2020',3,'Tuesday',7,189,'Weekday',28,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200708,'2020-07-08','2020/07/08','07/08/2020','08/07/2020',4,'Wednesday',8,190,'Weekday',28,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200709,'2020-07-09','2020/07/09','07/09/2020','09/07/2020',5,'Thursday',9,191,'Weekday',28,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200710,'2020-07-10','2020/07/10','07/10/2020','10/07/2020',6,'Friday',10,192,'Weekday',28,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200711,'2020-07-11','2020/07/11','07/11/2020','11/07/2020',7,'Saturday',11,193,'Weekend',28,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200712,'2020-07-12','2020/07/12','07/12/2020','12/07/2020',1,'Sunday',12,194,'Weekend',28,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200713,'2020-07-13','2020/07/13','07/13/2020','13/07/2020',2,'Monday',13,195,'Weekday',29,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200714,'2020-07-14','2020/07/14','07/14/2020','14/07/2020',3,'Tuesday',14,196,'Weekday',29,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200715,'2020-07-15','2020/07/15','07/15/2020','15/07/2020',4,'Wednesday',15,197,'Weekday',29,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200716,'2020-07-16','2020/07/16','07/16/2020','16/07/2020',5,'Thursday',16,198,'Weekday',29,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200717,'2020-07-17','2020/07/17','07/17/2020','17/07/2020',6,'Friday',17,199,'Weekday',29,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200718,'2020-07-18','2020/07/18','07/18/2020','18/07/2020',7,'Saturday',18,200,'Weekend',29,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200719,'2020-07-19','2020/07/19','07/19/2020','19/07/2020',1,'Sunday',19,201,'Weekend',29,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200720,'2020-07-20','2020/07/20','07/20/2020','20/07/2020',2,'Monday',20,202,'Weekday',30,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200721,'2020-07-21','2020/07/21','07/21/2020','21/07/2020',3,'Tuesday',21,203,'Weekday',30,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200722,'2020-07-22','2020/07/22','07/22/2020','22/07/2020',4,'Wednesday',22,204,'Weekday',30,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200723,'2020-07-23','2020/07/23','07/23/2020','23/07/2020',5,'Thursday',23,205,'Weekday',30,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200724,'2020-07-24','2020/07/24','07/24/2020','24/07/2020',6,'Friday',24,206,'Weekday',30,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200725,'2020-07-25','2020/07/25','07/25/2020','25/07/2020',7,'Saturday',25,207,'Weekend',30,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200726,'2020-07-26','2020/07/26','07/26/2020','26/07/2020',1,'Sunday',26,208,'Weekend',30,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200727,'2020-07-27','2020/07/27','07/27/2020','27/07/2020',2,'Monday',27,209,'Weekday',31,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200728,'2020-07-28','2020/07/28','07/28/2020','28/07/2020',3,'Tuesday',28,210,'Weekday',31,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200729,'2020-07-29','2020/07/29','07/29/2020','29/07/2020',4,'Wednesday',29,211,'Weekday',31,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200730,'2020-07-30','2020/07/30','07/30/2020','30/07/2020',5,'Thursday',30,212,'Weekday',31,'July',7,'N',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200731,'2020-07-31','2020/07/31','07/31/2020','31/07/2020',6,'Friday',31,213,'Weekday',31,'July',7,'Y',3,2020,'2020-07','2020Q3',8,3,2020,'2020-08','2020Q3'),(20200801,'2020-08-01','2020/08/01','08/01/2020','01/08/2020',7,'Saturday',1,214,'Weekend',31,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200802,'2020-08-02','2020/08/02','08/02/2020','02/08/2020',1,'Sunday',2,215,'Weekend',31,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200803,'2020-08-03','2020/08/03','08/03/2020','03/08/2020',2,'Monday',3,216,'Weekday',32,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200804,'2020-08-04','2020/08/04','08/04/2020','04/08/2020',3,'Tuesday',4,217,'Weekday',32,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200805,'2020-08-05','2020/08/05','08/05/2020','05/08/2020',4,'Wednesday',5,218,'Weekday',32,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200806,'2020-08-06','2020/08/06','08/06/2020','06/08/2020',5,'Thursday',6,219,'Weekday',32,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200807,'2020-08-07','2020/08/07','08/07/2020','07/08/2020',6,'Friday',7,220,'Weekday',32,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200808,'2020-08-08','2020/08/08','08/08/2020','08/08/2020',7,'Saturday',8,221,'Weekend',32,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200809,'2020-08-09','2020/08/09','08/09/2020','09/08/2020',1,'Sunday',9,222,'Weekend',32,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200810,'2020-08-10','2020/08/10','08/10/2020','10/08/2020',2,'Monday',10,223,'Weekday',33,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200811,'2020-08-11','2020/08/11','08/11/2020','11/08/2020',3,'Tuesday',11,224,'Weekday',33,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200812,'2020-08-12','2020/08/12','08/12/2020','12/08/2020',4,'Wednesday',12,225,'Weekday',33,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200813,'2020-08-13','2020/08/13','08/13/2020','13/08/2020',5,'Thursday',13,226,'Weekday',33,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200814,'2020-08-14','2020/08/14','08/14/2020','14/08/2020',6,'Friday',14,227,'Weekday',33,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200815,'2020-08-15','2020/08/15','08/15/2020','15/08/2020',7,'Saturday',15,228,'Weekend',33,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200816,'2020-08-16','2020/08/16','08/16/2020','16/08/2020',1,'Sunday',16,229,'Weekend',33,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200817,'2020-08-17','2020/08/17','08/17/2020','17/08/2020',2,'Monday',17,230,'Weekday',34,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200818,'2020-08-18','2020/08/18','08/18/2020','18/08/2020',3,'Tuesday',18,231,'Weekday',34,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200819,'2020-08-19','2020/08/19','08/19/2020','19/08/2020',4,'Wednesday',19,232,'Weekday',34,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200820,'2020-08-20','2020/08/20','08/20/2020','20/08/2020',5,'Thursday',20,233,'Weekday',34,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200821,'2020-08-21','2020/08/21','08/21/2020','21/08/2020',6,'Friday',21,234,'Weekday',34,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200822,'2020-08-22','2020/08/22','08/22/2020','22/08/2020',7,'Saturday',22,235,'Weekend',34,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200823,'2020-08-23','2020/08/23','08/23/2020','23/08/2020',1,'Sunday',23,236,'Weekend',34,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200824,'2020-08-24','2020/08/24','08/24/2020','24/08/2020',2,'Monday',24,237,'Weekday',35,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200825,'2020-08-25','2020/08/25','08/25/2020','25/08/2020',3,'Tuesday',25,238,'Weekday',35,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200826,'2020-08-26','2020/08/26','08/26/2020','26/08/2020',4,'Wednesday',26,239,'Weekday',35,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200827,'2020-08-27','2020/08/27','08/27/2020','27/08/2020',5,'Thursday',27,240,'Weekday',35,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200828,'2020-08-28','2020/08/28','08/28/2020','28/08/2020',6,'Friday',28,241,'Weekday',35,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200829,'2020-08-29','2020/08/29','08/29/2020','29/08/2020',7,'Saturday',29,242,'Weekend',35,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200830,'2020-08-30','2020/08/30','08/30/2020','30/08/2020',1,'Sunday',30,243,'Weekend',35,'August',8,'N',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200831,'2020-08-31','2020/08/31','08/31/2020','31/08/2020',2,'Monday',31,244,'Weekday',36,'August',8,'Y',3,2020,'2020-08','2020Q3',9,3,2020,'2020-09','2020Q3'),(20200901,'2020-09-01','2020/09/01','09/01/2020','01/09/2020',3,'Tuesday',1,245,'Weekday',36,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200902,'2020-09-02','2020/09/02','09/02/2020','02/09/2020',4,'Wednesday',2,246,'Weekday',36,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200903,'2020-09-03','2020/09/03','09/03/2020','03/09/2020',5,'Thursday',3,247,'Weekday',36,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200904,'2020-09-04','2020/09/04','09/04/2020','04/09/2020',6,'Friday',4,248,'Weekday',36,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200905,'2020-09-05','2020/09/05','09/05/2020','05/09/2020',7,'Saturday',5,249,'Weekend',36,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200906,'2020-09-06','2020/09/06','09/06/2020','06/09/2020',1,'Sunday',6,250,'Weekend',36,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200907,'2020-09-07','2020/09/07','09/07/2020','07/09/2020',2,'Monday',7,251,'Weekday',37,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200908,'2020-09-08','2020/09/08','09/08/2020','08/09/2020',3,'Tuesday',8,252,'Weekday',37,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200909,'2020-09-09','2020/09/09','09/09/2020','09/09/2020',4,'Wednesday',9,253,'Weekday',37,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200910,'2020-09-10','2020/09/10','09/10/2020','10/09/2020',5,'Thursday',10,254,'Weekday',37,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200911,'2020-09-11','2020/09/11','09/11/2020','11/09/2020',6,'Friday',11,255,'Weekday',37,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200912,'2020-09-12','2020/09/12','09/12/2020','12/09/2020',7,'Saturday',12,256,'Weekend',37,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200913,'2020-09-13','2020/09/13','09/13/2020','13/09/2020',1,'Sunday',13,257,'Weekend',37,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200914,'2020-09-14','2020/09/14','09/14/2020','14/09/2020',2,'Monday',14,258,'Weekday',38,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200915,'2020-09-15','2020/09/15','09/15/2020','15/09/2020',3,'Tuesday',15,259,'Weekday',38,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200916,'2020-09-16','2020/09/16','09/16/2020','16/09/2020',4,'Wednesday',16,260,'Weekday',38,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200917,'2020-09-17','2020/09/17','09/17/2020','17/09/2020',5,'Thursday',17,261,'Weekday',38,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200918,'2020-09-18','2020/09/18','09/18/2020','18/09/2020',6,'Friday',18,262,'Weekday',38,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200919,'2020-09-19','2020/09/19','09/19/2020','19/09/2020',7,'Saturday',19,263,'Weekend',38,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200920,'2020-09-20','2020/09/20','09/20/2020','20/09/2020',1,'Sunday',20,264,'Weekend',38,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200921,'2020-09-21','2020/09/21','09/21/2020','21/09/2020',2,'Monday',21,265,'Weekday',39,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200922,'2020-09-22','2020/09/22','09/22/2020','22/09/2020',3,'Tuesday',22,266,'Weekday',39,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200923,'2020-09-23','2020/09/23','09/23/2020','23/09/2020',4,'Wednesday',23,267,'Weekday',39,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200924,'2020-09-24','2020/09/24','09/24/2020','24/09/2020',5,'Thursday',24,268,'Weekday',39,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200925,'2020-09-25','2020/09/25','09/25/2020','25/09/2020',6,'Friday',25,269,'Weekday',39,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200926,'2020-09-26','2020/09/26','09/26/2020','26/09/2020',7,'Saturday',26,270,'Weekend',39,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200927,'2020-09-27','2020/09/27','09/27/2020','27/09/2020',1,'Sunday',27,271,'Weekend',39,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200928,'2020-09-28','2020/09/28','09/28/2020','28/09/2020',2,'Monday',28,272,'Weekday',40,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200929,'2020-09-29','2020/09/29','09/29/2020','29/09/2020',3,'Tuesday',29,273,'Weekday',40,'September',9,'N',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20200930,'2020-09-30','2020/09/30','09/30/2020','30/09/2020',4,'Wednesday',30,274,'Weekday',40,'September',9,'Y',3,2020,'2020-09','2020Q3',10,4,2020,'2020-10','2020Q4'),(20201001,'2020-10-01','2020/10/01','10/01/2020','01/10/2020',5,'Thursday',1,275,'Weekday',40,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201002,'2020-10-02','2020/10/02','10/02/2020','02/10/2020',6,'Friday',2,276,'Weekday',40,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201003,'2020-10-03','2020/10/03','10/03/2020','03/10/2020',7,'Saturday',3,277,'Weekend',40,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201004,'2020-10-04','2020/10/04','10/04/2020','04/10/2020',1,'Sunday',4,278,'Weekend',40,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201005,'2020-10-05','2020/10/05','10/05/2020','05/10/2020',2,'Monday',5,279,'Weekday',41,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201006,'2020-10-06','2020/10/06','10/06/2020','06/10/2020',3,'Tuesday',6,280,'Weekday',41,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201007,'2020-10-07','2020/10/07','10/07/2020','07/10/2020',4,'Wednesday',7,281,'Weekday',41,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201008,'2020-10-08','2020/10/08','10/08/2020','08/10/2020',5,'Thursday',8,282,'Weekday',41,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201009,'2020-10-09','2020/10/09','10/09/2020','09/10/2020',6,'Friday',9,283,'Weekday',41,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201010,'2020-10-10','2020/10/10','10/10/2020','10/10/2020',7,'Saturday',10,284,'Weekend',41,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201011,'2020-10-11','2020/10/11','10/11/2020','11/10/2020',1,'Sunday',11,285,'Weekend',41,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201012,'2020-10-12','2020/10/12','10/12/2020','12/10/2020',2,'Monday',12,286,'Weekday',42,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201013,'2020-10-13','2020/10/13','10/13/2020','13/10/2020',3,'Tuesday',13,287,'Weekday',42,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201014,'2020-10-14','2020/10/14','10/14/2020','14/10/2020',4,'Wednesday',14,288,'Weekday',42,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201015,'2020-10-15','2020/10/15','10/15/2020','15/10/2020',5,'Thursday',15,289,'Weekday',42,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201016,'2020-10-16','2020/10/16','10/16/2020','16/10/2020',6,'Friday',16,290,'Weekday',42,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201017,'2020-10-17','2020/10/17','10/17/2020','17/10/2020',7,'Saturday',17,291,'Weekend',42,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201018,'2020-10-18','2020/10/18','10/18/2020','18/10/2020',1,'Sunday',18,292,'Weekend',42,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201019,'2020-10-19','2020/10/19','10/19/2020','19/10/2020',2,'Monday',19,293,'Weekday',43,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201020,'2020-10-20','2020/10/20','10/20/2020','20/10/2020',3,'Tuesday',20,294,'Weekday',43,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201021,'2020-10-21','2020/10/21','10/21/2020','21/10/2020',4,'Wednesday',21,295,'Weekday',43,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201022,'2020-10-22','2020/10/22','10/22/2020','22/10/2020',5,'Thursday',22,296,'Weekday',43,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201023,'2020-10-23','2020/10/23','10/23/2020','23/10/2020',6,'Friday',23,297,'Weekday',43,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201024,'2020-10-24','2020/10/24','10/24/2020','24/10/2020',7,'Saturday',24,298,'Weekend',43,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201025,'2020-10-25','2020/10/25','10/25/2020','25/10/2020',1,'Sunday',25,299,'Weekend',43,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201026,'2020-10-26','2020/10/26','10/26/2020','26/10/2020',2,'Monday',26,300,'Weekday',44,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201027,'2020-10-27','2020/10/27','10/27/2020','27/10/2020',3,'Tuesday',27,301,'Weekday',44,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201028,'2020-10-28','2020/10/28','10/28/2020','28/10/2020',4,'Wednesday',28,302,'Weekday',44,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201029,'2020-10-29','2020/10/29','10/29/2020','29/10/2020',5,'Thursday',29,303,'Weekday',44,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201030,'2020-10-30','2020/10/30','10/30/2020','30/10/2020',6,'Friday',30,304,'Weekday',44,'October',10,'N',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201031,'2020-10-31','2020/10/31','10/31/2020','31/10/2020',7,'Saturday',31,305,'Weekend',44,'October',10,'Y',4,2020,'2020-10','2020Q4',11,4,2020,'2020-11','2020Q4'),(20201101,'2020-11-01','2020/11/01','11/01/2020','01/11/2020',1,'Sunday',1,306,'Weekend',44,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201102,'2020-11-02','2020/11/02','11/02/2020','02/11/2020',2,'Monday',2,307,'Weekday',45,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201103,'2020-11-03','2020/11/03','11/03/2020','03/11/2020',3,'Tuesday',3,308,'Weekday',45,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201104,'2020-11-04','2020/11/04','11/04/2020','04/11/2020',4,'Wednesday',4,309,'Weekday',45,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201105,'2020-11-05','2020/11/05','11/05/2020','05/11/2020',5,'Thursday',5,310,'Weekday',45,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201106,'2020-11-06','2020/11/06','11/06/2020','06/11/2020',6,'Friday',6,311,'Weekday',45,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201107,'2020-11-07','2020/11/07','11/07/2020','07/11/2020',7,'Saturday',7,312,'Weekend',45,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201108,'2020-11-08','2020/11/08','11/08/2020','08/11/2020',1,'Sunday',8,313,'Weekend',45,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201109,'2020-11-09','2020/11/09','11/09/2020','09/11/2020',2,'Monday',9,314,'Weekday',46,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201110,'2020-11-10','2020/11/10','11/10/2020','10/11/2020',3,'Tuesday',10,315,'Weekday',46,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201111,'2020-11-11','2020/11/11','11/11/2020','11/11/2020',4,'Wednesday',11,316,'Weekday',46,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201112,'2020-11-12','2020/11/12','11/12/2020','12/11/2020',5,'Thursday',12,317,'Weekday',46,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201113,'2020-11-13','2020/11/13','11/13/2020','13/11/2020',6,'Friday',13,318,'Weekday',46,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201114,'2020-11-14','2020/11/14','11/14/2020','14/11/2020',7,'Saturday',14,319,'Weekend',46,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201115,'2020-11-15','2020/11/15','11/15/2020','15/11/2020',1,'Sunday',15,320,'Weekend',46,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201116,'2020-11-16','2020/11/16','11/16/2020','16/11/2020',2,'Monday',16,321,'Weekday',47,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201117,'2020-11-17','2020/11/17','11/17/2020','17/11/2020',3,'Tuesday',17,322,'Weekday',47,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201118,'2020-11-18','2020/11/18','11/18/2020','18/11/2020',4,'Wednesday',18,323,'Weekday',47,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201119,'2020-11-19','2020/11/19','11/19/2020','19/11/2020',5,'Thursday',19,324,'Weekday',47,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201120,'2020-11-20','2020/11/20','11/20/2020','20/11/2020',6,'Friday',20,325,'Weekday',47,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201121,'2020-11-21','2020/11/21','11/21/2020','21/11/2020',7,'Saturday',21,326,'Weekend',47,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201122,'2020-11-22','2020/11/22','11/22/2020','22/11/2020',1,'Sunday',22,327,'Weekend',47,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201123,'2020-11-23','2020/11/23','11/23/2020','23/11/2020',2,'Monday',23,328,'Weekday',48,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201124,'2020-11-24','2020/11/24','11/24/2020','24/11/2020',3,'Tuesday',24,329,'Weekday',48,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201125,'2020-11-25','2020/11/25','11/25/2020','25/11/2020',4,'Wednesday',25,330,'Weekday',48,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201126,'2020-11-26','2020/11/26','11/26/2020','26/11/2020',5,'Thursday',26,331,'Weekday',48,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201127,'2020-11-27','2020/11/27','11/27/2020','27/11/2020',6,'Friday',27,332,'Weekday',48,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201128,'2020-11-28','2020/11/28','11/28/2020','28/11/2020',7,'Saturday',28,333,'Weekend',48,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201129,'2020-11-29','2020/11/29','11/29/2020','29/11/2020',1,'Sunday',29,334,'Weekend',48,'November',11,'N',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201130,'2020-11-30','2020/11/30','11/30/2020','30/11/2020',2,'Monday',30,335,'Weekday',49,'November',11,'Y',4,2020,'2020-11','2020Q4',12,4,2020,'2020-12','2020Q4'),(20201201,'2020-12-01','2020/12/01','12/01/2020','01/12/2020',3,'Tuesday',1,336,'Weekday',49,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201202,'2020-12-02','2020/12/02','12/02/2020','02/12/2020',4,'Wednesday',2,337,'Weekday',49,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201203,'2020-12-03','2020/12/03','12/03/2020','03/12/2020',5,'Thursday',3,338,'Weekday',49,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201204,'2020-12-04','2020/12/04','12/04/2020','04/12/2020',6,'Friday',4,339,'Weekday',49,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201205,'2020-12-05','2020/12/05','12/05/2020','05/12/2020',7,'Saturday',5,340,'Weekend',49,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201206,'2020-12-06','2020/12/06','12/06/2020','06/12/2020',1,'Sunday',6,341,'Weekend',49,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201207,'2020-12-07','2020/12/07','12/07/2020','07/12/2020',2,'Monday',7,342,'Weekday',50,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201208,'2020-12-08','2020/12/08','12/08/2020','08/12/2020',3,'Tuesday',8,343,'Weekday',50,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201209,'2020-12-09','2020/12/09','12/09/2020','09/12/2020',4,'Wednesday',9,344,'Weekday',50,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201210,'2020-12-10','2020/12/10','12/10/2020','10/12/2020',5,'Thursday',10,345,'Weekday',50,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201211,'2020-12-11','2020/12/11','12/11/2020','11/12/2020',6,'Friday',11,346,'Weekday',50,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201212,'2020-12-12','2020/12/12','12/12/2020','12/12/2020',7,'Saturday',12,347,'Weekend',50,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201213,'2020-12-13','2020/12/13','12/13/2020','13/12/2020',1,'Sunday',13,348,'Weekend',50,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201214,'2020-12-14','2020/12/14','12/14/2020','14/12/2020',2,'Monday',14,349,'Weekday',51,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201215,'2020-12-15','2020/12/15','12/15/2020','15/12/2020',3,'Tuesday',15,350,'Weekday',51,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201216,'2020-12-16','2020/12/16','12/16/2020','16/12/2020',4,'Wednesday',16,351,'Weekday',51,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201217,'2020-12-17','2020/12/17','12/17/2020','17/12/2020',5,'Thursday',17,352,'Weekday',51,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201218,'2020-12-18','2020/12/18','12/18/2020','18/12/2020',6,'Friday',18,353,'Weekday',51,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201219,'2020-12-19','2020/12/19','12/19/2020','19/12/2020',7,'Saturday',19,354,'Weekend',51,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201220,'2020-12-20','2020/12/20','12/20/2020','20/12/2020',1,'Sunday',20,355,'Weekend',51,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201221,'2020-12-21','2020/12/21','12/21/2020','21/12/2020',2,'Monday',21,356,'Weekday',52,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201222,'2020-12-22','2020/12/22','12/22/2020','22/12/2020',3,'Tuesday',22,357,'Weekday',52,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201223,'2020-12-23','2020/12/23','12/23/2020','23/12/2020',4,'Wednesday',23,358,'Weekday',52,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201224,'2020-12-24','2020/12/24','12/24/2020','24/12/2020',5,'Thursday',24,359,'Weekday',52,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201225,'2020-12-25','2020/12/25','12/25/2020','25/12/2020',6,'Friday',25,360,'Weekday',52,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201226,'2020-12-26','2020/12/26','12/26/2020','26/12/2020',7,'Saturday',26,361,'Weekend',52,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201227,'2020-12-27','2020/12/27','12/27/2020','27/12/2020',1,'Sunday',27,362,'Weekend',52,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201228,'2020-12-28','2020/12/28','12/28/2020','28/12/2020',2,'Monday',28,363,'Weekday',53,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201229,'2020-12-29','2020/12/29','12/29/2020','29/12/2020',3,'Tuesday',29,364,'Weekday',53,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201230,'2020-12-30','2020/12/30','12/30/2020','30/12/2020',4,'Wednesday',30,365,'Weekday',53,'December',12,'N',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1'),(20201231,'2020-12-31','2020/12/31','12/31/2020','31/12/2020',5,'Thursday',31,366,'Weekday',53,'December',12,'Y',4,2020,'2020-12','2020Q4',1,1,2021,'2021-01','2021Q1');
/*!40000 ALTER TABLE `tbl_date` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_status`
--

DROP TABLE IF EXISTS `tbl_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_status` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_status_tbl_date_idx` (`update_date`),
  CONSTRAINT `tbl_status_tbl_date` FOREIGN KEY (`update_date`) REFERENCES `tbl_date` (`date_key`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_status`
--

LOCK TABLES `tbl_status` WRITE;
/*!40000 ALTER TABLE `tbl_status` DISABLE KEYS */;
INSERT INTO `tbl_status` VALUES (700,'ACTIVE',20190801,1300,1),(701,'SUSPENDED',20190801,1300,1),(702,'BID CREATION',20190801,1300,1),(703,'BID SUBMITTED',20190801,1300,1),(704,'BID WON',20190801,1300,1),(705,'BUILT OUT',20190801,1300,1),(750,'NOTE',20190801,1300,1),(751,'TODO',20190801,1300,1),(752,'NOTE_COMPLETED',20190801,1300,1);
/*!40000 ALTER TABLE `tbl_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `vw_status_list`
--

DROP TABLE IF EXISTS `vw_status_list`;
/*!50001 DROP VIEW IF EXISTS `vw_status_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_status_list` AS SELECT 
 1 AS `id`,
 1 AS `name`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'application'
--

--
-- Dumping routines for database 'application'
--
/*!50003 DROP PROCEDURE IF EXISTS `PopulateDateDimension` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `PopulateDateDimension`(BeginDate DATETIME, EndDate DATETIME)
BEGIN

 # =============================================
 # Description: http://arcanecode.com/2009/11/18/populating-a-kimball-date-dimension/
 # =============================================

 # A few notes, this code does nothing to the existing table, no deletes
 # are triggered before hand. Because the DateKey is uniquely indexed,
 # it will simply produce errors if you attempt to insert duplicates.
 # You can however adjust the Begin/End dates and rerun to safely add
 # new dates to the table every year.
 #
 # If the begin date is after the end date, no errors occur but nothing
 # happens as the while loop never executes.

 # Holds a flag so we can determine if the date is the last day of month
 DECLARE LastDayOfMon CHAR(1);

 # Number of months to add to the date to get the current Fiscal date
 DECLARE FiscalYearMonthsOffset INT;

 # These two counters are used in our loop.
 DECLARE DateCounter DATETIME;    #Current date in loop
 DECLARE FiscalCounter DATETIME;  #Fiscal Year Date in loop

 # Set this to the number of months to add to the current date to get
 # the beginning of the Fiscal year. For example, if the Fiscal year
 # begins July 1, put a 6 there.
 # Negative values are also allowed, thus if your 2010 Fiscal year
 # begins in July of 2009, put a -6.
 SET FiscalYearMonthsOffset = 1;

 # Start the counter at the begin date
 SET DateCounter = BeginDate;

 WHILE DateCounter <= EndDate DO
            # Calculate the current Fiscal date as an offset of
            # the current date in the loop

            SET FiscalCounter = DATE_ADD(DateCounter, INTERVAL FiscalYearMonthsOffset MONTH);

            # Set value for IsLastDayOfMonth
            IF MONTH(DateCounter) = MONTH(DATE_ADD(DateCounter, INTERVAL 1 DAY)) THEN
               SET LastDayOfMon = 'N';
            ELSE
               SET LastDayOfMon = 'Y';
   END IF;

            # add a record into the date dimension table for this date
            INSERT  INTO tbl_date
       (date_key
       ,full_date
       ,date_name
       ,date_name_us
       ,date_name_eu
       ,day_of_week
       ,day_name_of_week
       ,day_of_month
       ,day_of_year
       ,weekday_weekend
       ,week_of_year
       ,month_name
       ,month_of_year
       ,is_last_day_of_month
       ,calendar_quarter
       ,calendar_year
       ,calendar_year_month
       ,calendar_year_qtr
       ,fiscal_month_of_year
       ,fiscal_quarter
       ,fiscal_year
       ,fiscal_year_month
       ,fiscal_year_qtr)
            VALUES  (
                      ( YEAR(DateCounter) * 10000 ) + ( MONTH(DateCounter)
                                                         * 100 )
                      + DAY(DateCounter)  #DateKey
                    , DateCounter # FullDate
                    , CONCAT(CAST(YEAR(DateCounter) AS CHAR(4)),'/',DATE_FORMAT(DateCounter,'%m'),'/',DATE_FORMAT(DateCounter,'%d')) #DateName
                    , CONCAT(DATE_FORMAT(DateCounter,'%m'),'/',DATE_FORMAT(DateCounter,'%d'),'/',CAST(YEAR(DateCounter) AS CHAR(4)))#DateNameUS
                    , CONCAT(DATE_FORMAT(DateCounter,'%d'),'/',DATE_FORMAT(DateCounter,'%m'),'/',CAST(YEAR(DateCounter) AS CHAR(4)))#DateNameEU
                    , DAYOFWEEK(DateCounter) #DayOfWeek
                    , DAYNAME(DateCounter) #DayNameOfWeek
                    , DAYOFMONTH(DateCounter) #DayOfMonth
                    , DAYOFYEAR(DateCounter) #DayOfYear
                    , CASE DAYNAME(DateCounter)
                        WHEN 'Saturday' THEN 'Weekend'
                        WHEN 'Sunday' THEN 'Weekend'
                        ELSE 'Weekday'
                      END #WeekdayWeekend
                    , WEEKOFYEAR(DateCounter) #WeekOfYear
                    , MONTHNAME(DateCounter) #MonthName
                    , MONTH(DateCounter) #MonthOfYear
                    , LastDayOfMon #IsLastDayOfMonth
                    , QUARTER(DateCounter) #CalendarQuarter
                    , YEAR(DateCounter) #CalendarYear
                    , CONCAT(CAST(YEAR(DateCounter) AS CHAR(4)),'-',DATE_FORMAT(DateCounter,'%m')) #CalendarYearMonth
                    , CONCAT(CAST(YEAR(DateCounter) AS CHAR(4)),'Q',QUARTER(DateCounter)) #CalendarYearQtr
                    , MONTH(FiscalCounter) #[FiscalMonthOfYear]
                    , QUARTER(FiscalCounter) #[FiscalQuarter]
                    , YEAR(FiscalCounter) #[FiscalYear]
                    , CONCAT(CAST(YEAR(FiscalCounter) AS CHAR(4)),'-',DATE_FORMAT(FiscalCounter,'%m')) #[FiscalYearMonth]
                    , CONCAT(CAST(YEAR(FiscalCounter) AS CHAR(4)),'Q',QUARTER(FiscalCounter)) #[FiscalYearQtr]
                    );

            # Increment the date counter for next pass thru the loop
            SET DateCounter = DATE_ADD(DateCounter, INTERVAL 1 DAY);
      END WHILE;

-- CALL PopulateDateDimension('2019/01/01', '2020/12/31');

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_status_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_status_list`()
begin
	select s.id,
    s.name
    
    from tbl_status s
    
    order by s.name asc;

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `vendors`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `vendors` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `vendors`;

--
-- Table structure for table `tbl_dimensions`
--

DROP TABLE IF EXISTS `tbl_dimensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_dimensions` (
  `callout` int(11) NOT NULL,
  `inches` int(11) NOT NULL,
  PRIMARY KEY (`callout`,`inches`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_dimensions`
--

LOCK TABLES `tbl_dimensions` WRITE;
/*!40000 ALTER TABLE `tbl_dimensions` DISABLE KEYS */;
INSERT INTO `tbl_dimensions` VALUES (10,12),(11,13),(12,14),(13,15),(14,16),(15,17),(16,18),(17,19),(18,20),(19,21),(20,24),(21,25),(22,26),(23,27),(24,28),(25,29),(26,30),(27,31),(28,32),(29,33),(30,36),(31,37),(32,38),(33,39),(34,40),(35,41),(36,42),(37,43),(38,44),(39,45),(40,48),(41,49),(42,50),(43,51),(44,52),(45,53),(46,54),(47,55),(48,56),(49,57),(50,60),(51,61),(52,62),(53,63),(54,64),(55,65),(56,66),(57,67),(58,68),(59,69),(60,72),(61,73),(62,74),(63,75),(64,76),(65,77),(66,78),(67,79),(68,80),(69,81),(70,84),(71,85),(72,86),(73,87),(74,88),(75,89),(76,90),(77,91),(78,92),(79,93),(80,96),(81,97),(82,98),(83,99),(84,100),(85,101),(86,102),(87,103),(88,104),(89,105),(90,108),(91,109),(92,110),(93,111),(94,112),(95,113),(96,114),(97,115),(98,116),(99,117),(100,120),(101,121),(102,122),(103,123),(104,124),(105,125),(106,126),(107,127),(108,128),(109,129),(110,22),(110,132),(111,23),(111,133),(112,134),(113,135),(114,136),(115,137),(116,138),(117,139),(118,140),(119,141),(120,144),(210,34),(211,35),(310,46),(311,47),(410,58),(411,59),(510,70),(511,71),(610,82),(611,83),(710,94),(711,95),(810,106),(811,107),(910,118),(911,119),(1010,130),(1011,131),(1110,142),(1111,143);
/*!40000 ALTER TABLE `tbl_dimensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_manufacturer`
--

DROP TABLE IF EXISTS `tbl_manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `mfg_abbr` varchar(2) DEFAULT NULL,
  `manufacturer_name` varchar(45) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `tbl_status_tbl_status_id` int(11) NOT NULL,
  PRIMARY KEY (`manufacturer_id`),
  KEY `tbl_manufacturer_tbl_date_idx` (`update_date`),
  KEY `fk_tbl_manufacturer_tbl_users_idx` (`updated_by`),
  KEY `fk_tbl_manufacturer_tbl_status_idx` (`tbl_status_tbl_status_id`),
  CONSTRAINT `fk_tbl_manufacturer_tbl_status` FOREIGN KEY (`tbl_status_tbl_status_id`) REFERENCES `application`.`tbl_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_manufacturer_tbl_users` FOREIGN KEY (`updated_by`) REFERENCES `xowindows`.`tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_manufacturer_tbl_date` FOREIGN KEY (`update_date`) REFERENCES `application`.`tbl_date` (`date_key`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_manufacturer`
--

LOCK TABLES `tbl_manufacturer` WRITE;
/*!40000 ALTER TABLE `tbl_manufacturer` DISABLE KEYS */;
INSERT INTO `tbl_manufacturer` VALUES (100,'AL','ALPINE',20190101,1300,1,700),(101,'CA','CASCADE',20190101,1300,1,700),(102,'MI','MI WINDOW DOOR',20190101,1300,1,700);
/*!40000 ALTER TABLE `tbl_manufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mfg_has_series`
--

DROP TABLE IF EXISTS `tbl_mfg_has_series`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mfg_has_series` (
  `fk_mfg_id` int(11) NOT NULL,
  `fk_idtbl_mfg_series` int(11) NOT NULL,
  PRIMARY KEY (`fk_mfg_id`,`fk_idtbl_mfg_series`),
  KEY `fk_mfg_has_tbl_mfg_series_idx` (`fk_idtbl_mfg_series`),
  CONSTRAINT `fk_mfg_has_tbl_mfg` FOREIGN KEY (`fk_mfg_id`) REFERENCES `tbl_manufacturer` (`manufacturer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_mfg_has_tbl_mfg_series` FOREIGN KEY (`fk_idtbl_mfg_series`) REFERENCES `tbl_mfg_series` (`idtbl_mfg_series`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mfg_has_series`
--

LOCK TABLES `tbl_mfg_has_series` WRITE;
/*!40000 ALTER TABLE `tbl_mfg_has_series` DISABLE KEYS */;
INSERT INTO `tbl_mfg_has_series` VALUES (102,1),(102,2),(102,3),(102,4),(102,5),(100,6),(100,7);
/*!40000 ALTER TABLE `tbl_mfg_has_series` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mfg_has_tbl_window_coating`
--

DROP TABLE IF EXISTS `tbl_mfg_has_tbl_window_coating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mfg_has_tbl_window_coating` (
  `tbl_manufacturer_manufacturer_id` int(11) NOT NULL,
  `tbl_window_coating_window_coating_id` int(11) NOT NULL,
  PRIMARY KEY (`tbl_manufacturer_manufacturer_id`,`tbl_window_coating_window_coating_id`),
  KEY `fk_tbl_manufacturer_has_tbl_window_coating_tbl_window_coati_idx` (`tbl_window_coating_window_coating_id`),
  KEY `fk_tbl_manufacturer_has_tbl_window_coating_tbl_manufacturer_idx` (`tbl_manufacturer_manufacturer_id`),
  CONSTRAINT `fk_tbl_manufacturer_has_tbl_window_coating_tbl_manufacturer1` FOREIGN KEY (`tbl_manufacturer_manufacturer_id`) REFERENCES `tbl_manufacturer` (`manufacturer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_manufacturer_has_tbl_window_coating_tbl_window_coating1` FOREIGN KEY (`tbl_window_coating_window_coating_id`) REFERENCES `tbl_window_coating` (`window_coating_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mfg_has_tbl_window_coating`
--

LOCK TABLES `tbl_mfg_has_tbl_window_coating` WRITE;
/*!40000 ALTER TABLE `tbl_mfg_has_tbl_window_coating` DISABLE KEYS */;
INSERT INTO `tbl_mfg_has_tbl_window_coating` VALUES (100,600);
/*!40000 ALTER TABLE `tbl_mfg_has_tbl_window_coating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mfg_has_tbl_window_color`
--

DROP TABLE IF EXISTS `tbl_mfg_has_tbl_window_color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mfg_has_tbl_window_color` (
  `tbl_manufacturer_manufacturer_id` int(11) NOT NULL,
  `tbl_window_color_window_color_id` int(11) NOT NULL,
  PRIMARY KEY (`tbl_manufacturer_manufacturer_id`,`tbl_window_color_window_color_id`),
  KEY `fk_tbl_manufacturer_has_tbl_window_color_tbl_window_color1_idx` (`tbl_window_color_window_color_id`),
  KEY `fk_tbl_manufacturer_has_tbl_window_color_tbl_manufacturer1_idx` (`tbl_manufacturer_manufacturer_id`),
  CONSTRAINT `fk_tbl_manufacturer_has_tbl_window_color_tbl_manufacturer1` FOREIGN KEY (`tbl_manufacturer_manufacturer_id`) REFERENCES `tbl_manufacturer` (`manufacturer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_manufacturer_has_tbl_window_color_tbl_window_color1` FOREIGN KEY (`tbl_window_color_window_color_id`) REFERENCES `tbl_window_color` (`window_color_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mfg_has_tbl_window_color`
--

LOCK TABLES `tbl_mfg_has_tbl_window_color` WRITE;
/*!40000 ALTER TABLE `tbl_mfg_has_tbl_window_color` DISABLE KEYS */;
INSERT INTO `tbl_mfg_has_tbl_window_color` VALUES (100,300),(100,301),(100,302),(100,303),(100,310),(100,399);
/*!40000 ALTER TABLE `tbl_mfg_has_tbl_window_color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mfg_has_tbl_window_fin`
--

DROP TABLE IF EXISTS `tbl_mfg_has_tbl_window_fin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mfg_has_tbl_window_fin` (
  `tbl_manufacturer_manufacturer_id` int(11) NOT NULL,
  `tbl_window_fin_window_fin_id` int(11) NOT NULL,
  PRIMARY KEY (`tbl_manufacturer_manufacturer_id`,`tbl_window_fin_window_fin_id`),
  KEY `fk_tbl_manufacturer_has_tbl_window_fin_tbl_window_fin1_idx` (`tbl_window_fin_window_fin_id`),
  KEY `fk_tbl_manufacturer_has_tbl_window_fin_tbl_manufacturer1_idx` (`tbl_manufacturer_manufacturer_id`),
  CONSTRAINT `fk_tbl_manufacturer_has_tbl_window_fin_tbl_manufacturer1` FOREIGN KEY (`tbl_manufacturer_manufacturer_id`) REFERENCES `tbl_manufacturer` (`manufacturer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_manufacturer_has_tbl_window_fin_tbl_window_fin1` FOREIGN KEY (`tbl_window_fin_window_fin_id`) REFERENCES `tbl_window_fin` (`window_fin_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mfg_has_tbl_window_fin`
--

LOCK TABLES `tbl_mfg_has_tbl_window_fin` WRITE;
/*!40000 ALTER TABLE `tbl_mfg_has_tbl_window_fin` DISABLE KEYS */;
INSERT INTO `tbl_mfg_has_tbl_window_fin` VALUES (100,500),(100,501);
/*!40000 ALTER TABLE `tbl_mfg_has_tbl_window_fin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mfg_has_tbl_window_material`
--

DROP TABLE IF EXISTS `tbl_mfg_has_tbl_window_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mfg_has_tbl_window_material` (
  `tbl_manufacturer_manufacturer_id` int(11) NOT NULL,
  `tbl_window_material_window_material_id` int(11) NOT NULL,
  PRIMARY KEY (`tbl_manufacturer_manufacturer_id`,`tbl_window_material_window_material_id`),
  KEY `fk_tbl_manufacturer_has_tbl_window_material_tbl_window_mate_idx` (`tbl_window_material_window_material_id`),
  KEY `fk_tbl_manufacturer_has_tbl_window_material_tbl_manufacture_idx` (`tbl_manufacturer_manufacturer_id`),
  CONSTRAINT `fk_tbl_manufacturer_has_tbl_window_material_tbl_manufacturer1` FOREIGN KEY (`tbl_manufacturer_manufacturer_id`) REFERENCES `tbl_manufacturer` (`manufacturer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_manufacturer_has_tbl_window_material_tbl_window_materi1` FOREIGN KEY (`tbl_window_material_window_material_id`) REFERENCES `tbl_window_material` (`window_material_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mfg_has_tbl_window_material`
--

LOCK TABLES `tbl_mfg_has_tbl_window_material` WRITE;
/*!40000 ALTER TABLE `tbl_mfg_has_tbl_window_material` DISABLE KEYS */;
INSERT INTO `tbl_mfg_has_tbl_window_material` VALUES (100,400),(100,410);
/*!40000 ALTER TABLE `tbl_mfg_has_tbl_window_material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mfg_has_window_dimensions`
--

DROP TABLE IF EXISTS `tbl_mfg_has_window_dimensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mfg_has_window_dimensions` (
  `fk_tbl_window_dimensions_idtbl_window_dimensions` int(11) NOT NULL,
  `fk_tbl_win_dims_mfg_mods_idtbl_win_dims_mfg_mods` int(11) NOT NULL,
  `fk_tbl_mfg_purchasing_idtbl_mfg_purchasing` int(11) DEFAULT NULL,
  `fk_tbl_mfg_series_idtbl_mfg_series` int(11) NOT NULL,
  `fk_tbl_mfg_series_fk_tbl_window_type_idtbl_window_type` int(11) NOT NULL,
  PRIMARY KEY (`fk_tbl_window_dimensions_idtbl_window_dimensions`,`fk_tbl_win_dims_mfg_mods_idtbl_win_dims_mfg_mods`,`fk_tbl_mfg_series_idtbl_mfg_series`,`fk_tbl_mfg_series_fk_tbl_window_type_idtbl_window_type`),
  KEY `fk_tbl_mfg_has_window_dimensions_tbl_window_dimensions1_idx` (`fk_tbl_window_dimensions_idtbl_window_dimensions`),
  KEY `fk_tbl_mfg_has_window_dimensions_tbl_window_dimensions_mfg__idx` (`fk_tbl_win_dims_mfg_mods_idtbl_win_dims_mfg_mods`),
  KEY `fk_tbl_mfg_has_window_dimensions_tbl_mfg_purchasing1_idx` (`fk_tbl_mfg_purchasing_idtbl_mfg_purchasing`),
  KEY `fk_tbl_mfg_has_window_dimensions_tbl_mfg_series1_idx` (`fk_tbl_mfg_series_idtbl_mfg_series`,`fk_tbl_mfg_series_fk_tbl_window_type_idtbl_window_type`),
  CONSTRAINT `fk_tbl_mfg_has_window_dimensions_tbl_mfg_purchasing1` FOREIGN KEY (`fk_tbl_mfg_purchasing_idtbl_mfg_purchasing`) REFERENCES `tbl_mfg_purchasing` (`idtbl_mfg_purchasing`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_mfg_has_window_dimensions_tbl_mfg_series1` FOREIGN KEY (`fk_tbl_mfg_series_idtbl_mfg_series`, `fk_tbl_mfg_series_fk_tbl_window_type_idtbl_window_type`) REFERENCES `tbl_mfg_series` (`idtbl_mfg_series`, `fk_tbl_window_type_idtbl_window_type`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_mfg_has_window_dimensions_tbl_window_dimensions1` FOREIGN KEY (`fk_tbl_window_dimensions_idtbl_window_dimensions`) REFERENCES `tbl_window_dimensions` (`idtbl_window_dimensions`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_mfg_has_window_dimensions_tbl_window_dimensions_mfg_mo1` FOREIGN KEY (`fk_tbl_win_dims_mfg_mods_idtbl_win_dims_mfg_mods`) REFERENCES `tbl_window_dimensions_mfg_mods` (`idtbl_window_dimensions_mfg_mods`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mfg_has_window_dimensions`
--

LOCK TABLES `tbl_mfg_has_window_dimensions` WRITE;
/*!40000 ALTER TABLE `tbl_mfg_has_window_dimensions` DISABLE KEYS */;
INSERT INTO `tbl_mfg_has_window_dimensions` VALUES (3010,8,1,1,91),(3030,5,2,2,10),(3030,8,3,1,10),(3030,9,4,6,58),(3016,8,5,1,91),(3020,8,6,1,91),(3026,8,7,1,91),(3036,8,8,1,91),(3040,8,9,1,91),(3046,8,10,1,91),(3050,8,11,1,91),(3060,8,12,1,91),(3070,8,13,1,91),(3080,8,14,1,91);
/*!40000 ALTER TABLE `tbl_mfg_has_window_dimensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mfg_purchasing`
--

DROP TABLE IF EXISTS `tbl_mfg_purchasing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mfg_purchasing` (
  `idtbl_mfg_purchasing` int(11) NOT NULL AUTO_INCREMENT,
  `fk_tbl_manufacturer_manufacturer_id` int(11) NOT NULL,
  `fk_tbl_mfg_series_idtbl_mfg_series` int(11) NOT NULL,
  `fk_tbl_mfg_series_fk_tbl_window_type_idtbl_window_type` int(11) NOT NULL,
  `fk_tbl_window_dimensions_idtbl_window_dimensions` int(11) NOT NULL,
  `base_amt` int(11) DEFAULT NULL,
  `e366_amt` int(11) DEFAULT NULL,
  `e340_amt` int(11) DEFAULT NULL,
  `tempered_amt` int(11) DEFAULT NULL,
  `grid_amt` int(11) DEFAULT NULL,
  PRIMARY KEY (`idtbl_mfg_purchasing`),
  KEY `fk_tbl_mfg_purchasing_tbl_manufacturer1_idx` (`fk_tbl_manufacturer_manufacturer_id`),
  KEY `fk_tbl_mfg_purchasing_tbl_mfg_series1_idx` (`fk_tbl_mfg_series_idtbl_mfg_series`,`fk_tbl_mfg_series_fk_tbl_window_type_idtbl_window_type`),
  KEY `fk_tbl_mfg_purchasing_tbl_window_dimensions1_idx` (`fk_tbl_window_dimensions_idtbl_window_dimensions`),
  CONSTRAINT `fk_tbl_mfg_purchasing_tbl_manufacturer1` FOREIGN KEY (`fk_tbl_manufacturer_manufacturer_id`) REFERENCES `tbl_manufacturer` (`manufacturer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_mfg_purchasing_tbl_mfg_series1` FOREIGN KEY (`fk_tbl_mfg_series_idtbl_mfg_series`, `fk_tbl_mfg_series_fk_tbl_window_type_idtbl_window_type`) REFERENCES `tbl_mfg_series` (`idtbl_mfg_series`, `fk_tbl_window_type_idtbl_window_type`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_mfg_purchasing_tbl_window_dimensions1` FOREIGN KEY (`fk_tbl_window_dimensions_idtbl_window_dimensions`) REFERENCES `tbl_window_dimensions` (`idtbl_window_dimensions`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mfg_purchasing`
--

LOCK TABLES `tbl_mfg_purchasing` WRITE;
/*!40000 ALTER TABLE `tbl_mfg_purchasing` DISABLE KEYS */;
INSERT INTO `tbl_mfg_purchasing` VALUES (1,102,1,91,3010,9500,300,600,3600,895),(2,102,2,10,3030,11900,900,1600,10800,895),(3,102,1,91,3030,14800,900,1600,10800,895),(4,100,6,58,3030,17900,3600,NULL,8000,2700),(5,102,1,91,3016,9500,500,800,5400,895),(6,102,1,91,3020,9500,600,1100,7200,895),(7,102,1,91,3026,11100,800,1400,9000,895),(8,102,1,91,3036,12600,1100,1900,12600,895),(9,102,1,91,3040,13400,1200,2100,14400,895),(10,102,1,91,3046,14100,1400,2400,16200,895),(11,102,1,91,3050,14900,1500,2700,9000,895),(12,102,1,91,3060,16400,1800,3200,21600,895),(13,102,1,91,3070,25000,2100,3700,25200,895),(14,102,1,91,3080,38500,0,0,0,895);
/*!40000 ALTER TABLE `tbl_mfg_purchasing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mfg_series`
--

DROP TABLE IF EXISTS `tbl_mfg_series`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mfg_series` (
  `idtbl_mfg_series` int(11) NOT NULL AUTO_INCREMENT,
  `fk_tbl_window_type_idtbl_window_type` int(11) NOT NULL,
  `series` varchar(10) DEFAULT NULL,
  `special_instruction` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtbl_mfg_series`,`fk_tbl_window_type_idtbl_window_type`),
  UNIQUE KEY `series_UNIQUE` (`series`),
  KEY `idx_mfg_series_type` (`series`),
  KEY `fk_tbl_mfg_series_tbl_window_type1_idx` (`fk_tbl_window_type_idtbl_window_type`),
  CONSTRAINT `fk_tbl_mfg_series_tbl_window_type1` FOREIGN KEY (`fk_tbl_window_type_idtbl_window_type`) REFERENCES `tbl_window_type` (`idtbl_window_type`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mfg_series`
--

LOCK TABLES `tbl_mfg_series` WRITE;
/*!40000 ALTER TABLE `tbl_mfg_series` DISABLE KEYS */;
INSERT INTO `tbl_mfg_series` VALUES (1,91,'5610',NULL),(2,10,'5800',NULL),(3,58,'550D','2-PANEL'),(4,58,'550T','3-PANEL'),(5,58,'5500',NULL),(6,58,'571',NULL),(7,58,'7SH2','2-PANEL');
/*!40000 ALTER TABLE `tbl_mfg_series` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_window_coating`
--

DROP TABLE IF EXISTS `tbl_window_coating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_window_coating` (
  `window_coating_id` int(11) NOT NULL,
  `window_coating_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`window_coating_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_window_coating`
--

LOCK TABLES `tbl_window_coating` WRITE;
/*!40000 ALTER TABLE `tbl_window_coating` DISABLE KEYS */;
INSERT INTO `tbl_window_coating` VALUES (600,'LOWe366');
/*!40000 ALTER TABLE `tbl_window_coating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_window_color`
--

DROP TABLE IF EXISTS `tbl_window_color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_window_color` (
  `window_color_id` int(11) NOT NULL,
  `window_color_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`window_color_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_window_color`
--

LOCK TABLES `tbl_window_color` WRITE;
/*!40000 ALTER TABLE `tbl_window_color` DISABLE KEYS */;
INSERT INTO `tbl_window_color` VALUES (300,'UNDEFINED'),(301,'ADOBE'),(302,'ALMOND'),(303,'BRONZE'),(310,'WHITE'),(399,'COLOR');
/*!40000 ALTER TABLE `tbl_window_color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_window_dimensions`
--

DROP TABLE IF EXISTS `tbl_window_dimensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_window_dimensions` (
  `idtbl_window_dimensions` int(11) NOT NULL,
  `width_callout` int(11) DEFAULT NULL,
  `height_callout` int(11) DEFAULT NULL,
  PRIMARY KEY (`idtbl_window_dimensions`),
  KEY `tbl_win_dim_tbl_dim_idx` (`width_callout`),
  KEY `tbl_win_dim_tbl_dim_h_idx` (`height_callout`),
  CONSTRAINT `tbl_win_dim_tbl_dim_h` FOREIGN KEY (`height_callout`) REFERENCES `tbl_dimensions` (`callout`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_win_dim_tbl_dim_w` FOREIGN KEY (`width_callout`) REFERENCES `tbl_dimensions` (`callout`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_window_dimensions`
--

LOCK TABLES `tbl_window_dimensions` WRITE;
/*!40000 ALTER TABLE `tbl_window_dimensions` DISABLE KEYS */;
INSERT INTO `tbl_window_dimensions` VALUES (3010,30,10),(3016,30,16),(3020,30,20),(3026,30,26),(3030,30,30),(3036,30,36),(3040,30,40),(3046,30,46),(3050,30,50),(3060,30,60),(3066,30,66),(3070,30,70),(3080,30,80),(4060,40,60);
/*!40000 ALTER TABLE `tbl_window_dimensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_window_dimensions_mfg_mods`
--

DROP TABLE IF EXISTS `tbl_window_dimensions_mfg_mods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_window_dimensions_mfg_mods` (
  `idtbl_window_dimensions_mfg_mods` int(11) NOT NULL AUTO_INCREMENT,
  `mod_description` varchar(25) DEFAULT NULL,
  `opening_w` decimal(6,4) DEFAULT NULL,
  `opening_h` decimal(6,4) DEFAULT NULL,
  `fixed_glass_w` decimal(6,4) DEFAULT NULL,
  `fixed_glass_h` decimal(6,4) DEFAULT NULL,
  `sash_glass_w` decimal(6,4) DEFAULT NULL,
  `sash_glass_h` decimal(6,4) DEFAULT NULL,
  `screen_w` decimal(6,4) DEFAULT NULL,
  `screen_h` decimal(6,4) DEFAULT NULL,
  PRIMARY KEY (`idtbl_window_dimensions_mfg_mods`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_window_dimensions_mfg_mods`
--

LOCK TABLES `tbl_window_dimensions_mfg_mods` WRITE;
/*!40000 ALTER TABLE `tbl_window_dimensions_mfg_mods` DISABLE KEYS */;
INSERT INTO `tbl_window_dimensions_mfg_mods` VALUES (5,'Normal',-0.5000,-0.5000,-2.2500,-2.5000,-2.2500,-5.0000,-1.1250,-2.3750),(6,'XO 4-0 adjustments',-0.5000,-0.5000,-1.2500,-2.5000,-3.2500,-5.0000,-2.1250,-2.3750),(7,'Normal',-0.5000,-0.5000,-2.5000,-2.2500,-5.0000,-2.2500,-2.3750,-0.9375),(8,'Normal',-0.5000,-0.5000,-2.5000,-2.5000,NULL,NULL,NULL,NULL),(9,'Normal',-0.5000,-0.5000,-2.3125,-1.8125,-4.3125,-3.0625,-2.1250,-1.8125);
/*!40000 ALTER TABLE `tbl_window_dimensions_mfg_mods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_window_fin`
--

DROP TABLE IF EXISTS `tbl_window_fin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_window_fin` (
  `window_fin_id` int(11) NOT NULL,
  `window_fin_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`window_fin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_window_fin`
--

LOCK TABLES `tbl_window_fin` WRITE;
/*!40000 ALTER TABLE `tbl_window_fin` DISABLE KEYS */;
INSERT INTO `tbl_window_fin` VALUES (500,'FINLESS'),(501,'1 3/8\"\n');
/*!40000 ALTER TABLE `tbl_window_fin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_window_material`
--

DROP TABLE IF EXISTS `tbl_window_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_window_material` (
  `window_material_id` int(11) NOT NULL,
  `window_material_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`window_material_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_window_material`
--

LOCK TABLES `tbl_window_material` WRITE;
/*!40000 ALTER TABLE `tbl_window_material` DISABLE KEYS */;
INSERT INTO `tbl_window_material` VALUES (400,'ALUMINUM'),(410,'VINYL');
/*!40000 ALTER TABLE `tbl_window_material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_window_type`
--

DROP TABLE IF EXISTS `tbl_window_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_window_type` (
  `idtbl_window_type` int(11) NOT NULL AUTO_INCREMENT,
  `window_type_abbr` varchar(10) DEFAULT NULL,
  `window_type_description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtbl_window_type`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_window_type`
--

LOCK TABLES `tbl_window_type` WRITE;
/*!40000 ALTER TABLE `tbl_window_type` DISABLE KEYS */;
INSERT INTO `tbl_window_type` VALUES (10,'XO','Single Slider'),(58,'SH','Single Hung'),(91,'PW','Picture Window'),(92,'XOX','Double Slider'),(93,'XOXs','Double Sliders');
/*!40000 ALTER TABLE `tbl_window_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `vw_manufacturer_list`
--

DROP TABLE IF EXISTS `vw_manufacturer_list`;
/*!50001 DROP VIEW IF EXISTS `vw_manufacturer_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_manufacturer_list` AS SELECT 
 1 AS `manufacturer_id`,
 1 AS `mfg_abbr`,
 1 AS `manufacturer_name`,
 1 AS `update_date`,
 1 AS `update_time`,
 1 AS `updated_by`,
 1 AS `tbl_status_tbl_status_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_manufacturer_series`
--

DROP TABLE IF EXISTS `vw_manufacturer_series`;
/*!50001 DROP VIEW IF EXISTS `vw_manufacturer_series`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_manufacturer_series` AS SELECT 
 1 AS `manufacturer_id`,
 1 AS `manufacturer_name`,
 1 AS `idtbl_mfg_series`,
 1 AS `series`,
 1 AS `special_instruction`,
 1 AS `idtbl_window_type`,
 1 AS `window_type_abbr`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_mfg_coatings`
--

DROP TABLE IF EXISTS `vw_mfg_coatings`;
/*!50001 DROP VIEW IF EXISTS `vw_mfg_coatings`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_mfg_coatings` AS SELECT 
 1 AS `manufacturer_id`,
 1 AS `manufacturer_name`,
 1 AS `window_coating_id`,
 1 AS `window_coating_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_mfg_colors`
--

DROP TABLE IF EXISTS `vw_mfg_colors`;
/*!50001 DROP VIEW IF EXISTS `vw_mfg_colors`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_mfg_colors` AS SELECT 
 1 AS `manufacturer_id`,
 1 AS `manufacturer_name`,
 1 AS `window_color_id`,
 1 AS `window_color_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_mfg_fins`
--

DROP TABLE IF EXISTS `vw_mfg_fins`;
/*!50001 DROP VIEW IF EXISTS `vw_mfg_fins`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_mfg_fins` AS SELECT 
 1 AS `manufacturer_id`,
 1 AS `manufacturer_name`,
 1 AS `window_fin_id`,
 1 AS `window_fin_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_mfg_materials`
--

DROP TABLE IF EXISTS `vw_mfg_materials`;
/*!50001 DROP VIEW IF EXISTS `vw_mfg_materials`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_mfg_materials` AS SELECT 
 1 AS `manufacturer_id`,
 1 AS `manufacturer_name`,
 1 AS `window_material_id`,
 1 AS `window_material_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_mfg_series_dims`
--

DROP TABLE IF EXISTS `vw_mfg_series_dims`;
/*!50001 DROP VIEW IF EXISTS `vw_mfg_series_dims`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_mfg_series_dims` AS SELECT 
 1 AS `manufacturer_id`,
 1 AS `manufacturer_name`,
 1 AS `idtbl_mfg_series`,
 1 AS `series`,
 1 AS `special_instruction`,
 1 AS `idtbl_window_type`,
 1 AS `window_type_abbr`,
 1 AS `idtbl_window_dimensions`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_mfg_series_dims_pricing`
--

DROP TABLE IF EXISTS `vw_mfg_series_dims_pricing`;
/*!50001 DROP VIEW IF EXISTS `vw_mfg_series_dims_pricing`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_mfg_series_dims_pricing` AS SELECT 
 1 AS `manufacturer_id`,
 1 AS `manufacturer_name`,
 1 AS `idtbl_mfg_series`,
 1 AS `series`,
 1 AS `special_instruction`,
 1 AS `idtbl_window_type`,
 1 AS `window_type_abbr`,
 1 AS `idtbl_window_dimensions`,
 1 AS `base_amt`,
 1 AS `e340_amt`,
 1 AS `e366_amt`,
 1 AS `tempered_amt`,
 1 AS `grid_amt`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'vendors'
--

--
-- Dumping routines for database 'vendors'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_crud_window_coating` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_crud_window_coating`(IN in_coating_name VARCHAR(45))
BEGIN
DECLARE FoundId INT;
    DECLARE Checkifexists INT;
	SET Checkifexists = 0;
    
	SELECT count(*) INTO Checkifexists
	FROM tbl_window_coating
	WHERE window_coating_name = in_coating_name;

	IF (Checkifexists > 0) THEN
		SELECT window_coating_id INTO FoundId
		FROM tbl_window_coating
		WHERE window_coating_name = in_coating_name;
	
		UPDATE tbl_window_coating
		SET window_coating_name = in_coating_name
		WHERE window_coating_id = FoundId;
	ELSE
		INSERT INTO tbl_window_coating (window_coating_name)
		VALUES (in_coating_name);
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_crud_window_color` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_crud_window_color`(IN in_color_name VARCHAR(45))
BEGIN
DECLARE FoundId INT;
    DECLARE Checkifexists INT;
	SET Checkifexists = 0;
    
	SELECT count(*) INTO Checkifexists
	FROM tbl_window_color
	WHERE window_color_name = in_color_name;

	IF (Checkifexists > 0) THEN
		SELECT window_color_id INTO FoundId
		FROM tbl_window_color
		WHERE window_color_name = in_color_name;
	
		UPDATE tbl_window_color
		SET window_color_name = in_color_name
		WHERE window_color_id = FoundId;
	ELSE
		INSERT INTO tbl_window_color (window_color_name)
		VALUES (in_color_name);
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_crud_window_fin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_crud_window_fin`(IN in_fin_name VARCHAR(45))
BEGIN
DECLARE FoundId INT;
    DECLARE Checkifexists INT;
	SET Checkifexists = 0;
    
	SELECT count(*) INTO Checkifexists
	FROM tbl_window_fin
	WHERE window_fin_name = in_fin_name;

	IF (Checkifexists > 0) THEN
		SELECT window_fin_id INTO FoundId
		FROM tbl_window_fin
		WHERE window_fin_name = in_fin_name;
	
		UPDATE tbl_window_fin
		SET window_fin_name = in_fin_name
		WHERE window_fin_id = FoundId;
	ELSE
		INSERT INTO tbl_window_fin (window_fin_name)
		VALUES (in_fin_name);
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_crud_window_material` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_crud_window_material`(IN in_material_name VARCHAR(45))
BEGIN
DECLARE FoundId INT;
    DECLARE Checkifexists INT;
	SET Checkifexists = 0;
    
	SELECT count(*) INTO Checkifexists
	FROM tbl_window_material
	WHERE window_material_name = in_material_name;

	IF (Checkifexists > 0) THEN
		SELECT window_material_id INTO FoundId
		FROM tbl_window_material
		WHERE window_material_name = in_material_name;
	
		UPDATE tbl_window_material
		SET window_material_name = in_material_name
		WHERE window_material_id = FoundId;
	ELSE
		INSERT INTO tbl_window_material (window_material_name)
		VALUES (in_material_name);
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_crud_window_type` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_crud_window_type`(IN in_abbr VARCHAR(10), in_description VARCHAR(45))
BEGIN
	DECLARE FoundId INT;
    DECLARE Checkifexists INT;
	SET Checkifexists = 0;
    
	SELECT count(*) INTO Checkifexists
	FROM tbl_window_type
	WHERE window_type_abbr = in_abbr;

	IF (Checkifexists > 0) THEN
		SELECT idtbl_window_type INTO FoundId
		FROM tbl_window_type
		WHERE window_type_abbr = in_abbr;
	
		UPDATE tbl_window_type
		SET window_type_abbr = in_abbr, window_type_description = in_description
		WHERE idtbl_window_type = FoundId;
	ELSE
		INSERT INTO tbl_window_type (window_type_abbr, window_type_description)
		VALUES (in_abbr, in_description);
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_manufacturers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_manufacturers`()
BEGIN
	SELECT
		manufacturer_id,
        mfg_abbr,
        manufacturer_name
        
	from tbl_manufacturer;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `xowindows`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `xowindows` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `xowindows`;

--
-- Table structure for table `tbl_branch`
--

DROP TABLE IF EXISTS `tbl_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_branch` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(45) DEFAULT NULL,
  `po_abbreviation` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_branch`
--

LOCK TABLES `tbl_branch` WRITE;
/*!40000 ALTER TABLE `tbl_branch` DISABLE KEYS */;
INSERT INTO `tbl_branch` VALUES (1,'Phoenix','PH'),(2,'Tucson','TU'),(3,'Henderson','LV');
/*!40000 ALTER TABLE `tbl_branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `level` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_users`
--

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` VALUES (1,'lking','e10adc3949ba59abbe56e057f20f883e','lance@xowindws.com','1'),(2,'scabrera','e10adc3949ba59abbe56e057f20f883e','selina@xowindows.com','2'),(3,'lwheeler','e10adc3949ba59abbe56e057f20f883e','laura@xowindows.com','3'),(4,'eloya','e10adc3949ba59abbe56e057f20f883e','eusevio@xowindows.com','3'),(5,'cknox','e10adc3949ba59abbe56e057f20f883e','starts3@xowindows.com','2');
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_zone`
--

DROP TABLE IF EXISTS `tbl_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_zone` (
  `zone_id` int(2) NOT NULL AUTO_INCREMENT,
  `zone_name` varchar(45) DEFAULT NULL,
  `zone_abbreviation` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`zone_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_zone`
--

LOCK TABLES `tbl_zone` WRITE;
/*!40000 ALTER TABLE `tbl_zone` DISABLE KEYS */;
INSERT INTO `tbl_zone` VALUES (1,'Northwest','NW'),(2,'North','N'),(3,'Northeast','NE'),(4,'East','E'),(5,'Southeast','SE'),(6,'South','S'),(7,'Southwest','SW'),(8,'West','W');
/*!40000 ALTER TABLE `tbl_zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `vw_branch_list`
--

DROP TABLE IF EXISTS `vw_branch_list`;
/*!50001 DROP VIEW IF EXISTS `vw_branch_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_branch_list` AS SELECT 
 1 AS `xo_branch_id`,
 1 AS `location`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_users_list`
--

DROP TABLE IF EXISTS `vw_users_list`;
/*!50001 DROP VIEW IF EXISTS `vw_users_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_users_list` AS SELECT 
 1 AS `id`,
 1 AS `login`,
 1 AS `password`,
 1 AS `email`,
 1 AS `level`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_zone_list`
--

DROP TABLE IF EXISTS `vw_zone_list`;
/*!50001 DROP VIEW IF EXISTS `vw_zone_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_zone_list` AS SELECT 
 1 AS `zone_id`,
 1 AS `zone_name`,
 1 AS `zone_abbreviation`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'xowindows'
--

--
-- Dumping routines for database 'xowindows'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_branch_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_branch_list`()
BEGIN
	SELECT
		id as xo_branch_id,
        location_name as location
        
	from tbl_branch;  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_one_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_one_user`(IN user_id_in INT)
begin
	select
		*
	from xowindows.tbl_users xo_users
    where xo_users.id = user_id_in;    
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_users_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_users_list`()
BEGIN
	SELECT
		id as user_id,
        login as user_login
        
	from tbl_users;  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_zones_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_zones_list`()
BEGIN
	SELECT
		id as zone_id,
        name as zone_name
        
	from tbl_zone;  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `builders`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `builders` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `builders`;

--
-- Temporary table structure for view `builders_history_changes`
--

DROP TABLE IF EXISTS `builders_history_changes`;
/*!50001 DROP VIEW IF EXISTS `builders_history_changes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `builders_history_changes` AS SELECT 
 1 AS `dt_datetime`,
 1 AS `action`,
 1 AS `builder_id`,
 1 AS `cust_status`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `tbl_buildercustomer`
--

DROP TABLE IF EXISTS `tbl_buildercustomer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_buildercustomer` (
  `builder_id` int(11) NOT NULL AUTO_INCREMENT,
  `builder_name` varchar(45) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `updated_by` int(3) DEFAULT NULL,
  `tbl_status_tbl_status_id` int(11) NOT NULL,
  PRIMARY KEY (`builder_id`),
  UNIQUE KEY `builder_name_UNIQUE` (`builder_name`),
  KEY `tbl_buildercustomer_tbl_status_idx` (`tbl_status_tbl_status_id`),
  KEY `tbl_buildercustomer_tbl_users_idx` (`updated_by`),
  KEY `tbl_buildercustomer_tbl_date_idx` (`update_date`),
  CONSTRAINT `tbl_buildercustomer_tbl_date` FOREIGN KEY (`update_date`) REFERENCES `application`.`tbl_date` (`date_key`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_buildercustomer_tbl_status` FOREIGN KEY (`tbl_status_tbl_status_id`) REFERENCES `application`.`tbl_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_buildercustomer_tbl_users` FOREIGN KEY (`updated_by`) REFERENCES `xowindows`.`tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_buildercustomer`
--

LOCK TABLES `tbl_buildercustomer` WRITE;
/*!40000 ALTER TABLE `tbl_buildercustomer` DISABLE KEYS */;
INSERT INTO `tbl_buildercustomer` VALUES (101,'AF STERLING',20190816,2133,1,700),(102,'ASHTIN HOMES',20190712,824,1,700),(103,'ASHTON',20190712,854,5,700),(104,'AV HOMES',20190101,1300,1,700),(105,'BEAZER',20190101,1300,1,700),(106,'BELA FLOR',20190101,1300,1,700),(107,'CALATLANTIC (STANDARD PACIFIC)',20190101,1300,1,700),(108,'CASH SALE',20190101,1300,1,700),(109,'COURTLAND',20190101,1300,1,700),(110,'CUSTOM',20190101,1300,1,700),(111,'DIAMANTE HOMES',20190101,1300,1,700),(112,'DORN',20190101,1300,1,700),(113,'DORN - TUC',20190101,1300,1,700),(114,'DR HORTON',20190825,1507,1,700),(115,'DR HORTON - TUC',20190101,1300,1,700),(116,'ELLIOT HOMES',20190101,1300,1,700),(117,'FD CONSTRUCTION',20190101,1300,1,700),(118,'GARRETT-WALKER',20190101,1300,1,700),(119,'HABITAT FOR HUMANITY',20190101,1300,1,700),(120,'HILLSTONE HOMES',20190101,1300,1,700),(121,'HOMES BY TOWNE',20190101,1300,1,700),(122,'K HOVNANIAN',20190101,1300,1,700),(123,'KAUFFMAN',20190101,1300,1,700),(124,'KEYSTONE',20190101,1300,1,700),(125,'LAWLER CONSTRUCTION',20190101,1300,1,700),(126,'LGI HOMES INC.',20190101,1300,1,700),(127,'LOR CONSTRUCTION',20190101,1300,1,700),(128,'MARACAY',20190101,1300,1,700),(129,'MARACAY - TUC',20190101,1300,1,700),(130,'MATTAMY',20190101,1300,1,700),(131,'MERITAGE',20190101,1300,1,700),(132,'MERITAGE - TUC',20190101,1300,1,700),(133,'MESQUITE HOMES - TUC',20190101,1300,1,700),(134,'MIRAMONTE HOMES',20190101,1300,1,700),(135,'MIRAMONTE HOMES - TUC',20190101,1300,1,700),(136,'MORGAN TAYLOR',20190101,1300,1,700),(137,'NEWGATE HOMES',20190101,1300,1,700),(138,'NEWMARK HOMES',20190101,1300,1,700),(139,'ON THE ROCK BUILDER',20190101,1300,1,700),(140,'PEPPER VINER - TUC',20190101,1300,1,700),(141,'PULTE',20190101,1300,1,700),(142,'PULTE - TUC',20190101,1300,1,700),(143,'REDWALL CONSTRUCTION',20190101,1300,1,700),(144,'RICHMOND',20190101,1300,1,700),(145,'ROBSON - TUC',20190101,1300,1,700),(146,'SHEA',20190820,1357,1,700),(147,'TAYLOR MORRISON',20190825,1443,1,700),(148,'TW LEWIS / DAVID WEEKLEY HOMES',20190101,1300,1,700),(149,'WILLIAM LYON',20190101,1300,1,700),(150,'WILLIAM RYAN',20190101,1300,1,700),(151,'WILSON PARKER',20190101,1300,1,700),(152,'WOODSIDE',20190101,1300,1,700),(168,'HANCOCK',20190825,1544,1,700);
/*!40000 ALTER TABLE `tbl_buildercustomer` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger builders.tbl_buildercustomer__ai after insert on builders.tbl_buildercustomer for each row
	-- insert into builders.tbl_history_buildercustomer select 'insert', NULL, NOW(), d.*
    -- from builders.tbl_buildercustomer as d where d.builder_id = NEW.builder_id
BEGIN
	DECLARE app_status_name_new varchar(25);
    DECLARE xow_user_name varchar(25);
    SET xow_user_name = (SELECT xousers.login from xowindows.tbl_users xousers WHERE xousers.id = new.updated_by);
    
	-- ID initial insert
		call sp_log_insert(
			'tbl_buildercustomer',
			'created',
			'builder_id',
			new.builder_id,
			xow_user_name,
			'CREATED',
			new.builder_id
		);
    
	-- Name initial insert
		call sp_log_insert(
			'tbl_buildercustomer',
			'created',
			'builder_name',
			new.builder_id,
			xow_user_name,
			'CREATED',
			new.builder_name
		);
    
	-- Status initial insert
		SET app_status_name_new = (SELECT appStatus.name from application.tbl_status appStatus where appStatus.id = new.tbl_status_tbl_status_id);
		call sp_log_insert(
			'tbl_buildercustomer',
			'created',
			'tbl_status_tbl_status_id',
			new.builder_id,
			xow_user_name,
			'CREATED',
			app_status_name_new
		);
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `builders`.`tbl_buildercustomer_AFTER_UPDATE` AFTER UPDATE ON `tbl_buildercustomer` FOR EACH ROW
BEGIN
	DECLARE app_status_name_old varchar(11);
    DECLARE app_status_name_new varchar(11);
    DECLARE xow_user_name varchar(11);
	SET xow_user_name = (SELECT xousers.login from xowindows.tbl_users xousers where xousers.id = new.updated_by);
    
    -- STATUS Change
		if (old.tbl_status_tbl_status_id <> new.tbl_status_tbl_status_id) then
		SET app_status_name_old = (SELECT appStatus.name from application.tbl_status appStatus where appStatus.id = old.tbl_status_tbl_status_id);
		SET app_status_name_new = (SELECT appStatus.name from application.tbl_status appStatus where appStatus.id = new.tbl_status_tbl_status_id);

		
		call sp_log_insert(
			'tbl_buildercustomer',
			'update',
			'status_id',
			old.builder_id,
			xow_user_name,
			app_status_name_old,
			app_status_name_new
			);
		end if;
        
	-- NAME change
		if (old.builder_name <> new.builder_name) then
			call sp_log_insert(
            'tbl_buildercustomer',
            'update',
            'builder_name',
            old.builder_id,
            xow_user_name,
            old.builder_name,
            new.builder_name
            );
        end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tbl_buildercustomer_has_install_types`
--

DROP TABLE IF EXISTS `tbl_buildercustomer_has_install_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_buildercustomer_has_install_types` (
  `tbl_install_types_install_types_id` int(11) NOT NULL,
  `tbl_buildercustomer_builder_id` int(11) NOT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`tbl_install_types_install_types_id`,`tbl_buildercustomer_builder_id`),
  KEY `fk_tbl_install_types_has_tbl_buildercustomer_tbl_buildercus_idx` (`tbl_buildercustomer_builder_id`),
  KEY `fk_tbl_install_types_has_tbl_buildercustomer_tbl_install_ty_idx` (`tbl_install_types_install_types_id`),
  KEY `fk_tbl_users_idx` (`updated_by`),
  KEY `fk_tbl_date_idx` (`update_date`),
  CONSTRAINT `fk_tbl_date` FOREIGN KEY (`update_date`) REFERENCES `application`.`tbl_date` (`date_key`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_install_types_has_tbl_buildercustomer_tbl_buildercusto1` FOREIGN KEY (`tbl_buildercustomer_builder_id`) REFERENCES `tbl_buildercustomer` (`builder_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_install_types_has_tbl_buildercustomer_tbl_install_types1` FOREIGN KEY (`tbl_install_types_install_types_id`) REFERENCES `tbl_install_types` (`install_types_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_users` FOREIGN KEY (`updated_by`) REFERENCES `xowindows`.`tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_buildercustomer_has_install_types`
--

LOCK TABLES `tbl_buildercustomer_has_install_types` WRITE;
/*!40000 ALTER TABLE `tbl_buildercustomer_has_install_types` DISABLE KEYS */;
INSERT INTO `tbl_buildercustomer_has_install_types` VALUES (205,118,20190820,932,1),(206,101,20190729,829,1),(206,146,20190820,1000,1),(207,101,20190729,829,1),(207,146,20190726,1024,1),(208,101,20190729,829,1),(217,101,20190726,1024,1),(217,114,NULL,NULL,1),(217,147,NULL,NULL,1),(217,168,NULL,NULL,1),(218,101,20190726,1024,1),(219,101,20190726,1024,1),(220,101,20190726,1024,1);
/*!40000 ALTER TABLE `tbl_buildercustomer_has_install_types` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `builders`.`tbl_buildercustomer_has_install_types_AFTER_INSERT` AFTER INSERT ON `tbl_buildercustomer_has_install_types` FOR EACH ROW
BEGIN
	DECLARE install_type_name_new varchar(45);
    DECLARE xow_user_name varchar(25);
    SET xow_user_name = (SELECT xousers.login from xowindows.tbl_users xousers WHERE xousers.id = new.updated_by);
        
	-- Builder's install type initial insert
		SET install_type_name_new = (SELECT instType.type from builders.tbl_install_types instType where instType.install_types_id = new.tbl_install_types_install_types_id);
		call sp_log_insert(
			'tbl_buildercustomer_has_install_types',
			'added',
			'tbl_install_types_install_types_id',
			new.tbl_buildercustomer_builder_id,
			xow_user_name,
			'ADDED',
			install_type_name_new
		);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tbl_history_buildercustomer`
--

DROP TABLE IF EXISTS `tbl_history_buildercustomer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_history_buildercustomer` (
  `action` varchar(8) DEFAULT 'insert',
  `revision` int(6) NOT NULL AUTO_INCREMENT,
  `dt_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `builder_id` int(11) NOT NULL,
  `builder_name` varchar(45) DEFAULT NULL,
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(3) DEFAULT NULL,
  `tbl_status_tbl_status_id` int(11) NOT NULL,
  PRIMARY KEY (`builder_id`,`revision`),
  KEY `tbl_buildercustomer_tbl_status_idx` (`tbl_status_tbl_status_id`),
  KEY `tbl_buildercustomer_tbl_users_idx` (`updated_by`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_history_buildercustomer`
--

LOCK TABLES `tbl_history_buildercustomer` WRITE;
/*!40000 ALTER TABLE `tbl_history_buildercustomer` DISABLE KEYS */;
INSERT INTO `tbl_history_buildercustomer` VALUES ('update',1,'2019-07-12 08:24:00',102,'ASHTIN HOMES','2018-01-01 00:00:00','2019-07-12 08:24:00',1,700),('update',1,'2019-07-12 11:31:41',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 11:31:41',5,700),('update',1,'2019-07-12 08:54:48',103,'ASHTON','2018-01-01 00:00:00','2019-07-12 08:54:48',5,700),('update',10,'2019-07-12 13:28:49',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:28:49',5,700),('update',2,'2019-07-12 11:36:49',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 11:36:49',5,700),('update',3,'2019-07-12 11:36:49',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 11:36:49',5,702),('update',4,'2019-07-12 12:57:08',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 12:57:08',5,702),('update',5,'2019-07-12 12:57:08',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 12:57:08',5,700),('update',9,'2019-07-12 13:28:43',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:28:43',5,700),('update',8,'2019-07-12 13:28:43',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:28:43',5,700),('update',7,'2019-07-12 13:06:18',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:06:18',5,700),('update',6,'2019-07-12 13:06:18',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:06:18',5,700),('update',11,'2019-07-12 13:28:49',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:28:49',5,700),('update',12,'2019-07-12 13:33:45',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:33:45',5,700),('update',13,'2019-07-12 13:33:45',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:33:45',5,700),('update',14,'2019-07-12 13:33:45',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:33:45',5,700),('update',15,'2019-07-12 13:33:45',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:33:45',5,700),('update',16,'2019-07-12 13:34:02',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:34:02',5,700),('update',17,'2019-07-12 13:34:02',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:34:02',5,700),('update',18,'2019-07-12 13:34:02',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:34:02',5,700),('update',19,'2019-07-12 13:34:02',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:34:02',5,700),('update',20,'2019-07-12 13:34:34',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:34:34',5,700),('update',21,'2019-07-12 13:34:34',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:34:34',5,700),('update',22,'2019-07-12 13:34:34',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:34:34',5,700),('update',23,'2019-07-12 13:34:34',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:34:34',5,700),('update',24,'2019-07-12 13:35:12',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:35:12',5,700),('update',25,'2019-07-12 13:35:12',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:35:12',5,700),('update',26,'2019-07-12 13:35:12',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:35:12',5,700),('update',27,'2019-07-12 13:35:12',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:35:12',5,700),('update',28,'2019-07-12 13:47:37',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:47:37',5,700),('update',29,'2019-07-12 13:47:37',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:47:37',5,700),('update',30,'2019-07-12 13:50:08',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:50:08',5,700),('update',31,'2019-07-12 13:50:08',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 13:50:08',5,700),('update',32,'2019-07-12 14:00:54',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:00:54',5,700),('update',33,'2019-07-12 14:00:54',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:00:54',5,700),('update',34,'2019-07-12 14:15:13',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:15:13',5,700),('update',35,'2019-07-12 14:15:13',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:15:13',5,700),('update',36,'2019-07-12 14:17:45',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:17:45',5,700),('update',37,'2019-07-12 14:17:45',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:17:45',5,700),('update',38,'2019-07-12 14:20:42',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:20:42',5,700),('update',39,'2019-07-12 14:20:42',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:20:42',5,700),('update',40,'2019-07-12 14:22:33',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:22:33',5,700),('update',41,'2019-07-12 14:22:33',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:22:33',5,700),('update',42,'2019-07-12 14:37:09',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:37:09',5,700),('update',43,'2019-07-12 14:37:09',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:37:09',5,700),('update',44,'2019-07-12 14:38:58',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:38:58',5,700),('update',45,'2019-07-12 14:38:58',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:38:58',5,700),('update',46,'2019-07-12 14:41:39',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:41:39',5,700),('update',47,'2019-07-12 14:41:39',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:41:39',5,700),('update',48,'2019-07-12 14:50:37',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:50:37',5,700),('update',49,'2019-07-12 14:50:37',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:50:37',5,700),('update',50,'2019-07-12 14:52:52',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:52:52',5,700),('update',51,'2019-07-12 14:52:52',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:52:52',5,700),('update',52,'2019-07-12 14:53:56',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:53:56',5,700),('update',53,'2019-07-12 14:53:56',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:53:56',5,700),('update',54,'2019-07-12 14:55:04',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:55:04',5,700),('update',55,'2019-07-12 14:55:04',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-12 14:55:04',5,700),('update',56,'2019-07-15 07:51:49',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 07:51:49',1,700),('update',57,'2019-07-15 07:51:49',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 07:51:49',1,700),('update',58,'2019-07-15 08:07:11',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 08:07:11',1,700),('update',59,'2019-07-15 08:07:11',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 08:07:11',1,700),('update',60,'2019-07-15 08:08:57',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 08:08:57',1,700),('update',61,'2019-07-15 08:08:57',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 08:08:57',1,700),('update',62,'2019-07-15 08:10:08',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 08:10:08',1,700),('update',63,'2019-07-15 08:10:08',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 08:10:08',1,700),('update',64,'2019-07-15 08:10:40',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 08:10:40',1,700),('update',65,'2019-07-15 08:10:40',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 08:10:40',1,700),('update',66,'2019-07-15 08:13:49',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 08:13:49',1,700),('update',67,'2019-07-15 08:13:49',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 08:13:49',1,700),('update',68,'2019-07-15 09:12:53',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:12:53',1,700),('update',69,'2019-07-15 09:12:53',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:12:53',1,700),('update',70,'2019-07-15 09:15:14',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:15:14',1,700),('update',71,'2019-07-15 09:15:14',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:15:14',1,700),('update',72,'2019-07-15 09:16:10',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:16:10',1,700),('update',73,'2019-07-15 09:16:11',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:16:11',1,700),('update',74,'2019-07-15 09:17:04',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:17:04',1,700),('update',75,'2019-07-15 09:17:04',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:17:04',1,700),('update',76,'2019-07-15 09:31:23',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:31:23',1,700),('update',77,'2019-07-15 09:31:24',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:31:24',1,700),('update',78,'2019-07-15 09:32:30',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:32:30',1,700),('update',79,'2019-07-15 09:32:31',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:32:31',1,700),('update',80,'2019-07-15 09:34:08',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:34:08',1,700),('update',81,'2019-07-15 09:34:08',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:34:08',1,700),('update',82,'2019-07-15 09:35:05',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:35:05',1,700),('update',83,'2019-07-15 09:35:05',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:35:05',1,700),('update',84,'2019-07-15 09:37:06',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:37:06',1,700),('update',85,'2019-07-15 09:37:06',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:37:06',1,700),('update',86,'2019-07-15 09:40:36',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:40:36',1,700),('update',87,'2019-07-15 09:40:36',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:40:36',1,700),('update',88,'2019-07-15 09:53:20',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:53:20',1,700),('update',89,'2019-07-15 09:53:20',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:53:20',1,700),('update',90,'2019-07-15 09:55:00',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:55:00',1,700),('update',91,'2019-07-15 09:55:01',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:55:01',1,700),('update',92,'2019-07-15 09:56:03',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:56:03',1,700),('update',93,'2019-07-15 09:56:03',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:56:03',1,700),('update',94,'2019-07-15 09:59:06',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:59:06',1,700),('update',95,'2019-07-15 09:59:06',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 09:59:06',1,700),('update',96,'2019-07-15 10:00:36',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 10:00:36',1,700),('update',97,'2019-07-15 10:00:36',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 10:00:36',1,700),('update',98,'2019-07-15 10:51:26',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 10:51:26',1,700),('update',99,'2019-07-15 10:51:26',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 10:51:26',1,700),('update',100,'2019-07-15 11:02:09',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 11:02:09',1,700),('update',101,'2019-07-15 11:54:24',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 11:54:24',1,700),('update',102,'2019-07-15 11:54:24',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 11:54:24',1,700),('update',103,'2019-07-15 12:09:45',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 12:09:45',1,700),('update',104,'2019-07-15 14:25:16',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 12:09:45',1,701),('update',105,'2019-07-15 14:27:10',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 12:09:45',1,701),('update',106,'2019-07-15 14:28:13',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 12:09:45',1,701),('update',107,'2019-07-15 14:30:41',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 12:09:45',1,700),('update',108,'2019-07-16 08:06:08',101,'AF STERLING - TUC','2018-01-01 00:00:00','2019-07-15 12:09:45',1,701);
/*!40000 ALTER TABLE `tbl_history_buildercustomer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_install_types`
--

DROP TABLE IF EXISTS `tbl_install_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_install_types` (
  `install_types_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(75) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`install_types_id`),
  UNIQUE KEY `type_UNIQUE` (`type`),
  KEY `tbl_install_types_tbl_date_idx` (`update_date`),
  CONSTRAINT `tbl_install_types_tbl_date` FOREIGN KEY (`update_date`) REFERENCES `application`.`tbl_date` (`date_key`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_install_types`
--

LOCK TABLES `tbl_install_types` WRITE;
/*!40000 ALTER TABLE `tbl_install_types` DISABLE KEYS */;
INSERT INTO `tbl_install_types` VALUES (200,'UNDEFINED',20190101,1300,'INITIAL_LOAD'),(201,'25 MIL MEMBRANE / SILL PAN',20190101,1300,'INITIAL_LOAD'),(202,'9\" FLASHING W/ SILL PAN',20190101,1300,'INITIAL_LOAD'),(203,'9\" PAPER W/ SILL PAN INSTALL',20190101,1300,'INITIAL_LOAD'),(204,'9\" RAIN BUSTER W/ SILL PANS',20190101,1300,'INITIAL_LOAD'),(205,'DOOR INSTALL ONLY 9\" PAPER',20190101,1300,'INITIAL_LOAD'),(206,'INSTALL',20190101,1300,'INITIAL_LOAD'),(207,'INSTALL SGD\'S',20190101,1300,'INITIAL_LOAD'),(208,'INSTALL W/ 9\" 25 MIL ON SILL',20190101,1300,'INITIAL_LOAD'),(209,'MEMBRANE ON SILL',20190101,1300,'INITIAL_LOAD'),(210,'PRESCOTT INSTALL',20190101,1300,'INITIAL_LOAD'),(211,'RAIN BUSTER INSTALL',20190101,1300,'INITIAL_LOAD'),(212,'RECESSED',20190101,1300,'INITIAL_LOAD'),(213,'SALESMANS CHOICE',20190101,1300,'INITIAL_LOAD'),(214,'SEE ROBIN',20190101,1300,'INITIAL_LOAD'),(215,'SEE TONY',20190101,1300,'INITIAL_LOAD'),(216,'SILL PAN',20190101,1300,'INITIAL_LOAD'),(217,'SILL PAN INSTALL',20190101,1300,'INITIAL_LOAD'),(218,'SILL PAN INSTALL (WATERBLOCK)',20190101,1300,'INITIAL_LOAD'),(219,'STAN PAC INSTALL',20190101,1300,'INITIAL_LOAD'),(220,'TUCSON INSTALL',20190101,1300,'INITIAL_LOAD'),(221,'TW LEWIS INSTALL',20190101,1300,'INITIAL_LOAD'),(222,'TYPAR',20190101,1300,'INITIAL_LOAD'),(223,'TYPAR INSTALL',20190101,1300,'INITIAL_LOAD'),(224,'TYVEK INSTALL',20190101,1300,'INITIAL_LOAD');
/*!40000 ALTER TABLE `tbl_install_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_log_builders`
--

DROP TABLE IF EXISTS `tbl_log_builders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_log_builders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_in_builders` varchar(45) DEFAULT NULL,
  `action` varchar(25) DEFAULT NULL,
  `field` varchar(45) DEFAULT NULL,
  `tbl_row_no` int(11) DEFAULT NULL,
  `changed_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `changed_by` varchar(25) DEFAULT NULL,
  `old_value` varchar(45) DEFAULT NULL,
  `new_value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=227 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_log_builders`
--

LOCK TABLES `tbl_log_builders` WRITE;
/*!40000 ALTER TABLE `tbl_log_builders` DISABLE KEYS */;
INSERT INTO `tbl_log_builders` VALUES (62,'tbl_buildercustomer_has_install_types','removed','tbl_install_types_install_types_id',101,'2019-07-30 16:09:40','lking','REMOVED','25 MIL MEMBRANE / SILL PAN'),(61,'tbl_buildercustomer_has_install_types','added','tbl_install_types_install_types_id',101,'2019-07-30 16:08:53','lking','ADDED','25 MIL MEMBRANE / SILL PAN'),(60,'tbl_buildercustomer','update','builder_name',101,'2019-07-30 16:08:53','lking','AF STERLING','AF STERLING - TUC'),(10,'tbl_buildercustomer','update','status_id',101,'2019-07-16 09:27:19','cknox','ACTIVE','SUSPENDED'),(11,'tbl_buildercustomer','update','status_id',101,'2019-07-16 09:41:42','cknox','SUSPENDED','ACTIVE'),(12,'tbl_buildercustomer','update','builder_name',101,'2019-07-16 09:50:43','cknox','AF STERLING - TUC','AF STERLING'),(13,'tbl_buildercustomer','update','status_id',101,'2019-07-16 09:50:44','cknox','ACTIVE','SUSPENDED'),(14,'tbl_buildercustomer','update','status_id',101,'2019-07-16 10:27:58','lking','SUSPENDED','ACTIVE'),(15,'tbl_buildercustomer','update','status_id',101,'2019-07-17 08:55:45','lking','ACTIVE','SUSPENDED'),(16,'tbl_buildercustomer','update','status_id',101,'2019-07-17 08:57:19','lking','SUSPENDED','ACTIVE'),(17,'tbl_buildercustomer','update','builder_name',101,'2019-07-17 08:59:10','lking','AF STERLING','AF STERLING - TUC'),(18,'tbl_buildercustomer','update','status_id',101,'2019-07-17 08:59:10','lking','ACTIVE','SUSPENDED'),(19,'tbl_buildercustomer','update','builder_name',101,'2019-07-17 08:59:26','lking','AF STERLING - TUC','AF STERLING'),(20,'tbl_buildercustomer','update','status_id',101,'2019-07-17 08:59:26','lking','SUSPENDED','ACTIVE'),(21,'tbl_buildercustomer','created','builder_id',157,'2019-07-25 08:14:34','lking','CREATED','157'),(22,'tbl_buildercustomer','created','builder_id',158,'2019-07-25 08:15:03','lking','CREATED','158'),(23,'tbl_buildercustomer','created','builder_id',153,'2019-07-25 08:15:57','lking','CREATED','153'),(24,'tbl_buildercustomer','created','builder_id',155,'2019-07-25 08:17:30','lking','CREATED','155'),(50,'tbl_buildercustomer_has_install_types','added','tbl_install_types_install_types_id',101,'2019-07-26 14:35:52','lking','ADDED','INSTALL W/ 9\" 25 MIL ON SILL'),(51,'tbl_buildercustomer_has_install_types','removed','tbl_install_types_install_types_id',101,'2019-07-29 07:22:35','lking','REMOVED','9\" FLASHING W/ SILL PAN'),(52,'tbl_buildercustomer_has_install_types','added','tbl_install_types_install_types_id',101,'2019-07-29 08:10:38','lking','ADDED','INSTALL'),(53,'tbl_buildercustomer_has_install_types','removed','tbl_install_types_install_types_id',101,'2019-07-29 08:11:15','lking','REMOVED','INSTALL'),(54,'tbl_buildercustomer_has_install_types','removed','tbl_install_types_install_types_id',101,'2019-07-29 08:11:44','lking','REMOVED','INSTALL W/ 9\" 25 MIL ON SILL'),(55,'tbl_buildercustomer_has_install_types','removed','tbl_install_types_install_types_id',101,'2019-07-29 08:16:12','lking','REMOVED','SEE TONY'),(56,'tbl_buildercustomer_has_install_types','removed','tbl_install_types_install_types_id',101,'2019-07-29 08:29:04','lking','REMOVED','SILL PAN'),(57,'tbl_buildercustomer_has_install_types','added','tbl_install_types_install_types_id',101,'2019-07-29 08:29:51','lking','ADDED','INSTALL'),(58,'tbl_buildercustomer_has_install_types','added','tbl_install_types_install_types_id',101,'2019-07-29 08:29:51','lking','ADDED','INSTALL SGD\'S'),(59,'tbl_buildercustomer_has_install_types','added','tbl_install_types_install_types_id',101,'2019-07-29 08:29:51','lking','ADDED','INSTALL W/ 9\" 25 MIL ON SILL'),(70,'tbl_subdivision','update','starts rep',1,'2019-08-04 20:01:14','lking','eloya','cknox'),(71,'tbl_subdivision','update','xo field rep',1,'2019-08-04 20:01:14','lking','lwheeler','eloya'),(89,'tbl_subdivision','created','starts rep',3,'2019-08-14 08:17:14','lking','CREATED','scabrera'),(88,'tbl_subdivision','created','branch',3,'2019-08-14 08:17:14','lking','CREATED','Phoenix'),(87,'tbl_subdivision','created','zone',3,'2019-08-14 08:17:14','lking','CREATED','Northwest'),(86,'tbl_subdivision','created','zip',3,'2019-08-14 08:17:14','lking','CREATED','85387'),(85,'tbl_subdivision','created','state',3,'2019-08-14 08:17:14','lking','CREATED','AZ'),(84,'tbl_subdivision','created','city',3,'2019-08-14 08:17:14','lking','CREATED','SURPRISE'),(83,'tbl_subdivision','created','subdivision_name',3,'2019-08-14 08:17:14','lking','CREATED','NORTH COPPER CANYON'),(82,'tbl_subdivision','created','tbl_status_tbl_status_id',3,'2019-08-14 08:17:14','lking','CREATED','ACTIVE'),(90,'tbl_subdivision','created','xo field rep',3,'2019-08-14 08:17:14','lking','CREATED','eloya'),(91,'tbl_subdivision','created','subdivision_id',3,'2019-08-14 08:17:14','lking','CREATED','3'),(92,'tbl_subdivision','update','zone',3,'2019-08-14 10:34:18','lking','Northwest','West'),(93,'tbl_subdivision','update','zone',3,'2019-08-14 10:34:49','lking','West','Northwest'),(94,'tbl_subdivision','update','zone',3,'2019-08-14 12:18:57','lking','West','Northwest'),(95,'tbl_subdivision','update','zone',1,'2019-08-14 12:19:08','lking','East','Southeast'),(96,'tbl_subdivision','update','zone',1,'2019-08-14 12:35:28','lking','Southeast','East'),(97,'tbl_subdivision','update','zone',1,'2019-08-14 12:40:58','lking','East','Southeast'),(98,'tbl_buildercustomer','update','builder_name',101,'2019-08-16 21:33:30','lking','AF STERLING - TUC','AF STERLING'),(159,'tbl_subdivision','created','tbl_status_tbl_status_id',4,'2019-08-25 14:44:57','lking','CREATED','ACTIVE'),(158,'tbl_buildercustomer_has_install_types','added','tbl_install_types_install_types_id',147,'2019-08-25 14:43:00','lking','ADDED','SILL PAN INSTALL'),(102,'tbl_buildercustomer_has_install_types','added','tbl_install_types_install_types_id',118,'2019-08-20 09:33:07','lking','ADDED','DOOR INSTALL ONLY 9\" PAPER'),(103,'tbl_buildercustomer_has_install_types','added','tbl_install_types_install_types_id',146,'2019-08-20 09:48:45','lking','ADDED','INSTALL'),(104,'tbl_buildercustomer_has_install_types','removed','tbl_install_types_install_types_id',146,'2019-08-20 10:20:03','lking','REMOVED','INSTALL'),(151,'tbl_buildercustomer_has_install_types','removed','tbl_install_types_install_types_id',146,'2019-08-24 16:15:34','lking','REMOVED','INSTALL'),(152,'tbl_buildercustomer_has_install_types','removed','tbl_install_types_install_types_id',146,'2019-08-24 16:23:45','lking','REMOVED','INSTALL'),(153,'tbl_subdivision','created','tbl_status_tbl_status_id',4,'2019-08-24 16:36:05','lking','CREATED','ACTIVE'),(154,'tbl_subdivision','created','subdivision_name',4,'2019-08-24 16:36:05','lking','CREATED','NORTH COPPER CANYON'),(155,'tbl_subdivision','created','city',4,'2019-08-24 16:36:05','lking','CREATED','SURPRISE'),(156,'tbl_subdivision','created','state',4,'2019-08-24 16:36:05','lking','CREATED','AZ'),(157,'tbl_subdivision','created','zip',4,'2019-08-24 16:36:05','lking','CREATED','85387'),(160,'tbl_subdivision','created','subdivision_name',4,'2019-08-25 14:44:57','lking','CREATED','GARDEN GROVE'),(161,'tbl_subdivision','created','city',4,'2019-08-25 14:44:57','lking','CREATED','GLENDALE'),(162,'tbl_subdivision','created','state',4,'2019-08-25 14:44:57','lking','CREATED','AZ'),(163,'tbl_subdivision','created','zip',4,'2019-08-25 14:44:57','lking','CREATED','85305'),(164,'tbl_subdivision','created','tbl_status_tbl_status_id',5,'2019-08-25 14:58:12','lking','CREATED','ACTIVE'),(165,'tbl_subdivision','created','subdivision_name',5,'2019-08-25 14:58:12','lking','CREATED','GARDEN GROVE'),(166,'tbl_subdivision','created','city',5,'2019-08-25 14:58:12','lking','CREATED','GLENDALE'),(167,'tbl_subdivision','created','state',5,'2019-08-25 14:58:12','lking','CREATED','AZ'),(168,'tbl_subdivision','created','zip',5,'2019-08-25 14:58:12','lking','CREATED','85305'),(169,'tbl_subdivision','created','zone',5,'2019-08-25 14:58:12','lking','CREATED','Northwest'),(170,'tbl_subdivision','created','branch',5,'2019-08-25 14:58:12','lking','CREATED','Phoenix'),(171,'tbl_subdivision','created','starts rep',5,'2019-08-25 14:58:12','lking','CREATED','cknox'),(172,'tbl_subdivision','created','xo field rep',5,'2019-08-25 14:58:12','lking','CREATED','eloya'),(173,'tbl_subdivision','created','subdivision_id',5,'2019-08-25 14:58:12','lking','CREATED','5'),(174,'tbl_buildercustomer_has_install_types','added','tbl_install_types_install_types_id',114,'2019-08-25 15:07:48','lking','ADDED','SILL PAN INSTALL'),(175,'tbl_subdivision','created','tbl_status_tbl_status_id',6,'2019-08-25 15:10:58','lking','CREATED','ACTIVE'),(176,'tbl_subdivision','created','subdivision_name',6,'2019-08-25 15:10:58','lking','CREATED','TRE VICINO TH'),(177,'tbl_subdivision','created','city',6,'2019-08-25 15:10:58','lking','CREATED','CHANDLER'),(178,'tbl_subdivision','created','state',6,'2019-08-25 15:10:58','lking','CREATED','AZ'),(179,'tbl_subdivision','created','zip',6,'2019-08-25 15:10:58','lking','CREATED','85225'),(180,'tbl_subdivision','created','zone',6,'2019-08-25 15:10:58','lking','CREATED','Southeast'),(181,'tbl_subdivision','created','branch',6,'2019-08-25 15:10:58','lking','CREATED','Phoenix'),(182,'tbl_subdivision','created','starts rep',6,'2019-08-25 15:10:58','lking','CREATED','cknox'),(183,'tbl_subdivision','created','xo field rep',6,'2019-08-25 15:10:58','lking','CREATED','eloya'),(184,'tbl_subdivision','created','subdivision_id',6,'2019-08-25 15:10:58','lking','CREATED','6'),(185,'tbl_subdivision','created','tbl_status_tbl_status_id',7,'2019-08-25 15:28:29','lking','CREATED','ACTIVE'),(186,'tbl_subdivision','created','subdivision_name',7,'2019-08-25 15:28:29','lking','CREATED','TRE VICINO TRIPLEX'),(187,'tbl_subdivision','created','city',7,'2019-08-25 15:28:29','lking','CREATED','CHANDLER'),(188,'tbl_subdivision','created','state',7,'2019-08-25 15:28:29','lking','CREATED','AZ'),(189,'tbl_subdivision','created','zip',7,'2019-08-25 15:28:29','lking','CREATED','85225'),(190,'tbl_subdivision','created','zone',7,'2019-08-25 15:28:29','lking','CREATED','Southeast'),(191,'tbl_subdivision','created','branch',7,'2019-08-25 15:28:29','lking','CREATED','Phoenix'),(192,'tbl_subdivision','created','starts rep',7,'2019-08-25 15:28:29','lking','CREATED','cknox'),(193,'tbl_subdivision','created','xo field rep',7,'2019-08-25 15:28:29','lking','CREATED','eloya'),(194,'tbl_subdivision','created','subdivision_id',7,'2019-08-25 15:28:29','lking','CREATED','7'),(195,'tbl_buildercustomer','created','builder_id',168,'2019-08-25 15:44:23','lking','CREATED','168'),(196,'tbl_buildercustomer','created','builder_name',168,'2019-08-25 15:44:23','lking','CREATED','HANCOCK'),(197,'tbl_buildercustomer','created','tbl_status_tbl_status_id',168,'2019-08-25 15:44:23','lking','CREATED','ACTIVE'),(198,'tbl_buildercustomer_has_install_types','added','tbl_install_types_install_types_id',168,'2019-08-25 15:44:55','lking','ADDED','SILL PAN INSTALL'),(199,'tbl_subdivision','created','tbl_status_tbl_status_id',8,'2019-08-25 15:50:14','lking','CREATED','ACTIVE'),(200,'tbl_subdivision','created','subdivision_name',8,'2019-08-25 15:50:14','lking','CREATED','VISTANCIA'),(201,'tbl_subdivision','created','city',8,'2019-08-25 15:50:14','lking','CREATED','PEORIA'),(202,'tbl_subdivision','created','state',8,'2019-08-25 15:50:14','lking','CREATED','AZ'),(203,'tbl_subdivision','created','zip',8,'2019-08-25 15:50:14','lking','CREATED','85383'),(204,'tbl_subdivision','created','zone',8,'2019-08-25 15:50:14','lking','CREATED','North'),(205,'tbl_subdivision','created','branch',8,'2019-08-25 15:50:14','lking','CREATED','Phoenix'),(206,'tbl_subdivision','created','starts rep',8,'2019-08-25 15:50:14','lking','CREATED','cknox'),(207,'tbl_subdivision','created','xo field rep',8,'2019-08-25 15:50:14','lking','CREATED','eloya'),(208,'tbl_subdivision','created','subdivision_id',8,'2019-08-25 15:50:14','lking','CREATED','8'),(209,'tbl_subdivision','created','super_name',8,'2019-08-25 15:50:14','lking','ADDED','JESSE'),(210,'tbl_subdivision','created','phone_number',8,'2019-08-25 15:50:14','lking','ADDED','6023095971'),(211,'tbl_subdivision','created','tbl_status_tbl_status_id',9,'2019-08-25 15:54:35','lking','CREATED','ACTIVE'),(212,'tbl_subdivision','created','subdivision_name',9,'2019-08-25 15:54:35','lking','CREATED','GERMANN COUNTRY ESTATES'),(213,'tbl_subdivision','created','city',9,'2019-08-25 15:54:35','lking','CREATED','CHANDLER'),(214,'tbl_subdivision','created','state',9,'2019-08-25 15:54:35','lking','CREATED','AZ'),(215,'tbl_subdivision','created','zip',9,'2019-08-25 15:54:35','lking','CREATED','85286'),(216,'tbl_subdivision','created','zone',9,'2019-08-25 15:54:35','lking','CREATED','Southeast'),(217,'tbl_subdivision','created','branch',9,'2019-08-25 15:54:35','lking','CREATED','Phoenix'),(218,'tbl_subdivision','created','starts rep',9,'2019-08-25 15:54:35','lking','CREATED','cknox'),(219,'tbl_subdivision','created','xo field rep',9,'2019-08-25 15:54:35','lking','CREATED','eloya'),(225,'tbl_subdivision','update','phone_number',1,'2019-08-25 19:03:24','lking','4803774050','4803774051'),(224,'tbl_subdivision','update','phone_number',1,'2019-08-25 18:36:05','lking','6024556616','4803774050'),(223,'tbl_subdivision','update','super_name',1,'2019-08-25 18:36:05','lking','Lance','JOHN'),(226,'tbl_subdivision','update','phone_number',1,'2019-08-25 19:04:09','lking','4803774051','4803774050');
/*!40000 ALTER TABLE `tbl_log_builders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_subdivision`
--

DROP TABLE IF EXISTS `tbl_subdivision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_subdivision` (
  `subdivision_id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_xo_branch_xo_branch_id` int(11) NOT NULL,
  `tbl_buildercustomer_builder_id` int(11) NOT NULL,
  `subdivision_name` varchar(60) NOT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `zip` int(5) DEFAULT NULL,
  `tbl_zone_zone_id` int(11) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `tbl_status_tbl_status_id` int(11) NOT NULL,
  `tbl_users_tbl_user_id_starts` int(11) DEFAULT NULL,
  `tbl_users_tbl_user_id_xosupers` int(11) DEFAULT NULL,
  `vendor_tbl_manufacturer_id` int(11) DEFAULT NULL,
  `tbl_install_types` int(11) DEFAULT NULL,
  `tbl_supers_super_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`subdivision_id`),
  UNIQUE KEY `idx_brnch_bldr_name` (`tbl_xo_branch_xo_branch_id`,`tbl_buildercustomer_builder_id`,`subdivision_name`),
  KEY `fk_tbl_subdivision_tbl_xo_branch1_idx` (`tbl_xo_branch_xo_branch_id`),
  KEY `fk_tbl_subdivision_tbl_buildercustomer1_idx` (`tbl_buildercustomer_builder_id`),
  KEY `fk_xowindows_tbl_zone_idx` (`tbl_zone_zone_id`),
  KEY `fk_application_tbl_status_idx` (`tbl_status_tbl_status_id`),
  KEY `fk_xowindows_tbl_users_xosupers_idx` (`tbl_users_tbl_user_id_xosupers`),
  KEY `fk_xowindows_tbl_users_starts_idx` (`tbl_users_tbl_user_id_starts`),
  KEY `tbl_subdivision_tbl_date_idx` (`update_date`),
  KEY `fk_tbl_subdivision_tbl_manufacturer_idx` (`vendor_tbl_manufacturer_id`),
  KEY `fk_tbl_install_type_idx` (`tbl_install_types`,`tbl_buildercustomer_builder_id`),
  KEY `fk_tbl_supers_super_id_idx` (`tbl_supers_super_id`),
  CONSTRAINT `fk_application_tbl_status` FOREIGN KEY (`tbl_status_tbl_status_id`) REFERENCES `application`.`tbl_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_builder_has_install_types` FOREIGN KEY (`tbl_install_types`, `tbl_buildercustomer_builder_id`) REFERENCES `tbl_buildercustomer_has_install_types` (`tbl_install_types_install_types_id`, `tbl_buildercustomer_builder_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_subdivision_tbl_buildercustomer1` FOREIGN KEY (`tbl_buildercustomer_builder_id`) REFERENCES `tbl_buildercustomer` (`builder_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_subdivision_tbl_manufacturer` FOREIGN KEY (`vendor_tbl_manufacturer_id`) REFERENCES `vendors`.`tbl_manufacturer` (`manufacturer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_subdivision_tbl_xo_branch1` FOREIGN KEY (`tbl_xo_branch_xo_branch_id`) REFERENCES `xowindows`.`tbl_branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_supers_super_id` FOREIGN KEY (`tbl_supers_super_id`) REFERENCES `tbl_supers` (`super_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_xowindows_tbl_users_starts` FOREIGN KEY (`tbl_users_tbl_user_id_starts`) REFERENCES `xowindows`.`tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_xowindows_tbl_users_xosupers` FOREIGN KEY (`tbl_users_tbl_user_id_xosupers`) REFERENCES `xowindows`.`tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_xowindows_tbl_zone` FOREIGN KEY (`tbl_zone_zone_id`) REFERENCES `xowindows`.`tbl_zone` (`zone_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_subdivision_tbl_date` FOREIGN KEY (`update_date`) REFERENCES `application`.`tbl_date` (`date_key`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_subdivision`
--

LOCK TABLES `tbl_subdivision` WRITE;
/*!40000 ALTER TABLE `tbl_subdivision` DISABLE KEYS */;
INSERT INTO `tbl_subdivision` VALUES (1,1,146,'GERMANN COUNTRY ESTATES','CHANDLER','AZ',85286,5,20190101,1300,'1',700,5,4,100,206,1),(3,1,118,'NORTH COPPER CANYON','SURPRISE','AZ',85387,1,20190814,817,'1',700,2,4,100,205,NULL),(5,1,147,'GARDEN GROVE','GLENDALE','AZ',85305,1,20190825,1458,'1',700,5,4,100,NULL,3),(6,1,114,'TRE VICINO TH','CHANDLER','AZ',85225,5,20190825,1510,'1',700,5,4,100,NULL,4),(7,1,114,'TRE VICINO TRIPLEX','CHANDLER','AZ',85225,5,20190825,1528,'1',700,5,4,100,NULL,5),(8,1,168,'VISTANCIA','PEORIA','AZ',85383,2,20190825,1550,'1',700,5,4,100,217,6);
/*!40000 ALTER TABLE `tbl_subdivision` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `builders`.`tbl_subdivision_AFTER_INSERT` AFTER INSERT ON `tbl_subdivision` FOR EACH ROW
BEGIN
	DECLARE app_status_name_new varchar(25);
    DECLARE xowindows_zone_name_new varchar(45);
    DECLARE xowindows_branch_name_new varchar(45);
    DECLARE xowindows_starts_name_new varchar(45);
    DECLARE xowindows_xosuper_name_new varchar(45);
    DECLARE xow_user_name varchar(25);
    SET xow_user_name = (SELECT xousers.login from xowindows.tbl_users xousers WHERE xousers.id = new.updated_by);

	-- status initial insert
		SET app_status_name_new = (SELECT appStatus.name from application.tbl_status appStatus where appStatus.id = new.tbl_status_tbl_status_id);
		call sp_log_insert(
			'tbl_subdivision',
			'created',
			'tbl_status_tbl_status_id',
			new.subdivision_id,
			xow_user_name,
			'CREATED',
			app_status_name_new
		);
     
	-- subdivision_name initial insert
		call sp_log_insert(
			'tbl_subdivision',
			'created',
			'subdivision_name',
			new.subdivision_id,
			xow_user_name,
			'CREATED',
			new.subdivision_name
		);
	
    -- city initial insert
		call sp_log_insert(
			'tbl_subdivision',
			'created',
			'city',
			new.subdivision_id,
			xow_user_name,
			'CREATED',
			new.city
		);
        
	-- state initial insert
		call sp_log_insert(
			'tbl_subdivision',
			'created',
			'state',
			new.subdivision_id,
			xow_user_name,
			'CREATED',
			new.state
		);
        
	-- zip initial insert
		call sp_log_insert(
			'tbl_subdivision',
			'created',
			'zip',
			new.subdivision_id,
			xow_user_name,
			'CREATED',
			new.zip
		);
        
	-- zone initial insert
        SET xowindows_zone_name_new = (SELECT xowZone.zone_name from xowindows.tbl_zone xowZone where xowZone.zone_id = new.tbl_zone_zone_id);    
		call sp_log_insert(
			'tbl_subdivision',
			'created',
			'zone',
			new.subdivision_id,
			xow_user_name,
			'CREATED',
			xowindows_zone_name_new
		);
        
	-- branch initial insert
		SET xowindows_branch_name_new = (SELECT xowBranch.location_name from xowindows.tbl_branch xowBranch where xowBranch.id = new.tbl_xo_branch_xo_branch_id);
		call sp_log_insert(
			'tbl_subdivision',
			'created',
			'branch',
			new.subdivision_id,
			xow_user_name,
			'CREATED',
			xowindows_branch_name_new
		);
        
	-- start initial insert
		SET xowindows_starts_name_new = (SELECT xowUsers.login from xowindows.tbl_users xowUsers where xowUsers.id = new.tbl_users_tbl_user_id_starts);
		call sp_log_insert(
			'tbl_subdivision',
			'created',
			'starts rep',
			new.subdivision_id,
			xow_user_name,
			'CREATED',
			xowindows_starts_name_new
		);
        
	-- field rep initial insert
        SET xowindows_xosuper_name_new = (SELECT xowUsers.login from xowindows.tbl_users xowUsers where xowUsers.id = new.tbl_users_tbl_user_id_xosupers);
		call sp_log_insert(
			'tbl_subdivision',
			'created',
			'xo field rep',
			new.subdivision_id,
			xow_user_name,
			'CREATED',
			xowindows_xosuper_name_new
		);

	-- subdivision_id initial insert
		call sp_log_insert(
			'tbl_subdivision',
			'created',
			'subdivision_id',
			new.subdivision_id,
			xow_user_name,
			'CREATED',
			new.subdivision_id
		);

    

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `builders`.`tbl_subdivision_AFTER_UPDATE` AFTER UPDATE ON `tbl_subdivision` FOR EACH ROW
BEGIN
	DECLARE app_status_name_old varchar(11);
    DECLARE app_status_name_new varchar(11);
    DECLARE xowindows_zone_name_old varchar(45);
    DECLARE xowindows_zone_name_new varchar(45);
    DECLARE xowindows_branch_name_old varchar(45);
    DECLARE xowindows_branch_name_new varchar(45);
    DECLARE xowindows_starts_name_old varchar(45);
    DECLARE xowindows_starts_name_new varchar(45);
    DECLARE xowindows_xosuper_name_old varchar(45);
    DECLARE xowindows_xosuper_name_new varchar(45);
    DECLARE xow_user_name varchar(45);
	SET xow_user_name = (SELECT xousers.login from xowindows.tbl_users xousers where xousers.id = new.updated_by);
    
    -- STATUS Change
		if (old.tbl_status_tbl_status_id <> new.tbl_status_tbl_status_id) then
		SET app_status_name_old = (SELECT appStatus.name from application.tbl_status appStatus where appStatus.id = old.tbl_status_tbl_status_id);
		SET app_status_name_new = (SELECT appStatus.name from application.tbl_status appStatus where appStatus.id = new.tbl_status_tbl_status_id);

		
		call sp_log_insert(
			'tbl_subdivision',
			'update',
			'status_id',
			old.subdivision_id,
			xow_user_name,
			app_status_name_old,
			app_status_name_new
			);
		end if;
        
	-- NAME change
		if (old.subdivision_name <> new.subdivision_name) then
			call sp_log_insert(
            'tbl_subdivision',
            'update',
            'subdivision_name',
            old.subdivision_id,
            xow_user_name,
            old.subdivision_name,
            new.subdivision_name
            );
        end if;
        
	-- CITY change
		if (old.city <> new.city) then
			call sp_log_insert(
            'tbl_subdivision',
            'update',
            'city',
            old.subdivision_id,
            xow_user_name,
            old.city,
            new.city
            );
        end if;
        
	-- STATE change
		if (old.state <> new.state) then
			call sp_log_insert(
            'tbl_subdivision',
            'update',
            'state',
            old.subdivision_id,
            xow_user_name,
            old.state,
            new.state
            );
        end if;
        
	-- ZIP change
		if (old.zip <> new.zip) then
			call sp_log_insert(
            'tbl_subdivision',
            'update',
            'zip',
            old.subdivision_id,
            xow_user_name,
            old.zip,
            new.zip
            );
        end if;
        
	-- ZONE change
		if (old.tbl_zone_zone_id <> new.tbl_zone_zone_id) then
        SET xowindows_zone_name_old = (SELECT xowZone.name from xowindows.tbl_zone xowZone where xowZone.id = old.tbl_zone_zone_id);
        SET xowindows_zone_name_new = (SELECT xowZone.name from xowindows.tbl_zone xowZone where xowZone.id = new.tbl_zone_zone_id);
			call sp_log_insert(
            'tbl_subdivision',
            'update',
            'zone',
            old.subdivision_id,
            xow_user_name,
            xowindows_zone_name_old,
            xowindows_zone_name_new
            );
        end if;
        
	-- BRANCH change
		if (old.tbl_xo_branch_xo_branch_id <> new.tbl_xo_branch_xo_branch_id) then
        SET xowindows_branch_name_old = (SELECT xowBranch.location_name from xowindows.tbl_branch xowBranch where xowBranch.id = old.tbl_xo_branch_xo_branch_id);
        SET xowindows_branch_name_new = (SELECT xowBranch.location_name from xowindows.tbl_branch xowBranch where xowBranch.id = new.tbl_xo_branch_xo_branch_id);
			call sp_log_insert(
            'tbl_subdivision',
            'update',
            'branch',
            old.subdivision_id,
            xow_user_name,
            xowindows_branch_name_old,
            xowindows_branch_name_new
            );
        end if;
        
	-- STARTS rep change
		if (old.tbl_users_tbl_user_id_starts <> new.tbl_users_tbl_user_id_starts) then
        SET xowindows_starts_name_old = (SELECT xowUsers.login from xowindows.tbl_users xowUsers where xowUsers.id = old.tbl_users_tbl_user_id_starts);
        SET xowindows_starts_name_new = (SELECT xowUsers.login from xowindows.tbl_users xowUsers where xowUsers.id = new.tbl_users_tbl_user_id_starts);
			call sp_log_insert(
            'tbl_subdivision',
            'update',
            'starts rep',
            old.subdivision_id,
            xow_user_name,
            xowindows_starts_name_old,
            xowindows_starts_name_new
            );
        end if;
        
	-- XO Super change
		if (old.tbl_users_tbl_user_id_xosupers <> new.tbl_users_tbl_user_id_xosupers) then
        SET xowindows_xosuper_name_old = (SELECT xowUsers.login from xowindows.tbl_users xowUsers where xowUsers.id = old.tbl_users_tbl_user_id_xosupers);
        SET xowindows_xosuper_name_new = (SELECT xowUsers.login from xowindows.tbl_users xowUsers where xowUsers.id = new.tbl_users_tbl_user_id_xosupers);
			call sp_log_insert(
            'tbl_subdivision',
            'update',
            'xo field rep',
            old.subdivision_id,
            xow_user_name,
            xowindows_xosuper_name_old,
            xowindows_xosuper_name_new
            );
        end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tbl_supers`
--

DROP TABLE IF EXISTS `tbl_supers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_supers` (
  `super_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_subdivision_id` int(11) DEFAULT NULL,
  `super_name` varchar(45) DEFAULT NULL,
  `phone_number` varchar(10) DEFAULT NULL,
  `update_date` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`super_id`),
  KEY `tbl_supers_tbl_date_idx` (`update_date`),
  KEY `tbl_supers_tbl_users_idx` (`updated_by`),
  KEY `tbl_supers_tbl_subdivision_idx` (`super_id`),
  CONSTRAINT `tbl_supers_tbl_date` FOREIGN KEY (`update_date`) REFERENCES `application`.`tbl_date` (`date_key`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_supers_tbl_users` FOREIGN KEY (`updated_by`) REFERENCES `xowindows`.`tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_supers`
--

LOCK TABLES `tbl_supers` WRITE;
/*!40000 ALTER TABLE `tbl_supers` DISABLE KEYS */;
INSERT INTO `tbl_supers` VALUES (1,1,'JOHN','4803774050',20190825,1835,1),(3,5,'TRENT','6024992808',20190825,1458,1),(4,6,'KENNETH','4802339017',20190825,1510,1),(5,7,'JOHN','4802160308',20190825,1528,1),(6,8,'JESSE','6023095971',20190825,1550,1);
/*!40000 ALTER TABLE `tbl_supers` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `builders`.`tbl_supers_AFTER_UPDATE` AFTER UPDATE ON `tbl_supers` FOR EACH ROW
BEGIN
	DECLARE xow_user_name varchar(25);
    SET xow_user_name = (SELECT xousers.login from xowindows.tbl_users xousers WHERE xousers.id = new.updated_by);

	-- when fk_subdivision_id first gets added NULL <> new_id
		if not (new.fk_subdivision_id <=> old.fk_subdivision_id) then
		-- initial super_name addition
			call sp_log_insert(
				'tbl_subdivision',
                'created',
                'super_name',
                new.fk_subdivision_id,
                xow_user_name,
                'ADDED',
                new.super_name
            );
		-- initial phone_number addition
            call sp_log_insert(
				'tbl_subdivision',
                'created',
                'phone_number',
                new.fk_subdivision_id,
                xow_user_name,
                'ADDED',
                new.phone_number
            );
        end if;

	-- super_name change
		if (old.super_name <> new.super_name) then
			call sp_log_insert(
				'tbl_subdivision',
				'update',
				'super_name',
				old.fk_subdivision_id,
				xow_user_name,
				old.super_name,
				new.super_name
			);
		end if;
        
    -- phone_number change
		if (old.phone_number <> new.phone_number) then
			call sp_log_insert(
				'tbl_subdivision',
				'update',
				'phone_number',
				old.fk_subdivision_id,
				xow_user_name,
				old.phone_number,
				new.phone_number
			);
		end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Temporary table structure for view `vw_builder_list`
--

DROP TABLE IF EXISTS `vw_builder_list`;
/*!50001 DROP VIEW IF EXISTS `vw_builder_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_builder_list` AS SELECT 
 1 AS `builder_id`,
 1 AS `builder_name`,
 1 AS `update_date`,
 1 AS `update_time`,
 1 AS `login`,
 1 AS `tbl_status_tbl_status_id`,
 1 AS `name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_builders_install_types`
--

DROP TABLE IF EXISTS `vw_builders_install_types`;
/*!50001 DROP VIEW IF EXISTS `vw_builders_install_types`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_builders_install_types` AS SELECT 
 1 AS `builder_id`,
 1 AS `builder_name`,
 1 AS `install_type_id`,
 1 AS `install_type`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_builders_log_changes`
--

DROP TABLE IF EXISTS `vw_builders_log_changes`;
/*!50001 DROP VIEW IF EXISTS `vw_builders_log_changes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_builders_log_changes` AS SELECT 
 1 AS `tbl_in_builders`,
 1 AS `tbl_row_no`,
 1 AS `action`,
 1 AS `field`,
 1 AS `changed_date`,
 1 AS `changed_by`,
 1 AS `old_value`,
 1 AS `new_value`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_install_types_list`
--

DROP TABLE IF EXISTS `vw_install_types_list`;
/*!50001 DROP VIEW IF EXISTS `vw_install_types_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_install_types_list` AS SELECT 
 1 AS `install_types_id`,
 1 AS `type`,
 1 AS `update_date`,
 1 AS `update_time`,
 1 AS `updated_by`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_subdivision_list`
--

DROP TABLE IF EXISTS `vw_subdivision_list`;
/*!50001 DROP VIEW IF EXISTS `vw_subdivision_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_subdivision_list` AS SELECT 
 1 AS `subdivision_id`,
 1 AS `subdivision_name`,
 1 AS `update_date`,
 1 AS `update_time`,
 1 AS `updated_by`,
 1 AS `login`,
 1 AS `city`,
 1 AS `state`,
 1 AS `zip`,
 1 AS `tbl_xo_branch_xo_branch_id`,
 1 AS `location`,
 1 AS `loc_abbr`,
 1 AS `builder_name`,
 1 AS `tbl_zone_zone_id`,
 1 AS `zone_name`,
 1 AS `tbl_status_tbl_status_id`,
 1 AS `tbl_status_name`,
 1 AS `tbl_buildercustomer_builder_id`,
 1 AS `tbl_users_tbl_user_id_starts`,
 1 AS `starts_login`,
 1 AS `tbl_users_tbl_user_id_xosupers`,
 1 AS `field_login`,
 1 AS `super_id`,
 1 AS `super_name`,
 1 AS `phone_number`,
 1 AS `manufacturer_id`,
 1 AS `manufacturer_name`,
 1 AS `mfg_abbr`,
 1 AS `install_type_id`,
 1 AS `installation_type`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'builders'
--

--
-- Dumping routines for database 'builders'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_builder_insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_builder_insert`(
	builder_name varchar(45),
    updated_by int(11),
    status_id int(11)
    )
begin
	insert into `tbl_buildercustomer`(
        `builder_name`,
        `updated_by`,
        `tbl_status_tbl_status_id`)
    values
		(
        builder_name,
        updated_by,
        status_id);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_builder_insert_install_types` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_builder_insert_install_types`(
	install_type int(3),
    builder_id int(3),
    updated_by int(3)
    )
begin
	insert into `tbl_buildercustomer_has_install_types`(
        `tbl_install_types_install_types_id`,
        `tbl_buildercustomer_builder_id`,
        `updated_by`)
    values
		(
        install_type,
        builder_id,
        updated_by);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_builders_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_builders_list`()
BEGIN
	SELECT b.builder_id,
	b.builder_name,
	b.update_date,
	u.login,
	b.tbl_status_tbl_status_id,
	s.name
	from tbl_buildercustomer b
	join application.tbl_status s on b.tbl_status_tbl_status_id = s.id
    join xowindows.tbl_users u on b.updated_by = u.id
	order by b.builder_name ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_install_types` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_install_types`(IN builderId INT(3))
BEGIN
	SELECT instTypes.install_types_id,
    instTypes.type,
    bHiT.tbl_install_types_install_types_id,
    bHiT.tbl_buildercustomer_builder_id

	from tbl_install_types instTypes
    left outer join tbl_buildercustomer_has_install_types bHiT on instTypes.install_types_id = bHiT.tbl_install_types_install_types_id AND
		bHiT.tbl_buildercustomer_builder_id = builderId;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_one_builder` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_one_builder`(IN builder_in INT)
begin
	select
		*
	from builders.tbl_buildercustomer
    where builder_id = builder_in;    
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_one_install_type` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_one_install_type`(IN it_id_in INT)
begin
	select
		*
	from builders.tbl_install_types its
    where its.install_types_id = it_id_in;   
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_one_subdivision` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_one_subdivision`(IN subdivisionId INT(3))
BEGIN
	SELECT
		*
        
	from tbl_subdivision
    
    where subdivision_id = subdivisionId;  

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_subdivision_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_subdivision_list`()
BEGIN
	SELECT s.subdivision_id,
    s.subdivision_name,
    s.update_date,
	s.updated_by,
	s.create_date,
	s.city,
	s.state,
	s.zip,
	branch.location_name as location,
	builder.builder_name, 
	zone.name as zone_name, 
	s.tbl_status_tbl_status_id, 
	state.name as tbl_status_name
    
    from tbl_subdivision s
    
    join tbl_buildercustomer builder 
		on s.tbl_buildercustomer_builder_id = builder.builder_id
        
	join xowindows.tbl_branch branch
		on s.tbl_xo_branch_xo_branch_id = branch.id
        
	join xowindows.tbl_zone zone
		on s.tbl_zone_zone_id = zone.id
        
	join application.tbl_status state 
		on s.tbl_status_tbl_status_id = state.id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_log_insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_log_insert`(
	tbl_in_builders varchar(45),
    table_action varchar(25),
    field varchar(45),
    tbl_row_no int(11),
    changed_by varchar(25),
    old_value varchar(50),
    new_value varchar(50)
    )
begin
	insert into `tbl_log_builders`(
        `tbl_in_builders`,
        `action`,
        `field`,
        `tbl_row_no`,
        `changed_by`,
        `old_value`,
        `new_value`,
		`changed_date`) 
    values
		(
        tbl_in_builders,
        table_action,
        field,
        tbl_row_no,
        changed_by,
        old_value,
        new_value,
        now());
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `jobs`
--

USE `jobs`;

--
-- Current Database: `bids`
--

USE `bids`;

--
-- Final view structure for view `vw_get_bid_details`
--

/*!50001 DROP VIEW IF EXISTS `vw_get_bid_details`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_get_bid_details` AS select `bids`.`tbl_bid_id` AS `tbl_bid_id`,`bids`.`tbl_subdivision_subdivision_id` AS `tbl_subdivision_subdivision_id`,`bids`.`plan` AS `Plan`,`bids`.`elevation` AS `Elevation`,`bids`.`tbl_install_types_install_types_id` AS `tbl_install_types_install_types_id`,`it`.`type` AS `Type`,`bids`.`tbl_manufacturer_manufacturer_id` AS `tbl_manufacturer_manufacturer_id`,`mfg`.`manufacturer_name` AS `manufacturer_name`,`bids`.`tbl_window_color_window_color_id` AS `tbl_window_color_window_color_id`,`wc`.`window_color_name` AS `window_color_name`,`bids`.`tbl_window_material_window_material_id` AS `tbl_window_material_window_material_id`,`wm`.`window_material_name` AS `window_material_name`,`bids`.`tbl_window_fin_window_fin_id` AS `tbl_window_fin_window_fin_id`,`wf`.`window_fin_name` AS `window_fin_name`,`bids`.`tbl_window_coating_window_coating_id` AS `tbl_window_coating_window_coating_id`,`wct`.`window_coating_name` AS `window_coating_name`,`bids`.`tbl_status_tbl_status_id` AS `tbl_status_tbl_status_id`,`b`.`builder_id` AS `builder_id`,`b`.`builder_name` AS `Builder`,`s`.`subdivision_name` AS `Subdivision`,`u`.`po_abbreviation` AS `Branch`,`s`.`city` AS `City`,`s`.`state` AS `State`,`bids`.`updated_by` AS `Prepped` from (((((((((`bids`.`tbl_bid` `bids` join `builders`.`tbl_subdivision` `s` on((`s`.`subdivision_id` = `bids`.`tbl_subdivision_subdivision_id`))) join `builders`.`tbl_buildercustomer` `b` on((`b`.`builder_id` = `s`.`tbl_buildercustomer_builder_id`))) join `xowindows`.`tbl_branch` `u` on((`u`.`id` = `s`.`tbl_xo_branch_xo_branch_id`))) left join `builders`.`tbl_install_types` `it` on((`it`.`install_types_id` = `bids`.`tbl_install_types_install_types_id`))) left join `vendors`.`tbl_manufacturer` `mfg` on((`mfg`.`manufacturer_id` = `bids`.`tbl_manufacturer_manufacturer_id`))) left join `vendors`.`tbl_window_color` `wc` on((`wc`.`window_color_id` = `bids`.`tbl_window_color_window_color_id`))) left join `vendors`.`tbl_window_coating` `wct` on((`wct`.`window_coating_id` = `bids`.`tbl_window_coating_window_coating_id`))) left join `vendors`.`tbl_window_material` `wm` on((`wm`.`window_material_id` = `bids`.`tbl_window_material_window_material_id`))) left join `vendors`.`tbl_window_fin` `wf` on((`wf`.`window_fin_id` = `bids`.`tbl_window_fin_window_fin_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_get_bldrsub_elevation`
--

/*!50001 DROP VIEW IF EXISTS `vw_get_bldrsub_elevation`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_get_bldrsub_elevation` AS select `bidtbl`.`plan` AS `plan`,`bidtbl`.`elevation` AS `elevation`,`bidtbl`.`tbl_subdivision_subdivision_id` AS `tbl_subdivision_subdivision_id` from `tbl_bid` `bidtbl` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_get_bldrsub_plan`
--

/*!50001 DROP VIEW IF EXISTS `vw_get_bldrsub_plan`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_get_bldrsub_plan` AS select `bidtbl`.`plan` AS `plan`,`bidtbl`.`tbl_subdivision_subdivision_id` AS `tbl_subdivision_subdivision_id` from `tbl_bid` `bidtbl` group by `bidtbl`.`plan` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_get_recent_bids`
--

/*!50001 DROP VIEW IF EXISTS `vw_get_recent_bids`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_get_recent_bids` AS select date_format(`bidtbl`.`update_date`,'%m/%d/%Y') AS `Date`,`bidtbl`.`tbl_bid_id` AS `tbl_bid_id`,`b`.`builder_name` AS `Builder`,`s`.`tbl_buildercustomer_builder_id` AS `BuilderID`,`s`.`subdivision_name` AS `Subdivision`,`bidtbl`.`plan` AS `Plan`,`bidtbl`.`elevation` AS `Elevation`,`u`.`po_abbreviation` AS `Branch`,`s`.`city` AS `City`,`s`.`state` AS `State`,`bidtbl`.`updated_by` AS `Prepped`,`bidtbl`.`tbl_status_tbl_status_id` AS `tbl_status_tbl_status_id`,`s`.`subdivision_id` AS `subdivision_id` from (((`bids`.`tbl_bid` `bidtbl` join `builders`.`tbl_subdivision` `s` on((`s`.`subdivision_id` = `bidtbl`.`tbl_subdivision_subdivision_id`))) join `builders`.`tbl_buildercustomer` `b` on((`b`.`builder_id` = `s`.`tbl_buildercustomer_builder_id`))) join `xowindows`.`tbl_branch` `u` on((`u`.`id` = `s`.`tbl_xo_branch_xo_branch_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Current Database: `application`
--

USE `application`;

--
-- Final view structure for view `vw_status_list`
--

/*!50001 DROP VIEW IF EXISTS `vw_status_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_status_list` AS select `s`.`id` AS `id`,`s`.`name` AS `name` from `tbl_status` `s` order by `s`.`name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Current Database: `vendors`
--

USE `vendors`;

--
-- Final view structure for view `vw_manufacturer_list`
--

/*!50001 DROP VIEW IF EXISTS `vw_manufacturer_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_manufacturer_list` AS select `tbl_manufacturer`.`manufacturer_id` AS `manufacturer_id`,`tbl_manufacturer`.`mfg_abbr` AS `mfg_abbr`,`tbl_manufacturer`.`manufacturer_name` AS `manufacturer_name`,`tbl_manufacturer`.`update_date` AS `update_date`,`tbl_manufacturer`.`update_time` AS `update_time`,`tbl_manufacturer`.`updated_by` AS `updated_by`,`tbl_manufacturer`.`tbl_status_tbl_status_id` AS `tbl_status_tbl_status_id` from `tbl_manufacturer` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_manufacturer_series`
--

/*!50001 DROP VIEW IF EXISTS `vw_manufacturer_series`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_manufacturer_series` AS select `tbl_manufacturer`.`manufacturer_id` AS `manufacturer_id`,`tbl_manufacturer`.`manufacturer_name` AS `manufacturer_name`,`tbl_mfg_series`.`idtbl_mfg_series` AS `idtbl_mfg_series`,`tbl_mfg_series`.`series` AS `series`,`tbl_mfg_series`.`special_instruction` AS `special_instruction`,`tbl_window_type`.`idtbl_window_type` AS `idtbl_window_type`,`tbl_window_type`.`window_type_abbr` AS `window_type_abbr` from (((`tbl_mfg_has_series` join `tbl_manufacturer` on((`tbl_mfg_has_series`.`fk_mfg_id` = `tbl_manufacturer`.`manufacturer_id`))) join `tbl_mfg_series` on((`tbl_mfg_has_series`.`fk_idtbl_mfg_series` = `tbl_mfg_series`.`idtbl_mfg_series`))) join `tbl_window_type` on((`tbl_mfg_series`.`fk_tbl_window_type_idtbl_window_type` = `tbl_window_type`.`idtbl_window_type`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_mfg_coatings`
--

/*!50001 DROP VIEW IF EXISTS `vw_mfg_coatings`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_mfg_coatings` AS select `tbl_manufacturer`.`manufacturer_id` AS `manufacturer_id`,`tbl_manufacturer`.`manufacturer_name` AS `manufacturer_name`,`tbl_window_coating`.`window_coating_id` AS `window_coating_id`,`tbl_window_coating`.`window_coating_name` AS `window_coating_name` from ((`tbl_mfg_has_tbl_window_coating` join `tbl_manufacturer` on((`tbl_mfg_has_tbl_window_coating`.`tbl_manufacturer_manufacturer_id` = `tbl_manufacturer`.`manufacturer_id`))) join `tbl_window_coating` on((`tbl_mfg_has_tbl_window_coating`.`tbl_window_coating_window_coating_id` = `tbl_window_coating`.`window_coating_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_mfg_colors`
--

/*!50001 DROP VIEW IF EXISTS `vw_mfg_colors`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_mfg_colors` AS select `tbl_manufacturer`.`manufacturer_id` AS `manufacturer_id`,`tbl_manufacturer`.`manufacturer_name` AS `manufacturer_name`,`tbl_window_color`.`window_color_id` AS `window_color_id`,`tbl_window_color`.`window_color_name` AS `window_color_name` from ((`tbl_mfg_has_tbl_window_color` join `tbl_manufacturer` on((`tbl_mfg_has_tbl_window_color`.`tbl_manufacturer_manufacturer_id` = `tbl_manufacturer`.`manufacturer_id`))) join `tbl_window_color` on((`tbl_mfg_has_tbl_window_color`.`tbl_window_color_window_color_id` = `tbl_window_color`.`window_color_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_mfg_fins`
--

/*!50001 DROP VIEW IF EXISTS `vw_mfg_fins`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_mfg_fins` AS select `tbl_manufacturer`.`manufacturer_id` AS `manufacturer_id`,`tbl_manufacturer`.`manufacturer_name` AS `manufacturer_name`,`tbl_window_fin`.`window_fin_id` AS `window_fin_id`,`tbl_window_fin`.`window_fin_name` AS `window_fin_name` from ((`tbl_mfg_has_tbl_window_fin` join `tbl_manufacturer` on((`tbl_mfg_has_tbl_window_fin`.`tbl_manufacturer_manufacturer_id` = `tbl_manufacturer`.`manufacturer_id`))) join `tbl_window_fin` on((`tbl_mfg_has_tbl_window_fin`.`tbl_window_fin_window_fin_id` = `tbl_window_fin`.`window_fin_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_mfg_materials`
--

/*!50001 DROP VIEW IF EXISTS `vw_mfg_materials`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_mfg_materials` AS select `tbl_manufacturer`.`manufacturer_id` AS `manufacturer_id`,`tbl_manufacturer`.`manufacturer_name` AS `manufacturer_name`,`tbl_window_material`.`window_material_id` AS `window_material_id`,`tbl_window_material`.`window_material_name` AS `window_material_name` from ((`tbl_mfg_has_tbl_window_material` join `tbl_manufacturer` on((`tbl_mfg_has_tbl_window_material`.`tbl_manufacturer_manufacturer_id` = `tbl_manufacturer`.`manufacturer_id`))) join `tbl_window_material` on((`tbl_mfg_has_tbl_window_material`.`tbl_window_material_window_material_id` = `tbl_window_material`.`window_material_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_mfg_series_dims`
--

/*!50001 DROP VIEW IF EXISTS `vw_mfg_series_dims`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_mfg_series_dims` AS select `tbl_manufacturer`.`manufacturer_id` AS `manufacturer_id`,`tbl_manufacturer`.`manufacturer_name` AS `manufacturer_name`,`tbl_mfg_series`.`idtbl_mfg_series` AS `idtbl_mfg_series`,`tbl_mfg_series`.`series` AS `series`,`tbl_mfg_series`.`special_instruction` AS `special_instruction`,`tbl_window_type`.`idtbl_window_type` AS `idtbl_window_type`,`tbl_window_type`.`window_type_abbr` AS `window_type_abbr`,`tbl_window_dimensions`.`idtbl_window_dimensions` AS `idtbl_window_dimensions` from (((((`tbl_mfg_has_window_dimensions` join `tbl_window_dimensions` on((`tbl_mfg_has_window_dimensions`.`fk_tbl_window_dimensions_idtbl_window_dimensions` = `tbl_window_dimensions`.`idtbl_window_dimensions`))) join `tbl_mfg_series` on((`tbl_mfg_has_window_dimensions`.`fk_tbl_mfg_series_idtbl_mfg_series` = `tbl_mfg_series`.`idtbl_mfg_series`))) join `tbl_mfg_has_series` on((`tbl_mfg_has_series`.`fk_idtbl_mfg_series` = `tbl_mfg_series`.`idtbl_mfg_series`))) join `tbl_manufacturer` on((`tbl_mfg_has_series`.`fk_mfg_id` = `tbl_manufacturer`.`manufacturer_id`))) join `tbl_window_type` on((`tbl_mfg_series`.`fk_tbl_window_type_idtbl_window_type` = `tbl_window_type`.`idtbl_window_type`))) order by `tbl_manufacturer`.`manufacturer_name`,`tbl_mfg_series`.`series`,`tbl_window_dimensions`.`idtbl_window_dimensions` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_mfg_series_dims_pricing`
--

/*!50001 DROP VIEW IF EXISTS `vw_mfg_series_dims_pricing`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_mfg_series_dims_pricing` AS select `tbl_manufacturer`.`manufacturer_id` AS `manufacturer_id`,`tbl_manufacturer`.`manufacturer_name` AS `manufacturer_name`,`tbl_mfg_series`.`idtbl_mfg_series` AS `idtbl_mfg_series`,`tbl_mfg_series`.`series` AS `series`,`tbl_mfg_series`.`special_instruction` AS `special_instruction`,`tbl_window_type`.`idtbl_window_type` AS `idtbl_window_type`,`tbl_window_type`.`window_type_abbr` AS `window_type_abbr`,`tbl_window_dimensions`.`idtbl_window_dimensions` AS `idtbl_window_dimensions`,`tbl_mfg_purchasing`.`base_amt` AS `base_amt`,`tbl_mfg_purchasing`.`e340_amt` AS `e340_amt`,`tbl_mfg_purchasing`.`e366_amt` AS `e366_amt`,`tbl_mfg_purchasing`.`tempered_amt` AS `tempered_amt`,`tbl_mfg_purchasing`.`grid_amt` AS `grid_amt` from ((((((`tbl_mfg_has_window_dimensions` join `tbl_window_dimensions` on((`tbl_mfg_has_window_dimensions`.`fk_tbl_window_dimensions_idtbl_window_dimensions` = `tbl_window_dimensions`.`idtbl_window_dimensions`))) join `tbl_mfg_series` on((`tbl_mfg_has_window_dimensions`.`fk_tbl_mfg_series_idtbl_mfg_series` = `tbl_mfg_series`.`idtbl_mfg_series`))) join `tbl_mfg_has_series` on((`tbl_mfg_has_series`.`fk_idtbl_mfg_series` = `tbl_mfg_series`.`idtbl_mfg_series`))) join `tbl_manufacturer` on((`tbl_mfg_has_series`.`fk_mfg_id` = `tbl_manufacturer`.`manufacturer_id`))) join `tbl_window_type` on((`tbl_mfg_series`.`fk_tbl_window_type_idtbl_window_type` = `tbl_window_type`.`idtbl_window_type`))) join `tbl_mfg_purchasing` on((`tbl_mfg_has_window_dimensions`.`fk_tbl_mfg_purchasing_idtbl_mfg_purchasing` = `tbl_mfg_purchasing`.`idtbl_mfg_purchasing`))) order by `tbl_manufacturer`.`manufacturer_name`,`tbl_mfg_series`.`series`,`tbl_window_dimensions`.`idtbl_window_dimensions` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Current Database: `xowindows`
--

USE `xowindows`;

--
-- Final view structure for view `vw_branch_list`
--

/*!50001 DROP VIEW IF EXISTS `vw_branch_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_branch_list` AS select `tbl_branch`.`id` AS `xo_branch_id`,`tbl_branch`.`location_name` AS `location` from `tbl_branch` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_users_list`
--

/*!50001 DROP VIEW IF EXISTS `vw_users_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_users_list` AS select `tbl_users`.`id` AS `id`,`tbl_users`.`login` AS `login`,`tbl_users`.`password` AS `password`,`tbl_users`.`email` AS `email`,`tbl_users`.`level` AS `level` from `tbl_users` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_zone_list`
--

/*!50001 DROP VIEW IF EXISTS `vw_zone_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_zone_list` AS select `tbl_zone`.`zone_id` AS `zone_id`,`tbl_zone`.`zone_name` AS `zone_name`,`tbl_zone`.`zone_abbreviation` AS `zone_abbreviation` from `tbl_zone` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Current Database: `builders`
--

USE `builders`;

--
-- Final view structure for view `builders_history_changes`
--

/*!50001 DROP VIEW IF EXISTS `builders_history_changes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `builders_history_changes` AS select `t2`.`dt_datetime` AS `dt_datetime`,`t2`.`action` AS `action`,`t1`.`builder_id` AS `builder_id`,if((`t1`.`tbl_status_tbl_status_id` = `t2`.`tbl_status_tbl_status_id`),`t1`.`tbl_status_tbl_status_id`,concat(`t1`.`tbl_status_tbl_status_id`,' to ',`t2`.`tbl_status_tbl_status_id`)) AS `cust_status` from (`tbl_history_buildercustomer` `t1` join `tbl_history_buildercustomer` `t2` on((`t1`.`builder_id` = `t2`.`builder_id`))) where (((`t1`.`revision` = 1) and (`t2`.`revision` = 1)) or (`t2`.`revision` = (`t1`.`revision` + 1))) order by `t1`.`builder_id`,`t2`.`revision` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_builder_list`
--

/*!50001 DROP VIEW IF EXISTS `vw_builder_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_builder_list` AS select `b`.`builder_id` AS `builder_id`,`b`.`builder_name` AS `builder_name`,`b`.`update_date` AS `update_date`,`b`.`update_time` AS `update_time`,`u`.`login` AS `login`,`b`.`tbl_status_tbl_status_id` AS `tbl_status_tbl_status_id`,`s`.`name` AS `name` from ((`builders`.`tbl_buildercustomer` `b` join `application`.`tbl_status` `s` on((`b`.`tbl_status_tbl_status_id` = `s`.`id`))) join `xowindows`.`tbl_users` `u` on((`b`.`updated_by` = `u`.`id`))) order by `b`.`builder_name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_builders_install_types`
--

/*!50001 DROP VIEW IF EXISTS `vw_builders_install_types`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_builders_install_types` AS select `builder`.`builder_id` AS `builder_id`,`builder`.`builder_name` AS `builder_name`,`inst_type`.`install_types_id` AS `install_type_id`,`inst_type`.`type` AS `install_type` from ((`tbl_buildercustomer_has_install_types` `bit` join `tbl_buildercustomer` `builder` on((`bit`.`tbl_buildercustomer_builder_id` = `builder`.`builder_id`))) join `tbl_install_types` `inst_type` on((`bit`.`tbl_install_types_install_types_id` = `inst_type`.`install_types_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_builders_log_changes`
--

/*!50001 DROP VIEW IF EXISTS `vw_builders_log_changes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_builders_log_changes` AS select `tlb`.`tbl_in_builders` AS `tbl_in_builders`,`tlb`.`tbl_row_no` AS `tbl_row_no`,`tlb`.`action` AS `action`,`tlb`.`field` AS `field`,`tlb`.`changed_date` AS `changed_date`,`tlb`.`changed_by` AS `changed_by`,`tlb`.`old_value` AS `old_value`,`tlb`.`new_value` AS `new_value` from `tbl_log_builders` `tlb` order by `tlb`.`changed_date` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_install_types_list`
--

/*!50001 DROP VIEW IF EXISTS `vw_install_types_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_install_types_list` AS select `tbl_install_types`.`install_types_id` AS `install_types_id`,`tbl_install_types`.`type` AS `type`,`tbl_install_types`.`update_date` AS `update_date`,`tbl_install_types`.`update_time` AS `update_time`,`tbl_install_types`.`updated_by` AS `updated_by` from `tbl_install_types` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_subdivision_list`
--

/*!50001 DROP VIEW IF EXISTS `vw_subdivision_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_subdivision_list` AS select `s`.`subdivision_id` AS `subdivision_id`,`s`.`subdivision_name` AS `subdivision_name`,`s`.`update_date` AS `update_date`,`s`.`update_time` AS `update_time`,`s`.`updated_by` AS `updated_by`,`updated_by_user`.`login` AS `login`,`s`.`city` AS `city`,`s`.`state` AS `state`,`s`.`zip` AS `zip`,`s`.`tbl_xo_branch_xo_branch_id` AS `tbl_xo_branch_xo_branch_id`,`branch`.`location_name` AS `location`,`branch`.`po_abbreviation` AS `loc_abbr`,`builder`.`builder_name` AS `builder_name`,`s`.`tbl_zone_zone_id` AS `tbl_zone_zone_id`,`zone`.`zone_name` AS `zone_name`,`s`.`tbl_status_tbl_status_id` AS `tbl_status_tbl_status_id`,`state`.`name` AS `tbl_status_name`,`s`.`tbl_buildercustomer_builder_id` AS `tbl_buildercustomer_builder_id`,`s`.`tbl_users_tbl_user_id_starts` AS `tbl_users_tbl_user_id_starts`,`starts_user`.`login` AS `starts_login`,`s`.`tbl_users_tbl_user_id_xosupers` AS `tbl_users_tbl_user_id_xosupers`,`xo_field_rep`.`login` AS `field_login`,`supers`.`super_id` AS `super_id`,`supers`.`super_name` AS `super_name`,`supers`.`phone_number` AS `phone_number`,`vendor`.`manufacturer_id` AS `manufacturer_id`,`vendor`.`manufacturer_name` AS `manufacturer_name`,`vendor`.`mfg_abbr` AS `mfg_abbr`,`inst_type`.`install_types_id` AS `install_type_id`,`inst_type`.`type` AS `installation_type` from ((((((((((`builders`.`tbl_subdivision` `s` join `builders`.`tbl_buildercustomer` `builder` on((`s`.`tbl_buildercustomer_builder_id` = `builder`.`builder_id`))) join `xowindows`.`tbl_branch` `branch` on((`s`.`tbl_xo_branch_xo_branch_id` = `branch`.`id`))) join `xowindows`.`tbl_zone` `zone` on((`s`.`tbl_zone_zone_id` = `zone`.`zone_id`))) join `vendors`.`tbl_manufacturer` `vendor` on((`s`.`vendor_tbl_manufacturer_id` = `vendor`.`manufacturer_id`))) left join `builders`.`tbl_install_types` `inst_type` on((`s`.`tbl_install_types` = `inst_type`.`install_types_id`))) left join `builders`.`tbl_supers` `supers` on((`s`.`tbl_supers_super_id` = `supers`.`super_id`))) left join `xowindows`.`tbl_users` `updated_by_user` on((`s`.`updated_by` = `updated_by_user`.`id`))) left join `xowindows`.`tbl_users` `starts_user` on((`s`.`tbl_users_tbl_user_id_starts` = `starts_user`.`id`))) left join `xowindows`.`tbl_users` `xo_field_rep` on((`s`.`tbl_users_tbl_user_id_xosupers` = `xo_field_rep`.`id`))) join `application`.`tbl_status` `state` on((`s`.`tbl_status_tbl_status_id` = `state`.`id`))) order by `s`.`subdivision_name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-14 14:08:23
